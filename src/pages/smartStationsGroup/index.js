import React, {useState, useRef, useEffect} from "react";
import {connect} from "react-redux";

import DataGrid, {Column, StateStoring, Button} from "devextreme-react/data-grid";
import notify from "devextreme/ui/notify";
import {Popup, Position, ToolbarItem} from "devextreme-react/popup";
import DataSource from "devextreme/data/data_source";

import {isEmpty} from "lodash-es";

import {deauthorize} from "../../store/commonSlice";

import {
    getGroupDisplayRelationDataStore,
    getDisplayGroupsDataStore,
    WINDOWS,
    getSystemWindowsDataStore, getSmartStationsGroupDataStore,
} from "../../dataStores";

import DisplayGroupsEditorPopup from "./smartStationsGroupsEditorPopup";

import config from "../../config";

const windowId = WINDOWS.SMART_STATIONS_GROUP.id;

const displaySmartStationsGroupsDataStore = getSmartStationsGroupDataStore({
    windowId: windowId,
});

const displaySmartStationsGroupsDataSource = new DataSource({
    store: displaySmartStationsGroupsDataStore,
    reshapeOnPush: true,
});

const systemWindowsDataStore = getSystemWindowsDataStore({
    windowId: windowId,
});

const DisplaySmartStationsGroupsPage = ({localDeauthorize}) => {
    const [editorPopupVisible, setEditorPopupVisible] = useState(false);
    const [deleteRowPopupVisible, setDeleteRowPopupVisible] = useState(false);
    const [editedRow, setEditedRow] = useState({});
    const [deletedRowKey, setDeletedRowKey] = useState(null);
    const [windowTitle, setWindowTitle] = useState();

    const datagridRef = useRef(null);

    const displayGroupsEditorOptions = config.pages.SmartStationsGroups;
    const displayGroupsGridOptions = displayGroupsEditorOptions.groupsGrid.dxOptions;

    function onDataErrorOccurred(e) {
        notify(e.error?.message, "error", config.common.errorMessageLifespan);
        if (e.error?.data?.reason === "noAuth") {
            localDeauthorize();
        }
    }

    function onToolbarPreparing(e) {
        let toolbarItems = e.toolbarOptions.items;

        toolbarItems.unshift({
            widget: "dxButton",
            options: {
                icon: "add",
                onClick: openCreateEntryPopup,
            },
            location: "before",
        });
    }

    const openEditorPopup = (e) => {
        const currentRow = displaySmartStationsGroupsDataSource.items().find((item) => item.id === e.row.key);

        if (!isEmpty(currentRow)) {
            setEditedRow(currentRow);
            setEditorPopupVisible(true);
        }
    };

    const openCreateEntryPopup = () => {
        setEditedRow({});
        setEditorPopupVisible(true);
    };

    const openDeleteRowPopup = (e) => {
        setEditedRow({});
        setDeletedRowKey(e.row.key);
        setDeleteRowPopupVisible(true);
    };

    const closeEditorPopup = () => {
        setEditorPopupVisible(false);
    };

    const closeDeleteRowPopup = () => {
        setDeleteRowPopupVisible(false);
    };

    const createEntry = (e) => {
        const groupSmartStationsRelationDataStoreActions = e.groupSmartStationsRelationDataStoreActions;

        displaySmartStationsGroupsDataSource
            .store()
            .insert(e.formData)
            .then((e) => {
                closeEditorPopup();
                const groupId = e.data;
                // setGroupDisplayRelations(groupSmartStationsRelationDataStoreActions, groupId);
                datagridRef?.current?.instance?.refresh();
            })
            .catch((error) => {
                onDataErrorOccurred({error});
                closeEditorPopup();
                datagridRef?.current?.instance?.refresh();
            });
    };

    const updateEntry = (e) => {
        const groupSmartStationsRelationDataStoreActions = e.groupSmartStationsRelationDataStoreActions;

        let smartStationIds = groupSmartStationsRelationDataStoreActions.map(el => el.smartStationId);



        displaySmartStationsGroupsDataSource
            .store()
            .update(e.editedRowId, {
                ...e.formData,
                smartStationIds
            })
            .then((e) => {
                closeEditorPopup();
                datagridRef?.current?.instance?.refresh();
            })
            .catch((error) => {
                onDataErrorOccurred({error});
                closeEditorPopup();
                datagridRef?.current?.instance?.refresh();
            });
    };

    const deleteEntry = (e) => {
        displaySmartStationsGroupsDataSource
            .store()
            .remove(deletedRowKey)
            .then(() => {
                closeDeleteRowPopup();
                datagridRef?.current?.instance?.refresh();
            })
            .catch((error) => {
                onDataErrorOccurred({error});
                closeDeleteRowPopup();
                datagridRef?.current?.instance?.refresh();
            });
    };


    useEffect(() => {
        systemWindowsDataStore.byKey(windowId).then((result) => {
            const windowName = result[0]?.name?.split("/").pop();

            setWindowTitle(windowName);
        });
    }, []);

    return (
        <React.Fragment>
            <h2 className="content-block">{windowTitle}</h2>
            {editorPopupVisible && (
                <DisplayGroupsEditorPopup
                    windowId={windowId}
                    editedRow={editedRow}
                    closeEditorPopup={closeEditorPopup}
                    updateEntry={updateEntry}
                    createEntry={createEntry}
                />
            )}
            {deleteRowPopupVisible && (
                <Popup
                    className="deleteRowPopup"
                    width="auto"
                    height="auto"
                    visible={true}
                    showCloseButton={false}
                    showTitle={false}
                    onHiding={closeDeleteRowPopup}
                >
                    <Position my="center" at="center" of={window}/>
                    <div>
                        <span>Вы уверены, что хотите удалить эту запись?</span>
                    </div>
                    <ToolbarItem
                        widget="dxButton"
                        location="center"
                        options={{text: "Да", onClick: deleteEntry}}
                        toolbar="bottom"
                    />
                    <ToolbarItem
                        widget="dxButton"
                        location="center"
                        options={{text: "Нет", onClick: closeDeleteRowPopup}}
                        toolbar="bottom"
                    />
                </Popup>
            )}
            <div className="content-block">
                <div className="dx-card responsive-paddings">
                    <DataGrid
                        ref={datagridRef}
                        dataSource={displaySmartStationsGroupsDataSource}
                        onDataErrorOccurred={onDataErrorOccurred}
                        onToolbarPreparing={onToolbarPreparing}
                        errorRowEnabled={false}
                        onContentReady={(e) => {
                            //Для каждой колонки форсированно задаём порядок отрисовки в таблице.
                            e.component.state().columns.forEach((col, index) => {
                                e.component.columnOption(col.name, "visibleIndex", index);
                            });
                        }}
                        {...displayGroupsGridOptions}
                    >
                        <StateStoring
                            enabled={true}
                            type="localStorage"
                            storageKey="pages.smartStationsGroups.grid"
                        />
                        <Column dataField="name" caption={"Название"}/>
                        <Column type="buttons">
                            <Button name="custom-edit" text="Изменить" icon="edit" onClick={openEditorPopup}/>
                            <Button
                                name="custom-delete"
                                text="Удалить"
                                icon="trash"
                                onClick={openDeleteRowPopup}
                            />
                        </Column>
                    </DataGrid>
                </div>
            </div>
        </React.Fragment>
    );
};

export default connect(
    (state) => ({}),
    (dispatch) => {
        return {
            localDeauthorize: () => dispatch(deauthorize()),
        };
    }
)(DisplaySmartStationsGroupsPage);
