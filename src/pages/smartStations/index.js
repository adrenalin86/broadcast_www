import React, {useEffect, useState} from 'react';
import {getSmartStationsDataStore, getSystemWindowsDataStore, WINDOWS} from "../../dataStores";
import notify from "devextreme/ui/notify";
import config from "../../config";
import {isEmpty} from "lodash-es";
import EditorPopupSmartStations from "./editorPopup";
import {Popup, Position, ToolbarItem} from "devextreme-react/popup";
import DataSource from "devextreme/data/data_source";
import {connect} from "react-redux";
import {deauthorize} from "../../store/commonSlice";
import {Tabs} from "devextreme-react";
import SmartStationsTable from "./smartStationsTable";
import SmartStationsMap from "./smartStationsMap";
import DisplaysCards from "../displays/displaysCards";
import VideoStreamPopup from "../../components/videoCameras/videoStreamPopup/videoStreamPopup";
import ViewPopupSmartStations from "./viewPopup";

const tabs = [
    {
        id: 0,
        text: "Таблица",
    },
    {
        id: 1,
        text: "Видеостена",
    },
    {
        id: 2,
        text: "Карта",
    },
];

const windowId = WINDOWS.SMART_STATIONS.id;

const roadSmartStations = getSmartStationsDataStore({windowId: windowId});

const roadSmartStationsDataStore = new DataSource({
    store: roadSmartStations,
    reshapeOnPush: true,
});


function SmartStationsPage({localDeauthorize}) {
    const [windowTitle, setWindowTitle] = useState('');
    const [editedRow, setEditedRow] = useState({});
    const [editorPopupVisible, setEditorPopupVisible] = useState(false);
    const [viewPopupVisible, setViewPopupVisible] = useState(false);
    const [deletedRowKey, setDeletedRowKey] = useState(null);
    const [deleteRowPopupVisible, setDeleteRowPopupVisible] = useState(false);


    const [videoCameraId, setVideoCameraId] = useState();
    const [videoStreamPopupVisible, setVideoStreamPopupVisible] = useState(false);

    const [isVisible, setIsVisible] = useState({ table: true, cards: false, map: false });
    const [isCardsLoaded, setIsCardsLoaded] = useState(false);
    const [isMapLoaded, setIsMapLoaded] = useState(false);
    const [counter, setCounter] = useState(0);


    const systemWindowsDataStore = getSystemWindowsDataStore({
        windowId: windowId,
    });



    useEffect(() => {
        systemWindowsDataStore.byKey(windowId).then((result) => {
            const windowName = result[0]?.name?.split("/").pop();

            setWindowTitle(windowName);
        });
    }, []);



    const clickOnTable = () => {
        setIsVisible({
            table: true,
            cards: false,
            map: false,
        });
    };

    const clickOnCards = () => {
        setIsVisible({
            table: false,
            cards: true,
            map: false,
        });

        setIsCardsLoaded(true);
    };

    const clickOnMap = () => {
        setIsVisible({
            table: false,
            cards: false,
            map: true,
        });

        setIsMapLoaded(true);
    };

    const closeVideoStreamPopup = () => {
        setVideoStreamPopupVisible(false);
    };

    const closeDeleteRowPopup = () => {
        setDeleteRowPopupVisible(false);
    };

    const closeEditorPopup = () => {
        setEditorPopupVisible(false);
    };

    const closeViewPopup = () => {
        setViewPopupVisible(false);
    };

    const openDeleteRowPopup = (e) => {
        setEditedRow({});
        setDeletedRowKey(e.row.key);
        setDeleteRowPopupVisible(true);
    };

    const openDeleteRowPopupFromMap = (key) => {
        setEditedRow({});
        setDeletedRowKey(key);
        setDeleteRowPopupVisible(true);
    };

    function onDataErrorOccurred(e) {
        notify(e.error?.message, "error", config.common.errorMessageLifespan);
        if (e.error?.data?.reason === "noAuth") {
            localDeauthorize();
        }
    }

    const openCreateEntryPopup = () => {
        setEditedRow({});
        setEditorPopupVisible(true);
    };

    const refreshDisplays = () => {
        setCounter(counter + 1);
    };

    const createEntry = (e) => {
        roadSmartStationsDataStore
            .store()
            .insert(e.formData)
            .then((e) => {
                closeEditorPopup();
                refreshDisplays();
            })
            .catch((error) => {
                onDataErrorOccurred({error});
                closeEditorPopup();
            });
    };

    const updateEntry = (e) => {
        if (!isEmpty(e.formData)) {
            roadSmartStationsDataStore
                .store()
                .update(e.editedRowId, e.formData)
                .then((data) => {
                    closeEditorPopup();
                    refreshDisplays();
                })
                .catch((error) => {
                    onDataErrorOccurred({error});
                    closeEditorPopup();
                });
        } else {
            closeEditorPopup();
        }
    };

    const openEditorPopup = (e) => {
        const currentRow = roadSmartStationsDataStore.items().find((item) => item.id === e.row.key);

        if (!isEmpty(currentRow)) {
            setEditedRow(currentRow);
            setEditorPopupVisible(true);
        }
    };

    const openEditorPopupFromMap = (id) => {
        const currentRow = roadSmartStationsDataStore.items().find((item) => item.id === id);
        if (!isEmpty(currentRow)) {
            setEditedRow(currentRow);
            setEditorPopupVisible(true);
        }
    };

    const openViewPopupFromMap = (id) => {
        const currentRow = roadSmartStationsDataStore.items().find((item) => item.id === id);
        if (!isEmpty(currentRow)) {
            setEditedRow(currentRow);
            setViewPopupVisible(true);
        }
    };

    const openVideoStreamPopup = (id) => {
        if (id) {
            setVideoCameraId(id);
            setVideoStreamPopupVisible(true);
        }
    };

    const deleteEntry = (e) => {
        roadSmartStationsDataStore
            .store()
            .remove(deletedRowKey)
            .then(() => {
                closeDeleteRowPopup();
                refreshDisplays();
            })
            .catch((error) => {
                onDataErrorOccurred({error});
                closeDeleteRowPopup();
            });
    };

    return (
        <React.Fragment>
            <h2 className={"content-block"}>{windowTitle}</h2>
            <div className={"content-block"}>
                {editorPopupVisible && (
                    <EditorPopupSmartStations
                        editedRow={editedRow}
                        windowId={windowId}
                        closeEditorPopup={closeEditorPopup}
                        updateEntry={updateEntry}
                        createEntry={createEntry}
                    />
                )}
                {viewPopupVisible && (
                    <ViewPopupSmartStations
                        editedRow={editedRow}
                        windowId={windowId}
                        openEditorPopup={openEditorPopupFromMap}
                        openDeleteRowPopup={openEditorPopupFromMap}
                        onDataErrorOccurred={onDataErrorOccurred}
                        closeEditorPopup={closeViewPopup}
                    />
                )}
                {deleteRowPopupVisible && (
                    <Popup
                        className="deleteRowPopup"
                        width="auto"
                        height="auto"
                        visible={true}
                        showCloseButton={false}
                        showTitle={false}
                        onHiding={closeDeleteRowPopup}
                    >
                        <Position my="center" at="center" of={window}/>
                        <div>
                            <span>Вы уверены, что хотите удалить эту запись?</span>
                        </div>
                        <ToolbarItem
                            widget="dxButton"
                            location="center"
                            options={{text: "Да", onClick: deleteEntry}}
                            toolbar="bottom"
                        />
                        <ToolbarItem
                            widget="dxButton"
                            location="center"
                            options={{text: "Нет", onClick: closeDeleteRowPopup}}
                            toolbar="bottom"
                        />
                    </Popup>
                )}
                {videoStreamPopupVisible && (
                    <VideoStreamPopup
                        closeVideoStreamPopup={closeVideoStreamPopup}
                        onDataErrorOccurred={onDataErrorOccurred}
                        videoCameraId={videoCameraId}
                    />
                )}
                <div className={"dx-card responsive-paddings"}>
                    <Tabs
                        dataSource={tabs}
                        defaultSelectedIndex={0}
                        width={374}
                        onItemClick={(e) => {
                            if (e.itemIndex === 0) {
                                clickOnTable();
                            } else if (e.itemIndex === 1) {
                                clickOnCards();
                            } else {
                                clickOnMap();
                            }
                        }}
                    />
                    <div className={!isVisible.table ? "hidden" : "visible"}>
                        <SmartStationsTable openVideoStreamPopup={openVideoStreamPopup} roadSmartStations={roadSmartStations} roadSmartStationsDataStore={roadSmartStationsDataStore}
                                            openDeleteRowPopup={openDeleteRowPopup} openEditorPopup={openEditorPopup}
                                             counter={counter} onDataErrorOccurred={onDataErrorOccurred}
                                            openCreateEntryPopup={openCreateEntryPopup}
                                            openViewPopupFromMap={openViewPopupFromMap}
                          />
                    </div>
                    {isCardsLoaded && (
                        <div className={!isVisible.cards ? "hidden" : "visible"}>
                            <DisplaysCards
                                openEditorPopup={openEditorPopupFromMap}
                                openDeleteRowPopup={openDeleteRowPopupFromMap}
                                openVideoStreamPopup={openVideoStreamPopup}
                                onDataErrorOccurred={onDataErrorOccurred}
                                counter={counter}
                                store={roadSmartStations}
                                windowId={windowId}
                            />
                        </div>
                    )}
                    {isMapLoaded && (
                        <div className={!isVisible.map ? "hidden" : "visible"}>
                            <SmartStationsMap openViewPopupFromMap={openViewPopupFromMap} openEditorPopupFromMap={openEditorPopupFromMap} onDataErrorOccurred={onDataErrorOccurred} counter={counter} />
                        </div>
                    )}
                </div>
            </div>
        </React.Fragment>
    );
}

export default connect((state) => ({}), {
    localDeauthorize: deauthorize,
})(SmartStationsPage);
