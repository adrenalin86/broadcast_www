import {useEffect, useState} from "react";

import {LoadIndicator} from "devextreme-react";

import {getDisplaysWithStatusDataStore, getSmartStationsDataStore, WINDOWS} from "../../dataStores";

import DisplaysCard from "./displaysCard";

import config from "../../config";


const AUTOMATIC_UPDATE_TIMEOUT_MS = config.pages.displays.displaysGrid.updateInterval;

const DisplaysCards = ({
                           openEditorPopup,
                           openDeleteRowPopup,
                           openVideoStreamPopup,
                           onDataErrorOccurred,
                           counter,
                           store,
                           windowId: windowIdProps
                       }) => {
    const [displays, setDisplays] = useState([]);
    const [isLoadedDisplays, setIsLoadedDisplays] = useState(false);

    const windowId = windowIdProps ? windowIdProps : WINDOWS.VIDEOCAMERAS.id;

    const displaysWithStatusDataStore = !store ? getDisplaysWithStatusDataStore({
        windowId: windowId,
    }) : store;


    useEffect(() => {
        const updateData = () => {
            if (displays) {
                displaysWithStatusDataStore
                    .load()
                    .then((result) => {
                        setDisplays(result.data);
                    })
                    .catch((error) => {
                        onDataErrorOccurred({error});
                    });
            }
        };

        const timer = setInterval(() => {
            updateData();
        }, AUTOMATIC_UPDATE_TIMEOUT_MS);

        return () => {
            clearInterval(timer);
        };
    }, []);

    useEffect(() => {
        displaysWithStatusDataStore
            .load()
            .then((res) => {
                setDisplays(res.data);
                setIsLoadedDisplays(true);
            })
            .catch((error) => {
                onDataErrorOccurred({error});
            });
    }, [counter]);

    return (
        <div
            style={{
                display: "flex",
                flexWrap: "wrap",
                height: "calc(100vh - 347px)",
                width: "100%",
                minWidth: "260px",
                overflowY: "auto",
                marginTop: "20px",
            }}
        >
            {displays.map((display) => (
                <DisplaysCard
                    openVideoStreamPopup={openVideoStreamPopup}
                    openEditorPopup={openEditorPopup}
                    openDeleteRowPopup={openDeleteRowPopup}
                    onDataErrorOccurred={onDataErrorOccurred}
                    mainVideoCameraId={display.mainVideoCameraId}
                    displayId={display.id}
                    displayName={display.name}
                    displayStatus={store ? display.mainVideoCamera?.status : display.status}
                    key={display.id}
                />
            ))}
            {displays.length === 0 && isLoadedDisplays && (
                <div
                    style={{
                        color: "#999",
                        fontSize: "17px",
                        display: "flex",
                        justifyContent: "center",
                        width: "100%",
                    }}
                >
                    Нет данных
                </div>
            )}
            {!isLoadedDisplays && (
                <div
                    style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        width: "100%",
                        height: "100%",
                    }}
                >
                    <LoadIndicator
                        id="large-indicator"
                        className="button-indicator"
                        height={40}
                        width={40}
                        visible={!isLoadedDisplays}
                    />
                </div>
            )}
        </div>
    );
};

DisplaysCards.defaultProps = {
    type: 'display'
};

export default DisplaysCards;
