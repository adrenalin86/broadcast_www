import { createSlice } from "@reduxjs/toolkit";

import config from "../config";
import { omitDeepByKeys } from "../utils";

export const DISPLAYS_PAGE_REDUCER_KEY = "displays";

const displayModelPageSlice = createSlice({
  name: DISPLAYS_PAGE_REDUCER_KEY,
  initialState: {
    ...omitDeepByKeys(config.pages[DISPLAYS_PAGE_REDUCER_KEY], ["dxOptions"]),
  },
  reducers: {
    setFilterRowVisibility: (state, { payload }) => {
      state.displaysGrid.dxOptions.filterRow = { visible: payload };
      return state;
    },
  },
});

export const { setFilterRowVisibility } = displayModelPageSlice.actions;

export const getDisplaysPage = (state) =>
  state.pages[DISPLAYS_PAGE_REDUCER_KEY];

export default displayModelPageSlice.reducer;
