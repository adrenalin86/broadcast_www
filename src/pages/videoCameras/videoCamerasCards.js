import React, { useEffect, useState } from "react";

import config from "../../config";

import { getVideoCamerasWithStatusesDataStore, WINDOWS } from "../../dataStores";

import VideoCamerasCard from "./videoCamerasCard";

import { LoadIndicator } from "devextreme-react";

const windowId = WINDOWS.VIDEOCAMERAS.id;

const videoCamerasWithStatusesDataStore = getVideoCamerasWithStatusesDataStore({
  windowId: windowId,
});

const AUTOMATIC_UPDATE_TIMEOUT_MS = config.pages.videoCameras.videoCamerasGrid.updateInterval;

const VideoCamerasCards = ({
  openEditorPopup,
  openDeleteRowPopup,
  openVideoStreamPopup,
  onDataErrorOccurred,
  counter,
}) => {
  const [videoCameras, setVideoCameras] = useState([]);
  const [isLoadedVideoCameras, setIsLoadedVideoCameras] = useState(false);

  useEffect(() => {
    const updateData = () => {
      if (videoCameras) {
        videoCamerasWithStatusesDataStore
          .load()
          .then((result) => {
            setVideoCameras(result.data);
          })
          .catch((error) => {
            onDataErrorOccurred({ error });
          });
      }
    };

    const timer = setInterval(() => {
      updateData();
    }, AUTOMATIC_UPDATE_TIMEOUT_MS);

    return () => {
      clearInterval(timer);
    };
  }, []);

  useEffect(() => {
    videoCamerasWithStatusesDataStore
      .load()
      .then((res) => {
        setVideoCameras(res.data);
        setIsLoadedVideoCameras(true);
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
      });
  }, [counter]);

  return (
    <div
      style={{
        display: "flex",
        flexWrap: "wrap",
        height: "calc(100vh - 347px)",
        width: "100%",
        minWidth: "260px",
        overflowY: "auto",
        marginTop: "20px",
      }}
    >
      {videoCameras.map((videoCamera) => (
        <VideoCamerasCard
          openVideoStreamPopup={openVideoStreamPopup}
          openEditorPopup={openEditorPopup}
          openDeleteRowPopup={openDeleteRowPopup}
          onDataErrorOccurred={onDataErrorOccurred}
          videoCameraId={videoCamera.id}
          videoCameraName={videoCamera.name}
          videoCameraStatus={videoCamera.status}
          videoCameraSettings={videoCamera.settings}
          key={videoCamera.id}
        />
      ))}
      {videoCameras.length === 0 && isLoadedVideoCameras && (
        <div
          style={{
            color: "#999",
            fontSize: "17px",
            display: "flex",
            justifyContent: "center",
            width: "100%",
          }}
        >
          Нет данных
        </div>
      )}
      {!isLoadedVideoCameras && (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
            height: "100%",
          }}
        >
          <LoadIndicator
            id="large-indicator"
            className="button-indicator"
            height={40}
            width={40}
            visible={!isLoadedVideoCameras}
          />
        </div>
      )}
    </div>
  );
};

export default VideoCamerasCards;
