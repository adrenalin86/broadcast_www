import CustomStore from "devextreme/data/custom_store";

import store from "../store";

import { getBackendUrl, getToken, getRegion } from "../store/commonSlice";

import { handleResponseErrors, handleDataErrors, handleCaughtErrors } from "./utils";

const getDataStore = ({ windowId }) => {
  const restArgs = {
    ct: 27,
    w: windowId,
  };

  return new CustomStore({
    key: "id",
    load: (data) => {
      let body = {
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        ...restArgs,
        cd: "loadAreas",
        data,
      };

      return fetch(getBackendUrl(store.getState()) + "?" + body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .then((data) => {
          return {
            data: data.data.entities,
            summary: data.data.summary,
            totalCount: data.data.totalCount,
          };
        })
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },
    insert: function (values) {
      let body = {
        ...restArgs,
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        cd: "insertArea",
        data: {
          ...values,
        },
      };

      return fetch(getBackendUrl(store.getState()) + "?" + body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },

    update: function (key, values) {
      let body = {
        ...restArgs,
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        cd: "updateArea",
        data: { id: key, values },
      };

      return fetch(getBackendUrl(store.getState()) + "?" + body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },

    remove: function (key) {
      let body = {
        ...restArgs,
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        cd: "removeArea",
        data: {
          id: key,
        },
      };

      return fetch(getBackendUrl(store.getState()) + "?" + body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },
    byKey: (key) => {
      let body = {
        ...restArgs,
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        cd: "loadAreas",
        data: { filter: ["id", "=", key] },
      };

      return fetch(getBackendUrl(store.getState()) + "?" + body.cd + "byKey", {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .then((data) => {
          return data.data.entities;
        })
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },
  });
};

export default getDataStore;
