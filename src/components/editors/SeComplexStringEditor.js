import React, { useState, useEffect } from "react";

import { Switch, TextBox, CheckBox, SelectBox, Button, NumberBox, TagBox } from "devextreme-react";
import { cloneDeep, isEmpty, get, has, set } from "lodash-es";

const emptyDisplayMessageData = {
  weatherString: undefined,
  simpleString: undefined,
  newsString: undefined,
};

const EDITOR_GROUP = {
  NOTHING: 0,
  SIMPLE_TEXT: 1,
  WEATHER_REPORT: 2,
  NEWS_REPORT: 3,
};

const SeComplexStringEditor = ({
  data,
  onFormItemValueChanged,
  editModeEnabled = false,
  allowDisabling = true,
  dataField,
  title,
  tagIdsSource,
  className = "",
}) => {
  const [currentEditorGroup, setCurrentEditorGroup] = useState(EDITOR_GROUP.NOTHING); //селектор выбранной группы редакторов.
  const [objectData, setObjectData] = useState(emptyDisplayMessageData); //текущее состояние компонента
  const [initialData, setInitialData] = useState(emptyDisplayMessageData); //начальное состояние компонента

  const [disabledState, setDisabledState] = useState({ disabled1: false, disabled2: false, disabled3: false });

  //для передачи данных из компонента модифицируем объект события полученый от редактора или кнопки

  useEffect(() => {
    recalculateDisabled();
  }, [objectData]);

  //Инициализация данных редактора
  useEffect(() => {
    const fieldData = get(data, dataField);
    if (!isEmpty(fieldData)) {
      setInitialData((oldData) => fieldData);
      setObjectData((oldData) => fieldData);

      //выбор первой открытой группы настроек источника
      if (!isEmpty(fieldData.simpleString)) {
        setCurrentEditorGroup(EDITOR_GROUP.SIMPLE_TEXT);
      } else if (!isEmpty(fieldData.weatherString)) {
        setCurrentEditorGroup(EDITOR_GROUP.WEATHER_REPORT);
      } else if (!isEmpty(fieldData.newsString)) {
        setCurrentEditorGroup(EDITOR_GROUP.NEWS_REPORT);
      }
    }
  }, []);

  //смена открытой подгруппы редакторов
  const onChangeEditorGroup = (e) => {
    setCurrentEditorGroup((oldData) => {
      return e.value;
    });

    //при смене подгруппы редакторов берём данные на основе начально полученных в редкатор, данные от прочих групп выставляем в null
    if (e.value === EDITOR_GROUP.SIMPLE_TEXT) {
      onFormItemValueChanged({
        value: { ...emptyDisplayMessageData, simpleString: initialData.simpleString },
        dataField: dataField,
      });
    } else if (e.value === EDITOR_GROUP.WEATHER_REPORT) {
      onFormItemValueChanged({
        value: { ...emptyDisplayMessageData, weatherString: initialData.weatherString },
        dataField: dataField,
      });
    } else if (e.value === EDITOR_GROUP.NEWS_REPORT) {
      onFormItemValueChanged({
        value: { ...emptyDisplayMessageData, newsString: initialData.newsString },
        dataField: dataField,
      });
    }
  };

  const onValueChanged = (e) => {
    const objectDataField = e?.component?.option("dataField");
    const enabler = e?.component?.option("enabler");
    let newData = cloneDeep(objectData);
    if (enabler) {
      if (objectDataField === "switchableEditor") {
        if (e.value) {
          set(newData, "weatherString", undefined);
          set(newData, "newsString", undefined);
          set(newData, "simpleString", undefined);
        } else {
          set(newData, "weatherString", null);
          set(newData, "newsString", null);
          set(newData, "simpleString", null);
        }
      } else {
        if (e.value) {
          set(newData, objectDataField, undefined);
        } else {
          set(newData, objectDataField, null);
        }
      }
    } else {
      newData = set(newData, objectDataField, e.value);
    }

    setObjectData((oldData) => cloneDeep(newData));
    onFormItemValueChanged({
      value: newData,
      dataField: dataField,
    });
  };

  const recalculateDisabled = () => {
    const disabled11 = allowDisabling && (get(objectData, "simpleString") === null || !has(objectData, "simpleString"));
    const disabled12 =
      allowDisabling && (get(objectData, "weatherString") === null || !has(objectData, "weatherString"));
    const disabled13 = allowDisabling && (get(objectData, "newsString") === null || !has(objectData, "newsString"));

    const disabled1 = disabled11 && disabled12 && disabled13;

    const disabled2 = allowDisabling && (get(objectData, "pixelShift") === null || !has(objectData, "pixelShift"));

    const disabled3 = allowDisabling && (get(objectData, "speed") === null || !has(objectData, "speed"));

    setDisabledState((oldData) => ({ disabled1: disabled1, disabled2: disabled2, disabled3: disabled3 }));
  };

  const cancelEditing = (e) => {
    setCurrentEditorGroup(EDITOR_GROUP.NOTHING);
    setObjectData((oldData) => {
      return {
        ...oldData,
        ...emptyDisplayMessageData,
      };
    });

    onFormItemValueChanged({
      dataField: dataField,
      value: {
        ...emptyDisplayMessageData,
      },
    });
  };

  return (
    <>
      <div className={"dx-field complex-custom-editor highlight-block-pale-gray " + className}>
        {currentEditorGroup === EDITOR_GROUP.NOTHING && (
          <>
            {allowDisabling && editModeEnabled && (
              <div className="field-enabler">
                <Switch
                  value={!disabledState.disabled1}
                  onValueChanged={onValueChanged}
                  dataField={"switchableEditor"}
                  enabler={true}
                />
              </div>
            )}
            <div className="dx-field-label" disabled={disabledState.disabled1}>
              {title}
            </div>
            <div className="dx-field-value ">
              <div className="source-type">
                <SelectBox
                  className="source-type-select"
                  placeholder={"Выберите вариант"}
                  dataSource={[
                    {
                      id: EDITOR_GROUP.SIMPLE_TEXT,
                      name: "Текстовая строка",
                    },
                    // {
                    //   id: EDITOR_GROUP.WEATHER_REPORT,
                    //   name: "Прогноз погоды",
                    // },
                    // {
                    //   id: EDITOR_GROUP.NEWS_REPORT,
                    //   name: "Новостная лента",
                    // },
                  ]}
                  valueExpr={"id"}
                  displayExpr={"name"}
                  onValueChanged={onChangeEditorGroup}
                  dropDownButtonComponent={(e) => {
                    return (
                      <div className="source-type-button">
                        <i className="dx-icon-more icon"></i>
                      </div>
                    );
                  }}
                  disabled={disabledState.disabled1}
                />
              </div>
            </div>
          </>
        )}
        {currentEditorGroup === EDITOR_GROUP.SIMPLE_TEXT && (
          <>
            {allowDisabling && editModeEnabled && (
              <div className="field-enabler">
                <Switch
                  defaultValue={!disabledState.disabled1}
                  onValueChanged={onValueChanged}
                  dataField={"switchableEditor"}
                  enabler={true}
                />
              </div>
            )}
            <div className="dx-field-label" disabled={disabledState.disabled1}>
              {title}
            </div>
            <div className="dx-field-value ">
              <div className="source-editor">
                <div className="source-editor-content">
                  <div className="textbox-editor simple-string inline-label">
                    <span
                      className="label"
                      disabled={allowDisabling && (get(data, dataField) === null || !has(data, dataField))}
                    >
                      Текст
                    </span>
                    <TextBox
                      value={objectData?.simpleString?.text}
                      onValueChanged={onValueChanged}
                      disabled={disabledState.disabled1}
                      dataField={"simpleString.text"}
                    />
                  </div>
                </div>
                <div className="source-editor-controls">
                  <Button
                    name="cancelEditing"
                    location="after"
                    icon="close"
                    onClick={cancelEditing}
                    disabled={disabledState.disabled1}
                  />
                </div>
              </div>
            </div>
          </>
        )}
        {currentEditorGroup === EDITOR_GROUP.WEATHER_REPORT && (
          <>
            {allowDisabling && editModeEnabled && (
              <div className="field-enabler">
                <Switch
                  defaultValue={!disabledState.disabled1}
                  onValueChanged={onValueChanged}
                  dataField={"switchableEditor"}
                  enabler={true}
                />
              </div>
            )}
            <div className="dx-field-label" disabled={disabledState.disabled1}>
              {title}
            </div>
            <div className="dx-field-value ">
              <div className="source-editor">
                <div className="source-editor-content">
                  <div className="checkbox-editor inline-label">
                    <span className="label">Температура</span>
                    <CheckBox
                      value={objectData?.weatherString?.showTemp}
                      onValueChanged={onValueChanged}
                      disabled={disabledState.disabled1}
                      dataField={"weatherString.showTemp"}
                    />
                  </div>
                  <div className="checkbox-editor inline-label">
                    <span className="label" disabled={disabledState.disabled1}>
                      Ветер
                    </span>
                    <CheckBox
                      value={objectData?.weatherString?.showWind}
                      onValueChanged={onValueChanged}
                      disabled={disabledState.disabled1}
                      dataField={"weatherString.showWind"}
                    />
                  </div>
                  <div className="checkbox-editor inline-label">
                    <span className="label" disabled={disabledState.disabled1}>
                      Давление
                    </span>
                    <CheckBox
                      value={objectData?.weatherString?.showPressure}
                      onValueChanged={onValueChanged}
                      disabled={disabledState.disabled1}
                      dataField={"weatherString.showPressure"}
                    />
                  </div>
                  <div className="checkbox-editor">
                    <span className="label" disabled={disabledState.disabled1}>
                      Иконка
                    </span>
                    <CheckBox
                      value={objectData?.weatherString?.showIco}
                      onValueChanged={onValueChanged}
                      disabled={disabledState.disabled1}
                      dataField={"weatherString.showIco"}
                    />
                  </div>
                  <div className="textbox-editor inline-label">
                    <span className="label" disabled={disabledState.disabled1}>
                      Формат
                    </span>
                    <TextBox
                      value={objectData?.weatherString?.format}
                      onValueChanged={onValueChanged}
                      disabled={disabledState.disabled1}
                      dataField={"weatherString.format"}
                    />
                  </div>
                </div>
                <div className="source-editor-controls">
                  <Button
                    name="cancelEditing"
                    location="after"
                    icon="close"
                    onClick={cancelEditing}
                    disabled={disabledState.disabled1}
                  />
                </div>
              </div>
            </div>
          </>
        )}
        {currentEditorGroup === EDITOR_GROUP.NEWS_REPORT && (
          <>
            {allowDisabling && editModeEnabled && (
              <div className="field-enabler">
                <Switch
                  defaultValue={!disabledState.disabled1}
                  onValueChanged={onValueChanged}
                  dataField={"switchableEditor"}
                  enabler={true}
                />
              </div>
            )}
            <div className="dx-field-label" disabled={disabledState.disabled1}>
              {title}
            </div>
            <div className="dx-field-value ">
              <div className="source-editor">
                <div className="source-editor-content">
                  <div className="numberbox-editor inline-label">
                    <span className="label" disabled={disabledState.disabled1}>
                      Лимит
                    </span>
                    <NumberBox
                      value={objectData?.newsString?.countLimit}
                      onValueChanged={onValueChanged}
                      showSpinButtons={true}
                      disabled={disabledState.disabled1}
                      dataField={"newsString.countLimit"}
                    />
                  </div>
                  <div className="numberbox-editor inline-label">
                    <span className="label" disabled={disabledState.disabled1}>
                      Дней
                    </span>
                    <NumberBox
                      value={objectData?.newsString?.lastDaysLimit}
                      onValueChanged={onValueChanged}
                      showSpinButtons={true}
                      disabled={disabledState.disabled1}
                      dataField={"newsString.lastDaysLimit"}
                    />
                  </div>
                  <div className="tagbox-editor inline-label">
                    <span className="label" disabled={disabledState.disabled1}>
                      Тэги
                    </span>
                    <TagBox
                      value={objectData?.newsString?.tagIds || []}
                      dataSource={tagIdsSource}
                      onValueChanged={onValueChanged}
                      valueExpr={"id"}
                      displayExpr={"name"}
                      placeholder="Нет"
                      multiline={true}
                      disabled={disabledState.disabled1}
                      dataField={"newsString.tagIds"}
                    />
                  </div>
                  <div className="textbox-editor inline-label">
                    <span className="label" disabled={disabledState.disabled1}>
                      Формат
                    </span>
                    <TextBox
                      value={objectData?.newsString?.format}
                      onValueChanged={onValueChanged}
                      showSpinButtons={true}
                      disabled={disabledState.disabled1}
                      dataField={"newsString.format"}
                    />
                  </div>
                </div>
                <div className="source-editor-controls">
                  <Button
                    name="cancelEditing"
                    location="after"
                    icon="close"
                    onClick={cancelEditing}
                    disabled={disabledState.disabled1}
                  />
                </div>
              </div>
            </div>
          </>
        )}
      </div>
      <div className={"dx-field number-field highlight-block-pale-gray " + className}>
        {allowDisabling && editModeEnabled && (
          <div className="field-enabler">
            <Switch
              defaultValue={!disabledState.disabled2}
              onValueChanged={onValueChanged}
              dataField={"pixelShift"}
              enabler={true}
            />
          </div>
        )}
        <div className="dx-field-label" disabled={disabledState.disabled2}>
          {"Сдвиг пикселей"}
        </div>
        <div className="dx-field-value   ">
          <NumberBox
            value={objectData.pixelShift}
            onValueChanged={onValueChanged}
            showSpinButtons={true}
            disabled={disabledState.disabled2}
            dataField={"pixelShift"}
          />
        </div>
      </div>
      <div className={"dx-field number-field highlight-block-pale-gray " + className}>
        {allowDisabling && editModeEnabled && (
          <div className="field-enabler">
            <Switch
              defaultValue={!disabledState.disabled3}
              onValueChanged={onValueChanged}
              dataField={"speed"}
              enabler={true}
            />
          </div>
        )}
        <div className="dx-field-label" disabled={disabledState.disabled3}>
          {"Скорость строки"}
        </div>
        <div className="dx-field-value   ">
          <NumberBox
            value={objectData?.speed}
            onValueChanged={onValueChanged}
            showSpinButtons={true}
            disabled={disabledState.disabled3}
            dataField={"speed"}
          />
        </div>
      </div>
    </>
  );
};

export { SeComplexStringEditor };
