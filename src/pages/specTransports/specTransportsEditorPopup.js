import { Position, ToolbarItem } from "devextreme-react/popup";
import {CheckBox, LoadIndicator, Popup, SelectBox, TextBox, ValidationSummary} from "devextreme-react";
import Form, { SimpleItem } from "devextreme-react/form";
import validationEngine from "devextreme/ui/validation_engine";
import Validator, { RequiredRule } from "devextreme-react/validator";
import { connect } from "react-redux";
import { set } from "lodash-es";
import { deauthorize } from "../../store/commonSlice";
import config from "../../config";
import { useEffect, useState } from "react";
import {
  getSpecCompaniesDataStore,
  getSpecTransportsDataStore,
  getSpecTransportTypesDataStore,
  WINDOWS,
} from "../../dataStores";

const windowId = WINDOWS.SPEC_TRANSPORTS.id;

const specTransportsDataStore = getSpecTransportsDataStore({
  windowId: windowId,
});

const specTransportTypesDataStore = getSpecTransportTypesDataStore({
  windowId: windowId,
});

const specCompaniesDataStore = getSpecCompaniesDataStore({
  windowId: windowId,
});

const specTransportTypesPopupOptions =
  config.pages.specTransports.specTransportsGrid.popupSpecTransportEditor.dxOptions;

const SpecTransportsEditorPopup = ({
  specTransportId,
  createEntry,
  updateEntry,
  closeEditorPopup,
  onDataErrorOccurred,
}) => {
  const [formData, setFormData] = useState({});
  const [changedFormData] = useState({});
  const [isLoadedData, setIsLoadedData] = useState(false);

  const setFormDataValue = (dataField, value) => {
    set(formData, dataField, value);
    set(changedFormData, dataField, value);
  };

  const onFormItemValueChanged = (e) => {
    const value = e.value !== undefined ? e.value : e?.component?.option("text");
    const dataField = e.component.option("dataField");

    setFormDataValue(dataField, value);
  };

  const onSave = () => {
    const isValid = validationEngine.validateGroup("formEditor").isValid;

    if (isValid) {
      if (!specTransportId) {
        createEntry(formData);
      } else {
        updateEntry({
          formData: changedFormData,
          editedRowId: formData.id,
        });
      }
    }
  };

  useEffect(() => {
    if (specTransportId) {
      specTransportsDataStore
        .byKey(specTransportId)
        .then((result) => {
          const [specTransport] = result;

          setFormData(specTransport);
          setIsLoadedData(true);
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
        });
    } else {
      setIsLoadedData(true);
    }
  }, []);

  return (
    <Popup
      visible={true}
      onHiding={closeEditorPopup}
      titleComponent={() => {
        return <span className="popup-title">{specTransportTypesPopupOptions.title}</span>;
      }}
      className="customEditorPopup"
      {...specTransportTypesPopupOptions}
    >
      <Position my="center" at="center" of={window} />
      {!isLoadedData && (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "100%",
          }}
        >
          <LoadIndicator
            id="large-indicator"
            className="button-indicator m-v-65"
            height={60}
            width={60}
            visible={!isLoadedData}
          />
        </div>
      )}
      <Form validationGroup="formEditor">
        {isLoadedData && (
          <SimpleItem>
            <div className="editor-content">
              <div className="dx-form-group-with-caption dx-form-group editor-group">
                <div className="editor-item">
                  <div className="dx-field">
                    <div className="dx-field-label">Гос. номер</div>
                    <div className="dx-field-value">
                      <TextBox
                        dataField="gosNumber"
                        defaultValue={formData.gosNumber}
                        onInput={onFormItemValueChanged}
                      >
                        <Validator validationGroup="formEditor">
                          <RequiredRule />
                        </Validator>
                      </TextBox>
                    </div>
                  </div>
                </div>
                <div className="editor-item">
                  <div className="dx-field">
                    <div className="dx-field-label">Гаражный номер</div>
                    <div className="dx-field-value">
                      <TextBox
                        dataField="garageNumber"
                        defaultValue={formData.garageNumber}
                        onInput={onFormItemValueChanged}
                      />
                    </div>
                  </div>
                </div>
                <div className="editor-item">
                  <div className="dx-field">
                    <div className="dx-field-label">Тип техники</div>
                    <div className="dx-field-value">
                      <SelectBox
                        dataSource={specTransportTypesDataStore}
                        dataField="specTransportTypeId"
                        valueExpr="id"
                        displayExpr="name"
                        showClearButton={true}
                        defaultValue={formData.specTransportTypeId}
                        onValueChanged={onFormItemValueChanged}
                      >
                        <Validator validationGroup="formEditor">
                          <RequiredRule />
                        </Validator>
                      </SelectBox>
                    </div>
                  </div>
                </div>
                <div className="editor-item">
                  <div className="dx-field">
                    <div className="dx-field-label">Организация</div>
                    <div className="dx-field-value">
                      <SelectBox
                        dataSource={specCompaniesDataStore}
                        dataField="specCompanyId"
                        valueExpr="id"
                        displayExpr="name"
                        showClearButton={true}
                        defaultValue={formData.specCompanyId}
                        onValueChanged={onFormItemValueChanged}
                      >
                        <Validator validationGroup="formEditor">
                          <RequiredRule />
                        </Validator>
                      </SelectBox>
                    </div>
                  </div>
                </div>
                <div className="editor-item">
                  <div className="dx-field">
                    <div className="dx-field-label">Идентификатор навигационного прибора</div>
                    <div className="dx-field-value">
                      <TextBox
                        dataField="deviceCode"
                        defaultValue={formData.deviceCode}
                        onInput={onFormItemValueChanged}
                      />
                    </div>
                  </div>
                </div>
                <div className="editor-item">
                  <div className="dx-field">
                    <div className="dx-field-label">Номер сим-карты</div>
                    <div className="dx-field-value">
                      <TextBox
                        dataField="phoneNumber"
                        defaultValue={formData.phoneNumber}
                        onInput={onFormItemValueChanged}
                      />
                    </div>
                  </div>
                </div>
                <div className="editor-item">
                  <div className="dx-field">
                    <div className="dx-field-label">Заметки</div>
                    <div className="dx-field-value">
                      <TextBox
                        dataField="note"
                        defaultValue={formData.note}
                        onInput={onFormItemValueChanged}
                      />
                    </div>
                  </div>
                </div>
                <div className="editor-item">
                  <div className="dx-field">
                    <div className="dx-field-label">Показывать на портале</div>
                    <div className="dx-field-value">
                      <CheckBox
                        dataField="showOnPortal"
                        defaultValue={formData.showOnPortal}
                        onInput={onFormItemValueChanged}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </SimpleItem>
        )}
      </Form>
      <ValidationSummary id="summary" validationGroup="formEditor" />
      <ToolbarItem
        widget="dxButton"
        location="after"
        toolbar="bottom"
        options={{ text: "Сохранить", onClick: onSave }}
        disabled={!isLoadedData}
      />
      <ToolbarItem
        widget="dxButton"
        location="after"
        toolbar="bottom"
        options={{ text: "Отменить", onClick: closeEditorPopup }}
      />
    </Popup>
  );
};

export default connect(null, {
  localDeauthorize: deauthorize,
})(SpecTransportsEditorPopup);
