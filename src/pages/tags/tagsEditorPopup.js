import React, { useState, useEffect } from "react";

import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import { ScrollView } from "devextreme-react/scroll-view";
import Form, { SimpleItem } from "devextreme-react/form";
import { TextBox, TagBox } from "devextreme-react";

import ValidationSummary from "devextreme-react/validation-summary";
import ValidationEngine from "devextreme/ui/validation_engine";
import Validator, { RequiredRule, StringLengthRule } from "devextreme-react/validator";

import { set, cloneDeep, isEmpty } from "lodash-es";

import { getEntityDefinitionDataStore, GENERIC_ENTITIES, WINDOWS } from "../../dataStores";

import config from "../../config";

const windowId = WINDOWS.TAGS.id;

const entityTypeDataStore = getEntityDefinitionDataStore({
  windowId: windowId,
  entityName: GENERIC_ENTITIES.ENTITY_DEFINITIONS,
});

const defaultValues = {};

const TagsEditorPopup = ({ editedRow, closeEditorPopup, updateEntry, createEntry }) => {
  const [formData, setFormData] = useState({});
  const [changedFormData] = useState({});

  const tagsPopupOptions = config.pages.tags.tagsGrid.popupTagsEditor.dxOptions;
  /**
   * Инициализация редактора.
   */
  useEffect(() => {
    if (isEmpty(editedRow)) {
      setFormData(cloneDeep(defaultValues));
    } else {
      setFormData(cloneDeep(editedRow));
    }
  }, []);

  const closePopup = () => {
    closeEditorPopup();
  };

  const onSave = () => {
    const isValid = ValidationEngine.validateGroup("formEditor").isValid;

    if (isValid) {
      if (isEmpty(editedRow)) {
        createEntry({
          formData: formData,
        });
      } else {
        updateEntry({
          formData: changedFormData,
          editedRowId: editedRow.id,
        });
      }
    }
  };

  const onCancel = () => {
    closePopup();
  };

  const onFormItemValueChanged = (e) => {
    const value = e.value !== undefined ? e.value : e?.component?.option("text");
    const dataField = e.component.option("dataField");

    setFormDataValue(dataField, value);
  };

  const setFormDataValue = (dataField, value) => {
    set(formData, dataField, value);
    set(changedFormData, dataField, value);
  };

  return (
    <React.Fragment>
      <Popup
        className="customEditorPopup"
        visible={true}
        onHiding={closePopup}
        titleComponent={() => {
          return <span className="popup-title">{tagsPopupOptions.title}</span>;
        }}
        {...tagsPopupOptions}
      >
        <Position my="center" at="center" of={window} />
        <ScrollView showScrollbar="always">
          <form>
            <Form validationGroup="formEditor">
              <SimpleItem>
                <div className="editor-content">
                  <div className="dx-form-group-with-caption dx-form-group editor-group">
                    <div className="dx-form-group-caption">Общие сведения</div>
                    <div className="dx-form-group-content editor-content">
                      <div className="editor-item cols-2">
                        <div className="dx-field">
                          <div className="dx-field-label">Название</div>
                          <div className="dx-field-value">
                            <TextBox defaultValue={formData.name} onInput={onFormItemValueChanged} dataField="name">
                              <Validator validationGroup="formEditor">
                                <RequiredRule />
                              </Validator>
                            </TextBox>
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">Символы</div>
                          <div className="dx-field-value">
                            <TextBox
                              defaultValue={formData.symbols}
                              onInput={onFormItemValueChanged}
                              dataField="symbols"
                            >
                              <Validator validationGroup="formEditor">
                                <RequiredRule />
                                <StringLengthRule min={0} max={4} />
                              </Validator>
                            </TextBox>
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">Применим к</div>
                          <div className="dx-field-value">
                            <TagBox
                              defaultValue={formData.entityTypeIds}
                              onValueChanged={onFormItemValueChanged}
                              dataField="entityTypeIds"
                              dataSource={entityTypeDataStore}
                              valueExpr={"id"}
                              displayExpr={"name"}
                              showClearButton={true}
                            ></TagBox>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </SimpleItem>
            </Form>
          </form>
        </ScrollView>
        <ValidationSummary id="summary" validationGroup="formEditor"></ValidationSummary>
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Сохранить", onClick: onSave }}
          toolbar="bottom"
        />
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Отмена", onClick: onCancel }}
          toolbar="bottom"
        />
      </Popup>
    </React.Fragment>
  );
};

export default TagsEditorPopup;
