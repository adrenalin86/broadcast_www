import React, { useState, useRef, useEffect } from "react";
import { connect } from "react-redux";

import DataGrid, {
  Column,
  StateStoring,
  Button,
  Export,
  ColumnChooser,
  Lookup,
} from "devextreme-react/data-grid";
import Toolbar, { Item as ToolbarItem } from "devextreme-react/toolbar";
import { exportDataGrid } from "devextreme/excel_exporter";
import notify from "devextreme/ui/notify";
import DataSource from "devextreme/data/data_source";
import { TabPanel, Item } from "devextreme-react/tab-panel";

import VehicleOrdersPopup from "./ordersPopup";

import { ValueType, Workbook } from "exceljs";
import saveAs from "file-saver";
import { filter, isEmpty, last } from "lodash-es";

import { deauthorize } from "../../store/commonSlice";

import {
  getVehiclesWithStatusesDataStore,
  WINDOWS,
  GENERIC_ENTITIES,
  getGenericEntityDataStore,
  getSystemWindowsDataStore,
} from "../../dataStores";
import { useInterval } from "../../utils";

import dayjs from "dayjs";
import dayjs_relativeTime from "dayjs/plugin/relativeTime";
import dayjs_utc from "dayjs/plugin/utc";
import deltaTime from "../../utils/deltatime";

import config from "../../config";

dayjs.extend(dayjs_relativeTime);
dayjs.extend(dayjs_utc);

function onExporting(e) {
  const workbook = new Workbook();
  const worksheet = workbook.addWorksheet("Main sheet");

  exportDataGrid({
    component: e.component,
    worksheet: worksheet,
    autoFilterEnabled: true,
  }).then(function () {
    workbook.xlsx.writeBuffer().then(function (buffer) {
      saveAs(new Blob([buffer], { type: "application/octet-stream" }), "DataGrid.xlsx");
    });
  });
  e.cancel = true;
}

const windowId = WINDOWS.CONTROL.id;

const vehiclesDataStore = getVehiclesWithStatusesDataStore({
  windowId: windowId,
});

const vehiclesDataSource = new DataSource({
  store: vehiclesDataStore,
  reshapeOnPush: true,
});

const companiesDataStore = getGenericEntityDataStore({
  windowId: windowId,
  entityName: GENERIC_ENTITIES.COMPANIES,
  ct: 11,
});

const modelsDataStore = getGenericEntityDataStore({
  windowId: windowId,
  entityName: GENERIC_ENTITIES.VEHICLE_MODELS,
  ct: 11,
});

const systemWindowsDataStore = getSystemWindowsDataStore({
  windowId: windowId,
});

const vehiclesGridOptions = config.pages.controlCenter.vehiclesGrid.dxOptions;

const AUTOMATIC_UPDATE_TIMEOUT_MS = config.pages.controlCenter.vehiclesGrid.updateInterval;

const UPDATE_TIMEOUT_CHECK_INTERVAL_MS = 500;

const TABS_INDEXES = {
  VEHICLES: 0,
};

const UPDATE_STATUSES = {
  SUCCESS: 0,
  INPROGRESS: 1,
  FAIL: 2,
};

const PageComponent = ({ localDeauthorize }) => {
  const [selectedTabIndex, setSelectedTabIndex] = useState(TABS_INDEXES.VEHICLES);
  const [ordersPopupOpened, setOrdersPopupOpened] = useState(false);
  const [selectedRow, setSelectedRow] = useState(null);
  const [windowTitle, setWindowTitle] = useState();
  const datagridRef = useRef(null);
  const lastUpdateTimeRef = useRef(dayjs());
  const lastUpdateStatusRef = useRef(UPDATE_STATUSES.INPROGRESS);
  const pageRef = useRef(null);

  useEffect(() => {
    vehiclesDataSource.on("loadError", onDataSourceError);
    vehiclesDataSource.on("loadingChanged", onDataSourceLoadingChanged);
  }, []);

  useInterval(updateVehiclesGrid, UPDATE_TIMEOUT_CHECK_INTERVAL_MS);

  function setLastUpdateContent(text) {
    if (!pageRef.current) {
      return null;
    }
    const element = pageRef.current.querySelector(".toolbar-label-status");
    if (element) {
      element.textContent = text;
    }
  }

  function updateLastUpdateTimeIndicator() {
    if (lastUpdateStatusRef.current === UPDATE_STATUSES.SUCCESS) {
      setLastUpdateContent(
        `✅ Последнее обновление: ${lastUpdateTimeRef.current.format("HH:mm:ss")}`
      );
    }
    if (lastUpdateStatusRef.current === UPDATE_STATUSES.INPROGRESS) {
      setLastUpdateContent(`🛠 Обновление...`);
    }
    if (lastUpdateStatusRef.current === UPDATE_STATUSES.FAIL) {
      setLastUpdateContent(`⛔ Не удалось обновить`);
    }
  }

  function onDataSourceError() {
    lastUpdateStatusRef.current = UPDATE_STATUSES.FAIL;
    updateLastUpdateTimeIndicator();
  }

  function onDataSourceLoadingChanged(isLoading) {
    if (isLoading) {
      lastUpdateStatusRef.current = UPDATE_STATUSES.INPROGRESS;
      updateLastUpdateTimeIndicator();
    } else {
      lastUpdateTimeRef.current = dayjs();
      lastUpdateStatusRef.current = UPDATE_STATUSES.SUCCESS;
      updateLastUpdateTimeIndicator();
    }
  }

  function onDataErrorOccurred(e) {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);
    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  }

  function calculatedColumnFilterExpression(filterValue, selectedFilterOperation) {
    if (selectedFilterOperation === "between" && Array.isArray(filterValue)) {
      const filterExpression = [
        [this.dataField, ">=", filterValue[0]],
        "and",
        [this.dataField, "<=", filterValue[1]],
      ];
      return filterExpression;
    } else if (selectedFilterOperation === "=" && !Array.isArray(filterValue)) {
      const filterExpression = [this.dataField, "contains", filterValue];
      return filterExpression;
    } else {
      const filterExpression = [this.dataField, selectedFilterOperation, filterValue];
      return filterExpression;
    }
  }

  function openOrdersPopup(e) {
    const currentRow = vehiclesDataSource.items().find((item) => item.id === e.row.key);

    if (!isEmpty(currentRow)) {
      setSelectedRow({
        date: dayjs().format("YYYY-MM-DD"),
        vehicleId: currentRow?.id,
        gosNumber: currentRow?.gosNumber,
      });
      setOrdersPopupOpened(true);
    }
  }

  function closeOrdersPopup() {
    setOrdersPopupOpened(false);
    setSelectedRow(null);
  }

  const onToolbarPreparing = (e) => {
    let toolbarItems = e.toolbarOptions.items;
    toolbarItems.unshift({
      html: `
          <div class="toolbar-label-status" style="font-size:14px">
            🛠 Обновление...
          </div>
      `,
      location: "before",
    });
  };

  function updateVehiclesGrid() {
    if (!isEmpty(vehiclesDataSource) && !ordersPopupOpened && datagridRef.current) {
      if (vehiclesDataSource.isLoading()) {
        return null;
      }

      if (dayjs().valueOf() - lastUpdateTimeRef.current.valueOf() < AUTOMATIC_UPDATE_TIMEOUT_MS) {
        return null;
      }

      datagridRef.current.instance.refresh();
    }
  }

  useEffect(() => {
    systemWindowsDataStore.byKey(windowId).then((result) => {
      const windowName = result[0]?.name?.split("/").pop();

      setWindowTitle(windowName);
    });
  }, []);

  return (
    <React.Fragment>
      <h2 className={"content-block"}>{windowTitle}</h2>
      {ordersPopupOpened && <VehicleOrdersPopup row={selectedRow} closePopup={closeOrdersPopup} />}
      <div className={"content-block"} ref={pageRef}>
        <div className={"dx-card responsive-paddings"}>
          <TabPanel
            tabIndex={selectedTabIndex}
            onOptionChanged={(e) => {
              if (e.name === "selectedIndex") {
                setSelectedTabIndex(e.value);
              }
            }}
            defaultSelectedIndex={0}
            itemTitleComponent={(e) => {
              return <span className="tabpanel-item-title">{e.data.title}</span>;
            }}
            swipeEnabled={false}
          >
            <Item title={"Транспорт"}>
              <DataGrid
                ref={datagridRef}
                dataSource={vehiclesDataSource}
                onDataErrorOccurred={onDataErrorOccurred}
                onExporting={onExporting}
                errorRowEnabled={false}
                style={{ margin: "10px 8px 8px 8px" }}
                remoteOperations={{
                  filtering: true,
                  paging: true,
                }}
                loadPanel={{
                  enabled: false,
                }}
                onToolbarPreparing={onToolbarPreparing}
                width={"100%"}
                columnAutoWidth={true}
                allowColumnReordering={true}
                {...vehiclesGridOptions}
              >
                <StateStoring
                  enabled={true}
                  type="localStorage"
                  storageKey="pages.controlCenter.vehiclesGrid"
                />
                <ColumnChooser enabled={true} />
                <Export enabled={true} allowExportSelectedData={true} />
                <Column dataField="id" caption="ID" type="string" />
                <Column dataField="gosNumber" caption="Госномер" type={"string"} />
                <Column
                  dataField={"currentOrderVehicle"}
                  caption={"Текущий наряд"}
                  name={"currentOrder"}
                  calculateFilterExpression={calculatedColumnFilterExpression}
                  cellComponent={(e) => {
                    const value = e?.data?.value;
                    let formatedValue = "";

                    if (value) {
                      const route = value?.route?.shortName || "Нет маршута";
                      let startTime = "Нет начала";

                      if (value?.startTimeUtc) {
                        const djsValue = dayjs.utc(value?.startTimeUtc);
                        if (djsValue.isValid()) {
                          startTime = deltaTime(
                            dayjs(new Date()).hour(0).minute(0).second(0),
                            djsValue.local()
                          );
                        }
                      }

                      let endTime = "Нет окончания";
                      if (value?.endTimeUtc) {
                        const djsValue = dayjs.utc(value?.endTimeUtc);
                        if (djsValue.isValid()) {
                          endTime = deltaTime(
                            dayjs(new Date()).hour(0).minute(0).second(0),
                            djsValue.local()
                          );
                        }
                      }

                      formatedValue = `${route}(${value?.graphicNumber}) [${startTime} - ${endTime}]`;
                    }

                    return <div>{formatedValue}</div>;
                  }}
                />
                <Column
                  dataField={"currentOrderElement"}
                  caption={"Текущий рейс"}
                  name={"currentSubroute"}
                  calculateFilterExpression={calculatedColumnFilterExpression}
                  cellComponent={(e) => {
                    const value = e?.data?.value;
                    let formatedValue = "";

                    if (value) {
                      const route = value?.subroute?.name || "Нет рейса";
                      let startTime = "Нет начала";

                      if (value?.startTimeUtc) {
                        const djsValue = dayjs.utc(value?.startTimeUtc);
                        if (djsValue.isValid()) {
                          startTime = deltaTime(
                            dayjs(new Date()).hour(0).minute(0).second(0),
                            djsValue.local()
                          );
                        }
                      }

                      let endTime = "Нет окончания";
                      if (value?.endTimeUtc) {
                        const djsValue = dayjs.utc(value?.endTimeUtc);
                        if (djsValue.isValid()) {
                          endTime = deltaTime(
                            dayjs(new Date()).hour(0).minute(0).second(0),
                            djsValue.local()
                          );
                        }
                      }

                      formatedValue = `${route} [${startTime} - ${endTime}]`;
                    }

                    return <div>{formatedValue}</div>;
                  }}
                />
                <Column dataField="garageNumber" dataType="string" caption="Номер гаража" />
                <Column dataField="modelId" caption="Модель" visible={false}>
                  <Lookup dataSource={modelsDataStore} valueExpr="id" displayExpr="name" />
                </Column>
                <Column dataField="companyId" caption="Компания" visible={false}>
                  <Lookup dataSource={companiesDataStore} valueExpr="id" displayExpr="name" />
                </Column>
                <Column
                  dataField="deviceCode"
                  dataType={"string"}
                  caption="Терминал"
                  visible={false}
                />
                <Column
                  dataField="operatorName"
                  dataType={"string"}
                  caption="Оператор"
                  visible={false}
                />
                <Column
                  dataField={"phoneNumber"}
                  dataType="string"
                  caption="Телефон"
                  visible={false}
                />
                <Column dataField={"note"} dataType="string" caption="Примечание" visible={false} />
                <Column type="buttons">
                  <Button name="custom-edit" text="Смотреть наряды" onClick={openOrdersPopup} />
                </Column>
              </DataGrid>
            </Item>
          </TabPanel>
        </div>
      </div>
    </React.Fragment>
  );
};

export default connect(
  (state) => ({}),
  (dispatch) => {
    return { localDeauthorize: () => dispatch(deauthorize()) };
  }
)(PageComponent);
