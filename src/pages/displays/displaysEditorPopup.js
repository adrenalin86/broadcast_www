import React, { useState, useEffect, Fragment } from 'react'
import { connect } from "react-redux";

import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import { ScrollView } from "devextreme-react/scroll-view";
import Form, { SimpleItem } from "devextreme-react/form";
import { TextBox, SelectBox, Switch, TagBox, LoadIndicator } from "devextreme-react";
import ValidationSummary from "devextreme-react/validation-summary";
import ValidationEngine from "devextreme/ui/validation_engine";
import Validator, { RequiredRule } from "devextreme-react/validator";

import { set, get, cloneDeep, merge } from "lodash-es";

import {
  getDisplayProtocolsDataStore,
  getDisplayModelsDataStore,
  getTagsToDisplayDataStore,
  WINDOWS,
  getVideoCamerasWithStatusesDataStore,
  getDisplaysDataStore,
} from "../../dataStores";

import { deauthorize } from "../../store/commonSlice";

import {
  ForecastsSettingsComponent,
  U646SettingsComponent,
  SeMapSelectPoint,
  ForecastSourceComponent,
  SmartJson2SettingsComponent,
  WeatherForecastsSettingsComponent, ScheduleSourceComponent,
} from "../../components";

import config from "../../config";
import { OnUnmountHandle } from '../broadcastPrograms/OnUnmountHandle'

const defaultValues = config.defaults.displays;

const windowId = WINDOWS.DISPLAYS.id;

const displayModelsDataStore = getDisplayModelsDataStore({
  windowId: windowId,
});

const displayProtocolsDataStore = getDisplayProtocolsDataStore({
  windowId: windowId,
});

const tagsDataStore = getTagsToDisplayDataStore({
  windowId: windowId,
});

const videoCamerasWithStatusesDataStore = getVideoCamerasWithStatusesDataStore({
  windowId: windowId,
});

const displaysDataStore = getDisplaysDataStore({
  windowId: windowId,
});

const displaysPopupOptions = config.pages.displays.displaysGrid.popupDisplayEditor.dxOptions;

const DisplaysEditorPopup = ({
  displayId,
  closeEditorPopup,
  updateEntry,
  createEntry,
  onDataErrorOccurred,
}) => {
  const [formData, setFormData] = useState({});
  const [changedFormData] = useState({});
  const [isLoadedData, setIsLoadedData] = useState(false);
  const [editModeEnabled, setEditModeEnabled] = useState(false);
  const [showSettingsOnReset, setShowSettingsOnReset] = useState(true);

  const setFormDataValue = (dataField, value, forceMerge = false) => {
    if (forceMerge) {
      /**
       * При forceMerge мы совмещаем данные изменённые редактором, вместо замены
       * это может быть полезно, если редактор оперирует частью сложной структуры и по какой-то
       * причине не удобно обрабатывать только её часть или если значением редактора является объект.
       */
      merge(formData, set(cloneDeep(formData), dataField, value));
      merge(changedFormData, set(cloneDeep(changedFormData), dataField, value));
    } else {
      set(formData, dataField, value);
      set(changedFormData, dataField, value);
    }
  };

  const setFormDataState = (dataField, value, forceMerge = false) => {
    if (forceMerge) {
      setFormData((oldData) => {
        let newData = cloneDeep(oldData);

        merge(newData, set(cloneDeep(newData), dataField, value));

        return newData;
      });

      merge(changedFormData, set(cloneDeep(changedFormData), dataField, value));
    } else {
      setFormData((oldData) => {
        let newData = cloneDeep(oldData);

        set(newData, dataField, value);

        return newData;
      });

      set(changedFormData, dataField, value);
    }
  };

  const onFormItemValueChangedData = ({
    value,
    dataField,
    enabler,
    forceRepaint,
    forceMerge = false,
  }) => {
    if (enabler) {
      setFormDataValue(dataField, value ? get(defaultValues, dataField) : null);
    } else {
      if (forceRepaint) {
        setFormDataState(dataField, value, forceMerge);
      } else {
        setFormDataValue(dataField, value, forceMerge);
      }
    }
  };

  const onFormItemValueChanged = (e) => {
    const value = e.value !== undefined ? e.value : e?.component?.option("text");
    const dataField = e.component.option("dataField");
    const enabler = e.component.option("enabler");
    const repaint = e?.forceRepaint || e.component.option("forceRepaint");
    const forceMerge = e.forceMerge || e.component.option("forceMerge");

    if (enabler) {
      setFormDataState(dataField, value ? get(defaultValues, dataField) : null);
    } else {
      if (repaint) {
        setFormDataState(dataField, value, forceMerge);
      } else {
        setFormDataValue(dataField, value, forceMerge);
      }
    }
  };

  const onDisplayModelChanged = (e) => {
    const displayModelId = e.value !== undefined ? e.value : e?.component?.option("text");
    const modelData = e?.component?._dataSource?._items?.find((item) => item.id === displayModelId);
    const { settings } = modelData;

    onFormItemValueChangedData({
      value: displayModelId,
      dataField: "displayModelId",
    });
    onFormItemValueChangedData({
      value: cloneDeep(settings),
      dataField: "settings",
      forceRepaint: true,
    });

    setShowSettingsOnReset(false)
  };

  const onSave = () => {
    const isValid = ValidationEngine.validateGroup("formEditor").isValid;

    if (isValid) {
      if (!displayId) {
        createEntry(formData);
      } else {
        updateEntry({
          formData: changedFormData,
          editedRowId: formData.id,
        });
      }
    }
  };

  useEffect(() => {
    if (displayId) {
      displaysDataStore
        .byKey(displayId)
        .then((result) => {
          const [display] = result;

          setFormData(display);
          setIsLoadedData(true);
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
        });
    } else {
      setIsLoadedData(true);
    }
  }, []);

  return (
    <>
      <Popup
        className="customEditorPopup"
        visible={true}
        onHiding={closeEditorPopup}
        titleComponent={() => {
          return <span className="popup-title">{displaysPopupOptions.title}</span>;
        }}
        {...displaysPopupOptions}
      >
        <Position my="center" at="center" of={window} />
        {!isLoadedData && (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100%",
            }}
          >
            <LoadIndicator
              id="large-indicator"
              className="button-indicator m-v-65"
              height={60}
              width={60}
              visible={!isLoadedData}
            />
          </div>
        )}
        <ScrollView showScrollbar="always">
          <Form validationGroup="formEditor">
            {isLoadedData && (
              <SimpleItem>
                <div className="editor-content">
                  <div className="dx-form-group-with-caption dx-form-group editor-group">
                    <div className="dx-form-group-caption">Общие сведения</div>
                    <div className="dx-form-group-content editor-content">
                      <div className="editor-item cols-2">
                        <div className="dx-field">
                          <div className="dx-field-label">Название</div>
                          <div className="dx-field-value">
                            <TextBox
                              dataField="name"
                              defaultValue={formData.name}
                              onInput={onFormItemValueChanged}
                            />
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">Адрес</div>
                          <div className="dx-field-value">
                            <TextBox
                              dataField="address"
                              defaultValue={formData.address}
                              onInput={onFormItemValueChanged}
                            />
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">imei устройства</div>
                          <div className="dx-field-value">
                            <TextBox
                              dataField="imei"
                              defaultValue={formData.imei}
                              onInput={onFormItemValueChanged}
                            >
                              <Validator validationGroup="formEditor">
                                <RequiredRule />
                              </Validator>
                            </TextBox>
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">Протокол</div>
                          <div className="dx-field-value">
                            <SelectBox
                              dataField="protocolName"
                              defaultValue={formData.protocolName}
                              dataSource={displayProtocolsDataStore}
                              allowClearing={true}
                              valueExpr="name"
                              displayExpr="name"
                              forceRepaint={true}
                              onValueChanged={onFormItemValueChanged}
                            >
                              <Validator validationGroup="formEditor">
                                <RequiredRule />
                              </Validator>
                            </SelectBox>
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">Модель дисплея</div>
                          <div className="dx-field-value">
                            <SelectBox
                              dataField="displayModelId"
                              defaultValue={formData.displayModelId}
                              dataSource={displayModelsDataStore}
                              valueExpr="id"
                              displayExpr="name"
                              allowClearing={true}
                              searchEnabled={false}
                              onValueChanged={onDisplayModelChanged}
                            >
                              <Validator validationGroup="formEditor">
                                <RequiredRule />
                              </Validator>
                            </SelectBox>
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">Примечание</div>
                          <div className="dx-field-value">
                            <TextBox
                              dataField="note"
                              defaultValue={formData.note}
                              onInput={onFormItemValueChanged}
                            />
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">Тэги</div>
                          <div className="dx-field-value">
                            <TagBox
                              dataField="tagIds"
                              defaultValue={formData.tagIds}
                              dataSource={tagsDataStore}
                              valueExpr="id"
                              displayExpr="name"
                              searchEnabled={true}
                              searchTimeout={2000}
                              showClearButton={true}
                              onValueChanged={onFormItemValueChanged}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="editor-item cols-2">
                        <div className="editor-item-label">Местоположение по карте</div>
                        <SeMapSelectPoint
                          formData={formData}
                          setFormDataValue={setFormDataValue}
                          latDatafield="lat"
                          lngDatafield="lng"
                          defaultLat={defaultValues.lat}
                          defaultLng={defaultValues.lng}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="dx-form-group-with-caption dx-form-group editor-group">
                    <div className="dx-form-group-caption">Видеокамеры</div>
                    <div className="dx-form-group-content editor-content">
                      <div className="editor-item cols-2">
                        <div className="dx-field">
                          <div className="dx-field-label item-label ">Основная видеокамера</div>
                          <div className="dx-field-value ">
                            <SelectBox
                              dataField="mainVideoCameraId"
                              defaultValue={formData.mainVideoCameraId}
                              dataSource={videoCamerasWithStatusesDataStore}
                              valueExpr="id"
                              displayExpr="name"
                              allowClearing={true}
                              searchEnabled={true}
                              showClearButton={true}
                              onValueChanged={onFormItemValueChanged}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="editor-item">
                        <div className="dx-field">
                          <div className="dx-field-label item-label">
                            Дополнительные видеокамеры
                          </div>
                          <div className="dx-field-value">
                            <TagBox
                              dataField="videoCameraIds"
                              defaultValue={formData.videoCameraIds}
                              dataSource={videoCamerasWithStatusesDataStore}
                              searchEnabled={true}
                              valueExpr="id"
                              displayExpr="name"
                              showClearButton={true}
                              onValueChanged={onFormItemValueChanged}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  {
                    showSettingsOnReset && (
                      <Fragment>
                        <OnUnmountHandle
                            onUnmount={() => {
                              setShowSettingsOnReset(true)
                            }}
                        />

                        <WeatherForecastsSettingsComponent
                          className="editor-group cols-2"
                          data={formData.settings ?? {}}
                          onFormItemValueChanged={onFormItemValueChanged}
                          windowId={windowId}
                          editModeEnabled={editModeEnabled}
                          dataField={"settings"}
                          onError={(error) => {
                            onDataErrorOccurred({ error });
                          }}
                        />

                        <ForecastsSettingsComponent
                          className="editor-group cols-2"
                          data={formData.settings ?? {}}
                          onFormItemValueChanged={onFormItemValueChanged}
                          windowId={windowId}
                          editModeEnabled={editModeEnabled}
                          dataField={"settings"}
                          onError={(error) => {
                            onDataErrorOccurred({ error });
                          }}
                        />
                        <ForecastSourceComponent
                          className="editor-group cols-2"
                          data={formData.settings?.forecastSource ?? {}}
                          onFormItemValueChanged={onFormItemValueChanged}
                          windowId={windowId}
                          editModeEnabled={editModeEnabled}
                          dataField={"settings.forecastSource"}
                          onError={(error) => {
                            onDataErrorOccurred({ error });
                          }}
                        />
                        <ScheduleSourceComponent
                          className="editor-group cols-2"
                          data={formData.settings?.scheduleSource ?? {}}
                          onFormItemValueChanged={onFormItemValueChanged}
                          windowId={windowId}
                          editModeEnabled={editModeEnabled}
                          dataField={"settings.scheduleSource"}
                          onError={(error) => {
                            onDataErrorOccurred({ error });
                          }}
                        />
                        {formData.protocolName === "itLineU646" && (
                          <U646SettingsComponent
                            className="editor-group"
                            data={formData.settings?.itLineU646 ?? {}}
                            onFormItemValueChanged={onFormItemValueChanged}
                            onFormItemValueChangedData={onFormItemValueChangedData}
                            editModeEnabled={editModeEnabled}
                            dataField={"settings.itLineU646"}
                            windowId={windowId}
                            onError={(error) => {
                              onDataErrorOccurred({ error });
                            }}
                          />
                        )}
                        {formData.protocolName === "smartJson2" && (
                          <SmartJson2SettingsComponent
                            className="editor-group"
                            data={formData.settings?.smartJson2 ?? {}}
                            onFormItemValueChanged={onFormItemValueChanged}
                            onFormItemValueChangedData={onFormItemValueChangedData}
                            editModeEnabled={editModeEnabled}
                            dataField={"settings.smartJson2"}
                            windowId={windowId}
                            onError={(error) => {
                              onDataErrorOccurred({ error });
                            }}
                          />
                        )}
                      </Fragment>
                    )
                  }
                </div>
              </SimpleItem>
            )}
          </Form>
        </ScrollView>
        <ValidationSummary id="summary" validationGroup="formEditor" />
        {isLoadedData && (
          <ToolbarItem
            widget="dxButton"
            location="after"
            toolbar="bottom"
            options={{ text: "Сохранить", onClick: onSave }}
          />
        )}
        <ToolbarItem
          widget="dxButton"
          location="after"
          toolbar="bottom"
          options={{ text: "Отмена", onClick: closeEditorPopup }}
        />
        <ToolbarItem toolbar="bottom" location="left">
          <Switch
            defaultValue={editModeEnabled}
            switchedOffText="Настройка редактора"
            switchedOnText="Закончить настройку"
            width="8rem"
            onValueChanged={(e) => {
              setEditModeEnabled(e.value);
            }}
          />
        </ToolbarItem>
      </Popup>
    </>
  );
};

export default connect(null, {
  localDeauthorize: deauthorize,
})(DisplaysEditorPopup);
