import React, {useState, useEffect} from "react";
import {Popup, Position, ToolbarItem} from "devextreme-react/popup";
import {ScrollView} from "devextreme-react/scroll-view";
import Form, {SimpleItem} from "devextreme-react/form";
import {cloneDeep, isEmpty} from "lodash-es";
import config from "../../config";
import ColumnForDevices from "./ColumnForDevices";
import {activeIcon} from "../../utils/utils";
import VideoStreamPopup from "../../components/videoCameras/videoStreamPopup/videoStreamPopup";
import DisplaysCard from "../displays/displaysCard";

const ViewPopupSmartStations = ({
                                    editedRow,
                                    closeEditorPopup,
                                    onDataErrorOccurred,
                                    openDeleteRowPopup,
                                    openEditorPopup,
                                    windowId
                                }) => {
    const [formData, setFormData] = useState({});

    const popupOptions = config.pages.smartStations.grid.viewPopup.dxOptions;

    const [videoCameraId, setVideoCameraId] = useState();
    const [videoStreamPopupVisible, setVideoStreamPopupVisible] = useState(false);

    const openVideoStreamPopup = (id) => {
        if (id) {
            setVideoCameraId(id);
            setVideoStreamPopupVisible(true);
        }
    };

    const closeVideoStreamPopup = () => {
        setVideoStreamPopupVisible(false);
    };

    const closePopup = () => {
        closeEditorPopup();
    };

    const onCancel = () => {
        closePopup();
    };

    useEffect(() => {
        if (!isEmpty(editedRow)) {
            setFormData(cloneDeep(editedRow));
        }
    }, []);


    return (
        <React.Fragment>
            {videoStreamPopupVisible && (
                <VideoStreamPopup
                    closeVideoStreamPopup={closeVideoStreamPopup}
                    onDataErrorOccurred={onDataErrorOccurred}
                    videoCameraId={videoCameraId}
                />
            )}
            <Popup
                className="customEditorPopup"
                visible={true}
                onHiding={closePopup}
                titleComponent={() => {
                    return <span className="popup-title">{popupOptions.title}</span>;
                }}
                {...popupOptions}
            >
                <Position my="center" at="center" of={window}/>
                <ScrollView showScrollbar="always">
                    <div className="editor-content">
                        <div className="editor-item">
                            <div className="dx-field">
                                <div className="dx-field-label">Название</div>
                                <div className="dx-see-value">
                                    {formData.name}
                                </div>
                            </div>
                            <div className="dx-field">
                                <div className="dx-field-label">Адрес</div>
                                <div className="dx-see-value">
                                    Адрес
                                </div>
                            </div>
                            <div className="dx-field">
                                <div className="dx-field-label">Модель</div>
                                <div className="dx-see-value">
                                    Модель
                                </div>
                            </div>
                            <div className="dx-field">
                                <div className="dx-field-label">Оборудование</div>
                                <div className="dx-see-value">
                                    {
                                        formData?.deviceIds?.length ? formData?.deviceIds.map(el => {
                                            const active = activeIcon(formData?.devices, el, 'deviceId')
                                            return <div className="dx-see-value">
                                                <span>{formData?.devices?.find(find => find.id === el)?.name}</span>
                                                <ColumnForDevices seeTooltip={false} rowIndex={'1'} el={el}
                                                                  active={active} data={formData?.devices}
                                                                  type="device"/>
                                            </div>
                                        }) : null
                                    }
                                </div>
                            </div>
                            <div className="dx-field">
                                <div className="dx-field-label">Табло</div>
                                <div className="dx-see-value">
                                    {
                                        formData?.displayIds?.length ? formData?.displayIds.map(el => {
                                            const active = activeIcon(formData?.displays, el, 'displayId')
                                            return <div className="dx-see-value">
                                                <span>{formData?.displays?.find(find => find.id === el)?.name}</span><ColumnForDevices
                                                seeTooltip={false}
                                                rowIndex={'2'} el={el}
                                                active={active} data={formData?.displays}
                                                type="display"/>
                                            </div>
                                        }) : null
                                    }
                                </div>
                            </div>
                            <div className="dx-field">
                                <div className="dx-field-label">Камеры</div>
                            </div>
                            <div
                                style={{
                                    display: "flex",
                                    flexWrap: "wrap",
                                    width: "100%",
                                    minWidth: "260px",
                                    overflowY: "auto",
                                    marginTop: "20px",
                                }}
                            >
                                <DisplaysCard
                                    openVideoStreamPopup={openVideoStreamPopup}
                                    openEditorPopup={openEditorPopup}
                                    openDeleteRowPopup={openDeleteRowPopup}
                                    onDataErrorOccurred={onDataErrorOccurred}
                                    mainVideoCameraId={formData?.mainVideoCameraId}
                                    displayId={formData?.mainVideoCameraId}
                                    displayName={formData?.mainVideoCamera?.name}
                                    displayStatus={formData?.mainVideoCamera?.status}
                                    key={formData?.mainVideoCamera?.id}
                                    modeSee
                                />
                                {formData?.videoCameras?.map((camera) => (
                                    <DisplaysCard
                                        openVideoStreamPopup={openVideoStreamPopup}
                                        openEditorPopup={openEditorPopup}
                                        openDeleteRowPopup={openDeleteRowPopup}
                                        onDataErrorOccurred={onDataErrorOccurred}
                                        mainVideoCameraId={camera.mainVideoCameraId}
                                        displayId={camera.id}
                                        displayName={camera.name}
                                        displayStatus={camera.status}
                                        key={camera.id}
                                        modeSee
                                    />
                                ))}
                            </div>
                        </div>
                    </div>
                </ScrollView>
                <ToolbarItem
                    widget="dxButton"
                    location="after"
                    options={{text: "Закрыть", onClick: onCancel}}
                    toolbar="bottom"
                />
            </Popup>
        </React.Fragment>
    );
};

export default ViewPopupSmartStations;
