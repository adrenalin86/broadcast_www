import React, {useEffect, useRef} from "react";

import DataGrid, {Column, Button, Export, Lookup, StateStoring, ColumnChooser} from "devextreme-react/data-grid";
import DataSource from "devextreme/data/data_source";

import config from "../../config";

import {
    getDisplayModelsDataStore,
    getDisplaysWithStatusDataStore,
    WINDOWS,
} from "../../dataStores";

import dayjs from "dayjs";

const windowId = WINDOWS.DISPLAYS.id;

const displaysWithStatusDataStore = getDisplaysWithStatusDataStore({
    windowId: windowId,
});

const displaysWithStatusDataSource = new DataSource({
    store: displaysWithStatusDataStore,
    reshapeOnPush: true,
});

const displayModelsDataStore = getDisplayModelsDataStore({
    windowId: windowId,
});

const AUTOMATIC_UPDATE_TIMEOUT_MS = config.pages.displays.displaysGrid.updateInterval;

const DisplaysTable = ({
                           openEditorPopup,
                           openDeleteRowPopup,
                           openCreateEntryPopup,
                           onDataErrorOccurred,
                           counter,
                       }) => {
    const datagridRef = useRef(null);

    const displaysGridOptions = config.pages.displays.displaysGrid.dxOptions;

    const onToolbarPreparing = (e) => {
        let toolbarItems = e.toolbarOptions.items;

        toolbarItems.unshift({
            widget: "dxButton",
            options: {
                icon: "add",
                text: "Добавить табло",
                onClick: openCreateEntryPopup,
            },
            location: "before",
        });
    };

    useEffect(() => {
        const updateData = () => {
            displaysWithStatusDataStore
                .load()
                .then((result) => {
                    let changes = [];

                    result.data.forEach((item) => {
                        changes.push({
                            type: "update",
                            key: item.id,
                            data: {
                                status: item.status,
                            },
                        });
                    });

                    displaysWithStatusDataStore.push(changes);
                })
                .catch((error) => {
                    onDataErrorOccurred({error});
                });
        };

        const timer = setInterval(() => {
            updateData();
        }, AUTOMATIC_UPDATE_TIMEOUT_MS);

        return () => {
            clearInterval(timer);
        };
    }, []);

    useEffect(() => {
        datagridRef?.current?.instance?.refresh();
    }, [counter]);

    return (
        <>
            <DataGrid
                style={{marginTop: "20px"}}
                ref={datagridRef}
                dataSource={displaysWithStatusDataSource}
                onToolbarPreparing={onToolbarPreparing}
                onDataErrorOccurred={onDataErrorOccurred}
                errorRowEnabled={false}
                {...displaysGridOptions}
            >
                <Export enabled={true} allowExportSelectedData={true}/>
                <StateStoring
                    enabled={true}
                    type="localStorage"
                    storageKey="pages.displays.displaysGrid"
                />
                <ColumnChooser enabled={true}/>
                <Column width={50} dataField="id" caption="id" alignment="left"/>
                <Column
                    width={70}
                    dataField="status.connected"
                    caption="Статус"
                    alignment="center"
                    cellComponent={(e) => {
                        return (
                            <div
                                className="datagrid-display-status"
                                style={{
                                    display: "flex",
                                    flexWrap: "nowrap",
                                    wordBreak: "break-word",
                                    whiteSpace: "normal",
                                    alignItems: "center",
                                    justifyContent: "center",
                                }}
                            >
                                {e.data.data.status?.connected === 1 && (
                                    <i
                                        className="dx-icon-check"
                                        style={{
                                            color: "green",
                                            fontSize: "1.2em",
                                            marginRight: "4px",
                                        }}
                                    />
                                )}
                                {(e.data.data.status?.connected === 0 || !e.data.data.status?.connected) && (
                                    <i
                                        className="dx-icon-close"
                                        style={{
                                            color: "gray",
                                            fontSize: "1.2em",
                                            marginRight: "4px",
                                        }}
                                    />
                                )}
                            </div>
                        );
                    }}
                />
                <Column dataField="name" caption="Название"/>
                <Column dataField="imei" caption="imei устройства"/>
                <Column width={140} dataField="displayModelId" caption="Модель дисплея">
                    <Lookup dataSource={displayModelsDataStore} valueExpr="id" displayExpr="name"/>
                </Column>
                {/* <Column dataField="settings.station_Id" caption="Станция" />
        <Column dataField="tagIds" caption="Тэги" /> */}
                <Column
                    width={185}
                    dataField="status"
                    caption="Последнее подключение"
                    cellComponent={(e) => {
                        const formatDateTime = (value) => {
                            const dateTime = dayjs.utc(value).local();
                            const isBeforeDayAgo = dateTime.isBefore(dayjs.utc().local().subtract(1, "day"));
                            let formatedDateTime = "";

                            if (isBeforeDayAgo) {
                                formatedDateTime = dateTime.format("HH:mm DD.MM.YYYY");
                            } else {
                                const isInPast = dateTime.isBefore(dayjs.utc().local());

                                if (isInPast) {
                                    formatedDateTime = dateTime.fromNow();
                                } else {
                                    formatedDateTime = "только что";
                                }
                            }

                            return formatedDateTime;
                        };

                        return (
                            <div
                                className={"datagrid-display-status"}
                                style={{
                                    display: "flex",
                                    flexWrap: "nowrap",
                                    wordBreak: "break-word",
                                    whiteSpace: "normal",
                                }}
                            >
                <span>{`${
                    e.data.data.status?.lastConnectionTimeUtc
                        ? formatDateTime(e.data.data.status?.lastConnectionTimeUtc)
                        : "нет"
                }`}</span>{" "}
                            </div>
                        );
                    }}
                />
                <Column dataField="note" caption="Примечание"/>
                <Column visible={false} dataField="lat" caption="Широта"/>
                <Column visible={false} dataField="lng" caption="Долгота"/>
                <Column type="buttons">
                    <Button
                        name="custom-edit"
                        text="Изменить"
                        icon="edit"
                        onClick={(e) => openEditorPopup(e.row.key)}
                    />
                    <Button
                        name="custom-delete"
                        text="Удалить"
                        icon="trash"
                        onClick={(e) => openDeleteRowPopup(e.row.key)}
                    />
                </Column>
            </DataGrid>
        </>
    );
};

export default DisplaysTable;
