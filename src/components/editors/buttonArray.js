import React, { useRef, useEffect } from "react";
import { nanoid } from "nanoid";
import { updateBit, getBit } from "../../utils";
import { CheckBox } from "devextreme-react";
import "./buttonArray.scss";

const output = {
  BINARY: 0,
  ARRAY: 1,
  OBJECT: 2,
};

const mode = {
  RADIO: 0,
  CHECK: 1,
};

const apearance = {
  BUNCH: 0,
  GROUP: 1,
};

/**
 *
 *  */

const Component = ({
  buttons,
  defaultValue,
  dataField,
  onFormItemValueChangedData,
}) => {
  const uid = useRef(nanoid());
  const widgetRef = useRef(null);

  const onChange = () => {
    if (widgetRef.current) {
      const nodeList = widgetRef.current.querySelectorAll(`input`);
      const buttonsStatus = [];
      nodeList.forEach((node, index) => {
        buttonsStatus.push({
          value: node.value === "true" ? true : false,
        });
      });

      let result = 0;
      buttonsStatus.forEach((item, index) => {
        result = updateBit(result, index, +item.value);
      });

      onFormItemValueChangedData({ value: result, dataField: dataField });
    }
  };

  return (
    <div className="button-array-widget" ref={widgetRef}>
      {buttons.map((item, index) => {
        return (
          <CheckBox
            name={uid.current}
            onValueChanged={onChange}
            text={item.text}
            key={item.key}
            defaultValue={!!getBit(defaultValue, index)}
            className={"button-array-widget_button"}
          />
        );
      })}
    </div>
  );
};

export default Component;
