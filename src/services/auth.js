import store from "../store";
import {
    setUserData,
    setToken,
    setLoading,
    setRememberUser,
    setBackendUrl,
    setUserRights,
    setRegion,
    deauthorize,
} from "../store/commonSlice";

import {signIn as sendSignInRequest, signOut as sendSignOutRequest} from "../api/auth";

export const signIn = async (user, password, reg, rememberMe, backendUrl) => {
    store.dispatch(setLoading(true));

    const result = await sendSignInRequest(user, password, backendUrl);


    if (result.isOk) {
        store.dispatch(setRememberUser(rememberMe));
        store.dispatch(setBackendUrl(backendUrl));
        store.dispatch(
            setUserData({
                userDescription: result.data.userDescription,
                userId: result.data.userId,
            })
        );
        store.dispatch(
            setUserRights({
                rights: result.data.rights,
                regRights: result.data.regRights,
                windowsRights: result.data.windowsRights,
            })
        );
        store.dispatch(setRegion(reg));
        store.dispatch(setToken(result.data.token));
        store.dispatch(setLoading(false));
        return result;
    } else {
        store.dispatch(setLoading(false));
        return result;
    }
};

export const signOut = async (token, backendUrl) => {
    store.dispatch(setLoading(true));

    sendSignOutRequest(token, backendUrl);

    store.dispatch(deauthorize());

    store.dispatch(setLoading(false));
};
