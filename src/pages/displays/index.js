import React, { useEffect, useState } from "react";
import { connect } from "react-redux";

import notify from "devextreme/ui/notify";
import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import { Tabs } from "devextreme-react";

import { isEmpty } from "lodash-es";

import {
  WINDOWS,
  getDisplaysWithStatusDataStore,
  getSystemWindowsDataStore,
} from "../../dataStores";
import { deauthorize } from "../../store/commonSlice";

import config from "../../config";

import DisplaysEditorPopup from "./displaysEditorPopup";
import DisplaysTable from "./displaysTable";
import DisplaysCards from "./displaysCards";
import DisplaysMap from "./displaysMap";
import VideoStreamPopup from "../../components/videoCameras/videoStreamPopup/videoStreamPopup";

const windowId = WINDOWS.DISPLAYS.id;

const displaysWithStatusDataStore = getDisplaysWithStatusDataStore({
  windowId: windowId,
});

const systemWindowsDataStore = getSystemWindowsDataStore({
  windowId: windowId,
});

const tabs = [
  {
    id: 0,
    text: "Таблица",
  },
  {
    id: 1,
    text: "Видеостена",
  },
  {
    id: 2,
    text: "Карта",
  },
];

const DisplaysPage = ({ localDeauthorize }) => {
  const [editorPopupVisible, setEditorPopupVisible] = useState(false);
  const [videoStreamPopupVisible, setVideoStreamPopupVisible] = useState(false);
  const [deleteRowPopupVisible, setDeleteRowPopupVisible] = useState(false);

  const [isVisible, setIsVisible] = useState({ table: true, cards: false, map: false });
  const [isCardsLoaded, setIsCardsLoaded] = useState(false);
  const [isMapLoaded, setIsMapLoaded] = useState(false);

  const [displayId, setDisplayId] = useState();
  const [videoCameraId, setVideoCameraId] = useState();
  const [counter, setCounter] = useState(0);
  const [windowTitle, setWindowTitle] = useState();

  const onDataErrorOccurred = (e) => {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);

    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  };

  const openEditorPopup = (id) => {
    setDisplayId(id);
    setEditorPopupVisible(true);
  };

  const openVideoStreamPopup = (id) => {
    if (id) {
      setVideoCameraId(id);
      setVideoStreamPopupVisible(true);
    }
  };

  const openCreateEntryPopup = () => {
    setDisplayId(null);
    setEditorPopupVisible(true);
  };

  const openDeleteRowPopup = (id) => {
    setDisplayId(id);
    setDeleteRowPopupVisible(true);
  };

  const closeEditorPopup = () => {
    setEditorPopupVisible(false);
  };

  const closeDeleteRowPopup = () => {
    setDeleteRowPopupVisible(false);
  };

  const closeVideoStreamPopup = () => {
    setVideoStreamPopupVisible(false);
  };

  const refreshDisplays = () => {
    setCounter(counter + 1);
  };

  const createEntry = (formData) => {
    displaysWithStatusDataStore
      .insert(formData)
      .then(() => {
        closeEditorPopup();
        refreshDisplays();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
      });
  };

  const updateEntry = ({ formData, editedRowId }) => {
    if (!isEmpty(formData)) {
      displaysWithStatusDataStore
        .update(editedRowId, formData)
        .then(() => {
          closeEditorPopup();
          refreshDisplays();
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
        });
    } else {
      closeEditorPopup();
    }
  };

  const deleteEntry = () => {
    displaysWithStatusDataStore
      .remove(displayId)
      .then(() => {
        closeDeleteRowPopup();
        refreshDisplays();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
      });
  };

  const clickOnTable = () => {
    setIsVisible({
      table: true,
      cards: false,
      map: false,
    });
  };

  const clickOnCards = () => {
    setIsVisible({
      table: false,
      cards: true,
      map: false,
    });

    setIsCardsLoaded(true);
  };

  const clickOnMap = () => {
    setIsVisible({
      table: false,
      cards: false,
      map: true,
    });

    setIsMapLoaded(true);
  };

  useEffect(() => {
    systemWindowsDataStore.byKey(windowId).then((result) => {
      const windowName = result[0]?.name?.split("/").pop();

      setWindowTitle(windowName);
    });
  }, []);

  return (
    <>
      <h2 className="content-block">{windowTitle}</h2>
      <div className="content-block">
        {editorPopupVisible && (
          <DisplaysEditorPopup
            displayId={displayId}
            closeEditorPopup={closeEditorPopup}
            updateEntry={updateEntry}
            createEntry={createEntry}
            onDataErrorOccurred={onDataErrorOccurred}
          />
        )}
        {deleteRowPopupVisible && (
          <Popup
            className="deleteRowPopup"
            width="auto"
            height="auto"
            visible={true}
            showCloseButton={false}
            showTitle={false}
            onHiding={closeDeleteRowPopup}
          >
            <Position my="center" at="center" of={window} />
            <div>
              <span>Вы уверены, что хотите удалить эту запись?</span>
            </div>
            <ToolbarItem
              widget="dxButton"
              location="center"
              toolbar="bottom"
              options={{ text: "Да", onClick: deleteEntry }}
            />
            <ToolbarItem
              widget="dxButton"
              location="center"
              toolbar="bottom"
              options={{ text: "Нет", onClick: closeDeleteRowPopup }}
            />
          </Popup>
        )}
        {videoStreamPopupVisible && (
          <VideoStreamPopup
            closeVideoStreamPopup={closeVideoStreamPopup}
            onDataErrorOccurred={onDataErrorOccurred}
            videoCameraId={videoCameraId}
          />
        )}
        <div className="dx-card responsive-paddings">
          <Tabs
            dataSource={tabs}
            defaultSelectedIndex={0}
            width={374}
            onItemClick={(e) => {
              if (e.itemIndex === 0) {
                clickOnTable();
              } else if (e.itemIndex === 1) {
                clickOnCards();
              } else {
                clickOnMap();
              }
            }}
          />
          <div className={!isVisible.table ? "hidden" : "visible"}>
            <DisplaysTable
              openEditorPopup={openEditorPopup}
              openDeleteRowPopup={openDeleteRowPopup}
              openCreateEntryPopup={openCreateEntryPopup}
              onDataErrorOccurred={onDataErrorOccurred}
              counter={counter}
            />
          </div>
          {isCardsLoaded && (
            <div className={!isVisible.cards ? "hidden" : "visible"}>
              <DisplaysCards
                openEditorPopup={openEditorPopup}
                openDeleteRowPopup={openDeleteRowPopup}
                openVideoStreamPopup={openVideoStreamPopup}
                onDataErrorOccurred={onDataErrorOccurred}
                counter={counter}
              />
            </div>
          )}
          {isMapLoaded && (
            <div className={!isVisible.map ? "hidden" : "visible"}>
              <DisplaysMap onDataErrorOccurred={onDataErrorOccurred} counter={counter} />
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default connect(
  (state) => ({}),
  (dispatch) => {
    return { localDeauthorize: () => dispatch(deauthorize()) };
  }
)(DisplaysPage);
