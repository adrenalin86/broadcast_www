import React, { useState, useRef, useEffect, useCallback } from "react";
import { connect } from "react-redux";

import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import Form, { SimpleItem } from "devextreme-react/form";
import { TextBox } from "devextreme-react/text-box";
import { TabPanel, Item } from "devextreme-react/tab-panel";
import { DataGrid, Column, Lookup, Scrolling } from "devextreme-react/data-grid";
import { Button } from "devextreme-react/button";
import { ScrollView } from "devextreme-react/scroll-view";
import DataSource from "devextreme/data/data_source";
import notify from "devextreme/ui/notify";

import ValidationSummary from "devextreme-react/validation-summary";
import ValidationEngine from "devextreme/ui/validation_engine";
import Validator, { RequiredRule } from "devextreme-react/validator";

import { set, isEmpty, differenceWith, uniqWith, cloneDeep } from "lodash-es";

import { useScreenSize } from "../../utils";

import { deauthorize } from "../../store/commonSlice";

import {
  getGroupDisplayRelationDataStore,
  getDisplayModelsDataStore,
  getDisplaysDataStore,
  WINDOWS,
} from "../../dataStores";

import config from "../../config";
import { SeMapAreaSelect } from "../../components/editors";
const windowId = WINDOWS.DISPLAY_GROUPS.id;

const defaultValues = config.defaults.displayGroups;

const displayModelsDataStore = getDisplayModelsDataStore({
  windowId: windowId,
});

const DisplayGroupsEditorPopup = ({ localDeauthorize, editedRow, closeEditorPopup, updateEntry, createEntry }) => {
  const [formData, setFormData] = useState({});
  const [changedFormData] = useState({});
  const [groupDisplayRelationDataStoreActions] = useState({
    insert: [],
    delete: [],
  });
  const [groupDisplayRelationsArray, setGroupDisplayRelationsArray] = useState([]);
  const [displaysArray, setDisplaysArray] = useState([]);
  const [selectedTabIndex, setSelectedTabIndex] = useState(0);
  const [selectedMarkersIds, setSelectedMarkersIds] = useState([]);

  const relationsGridRef = useRef(null);
  const sourceGridRef = useRef(null);

  const { isSmall, isXSmall } = useScreenSize();

  const groupDisplayRelationDataSource = new DataSource({
    store: groupDisplayRelationsArray,
    reshapeOnPush: true,
  });

  const displaysDataSource = new DataSource({
    store: displaysArray,
    reshapeOnPush: true,
    postProcess: (displays) => {
      //Для отдачи только не связанных табло производиться их фильтрация перед возвартом из datasource.
      const diff = differenceWith(displays, groupDisplayRelationDataSource.items(), (itemA, itemB) => {
        return itemA.id === itemB.entityId;
      });
      return diff;
    },
    onLoadError: (error) => {
      onDataErrorOccurred({ error });
    },
  });

  const relationsFilterValue = ["groupId", "=", [isEmpty(editedRow) ? "new" : editedRow.id]];

  const displayGroupsPageOptions = config.pages.displayGroups.groupsGrid.groupsEditorPopup;
  const displayGroupsEditorPopupOptions = displayGroupsPageOptions.dxOptions;
  const relationsGridOptions = displayGroupsPageOptions.groupItemsEditor.groupDisplaysGrid.dxOptions;
  const displaySourcesGridOptions = displayGroupsPageOptions.groupItemsEditor.displaySources.displaysGrid.dxOptions;

  /**
   * Инициализация хранилища табло. Производиться здесь, что-бы при каждом обновлении использующих
   * его компонентов не происходило повторных запросов.
   */
  const initDisplaysDataStore = useCallback(async () => {
    let response = [];

    if (!isEmpty(editedRow)) {
      const store = getDisplaysDataStore({
        windowId: windowId,
      });

      response = await store
        .load()
        .then((data) => {
          return data.data;
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
        });
    }

    setDisplaysArray(response);
  });

  /**
   * Инициализация хранилища связей табло и групп. Производиться здесь, что-бы при каждом обновлении использующих
   * его компонентов не происходило повторных запросов.
   */
  const initGroupDisplayRelationDataStore = useCallback(async () => {
    let response = [];

    if (!isEmpty(editedRow)) {
      const store = getGroupDisplayRelationDataStore({
        windowId: windowId,
      });

      response = await store
        .load()
        .then((data) => {
          return data.data;
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
        });
    }

    setGroupDisplayRelationsArray(response);
  });

  /**
   * Инициализация редактора.
   */
  useEffect(() => {
    if (isEmpty(editedRow)) {
      setFormData({});
    } else {
      setFormData(cloneDeep(editedRow));
    }

    initDisplaysDataStore();
    initGroupDisplayRelationDataStore();
  }, []);

  function onDataErrorOccurred(e) {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);
    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  }

  const closePopup = () => {
    closeEditorPopup();
  };

  const onSave = () => {
    const isValid = ValidationEngine.validateGroup("userFormEditor").isValid;

    if (isValid) {
      if (isEmpty(editedRow)) {
        createEntry({
          formData: formData,
          groupDisplayRelationDataStoreActions: groupDisplayRelationDataStoreActions,
        });
      } else {
        updateEntry({
          formData: changedFormData,
          editedRowId: editedRow.id,
          groupDisplayRelationDataStoreActions: groupDisplayRelationDataStoreActions,
        });
      }
    }
  };

  const onCancel = () => {
    closePopup();
  };

  const onSelectionChanged = (e) => {
    //Этот код позволяет выбирать множество записей по клику на строку всегда, а не только когда уже начат выбор множества записей
    if (
      e.selectedRowKeys.length === 1 &&
      e.currentSelectedRowKeys.length === 1 &&
      e.selectedRowKeys[0] === e.currentSelectedRowKeys[0]
    ) {
      e.component.selectRows(e.currentSelectedRowKeys.concat(e.currentDeselectedRowKeys));
    }
  };

  const onMarkersSelected = (e) => {
    setSelectedMarkersIds(() => {
      return e.selectedMarkersIds;
    });
  };

  const onFormItemValueChanged = (e) => {
    const value = e.value !== undefined ? e.value : e?.component?.option("text");
    const dataField = e.component.option("dataField");
    setFormDataValue(dataField, value);
  };

  const setFormDataValue = (dataField, value) => {
    set(formData, dataField, value);
    set(changedFormData, dataField, value);
  };

  const onClickPushToRelationsGrid = () => {
    let selectedItems = [];
    //В зависимости от того с каким редактором, какая вкладка tanpanel выбрана, происходит работа
    //выбирается способ получения данных из него.
    if (selectedTabIndex === 0) {
      selectedItems = sourceGridRef.current.instance.getSelectedRowsData().map((item) => {
        //Форматирование данных в удобный формат.
        return {
          entityId: item.id,
          groupId: isEmpty(editedRow) ? "new" : editedRow.id,
        };
      });
    } else if (selectedTabIndex === 1) {
      selectedItems = selectedMarkersIds.map((item) => {
        //Форматирование данных в удобный формат.
        return {
          entityId: item,
          groupId: isEmpty(editedRow) ? "new" : editedRow.id,
        };
      });
    }

    //Избегаем лишней перерисовки.
    if (selectedMarkersIds.length !== 0) {
      setSelectedMarkersIds(() => {
        return [];
      });
    }
    sourceGridRef.current.instance.deselectAll();

    if (selectedItems.length === 0) {
      return;
    }

    pushSelectedToRelationsGrid(selectedItems);
  };

  const pushSelectedToRelationsGrid = (selectedItems) => {
    setGroupDisplayRelationsArray((oldValue) => {
      //Защита от возможных дублей.
      const newValue = oldValue.concat(selectedItems);
      const uniq = uniqWith(newValue, (itemA, itemB) => {
        return itemA.entityId === itemB.entityId && itemA.groupId === itemB.groupId;
      });
      return uniq;
    });

    //добавляем действия на запись в datastore связей group display
    for (let i = 0; i < selectedItems.length; i++) {
      //Проверяем что нет действия на удаление такой-же записи
      const includes = groupDisplayRelationDataStoreActions.delete.some(
        (item) => item.entityId === selectedItems[i].entityId
      );

      if (includes) {
        //если есть удаляем действие удаления записи, что-бы не создавать лишних запросов в последствии
        groupDisplayRelationDataStoreActions.delete = groupDisplayRelationDataStoreActions.delete.filter(
          (item) => item.entityId !== selectedItems[i].entityId
        );
      } else {
        groupDisplayRelationDataStoreActions.insert.push(selectedItems[i]);
      }
    }
  };

  const onClickRemoveSelectedFromRelationsGrid = () => {
    let selectedItems = relationsGridRef.current.instance.getSelectedRowsData();

    if (selectedItems.length === 0) {
      return;
    }

    relationsGridRef.current.instance.deselectAll();

    removeSelectedFromRelationsGrid(selectedItems);
  };

  const removeSelectedFromRelationsGrid = (selectedItems) => {
    setGroupDisplayRelationsArray((oldValue) => {
      const diff = differenceWith(oldValue, selectedItems, (itemA, itemB) => {
        return itemA.entityId === itemB.entityId;
      });
      return diff;
    });

    //добавляем действия на удаление в datastore связей group display
    for (let i = 0; i < selectedItems.length; i++) {
      //Проверяем что нет действия на удаление такой-же записи
      const includes = groupDisplayRelationDataStoreActions.insert.some(
        (item) => item.entityId === selectedItems[i].entityId
      );

      if (includes) {
        //если есть удаляем действие удаления записи, что-бы не создавать лишних запросов в последствии
        groupDisplayRelationDataStoreActions.insert = groupDisplayRelationDataStoreActions.insert.filter((item) => {
          return item.entityId !== selectedItems[i].entityId;
        });
      } else {
        //иначе просто добавляем действие на запись в список действий
        groupDisplayRelationDataStoreActions.delete.push(selectedItems[i]);
      }
    }
  };

  return (
    <React.Fragment>
      <Popup
        className="customEditorPopup displayGroupsEditor"
        visible={true}
        onHiding={closePopup}
        titleComponent={() => {
          return <span className="popup-title">{displayGroupsEditorPopupOptions.title}</span>;
        }}
        {...displayGroupsEditorPopupOptions}
      >
        <Position my="center" at="center" of={window} />
        <ScrollView showScrollbar="always">
          <Form validationGroup="formEditor">
            <SimpleItem>
              <div className="editor-content">
                <div className="dx-form-group-with-caption dx-form-group editor-group">
                  <div className="dx-form-group-caption">Общие сведения</div>
                  <div className="dx-form-group-content editor-content">
                    <div className="editor-item">
                      <div className="dx-field">
                        <div className="dx-field-label">Название</div>
                        <div className="dx-field-value">
                          <TextBox defaultValue={formData.name} onInput={onFormItemValueChanged} dataField="name">
                            <Validator validationGroup="displayGroupFormEditor">
                              <RequiredRule />
                            </Validator>
                          </TextBox>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  className="dx-form-group-with-caption dx-form-group editor-group group-composition-configuration"
                  disabled={isEmpty(editedRow)}
                >
                  {isEmpty(editedRow) && (
                    <div className="group-composition-overlay">
                        <div className="group-composition-overlay-message">{"Будет доступно после создания"}</div>
                    </div>
                  )}
                  <div className="dx-form-group-caption">Настройка состава группы</div>
                  <div className="dx-form-group-content editor-content displaygroup-content-editor">
                    <div className="editor-item datagrid-item displaygroup-item tabpanel-item">
                      <TabPanel
                        tabIndex={selectedTabIndex}
                        onOptionChanged={(e) => {
                          if (e.name === "selectedIndex") {
                            setSelectedTabIndex(e.value);
                          }
                        }}
                        defaultSelectedIndex={0}
                        itemTitleComponent={(e) => {
                          return <span className="tabpanel-item-title">{e.data.title}</span>;
                        }}
                        swipeEnabled={false}
                        disabled={isEmpty(editedRow)}
                      >
                        <Item title="Список всех табло" disabled={isEmpty(editedRow)}>
                          <div className="dx-field">
                            <DataGrid
                              dataSource={displaysDataSource}
                              onDataErrorOccurred={onDataErrorOccurred}
                              onSelectionChanged={onSelectionChanged}
                              ref={sourceGridRef}
                              disabled={isEmpty(editedRow)}
                              {...displaySourcesGridOptions}
                            >
                              <Scrolling mode="standart" />
                              <Column dataField={"id"} visible={false} dataType="number" />
                              <Column dataField={"imei"} caption="imei" />
                              <Column dataField={"name"} caption="Название" />
                              <Column dataField={"address"} caption="Адрес" />
                            </DataGrid>
                          </div>
                        </Item>
                        <Item title="Карта всех табло" disabled={isEmpty(editedRow)}>
                          <div className="dx-field">
                            <SeMapAreaSelect
                              disabled={isEmpty(editedRow)}
                              startingCenter={{
                                lat: defaultValues.lat,
                                lng: defaultValues.lng,
                              }}
                              dataSource={displaysDataSource}
                              selectedMarkersIds={selectedMarkersIds}
                              onMarkersSelected={onMarkersSelected}
                              onError={(error) => {
                                onDataErrorOccurred({ error });
                              }}
                            />
                          </div>
                        </Item>
                      </TabPanel>
                    </div>
                    <div className="editor-item displaygroup-controls">
                      <Button
                        icon={isSmall || isXSmall ? "arrowup" : "arrowleft"}
                        onClick={onClickRemoveSelectedFromRelationsGrid}
                        disabled={isEmpty(editedRow)}
                      />
                      <Button
                        icon={isSmall || isXSmall ? "arrowdown" : "arrowright"}
                        onClick={onClickPushToRelationsGrid}
                        disabled={isEmpty(editedRow)}
                      />
                    </div>
                    <div className="editor-item datagrid-item displaygroup-item">
                      <div className="dx-field">
                        <div className="dx-field-label item-label">В группе</div>
                        <div className="dx-field-value">
                          <DataGrid
                            dataSource={groupDisplayRelationDataSource}
                            filterValue={relationsFilterValue}
                            onDataErrorOccurred={onDataErrorOccurred}
                            onSelectionChanged={onSelectionChanged}
                            ref={relationsGridRef}
                            disabled={isEmpty(editedRow)}
                            {...relationsGridOptions}
                          >
                            <Scrolling mode="standart" />
                            <Column dataField="groupId" visible={false} dataType="number" />
                            <Column dataField={"entityId"} name="imei" caption="imei" dataType="number">
                              <Lookup dataSource={displaysArray} valueExpr="id" displayExpr="imei" />
                            </Column>
                            <Column dataField={"entityId"} name="name" caption="Название" dataType="number">
                              <Lookup dataSource={displaysArray} valueExpr="id" displayExpr="name" />
                            </Column>
                            <Column dataField={"entityId"} name="address" caption="Адрес" dataType="number">
                              <Lookup dataSource={displaysArray} valueExpr="id" displayExpr="address" />
                            </Column>
                          </DataGrid>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </SimpleItem>
          </Form>
        </ScrollView>
        <ValidationSummary id="summary" validationGroup="userFormEditor"></ValidationSummary>
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Сохранить", onClick: onSave }}
          toolbar="bottom"
        />
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Отмена", onClick: onCancel }}
          toolbar="bottom"
        />
      </Popup>
    </React.Fragment>
  );
};

export default connect(
  (state) => ({}),
  (dispatch) => {
    return {
      localDeauthorize: () => dispatch(deauthorize()),
    };
  }
)(DisplayGroupsEditorPopup);
