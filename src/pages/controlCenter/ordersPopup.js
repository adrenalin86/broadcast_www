import React, { useState, useEffect, useRef } from "react";

import DataGrid, { Column } from "devextreme-react/data-grid";
import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import { ScrollView } from "devextreme-react/scroll-view";
import LoadPanel from "devextreme-react/load-panel";

import notify from "devextreme/ui/notify";

import { WINDOWS, getVehicleOrdersDataStore } from "../../dataStores";

import config from "../../config";
import { connect } from "react-redux";
import { deauthorize } from "../../store/commonSlice";
import dayjs from "dayjs";
import dayjs_utc from "dayjs/plugin/utc";
import dayjs_isToday from "dayjs/plugin/isToday";
import deltaTime from "../../utils/deltatime";
import timezone from "dayjs/plugin/timezone";

dayjs.extend(dayjs_utc);
dayjs.extend(dayjs_isToday);
dayjs.extend(timezone);

const windowId = WINDOWS.CONTROL.id;

const popupOptions =
  config.pages.controlCenter.vehiclesGrid.ordersPopup.dxOptions;

const vehicleOrdersDataStore = getVehicleOrdersDataStore({ windowId });

const PopupComponent = ({ row, closePopup, localDeauthorize }) => {
  const [orderVehiclesArray, setOrderVehiclesArray] = useState([]);
  const [orderElementsArray, setOrderElementsArray] = useState([]);
  const [orderVehiclesGridInitialized, setOrderVehiclesGridInitialized] =
    useState(false);
  const [orderElementsGridInitalized, setOrderElementsGridInitialized] =
    useState(false);
  const orderVehiclesGridRef = useRef(null);
  const orderElemenentsGridRef = useRef(null);
  const [loadPanelVisible, setLoadPanelVisible] = useState(false);
  const gridsContainerRef = useRef(null);

  useEffect(() => {
    if (row && orderVehiclesGridInitialized && orderElementsGridInitalized) {
      setLoadPanelVisible(true);
      vehicleOrdersDataStore
        .load({
          vehicleId: row.vehicleId,
          date: row.date,
        })
        .then((response) => {
          if (
            !orderElemenentsGridRef.current &&
            !orderVehiclesGridRef.current
          ) {
            return null;
          }
          if (response?.data?.orderVehicles) {
            setOrderVehiclesArray((oldData) => response?.data?.orderVehicles);
            orderVehiclesGridRef.current.instance.refresh();
          }
          if (response?.data?.orderElements) {
            setOrderElementsArray((oldData) => response?.data?.orderElements);
            orderElemenentsGridRef.current.instance.refresh();
          }
          setLoadPanelVisible(false);
        })
        .catch((error) => {
          setLoadPanelVisible(false);

          console.log(error);
        });
    } else {
      setOrderVehiclesArray((oldData) => []);
      setOrderElementsArray((oldData) => []);
      orderVehiclesGridRef.current?.instance?.refresh();
      orderElemenentsGridRef.current?.instance?.refresh();
    }
  }, [row, orderVehiclesGridInitialized, orderVehiclesGridInitialized]);

  function onDataErrorOccurred(e) {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);
    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  }

  const onCancel = () => {
    closePopup();
  };

  return (
    <React.Fragment>
      <Popup
        className="customEditorPopup"
        visible={true}
        onHiding={closePopup}
        titleComponent={() => {
          return (
            <span className="popup-title">
              {popupOptions.title + " " + row?.gosNumber + " на " + row?.date}
            </span>
          );
        }}
        {...popupOptions}
      >
        <Position my="center" at="center" of={window} />
        <ScrollView showScrollbar="always" ref={gridsContainerRef}>
          <LoadPanel visible={loadPanelVisible} />
          <fieldset>
            <legend>Список нарядов</legend>
            <DataGrid
              ref={orderVehiclesGridRef}
              dataSource={orderVehiclesArray}
              onDataErrorOccurred={onDataErrorOccurred}
              errorRowEnabled={false}
              onInitialized={() => {
                setOrderVehiclesGridInitialized((oldData) => true);
              }}
              columnAutoWidth={true}
              onContentReady={(e) => {
                //Для каждой колонки форсированно задаём порядок отрисовки в таблице.
                e.component.state().columns.forEach((col, index) => {
                  e.component.columnOption(col.name, "visibleIndex", index);
                });
              }}
              loadPanel={{
                enabled: false,
              }}
            >
              <Column
                dataField={"route.shortName"}
                type="string"
                caption="Маршрут"
              />
              <Column
                dataField={"graphicNumber"}
                type="number"
                caption="График"
              />
              <Column
                dataField={"startTimeUtc"}
                name="startTime"
                caption="Начало"
                cellComponent={(e) => {
                  const value = e?.data?.value;

                  let formatedValue = "Не установлено";

                  if (value) {
                    const djsValue = dayjs.utc(value);
                    if (djsValue.isValid()) {
                      formatedValue  = deltaTime(
                        dayjs(row.date).hour(0).minute(0).second(0),
                        djsValue.local()
                      );
                    }
                  }
                  return <div>{formatedValue}</div>;
                }}
              />
              <Column
                dataField={"endTimeUtc"}
                name="endTime"
                caption={"Окончание"}
                cellComponent={(e) => {
                  const value = e?.data?.value;

                  let formatedValue = "Не установлено";

                  if (value) {
                    const djsValue = dayjs.utc(value);
                    if (djsValue.isValid()) {
                      formatedValue = deltaTime(
                        dayjs(row.date).hour(0).minute(0).second(0),
                        djsValue.local()
                      );
                    }
                  }
                  return <div>{formatedValue}</div>;
                }}
              />
            </DataGrid>
          </fieldset>
          <fieldset>
            <legend>Список рейсов</legend>
            <DataGrid
              ref={orderElemenentsGridRef}
              dataSource={orderElementsArray}
              onDataErrorOccurred={onDataErrorOccurred}
              errorRowEnabled={false}
              columnAutoWidth={true}
              onInitialized={() => {
                setOrderElementsGridInitialized((oldData) => true);
              }}
              onContentReady={(e) => {
                //Для каждой колонки форсированно задаём порядок отрисовки в таблице.
                e.component.state().columns.forEach((col, index) => {
                  e.component.columnOption(col.name, "visibleIndex", index);
                });
              }}
              loadPanel={{
                enabled: false,
              }}
            >
              <Column dataField={"ord"} type="number" caption="Номер рейса" />
              <Column
                dataField={"route.shortName"}
                type="string"
                caption="Маршрут"
              />
              <Column
                dataField={"subroute.name"}
                type="string"
                caption="Подмаршрут"
              />
              <Column
                dataField={"startTimeUtc"}
                name="startTime"
                caption="Начало"
                cellComponent={(e) => {
                  const value = e?.data?.value;

                  let formatedValue = "Не установлено";

                  if (value) {
                    const djsValue = dayjs.utc(value);
                    if (djsValue.isValid()) {
                      formatedValue = deltaTime(
                        dayjs(row.date).hour(0).minute(0).second(0),
                        djsValue.local()
                      );
                    }
                  }
                  return <div>{formatedValue}</div>;
                }}
              />
              <Column
                dataField={"endTimeUtc"}
                name="endTime"
                caption={"Окончание"}
                cellComponent={(e) => {
                  const value = e?.data?.value;

                  let formatedValue = "Не установлено";

                  if (value) {
                    const djsValue = dayjs.utc(value);
                    if (djsValue.isValid()) {
                      formatedValue = deltaTime(
                        dayjs(row.date).hour(0).minute(0).second(0),
                        djsValue.local()
                      );
                    }
                  }
                  return <div>{formatedValue}</div>;
                }}
              />
            </DataGrid>
          </fieldset>
        </ScrollView>
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Закрыть", onClick: onCancel }}
          toolbar="bottom"
        />
      </Popup>
    </React.Fragment>
  );
};

export default connect(null, { localDeauthorize: deauthorize })(PopupComponent);
