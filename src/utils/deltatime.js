import dayjs from "dayjs";
import dayjs_utc from "dayjs/plugin/utc";
dayjs.extend(dayjs_utc);

/**
 *
 * @param {Date} baseDate dayjs
 * @param {Date} pointDate dayks
 * @param {string} format
 * @returns String
 */
function deltaTime(baseDate, pointDate) {
  const ddate = pointDate.diff(baseDate);
  const ddays = pointDate.diff(baseDate, "day");
  return "" + (ddays !== 0 ? ddays + "." : "") + dayjs.utc(ddate).format("HH:mm");
}

export default deltaTime;
