import { SeNumberBox, SeComplexImageEditor } from "../editors";

import { isEmpty, noop } from "lodash-es";

const U653SettingsComponent = ({
  data,
  onFormItemValueChanged,
  onFormItemValueChangedData,
  className,
  editModeEnabled = false,
  dataField,
  onError = noop
}) => {
  const onValueChanged = (e) => {
    const eventDataField = e.component.option("dataField");
    const fullDataField = isEmpty(dataField)
      ? eventDataField
      : dataField?.endsWith(".")
      ? dataField + eventDataField
      : dataField + "." + eventDataField;

    e.component.option("dataField", fullDataField);
    onFormItemValueChanged(e);
  };

  const onValueChangedData = (data) => {
    const eventDataField = data.dataField;
    const fullDataField = isEmpty(dataField)
      ? eventDataField
      : dataField?.endsWith(".")
      ? dataField + eventDataField
      : dataField + "." + eventDataField;
    onFormItemValueChangedData({
      value: data.value,
      dataField: fullDataField,
      enabler: data.enabler,
    });
  };

  return (
    <div className={"dx-form-group-with-caption dx-form-group " + className}>
      <div className="dx-form-group-caption">Настройки протокола U653</div>
      <div className="dx-form-group-content">
        <div className="editor-item">
          <SeComplexImageEditor
            data={data}
            onFormItemValueChanged={onValueChangedData}
            editModeEnabled={editModeEnabled}
            dataField={"image"}
            title="Изображение"
          />
          <SeNumberBox
            data={data}
            title={"Номер режима с прогнозами"}
            onFormItemValueChanged={onValueChanged}
            editModeEnabled={editModeEnabled}
            dataField="forecastsModeNumber"
          />
          <SeNumberBox
            data={data}
            title={"Номер режима с картинкой"}
            onFormItemValueChanged={onValueChanged}
            editModeEnabled={editModeEnabled}
            dataField="pictureModeNumber"
          />
          <SeNumberBox
            data={data}
            title={"Номер первой ячейки прогноза"}
            onFormItemValueChanged={onValueChanged}
            editModeEnabled={editModeEnabled}
            dataField="rssStartIndex"
          />
          <SeNumberBox
            data={data}
            title={"Номер первой ячейки с иконкой"}
            onFormItemValueChanged={onValueChanged}
            editModeEnabled={editModeEnabled}
            dataField="rnStartIndex"
          />
          <SeNumberBox
            data={data}
            title={"Номер иконки для автобуса"}
            onFormItemValueChanged={onValueChanged}
            editModeEnabled={editModeEnabled}
            dataField="busIcoType"
          />
          <SeNumberBox
            data={data}
            title={"Номер иконки для маршрутки"}
            onFormItemValueChanged={onValueChanged}
            editModeEnabled={editModeEnabled}
            dataField="marshIcoType"
          />
          <SeNumberBox
            data={data}
            title={"Номер иконки для троллейбуса"}
            onFormItemValueChanged={onValueChanged}
            editModeEnabled={editModeEnabled}
            dataField="trollIcoType"
          />
          <SeNumberBox
            data={data}
            title={"Номер иконки для трамвая"}
            onFormItemValueChanged={onValueChanged}
            editModeEnabled={editModeEnabled}
            dataField="tramIcoType"
          />
        </div>
      </div>
    </div>
  );
};

export default U653SettingsComponent;
