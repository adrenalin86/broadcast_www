import { SeLookup, SeCheckBox, SeNumberBox } from "../editors";

import { getGenericEntityDataStore as getForecastsSourceStationIdDataStore, GENERIC_ENTITIES } from "../../dataStores";

import { isEmpty, noop } from "lodash-es";

const Component = ({
  windowId,
  data,
  onFormItemValueChanged,
  className,
  editModeEnabled = false,
  dataField,
  onError = noop,
}) => {
  const forecastsSourceStationIdDataStore = getForecastsSourceStationIdDataStore({
    windowId: windowId,
    entityName: GENERIC_ENTITIES.STATIONS,
  });

  const onValueChanged = (e) => {
    const eventDataField = e.component.option("dataField");
    const fullDataField = isEmpty(dataField)
      ? eventDataField
      : dataField?.endsWith(".")
      ? dataField + eventDataField
      : dataField + "." + eventDataField;

    e.component.option("dataField", fullDataField);
    onFormItemValueChanged(e);
  };

  return (
    <div className={"dx-form-group-with-caption dx-form-group " + className}>
      <div className="dx-form-group-caption">Настройки источника прогнозов</div>
      <div className="dx-form-group-content">
        <div className="editor-item">
          <SeCheckBox
            data={data}
            onFormItemValueChanged={onValueChanged}
            editModeEnabled={editModeEnabled}
            dataField={"turnOffForecasts"}
            title={"Выключить прогнозы"}
          />
          <SeCheckBox
            data={data}
            onFormItemValueChanged={onValueChanged}
            editModeEnabled={editModeEnabled}
            dataField={"sortForesByRoute"}
            title={"Сортировка по маршруту"}
          />
          <SeCheckBox
            data={data}
            onFormItemValueChanged={onValueChanged}
            editModeEnabled={editModeEnabled}
            dataField={"showAtEndStation"}
            title={"Показывать транспорт на конечных"}
          />
          <SeNumberBox
            data={data}
            onFormItemValueChanged={onValueChanged}
            editModeEnabled={editModeEnabled}
            dataField={"arrtimeThreshold"}
            title={"Порог отображения прогнозов, сек."}
          />
          <SeLookup
            data={data}
            onFormItemValueChanged={onValueChanged}
            editModeEnabled={editModeEnabled}
            dataField={"forecastsSourceStationId"}
            title={"Привязка к остановке"}
            dataSource={{
              store: forecastsSourceStationIdDataStore,
              onLoadError: (error) => {
                onError(error);
              },
            }}
            valueExpr={"id"}
            displayExpr={(e) => {
              return `${e?.name || ""} ${e?.description ? ", " + e.description : ""}`;
            }}
            searchExpr={["name"]}
            widgetOptions={{ allowClearing: true }}
          />
        </div>
      </div>
    </div>
  );
};

export default Component;
