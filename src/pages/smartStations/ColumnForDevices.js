import React, {memo, useMemo} from 'react';
import ReactDOMServer from "react-dom/server";
import {formatDateTime} from "../../utils/date";
import ReactTooltip from "react-tooltip";

function ColumnForDevices({el, active, data, openVideoStreamPopup, type = 'camera', seeTooltip = true}) {

    const handleClickDevice = () => {
        if (type === 'camera')
            openVideoStreamPopup(el)
    }

    const Icon = useMemo(() => {
        return <><img data-html={seeTooltip} data-tip={seeTooltip ?  ReactDOMServer.renderToString(<div>
            <div>Название: <span>{Array.isArray(data) ? data?.find(_ => _.id === el)?.name : data?.name}</span></div>
            <div>Последнее
                подключение: <span>{Array.isArray(data) ? data?.find(_ => _.id === el)?.status?.lastConnectionTimeUtc ? formatDateTime(data?.find(_ => _.id === el)?.status?.lastConnectionTimeUtc)
                    : "нет" : data?.status?.lastConnectionTimeUtc ? formatDateTime(data?.status?.lastConnectionTimeUtc) : "нет"}</span>
            </div>
        </div>) : null} onClick={handleClickDevice} style={{cursor: type === 'camera' && 'pointer'}} key={el} className="smartStations-icon"
                    src={active === 2 ? `ico/smartStations/${type}.svg` :
                        active === 1 ? `ico/smartStations/${type}Disabled.svg` : `ico/smartStations/${type}Off.svg`}
                    alt={type}/>
            <ReactTooltip className="tooltip" place="bottom" arrowColor="#ffffff" borderColor="#ddd" border/>
        </>
    }, [])


    if (type === 'device') {
        let deviceIcon = Array.isArray(data) ? data?.find(_ => _.id === el)?.deviceType?.iconName : data?.iconName
        return <div>
            <img data-html={seeTooltip ? true : false} data-tip={seeTooltip ?  ReactDOMServer.renderToString(<div>
                <div>Название: <span>{Array.isArray(data) ? data?.find(_ => _.id === el)?.name : data?.name}</span></div>
                <div>Последнее
                    подключение: <span>{Array.isArray(data) ? data?.find(_ => _.id === el)?.status?.lastConnectionTimeUtc ? formatDateTime(data?.find(_ => _.id === el)?.status?.lastConnectionTimeUtc)
                        : "нет" : data?.status?.lastConnectionTimeUtc ? formatDateTime(data?.status?.lastConnectionTimeUtc) : "нет"}</span>
                </div>
            </div>) : null} onClick={handleClickDevice} style={{cursor: type === 'camera' && 'pointer'}}
                 key={el} className="smartStations-icon"
                 src={active === 2 ? `ico/deviceTypes/${deviceIcon}.svg` :
                     active === 1 ? `ico/deviceTypes/${type}Disabled.svg` : `ico/deviceTypes/${type}Off.svg`}
                 alt={type}/>
            <ReactTooltip className="tooltip" place="bottom" arrowColor="#ffffff" borderColor="#ddd" border/>
        </div>
    }

    return <div>
        {Icon}
    </div>
}

export default memo(ColumnForDevices);
