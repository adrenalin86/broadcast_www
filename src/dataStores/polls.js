import CustomStore from "devextreme/data/custom_store";

import store from "../store";

import { getBackendUrl, getToken, getRegion } from "../store/commonSlice";

import { handleResponseErrors, handleDataErrors, handleCaughtErrors } from "./utils";

const getDataStore = ({ windowId }) => {
  const restArgs = {
    w: windowId,
    ct: 30,
  };

  return new CustomStore({
    key: "id",
    load: (data) => {
      let body = {
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        ...restArgs,
        cd: "getPolls",
        data: data,
      };

      return fetch(getBackendUrl(store.getState()) + "?" + body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .then((data) => {
          return {
            data: data.data.entities,
            summary: data.data.summary,
            totalCount: data.data.totalCount,
          };
        })
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },
    insert: (values) => {
      let body = {
        ...restArgs,
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        cd: "bulkInsertPoll",
        data: values,
      };

      return fetch(getBackendUrl(store.getState()) + "?" + body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },
    update: (values) => {
      let body = {
        ...restArgs,
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        cd: "bulkUpdatePoll",
        data: values,
      };

      return fetch(getBackendUrl(store.getState()) + "?" + body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },
    remove: (values) => {
      let body = {
        ...restArgs,
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        cd: "bulkRemovePoll",
        data: values,
      };

      return fetch(getBackendUrl(store.getState()) + "?" + body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },
    byKey: (key) => {
      let body = {
        ...restArgs,
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        cd: "getPollById",
        data: { id: key },
      };

      return fetch(getBackendUrl(store.getState()) + "?" + body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .then((data) => {
          return data.data;
        })
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },
  });
};

export default getDataStore;
