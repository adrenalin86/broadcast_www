import React from "react";
import { Button } from "devextreme-react";

import { get, cloneDeep, uniqueId } from "lodash-es";

import { SMART_JSON_2_FRAME_TYPES } from "../../dataStores";

import { SeTextBox, SeSelectBox, SeImageUploader, SeNumberBox } from "../editors";
import config from "../../config";

const SMART_JSON_2_FRAME_TYPES_NO5 = SMART_JSON_2_FRAME_TYPES.filter((item) => item.value !== 5);
const CONFIG = config.defaults.displays.settings.smartJson2;

const MultipleFrameEditorElement = ({
  data,
  onFormItemValueChangedData,
  editModeEnabled,
  dataField,
  frameIndex,
  onRemoveFrame,
  subFrameTitle,
}) => {
  const onValueChangedEventData = (e) => {
    const eventDataField = e.component.option("dataField");
    onFormItemValueChangedData({
      value: e.value ? e.value : e?.component?.option("text"),
      dataField: eventDataField,
      enabler: e.component.option("enabler"),
      id: get(data, dataField, {}).id,
    });
  };

  const onValueChangedRawData = (e) => {
    e.id = get(data, dataField, {}).id;

    onFormItemValueChangedData(e);
  };

  const onFrameTypeChanged = (e) => {
    const valuesToReset = {};

    switch (e.value) {
      case 1:
        valuesToReset.topText = CONFIG.frame.topText;
        valuesToReset.bottomText = CONFIG.frame.bottomText;
        break;
      case 2:
        valuesToReset.imageUrl = CONFIG.frame.imageUrl;
        valuesToReset.imageData = CONFIG.frame.imageData;
        break;
      case 3:
        valuesToReset.videoUrl = CONFIG.frame.videoUrl;
        break;
      case 4:
        valuesToReset.topText = CONFIG.frame.topText;
        valuesToReset.bottomText = CONFIG.frame.bottomText;
        valuesToReset.imageUrl = CONFIG.frame.imageUrl;
        valuesToReset.imageData = CONFIG.frame.imageData;
        break;
      default:
        break;
    }
    valuesToReset.frameType = e.value;

    onFormItemValueChangedData({
      value: valuesToReset,
      dataField: e.component.option("dataField"),
      id: get(data, dataField, {}).id,
      overwriteItem: true,
    });
  };

  return (
    <>
      <fieldset className="editor-item">
        <legend className="editor-item-title">
          {subFrameTitle} {frameIndex + 1} <Button onClick={onRemoveFrame} icon="trash" frameIndex={frameIndex} />
        </legend>
        <SeSelectBox
          data={get(data, dataField, {})}
          onFormItemValueChanged={onFrameTypeChanged}
          editModeEnabled={editModeEnabled}
          dataField={"frameType"}
          dataSource={SMART_JSON_2_FRAME_TYPES_NO5}
          valueExpr="value"
          displayExpr="name"
          searchEnabled={false}
          title={"Тип кадра"}
          allowDisabling={false}
          controlled={true}
        />
        {get(data, dataField + "frameType") === 1 || get(data, dataField + "frameType") === 4 ? (
          <SeTextBox
            data={get(data, dataField, {})}
            onFormItemValueChanged={onValueChangedEventData}
            editModeEnabled={editModeEnabled}
            dataField={"topText"}
            title={"Верхний текст"}
            allowDisabling={false}
          />
        ) : null}
        {get(data, dataField + "frameType") === 1 || get(data, dataField + "frameType") === 4 ? (
          <SeTextBox
            data={get(data, dataField, {})}
            onFormItemValueChanged={onValueChangedEventData}
            editModeEnabled={editModeEnabled}
            dataField={"bottomText"}
            title={"Нижний текст"}
            allowDisabling={false}
          />
        ) : null}
        {get(data, dataField + "frameType") === 2 || get(data, dataField + "frameType") === 4 ? (
          <SeTextBox
            data={get(data, dataField, {})}
            onFormItemValueChanged={onValueChangedEventData}
            editModeEnabled={editModeEnabled}
            dataField={"imageUrl"}
            title={"Url картинки"}
            allowDisabling={false}
          />
        ) : null}
        {get(data, dataField + "frameType") === 2 || get(data, dataField + "frameType") === 4 ? (
          <SeImageUploader
            data={get(data, dataField, {})}
            onFormItemValueChanged={onValueChangedEventData}
            onFormItemValueChangedData={onValueChangedRawData}
            editModeEnabled={editModeEnabled}
            dataField={"imageData"}
            title={"Файл картинки"}
            maxNumber={1}
            allowDisabling={false}
          />
        ) : null}
        {get(data, dataField + "frameType") === 3 ? (
          <SeTextBox
            data={get(data, dataField, {})}
            onFormItemValueChanged={onValueChangedEventData}
            editModeEnabled={editModeEnabled}
            dataField={"videoUrl"}
            title={"Url видео"}
            allowDisabling={false}
          />
        ) : null}
        <SeNumberBox
          data={get(data, dataField, {})}
          onFormItemValueChanged={onValueChangedEventData}
          editModeEnabled={editModeEnabled}
          dataField={"duration"}
          title={"Длительность"}
          allowDisabling={false}
        />
      </fieldset>
    </>
  );
};

const SeMultipleFrameEditor = ({
  data,
  onFormItemValueChangedData,
  editModeEnabled,
  dataField,
  maxNumber = -1,
  title,
  subFrameTitle,
}) => {
  const onValueChanged = (e) => {
    const newData = get(data, dataField, []).map((item) => {
      if (item.id === e.id) {
        if (e.overwriteItem) {
          return e.value;
        } else {
          return { ...item, [e.dataField]: e.value };
        }
      } else {
        return item;
      }
    });
    e.dataField = dataField;
    e.value = newData;
    onFormItemValueChangedData(e);
  };

  const addElement = (e) => {
    const newData = get(data, dataField, []);
    if (maxNumber >= 0) {
      if (newData.length >= maxNumber) {
        return;
      }
    }

    const usedIds = [];
    newData.forEach((value) => {
      if (value.id) {
        usedIds.push(value.id);
      }
    });

    const id = parseInt(getUniqueNumber(usedIds));

    onFormItemValueChangedData({
      value: [
        ...newData,
        {
          id: id,
          frameType: CONFIG.frame.frameType,
          bottomText: CONFIG.frame.bottomText,
          topText: CONFIG.frame.topText,
          imageUrl: CONFIG.frame.imageUrl,
          imageData: CONFIG.frame.imageData,
          videoUrl: CONFIG.frame.videoUrl,
          duration: CONFIG.frame.duration,
        },
      ],
      dataField,
      forceRepaint: true,
      forceMerge: true,
    });
  };

  const removeElement = (e) => {
    let newData = cloneDeep(get(data, dataField, []));
    const frameIndex = e.component.option("frameIndex");
    newData.splice(frameIndex, 1);
    onFormItemValueChangedData({ value: newData, dataField, forceRepaint: true, forceMerge: false });
  };

  const getUniqueNumber = (usedIds = []) => {
    const id = uniqueId();

    // Стоит добавить больше зашиты от дурака
    if (usedIds.includes(id)) {
      return getUniqueNumber(usedIds, id + 1);
    } else {
      return id;
    }
  };

  const itemsList = get(data, dataField, []).map((item, index) => {
    if (!item?.id && item) {
      const usedIds = [];
      get(data, dataField, []).forEach((value) => {
        if (value.id) {
          usedIds.push(value.id);
        }
      });
      item.id = parseInt(getUniqueNumber(usedIds));
    }

    return (
      <MultipleFrameEditorElement
        key={item.id}
        frameIndex={index}
        dataField={`${dataField}[${index}]`}
        onRemoveFrame={removeElement}
        data={data}
        onFormItemValueChangedData={onValueChanged}
        editModeEnabled={editModeEnabled}
        subFrameTitle={subFrameTitle}
      />
    );
  });

  return (
    <>
      <div className="editor-item-title">
        {title} <Button onClick={addElement} icon="add" />
      </div>
      {itemsList}
    </>
  );
};

export { SeMultipleFrameEditor };
