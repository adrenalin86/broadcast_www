import React, {useEffect, useRef} from "react";

import DataGrid, {Column, Button, Export, ColumnChooser, StateStoring} from "devextreme-react/data-grid";
import DataSource from "devextreme/data/data_source";

import config from "../../config";

import {getVideoCamerasWithStatusesDataStore, WINDOWS} from "../../dataStores";

import dayjs from "dayjs";

const windowId = WINDOWS.VIDEOCAMERAS.id;

const videoCamerasWithStatusesDataStore = getVideoCamerasWithStatusesDataStore({
    windowId: windowId,
});

const videoCamerasWithStatusesDataSource = new DataSource({
    store: videoCamerasWithStatusesDataStore,
    reshapeOnPush: true,
});

const AUTOMATIC_UPDATE_TIMEOUT_MS = config.pages.videoCameras.videoCamerasGrid.updateInterval;

const VideoCamerasTable = ({
                               openEditorPopup,
                               openDeleteRowPopup,
                               openCreateEntryPopup,
                               openVideoStreamPopup,
                               onDataErrorOccurred,
                               counter,
                           }) => {
    const datagridRef = useRef(null);

    const videoCamerasGridOptions = config.pages.videoCameras.videoCamerasGrid.dxOptions;

    const onToolbarPreparing = (e) => {
        let toolbarItems = e.toolbarOptions.items;

        toolbarItems.unshift({
            widget: "dxButton",
            options: {
                icon: "add",
                text: "Добавить видеокамеру",
                onClick: openCreateEntryPopup,
            },
            location: "before",
        });
    };

    useEffect(() => {
        const updateData = () => {
            videoCamerasWithStatusesDataStore
                .load()
                .then((result) => {
                    let changes = [];

                    result.data.forEach((item) => {
                        changes.push({
                            type: "update",
                            key: item.id,
                            data: {
                                status: item.status,
                            },
                        });
                    });

                    videoCamerasWithStatusesDataStore.push(changes);
                })
                .catch((error) => {
                    onDataErrorOccurred({error});
                });
        };

        const timer = setInterval(() => {
            updateData();
        }, AUTOMATIC_UPDATE_TIMEOUT_MS);

        return () => {
            clearInterval(timer);
        };
    }, []);

    useEffect(() => {
        datagridRef?.current?.instance?.refresh();
    }, [counter]);

    return (
        <>
            <DataGrid
                style={{marginTop: "20px"}}
                ref={datagridRef}
                dataSource={videoCamerasWithStatusesDataSource}
                onToolbarPreparing={onToolbarPreparing}
                onDataErrorOccurred={onDataErrorOccurred}
                errorRowEnabled={false}
                {...videoCamerasGridOptions}
            >
                <StateStoring
                    enabled={true}
                    type="localStorage"
                    storageKey="pages.videoCameras.displaysGrid"
                />
                <Export enabled={true} allowExportSelectedData={true}/>
                <ColumnChooser enabled={true}/>
                <Column width={50} dataField="id" caption="id" alignment="left"/>
                <Column
                    width={70}
                    dataField="status.connected"
                    caption="Статус"
                    alignment="center"
                    cellComponent={(e) => {
                        return (
                            <div
                                className="datagrid-display-status"
                                style={{
                                    display: "flex",
                                    flexWrap: "nowrap",
                                    wordBreak: "break-word",
                                    whiteSpace: "normal",
                                    alignItems: "center",
                                    justifyContent: "center",
                                }}
                            >
                                {e.data.data.status?.connected === 1 && (
                                    <i
                                        className="dx-icon-check"
                                        style={{
                                            color: "green",
                                            fontSize: "1.2em",
                                            marginRight: "4px",
                                        }}
                                    />
                                )}
                                {(e.data.data.status?.connected === 0 || !e.data.data.status?.connected) && (
                                    <i
                                        className="dx-icon-close"
                                        style={{
                                            color: "gray",
                                            fontSize: "1.2em",
                                            marginRight: "4px",
                                        }}
                                    />
                                )}
                            </div>
                        );
                    }}
                />
                <Column dataField="name" caption="Название"/>
                <Column width={170} dataField="protocolName" caption="Протокол работы"/>
                <Column
                    dataField="status"
                    caption="Последнее подключение"
                    cellComponent={(e) => {
                        const formatDateTime = (value) => {
                            const dateTime = dayjs.utc(value).local();
                            const isBeforeDayAgo = dateTime.isBefore(dayjs.utc().local().subtract(1, "day"));
                            let formatedDateTime = "";

                            if (isBeforeDayAgo) {
                                formatedDateTime = dateTime.format("HH:mm DD.MM.YYYY");
                            } else {
                                const isInPast = dateTime.isBefore(dayjs.utc().local());

                                if (isInPast) {
                                    formatedDateTime = dateTime.fromNow();
                                } else {
                                    formatedDateTime = "только что";
                                }
                            }

                            return formatedDateTime;
                        };

                        return (
                            <div
                                className={"datagrid-display-status"}
                                style={{
                                    display: "flex",
                                    flexWrap: "nowrap",
                                    wordBreak: "break-word",
                                    whiteSpace: "normal",
                                }}
                            >
                <span>{`${
                    e.data.data.status?.lastConnectionTimeUtc
                        ? formatDateTime(e.data.data.status?.lastConnectionTimeUtc)
                        : "нет"
                }`}</span>
                            </div>
                        );
                    }}
                />
                <Column dataField="note" caption="Примечания"/>
                <Column
                    caption="Изображение"
                    cellComponent={(e) => {
                        const status = e.data.data.status?.lastConnectionTimeUtc;
                        const id = e.data?.key;

                        return status ? (
                            <div
                                className={"datagrid-display-status"}
                                style={{
                                    display: "flex",
                                    flexWrap: "nowrap",
                                    wordBreak: "break-word",
                                    whiteSpace: "normal",
                                    alignItems: "center",
                                }}
                            >
                                <i
                                    className="dx-icon-image"
                                    style={{margin: "5px 4px 0px 0px", cursor: "pointer"}}
                                    onClick={() => openVideoStreamPopup(id)}
                                />
                                <span
                                    style={{cursor: "pointer", color: "rgb(51, 122, 183)"}}
                                    onClick={() => openVideoStreamPopup(id)}
                                >
                  смотреть
                </span>
                            </div>
                        ) : (
                            <div/>
                        );
                    }}
                />
                <Column visible={false} dataField="lat" caption="Широта"/>
                <Column visible={false} dataField="lng" caption="Долгота"/>
                <Column type="buttons" alignment="center">
                    <Button
                        name="custom-edit"
                        text="редактировать"
                        icon="edit"
                        onClick={(e) => openEditorPopup(e.row.key)}
                    />
                    <Button
                        name="custom-delete"
                        text="удалить"
                        icon="trash"
                        onClick={(e) => openDeleteRowPopup(e.row.key)}
                    />
                </Column>
            </DataGrid>
        </>
    );
};

export default VideoCamerasTable;
