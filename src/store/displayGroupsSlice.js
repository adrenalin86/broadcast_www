import { createSlice } from "@reduxjs/toolkit";
import config from "../config";

import { omitDeepByKeys } from "../utils";

export const DISPLAY_GROUPS_PAGE_REDUCER_KEY = "displayGroup";

const displayGroupsPageSlice = createSlice({
  name: DISPLAY_GROUPS_PAGE_REDUCER_KEY,
  initialState: {
    ...omitDeepByKeys(config.pages[DISPLAY_GROUPS_PAGE_REDUCER_KEY], [
      "dxOptions",
    ]),
  },
  reducers: {},
});


export const getDisplayGroupPage = (state) =>
  state.pages[DISPLAY_GROUPS_PAGE_REDUCER_KEY];

export default displayGroupsPageSlice.reducer;
