import React, { useCallback, useEffect, useState } from "react";
import { connect } from "react-redux";
import Form, { Item, Label, ButtonItem, ButtonOptions, RequiredRule } from "devextreme-react/form";
import LoadIndicator from "devextreme-react/load-indicator";
import notify from "devextreme/ui/notify";

import { signIn } from "../../services/auth";
import { getLoading, getBackendUrl, getRegion, getRememberUser } from "../../store/commonSlice";

import config from "../../config";
import "./login-form.scss";

const emailEditorOptions = {
  stylingMode: "filled",
  placeholder: "Псевдоним",
  mode: "user",
};
const passwordEditorOptions = {
  stylingMode: "filled",
  placeholder: "Пароль",
  mode: "password",
};
const rememberMeEditorOptions = {
  text: "Запомнить меня",
  elementAttr: { class: "form-text" },
};
const backendUrlEditorOptions = {
  placeholder: "URL",
  stylingMode: "filled",
};

const regionEditorOptions = {
  placeholder: "Номер региона",
  stylingMode: "filled",
  showSpinButtons: true,
};

const LoginForm = ({ loading, region, backendUrl, rememberMe }) => {
  const [formData, setFormData] = useState({});
  useEffect(() => {
    setFormData((oldData) => {
      return {
        region,
        backendUrl,
        rememberMe,
      };
    });
  }, []);

  const onSubmit = useCallback(async (e) => {
    e.preventDefault();

    let { user, password, rememberMe, backendUrl, region } = formData;

    backendUrl = backendUrl.trim();

    const result = await signIn(user, password, region, rememberMe, backendUrl);

    if (!result.isOk) {
      let reason = "Ошибка входа";
      if (!result.data) {
        if (result.message) {
          reason = result.message;
        }
      } else if (result.data.reason) {
        switch (result.data.reason) {
          case "wrong pass":
            reason = "Неверный пароль";
            break;
          default:
            reason = result.data.reason;
        }
      }
      notify(reason, "error", config.common.errorMessageLifespan);
    }
  });

  return (
    <form className={"login-form"} onSubmit={onSubmit}>
      <Form formData={formData} disabled={loading}>
        <Item dataField={"user"} editorType={"dxTextBox"} editorOptions={emailEditorOptions}>
          <RequiredRule message="Псевдоним обязателен" />
          <Label visible={false} />
        </Item>
        <Item dataField={"password"} editorType={"dxTextBox"} editorOptions={passwordEditorOptions}>
          <RequiredRule message="Пароль обязателен" />
          <Label visible={false} />
        </Item>
        {config.pages.login.showUrlInput && (
          <Item dataField={"backendUrl"} editorType={"dxTextBox"} editorOptions={backendUrlEditorOptions}>
            <Label visible={false} />
            <RequiredRule message="URL сервера обязателен" />
          </Item>
        )}
        {config.pages.login.showRegionInput && (
          <Item dataField={"region"} editorType={"dxNumberBox"} editorOptions={regionEditorOptions} colSpan="2">
            <Label visible={false} />
            <RequiredRule message="Регион обязателен" />
          </Item>
        )}
        <Item dataField={"rememberMe"} editorType={"dxCheckBox"} editorOptions={rememberMeEditorOptions}>
          <Label visible={false} />
        </Item>
        <ButtonItem>
          <ButtonOptions width={"100%"} type={"default"} useSubmitBehavior={true}>
            <span className="dx-button-text">
              {loading ? <LoadIndicator width={"24px"} height={"24px"} visible={true} /> : "Войти"}
            </span>
          </ButtonOptions>
        </ButtonItem>
      </Form>
    </form>
  );
};

export default connect((state) => ({
  loading: getLoading(state),
  region: getRegion(state),
  backendUrl: getBackendUrl(state),
  rememberMe: getRememberUser(state),
}))(LoginForm);
