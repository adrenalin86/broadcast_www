import React, { useState, useRef, useEffect } from "react";
import { connect } from "react-redux";

import DataGrid, {
  Column,
  StateStoring,
  Button,
  Export,
  ColumnChooser,
} from "devextreme-react/data-grid";
import "devextreme-react/text-area";
import { exportDataGrid } from "devextreme/excel_exporter";
import notify from "devextreme/ui/notify";
import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import DataSource from "devextreme/data/data_source";
import { TagBox } from "devextreme-react";

import { Workbook } from "exceljs";
import saveAs from "file-saver";
import { isEmpty } from "lodash-es";

import { deauthorize } from "../../store/commonSlice";

import { getSystemWindowsDataStore, getVehicleRoutesDataStore, WINDOWS } from "../../dataStores";

import EditorPopup from "./vehicleRoutesEditorPopup";
import SubroutesPopup from "./vehicleSubroutesPopup";

import config from "../../config";

function onExporting(e) {
  const workbook = new Workbook();
  const worksheet = workbook.addWorksheet("Main sheet");

  exportDataGrid({
    component: e.component,
    worksheet: worksheet,
    autoFilterEnabled: true,
  }).then(function () {
    workbook.xlsx.writeBuffer().then(function (buffer) {
      saveAs(new Blob([buffer], { type: "application/octet-stream" }), "DataGrid.xlsx");
    });
  });
  e.cancel = true;
}

const windowId = WINDOWS.ROUTES.id;

const vehicleRoutesDataStore = getVehicleRoutesDataStore({
  windowId: windowId,
});

const vehicleRoutesDataSource = new DataSource({
  store: vehicleRoutesDataStore,
  reshapeOnPush: true,
});

const systemWindowsDataStore = getSystemWindowsDataStore({
  windowId: windowId,
});

const Page = ({ localDeauthorize }) => {
  const [editorPopupVisible, setEditorPopupVisible] = useState(false);
  const [editedRow, setEditedRow] = useState({});
  const [deleteRowPopupVisible, setDeleteRowPopupVisible] = useState(false);
  const [deletedRowKey, setDeletedRowKey] = useState(null);
  const [selectedRoute, setSelectedRoute] = useState(null);
  const [showSubroutesPopup, setShowSubroutesPopup] = useState(false);
  const [windowTitle, setWindowTitle] = useState();

  const datagridRef = useRef(null);

  const vehicleRoutesGridOptions = config.pages.vehicleRoutes.grid.dxOptions;

  function onDataErrorOccurred(e) {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);
    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  }

  function onToolbarPreparing(e) {
    let toolbarItems = e.toolbarOptions.items;

    toolbarItems.unshift({
      widget: "dxButton",
      options: {
        icon: "add",
        onClick: openCreateEntryPopup,
      },
      location: "before",
    });
  }

  const openEditorPopup = (e) => {
    const currentRow = vehicleRoutesDataSource.items().find((item) => item.id === e.row.key);

    if (!isEmpty(currentRow)) {
      setEditedRow(currentRow);
      setEditorPopupVisible(true);
    }
  };

  const openCreateEntryPopup = () => {
    setEditedRow({});
    setEditorPopupVisible(true);
  };

  const openDeleteRowPopup = (e) => {
    setEditedRow({});
    setDeletedRowKey(e.row.key);
    setDeleteRowPopupVisible(true);
  };

  const closeEditorPopup = () => {
    setEditorPopupVisible(false);
  };

  const closeDeleteRowPopup = () => {
    setDeleteRowPopupVisible(false);
  };

  const createEntry = (e) => {
    vehicleRoutesDataSource
      .store()
      .insert(e.formData)
      .then((e) => {
        closeEditorPopup();
        datagridRef?.current?.instance?.refresh();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
        closeEditorPopup();
        datagridRef?.current?.instance?.refresh();
      });
  };

  const updateEntry = (e) => {
    if (!isEmpty(e.formData)) {
      vehicleRoutesDataSource
        .store()
        .update(e.editedRowId, e.formData)
        .then(() => {
          closeEditorPopup();
          datagridRef?.current?.instance?.refresh();
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
          closeEditorPopup();
          datagridRef?.current?.instance?.refresh();
        });
    } else {
      closeEditorPopup();
    }
  };

  const deleteEntry = (e) => {
    vehicleRoutesDataSource
      .store()
      .remove(deletedRowKey)
      .then(() => {
        closeDeleteRowPopup();
        datagridRef?.current?.instance?.refresh();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
        closeDeleteRowPopup();
        datagridRef?.current?.instance?.refresh();
      });
  };

  const openSubroutesPopup = (e) => {
    const currentRow = vehicleRoutesDataSource.items().find((item) => item.id === e.row.key);

    if (currentRow?.id) {
      setSelectedRoute(currentRow.id);
      setShowSubroutesPopup(true);
    }
  };

  const closeSubroutesPopup = () => {
    setSelectedRoute(null);
    setShowSubroutesPopup(false);
  };

  useEffect(() => {
    systemWindowsDataStore.byKey(windowId).then((result) => {
      const windowName = result[0]?.name?.split("/").pop();

      setWindowTitle(windowName);
    });
  }, []);

  return (
    <React.Fragment>
      <h2 className={"content-block"}>{windowTitle}</h2>
      {editorPopupVisible && (
        <EditorPopup
          editedRow={editedRow}
          closeEditorPopup={closeEditorPopup}
          updateEntry={updateEntry}
          createEntry={createEntry}
          windowId={windowId}
        />
      )}
      {showSubroutesPopup && (
        <SubroutesPopup selectedRoute={selectedRoute} closePopup={closeSubroutesPopup} />
      )}

      {deleteRowPopupVisible && (
        <Popup
          className="deleteRowPopup"
          width="auto"
          height="auto"
          visible={true}
          showCloseButton={false}
          showTitle={false}
          onHiding={closeDeleteRowPopup}
        >
          <Position my="center" at="center" of={window} />
          <div>
            <span>Вы уверены, что хотите удалить эту запись?</span>
          </div>
          <ToolbarItem
            widget="dxButton"
            location="center"
            options={{ text: "Да", onClick: deleteEntry }}
            toolbar="bottom"
          />
          <ToolbarItem
            widget="dxButton"
            location="center"
            options={{ text: "Нет", onClick: closeDeleteRowPopup }}
            toolbar="bottom"
          />
        </Popup>
      )}
      <div className={"content-block"}>
        <div className={"dx-card responsive-paddings"}>
          <DataGrid
            ref={datagridRef}
            dataSource={vehicleRoutesDataSource}
            onDataErrorOccurred={onDataErrorOccurred}
            onToolbarPreparing={onToolbarPreparing}
            onExporting={onExporting}
            errorRowEnabled={false}
            onContentReady={(e) => {
              //Для каждой колонки форсированно задаём порядок отрисовки в таблице.
              e.component.state().columns.forEach((col, index) => {
                e.component.columnOption(col.name, "visibleIndex", index);
              });
            }}
            remoteOperations={{
              filtering: true,
              paging: true,
            }}
            {...vehicleRoutesGridOptions}
          >
            <StateStoring
              enabled={true}
              type="localStorage"
              storageKey="pages.vehicleRoutesEditor.grid"
            />
            <ColumnChooser enabled={true} />
            <Export enabled={true} allowExportSelectedData={true} />

            <Column dataField="id" caption="id" />
            <Column dataField="name" dataType="string" caption="Название" />
            <Column dataField="number" dataType="number" caption="Номер маршрута" />
            <Column
              dataField="isActive"
              dataType="boolean"
              caption="Активен"
              width={"120px"}
              calculateFilterExpression={function (filterValue, selectedFilterOperation) {
                if (selectedFilterOperation === "between" && Array.isArray(filterValue)) {
                  const filterExpression = [
                    [this.dataField, ">=", filterValue[0]],
                    "and",
                    [this.dataField, "<=", filterValue[1]],
                  ];
                  return filterExpression;
                } else if (selectedFilterOperation === "=" && !Array.isArray(filterValue)) {
                  const filterExpression = [this.dataField, "=", filterValue ? 1 : 0];
                  return filterExpression;
                } else {
                  const filterExpression = [this.dataField, selectedFilterOperation, filterValue];
                  return filterExpression;
                }
              }}
            />
            <Column type={"buttons"}>
              <Button name="showSubroutes" text="Подмаршруты" onClick={openSubroutesPopup} />
            </Column>

            <Column type="buttons">
              <Button name="custom-edit" text="Изменить" icon="edit" onClick={openEditorPopup} />
              <Button
                name="custom-delete"
                text="Удалить"
                icon="trash"
                onClick={openDeleteRowPopup}
              />
            </Column>
          </DataGrid>
        </div>
      </div>
    </React.Fragment>
  );
};

export default connect(
  (state) => ({}),
  (dispatch) => {
    return { localDeauthorize: () => dispatch(deauthorize()) };
  }
)(Page);
