import React, { useState, useRef, useEffect } from "react";
import { connect } from "react-redux";

import DataGrid, {
  Column,
  Lookup,
  StateStoring,
  Button,
  Export,
  ColumnChooser,
} from "devextreme-react/data-grid";
import notify from "devextreme/ui/notify";
import DataSource from "devextreme/data/data_source";
import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import { exportDataGrid } from "devextreme/excel_exporter";

import { Workbook } from "exceljs";
import saveAs from "file-saver";
import { isEmpty, set } from "lodash-es";
import dayjs from "dayjs";
import dayjs_utc from "dayjs/plugin/utc";

import { getPollsDataStore, getSystemWindowsDataStore, WINDOWS } from "../../dataStores";

import { deauthorize } from "../../store/commonSlice";

import EditorPopup from "./pollsEditor";

import config from "../../config";

dayjs.extend(dayjs_utc);

const windowId = WINDOWS.POLLS.id;

function onExporting(e) {
  const workbook = new Workbook();
  const worksheet = workbook.addWorksheet("Main sheet");

  exportDataGrid({
    component: e.component,
    worksheet: worksheet,
    autoFilterEnabled: true,
  }).then(function () {
    workbook.xlsx.writeBuffer().then(function (buffer) {
      saveAs(new Blob([buffer], { type: "application/octet-stream" }), "DataGrid.xlsx");
    });
  });
  e.cancel = true;
}

const pollsDataStore = getPollsDataStore({ windowId: windowId });
const pollsDataSource = new DataSource({
  store: pollsDataStore,
  reshapeOnPush: true,
});

const systemWindowsDataStore = getSystemWindowsDataStore({
  windowId: windowId,
});

const PageComponent = ({ localDeauthorize }) => {
  const [editorPopupVisible, setEditorPopupVisible] = useState(false);
  const [editedRow, setEditedRow] = useState({});
  const [deleteRowPopupVisible, setDeleteRowPopupVisible] = useState(false);
  const [deletedRowKey, setDeletedRowKey] = useState(null);
  const [windowTitle, setWindowTitle] = useState();

  const gridOptions = config.pages.polls.grid.dxOptions;

  const datagridRef = useRef(null);

  function onDataErrorOccurred(e) {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);
    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  } //TODO: рассмотреть целесообразность и способы выноса обработки ошибок из компонентов в ещё большей степени.

  function onToolbarPreparing(e) {
    let toolbarItems = e.toolbarOptions.items;

    toolbarItems.unshift({
      widget: "dxButton",
      options: {
        icon: "add",
        onClick: openCreateEntryPopup,
      },
      location: "before",
    });
  }

  const openEditorPopup = (e) => {
    const currentRow = pollsDataSource.items().find((item) => item.id === e.row.key);

    if (!isEmpty(currentRow)) {
      pollsDataSource
        .store()
        .byKey(currentRow.id)
        .then((e) => {
          setEditedRow(e);
          setEditorPopupVisible(true);
        })

        .catch((error) => {
          onDataErrorOccurred({ error });
          closeEditorPopup();
          datagridRef?.current?.instance?.refresh();
        });
    }
  };

  const openCreateEntryPopup = () => {
    setEditedRow({});
    setEditorPopupVisible(true);
  };

  const openDeleteRowPopup = (e) => {
    setEditedRow({});
    setDeletedRowKey(e.row.key);
    setDeleteRowPopupVisible(true);
  };

  const closeEditorPopup = () => {
    setEditorPopupVisible(false);
  };

  const closeDeleteRowPopup = () => {
    setDeleteRowPopupVisible(false);
  };

  const createEntry = (e) => {
    pollsDataSource
      .store()
      .insert({ poll: e.formData })
      .then((e) => {
        datagridRef?.current?.instance?.refresh();
        closeEditorPopup();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
        datagridRef?.current?.instance?.refresh();
        closeEditorPopup();
      });
  };

  const updateEntry = (e) => {
    if (!isEmpty(e.data)) {
      let createData = e.data.create;
      let updateData = e.data.update;
      let removeData = e.data.remove;

      const promiseList = [];

      if (!isEmpty(createData.questions) || !isEmpty(createData.options)) {
        promiseList.push(async () => pollsDataSource.store().insert(createData));
      }

      if (
        !isEmpty(updateData.poll) ||
        !isEmpty(updateData.questions) ||
        !isEmpty(updateData.options)
      ) {
        promiseList.push(async () => pollsDataSource.store().update(updateData));
      }

      if (!isEmpty(removeData.questionIds) || !isEmpty(removeData.optionIds)) {
        promiseList.push(async () => pollsDataSource.store().remove(removeData));
      }

      Promise.all(promiseList.map((f) => f()))
        .then((e) => {
          datagridRef?.current?.instance?.refresh();
          closeEditorPopup();
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
          datagridRef?.current?.instance?.refresh();
          closeEditorPopup();
        });
    } else {
      closeEditorPopup();
    }
  };

  const deleteEntry = (e) => {
    pollsDataSource
      .store()
      .remove({
        pollIds: [deletedRowKey],
      })
      .then(() => {
        closeDeleteRowPopup();
        datagridRef?.current?.instance?.refresh();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
        closeDeleteRowPopup();
      });
  };

  useEffect(() => {
    systemWindowsDataStore.byKey(windowId).then((result) => {
      const windowName = result[0]?.name?.split("/").pop();

      setWindowTitle(windowName);
    });
  }, []);

  return (
    <React.Fragment>
      <h2 className={"content-block"}>{windowTitle}</h2>
      {editorPopupVisible && (
        <EditorPopup
          editedRow={editedRow}
          windowId={windowId}
          closeEditorPopup={closeEditorPopup}
          updateEntry={updateEntry}
          createEntry={createEntry}
        />
      )}
      {deleteRowPopupVisible && (
        <Popup
          className="deleteRowPopup"
          width="auto"
          height="auto"
          visible={true}
          showCloseButton={false}
          showTitle={false}
          onHiding={closeDeleteRowPopup}
        >
          <Position my="center" at="center" of={window} />
          <div>
            <span>Вы уверены, что хотите удалить эту запись?</span>
          </div>
          <ToolbarItem
            widget="dxButton"
            location="center"
            options={{ text: "Да", onClick: deleteEntry }}
            toolbar="bottom"
          />
          <ToolbarItem
            widget="dxButton"
            location="center"
            options={{ text: "Нет", onClick: closeDeleteRowPopup }}
            toolbar="bottom"
          />
        </Popup>
      )}
      <div className={"content-block"}>
        <div className={"dx-card responsive-paddings"}>
          <DataGrid
            id="displayEditorDataGrid"
            ref={datagridRef}
            dataSource={pollsDataSource}
            onDataErrorOccurred={onDataErrorOccurred}
            onToolbarPreparing={onToolbarPreparing}
            onExporting={onExporting}
            errorRowEnabled={false}
            onContentReady={(e) => {
              //Для каждой колонки форсированно задаём порядок отрисовки в таблице.
              e.component.state().columns.forEach((col, index) => {
                e.component.columnOption(col.name, "visibleIndex", index);
              });
            }}
            {...gridOptions}
          >
            <StateStoring
              enabled={true}
              type="localStorage"
              storageKey="pages.pollsEditor.displaysGrid"
            />
            <ColumnChooser enabled={true} />
            <Export enabled={true} allowExportSelectedData={true} />
            <Column type="string" dataField="title" caption="Название" />
            <Column type="datetime" dataField="startTimeUtc" caption="Начало опроса" />
            <Column type="datetime" dataField="endTimeUtc" caption="Окончание опроса" />
            <Column type="buttons">
              <Button name="custom-edit" text="Изменить" icon="edit" onClick={openEditorPopup} />
              <Button
                name="custom-delete"
                text="Удалить"
                icon="trash"
                onClick={openDeleteRowPopup}
              />
            </Column>
          </DataGrid>
        </div>
      </div>
    </React.Fragment>
  );
};

export default connect(
  (state) => ({}),
  (dispatch) => {
    return { localDeauthorize: () => dispatch(deauthorize()) };
  }
)(PageComponent);
