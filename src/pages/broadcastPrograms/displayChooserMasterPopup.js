import React, { useState, useEffect, useRef, useCallback } from "react";

import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import { ScrollView } from "devextreme-react/scroll-view";
import Form, { SimpleItem } from "devextreme-react/form";
import { TabPanel, Item } from "devextreme-react/tab-panel";
import { DataGrid, Column, Lookup, Scrolling } from "devextreme-react/data-grid";
import notify from "devextreme/ui/notify";
import DataSource from "devextreme/data/data_source";
import { Button } from "devextreme-react/button";

import { isEmpty, get, cloneDeep, uniqWith, differenceWith } from "lodash-es";

import { useScreenSize } from "../../utils";

import { getDisplaysDataStore, getDisplayModelsDataStore } from "../../dataStores";

import { SeMapAreaSelect } from "../../components/editors";

import config from "../../config";
import { connect } from "react-redux";
import { deauthorize } from "../../store/commonSlice";

const defaultValues = config.defaults.broadcastRequestForDisplayInfo;

const DisplayChooserMasterPopup = ({
  windowId,
  data = {},
  dataPath = "",
  dataField = "",
  closeEditorPopup,
  saveResult,
  localDeauthorize,
}) => {
  const [chosenDisplaysIds, setChosenDisplaysIds] = useState([]);
  const [selectedTabIndex, setSelectedTabIndex] = useState(0);
  const [selectedMarkersIds, setSelectedMarkersIds] = useState([]);

  const relationsGridRef = useRef(null);
  const sourceGridRef = useRef(null);

  const { isSmall, isXSmall } = useScreenSize();

  const displaysMasterPopupOptions =
    config.pages.broadcastPrograms.broadcastProgramsGrid.popupBroadcastProgramsEditor.displaysMaster.dxOptions;
  const chosenDisplaysGridOptions =
    config.pages.broadcastPrograms.broadcastProgramsGrid.popupBroadcastProgramsEditor.displaysMaster.chosenDisplays
      .dxOptions;
  const displaySourcesGridOptions =
    config.pages.broadcastPrograms.broadcastProgramsGrid.popupBroadcastProgramsEditor.displaysMaster.displaySources
      .dxOptions;

  const [displaysArray, setDisplaysArray] = useState([]);
  const [displayModelsArray, setDisplayModelsArray] = useState([]);

  const notChosenDisplaysIdsDataSource = new DataSource({
    store: displaysArray,
    reshapeOnPush: true,
    postProcess: (displays) => {
      //Фильтр только табло которые ещё не выбраны
      return differenceWith(displays, chosenDisplaysIds, (itemA, itemB) => {
        return itemA.id === itemB;
      });
    },
  });

  const chosenDisplaysIdsDataSource = new DataSource({
    store: displaysArray,
    reshapeOnPush: true,
    postProcess: (displays) => {
      //Фильтр только табло которые уже выбраны
      return displays.filter((item) => {
        return chosenDisplaysIds.includes(item.id);
      });
    },
  });

  /**
   * Получение полного пути до переменной внутри объекта данных
   */
  const getFullDatafield = () => {
    return isEmpty(dataPath) ? dataField : dataPath?.endsWith(".") ? dataPath + dataField : dataPath + "." + dataField;
  };

  /**
   * Инициализация редактора.
   */
  useEffect(() => {
    setChosenDisplaysIds(cloneDeep(get(data, getFullDatafield())));
    initDisplaysArray();
    initDisplayModelsArray();
  }, []);

  /**
   * Инициализация хранилища табло.
   */
  const initDisplaysArray = useCallback(async () => {
    let response = [];

    const store = getDisplaysDataStore({
      windowId: windowId,
    });

    response = await store
      .load()
      .then((data) => {
        return data.data;
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
      });

    setDisplaysArray(response);
  });

  /**
   * Инициализация хранилища моделей табло.
   */
  const initDisplayModelsArray = useCallback(async () => {
    let response = [];

    const store = getDisplayModelsDataStore({
      windowId: windowId,
    });

    response = await store
      .load()
      .then((data) => {
        return data.data;
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
      });

    setDisplayModelsArray(response);
  });

  function onDataErrorOccurred(e) {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);
    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  }

  const onSave = () => {
    saveResult({
      chosenDisplaysIds: chosenDisplaysIds,
      dataField: getFullDatafield(),
    });
  };

  const onCancel = () => {
    closePopup();
  };

  const closePopup = () => {
    closeEditorPopup();
  };

  const onSelectionChanged = (e) => {
    //Этот код позволяет выбирать множество записей по клику на строку всегда, а не только когда уже начат выбор множества записей
    if (
      e.selectedRowKeys.length === 1 &&
      e.currentSelectedRowKeys.length === 1 &&
      e.selectedRowKeys[0] === e.currentSelectedRowKeys[0]
    ) {
      e.component.selectRows(e.currentSelectedRowKeys.concat(e.currentDeselectedRowKeys));
    }
  };

  const onMarkersSelected = (e) => {
    setSelectedMarkersIds(() => {
      return e.selectedMarkersIds;
    });
  };

  const onClickPushToResultGrid = () => {
    let selectedItems = [];
    //В зависимости от того с каким редактором, какая вкладка tanpanel выбрана, происходит работа
    //выбирается способ получения данных из него.
    if (selectedTabIndex === 0) {
      selectedItems = sourceGridRef.current.instance.getSelectedRowsData().map((item) => {
        //Форматирование данных в удобный формат.
        return item.id;
      });
    } else if (selectedTabIndex === 1) {
      selectedItems = selectedMarkersIds;
    }

    //Избегаем лишней перерисовки.
    if (selectedMarkersIds.length !== 0) {
      setSelectedMarkersIds(() => {
        return [];
      });
    }
    sourceGridRef.current.instance.deselectAll();

    if (selectedItems.length === 0) {
      return;
    }

    pushSelectedToResultGrid(selectedItems);
  };

  const pushSelectedToResultGrid = (selectedItems) => {
    setChosenDisplaysIds((oldValue) => {
      //Защита от возможных дублей.
      const newValue = oldValue.concat(selectedItems);
      const uniq = uniqWith(newValue, (itemA, itemB) => {
        return itemA === itemB;
      });

      return uniq;
    });
  };

  const onClickRemoveSelectedFromResultGrid = () => {
    let selectedItems = relationsGridRef.current.instance.getSelectedRowsData();

    if (selectedItems.length === 0) {
      return;
    }

    relationsGridRef.current.instance.deselectAll();

    removeSelectedFromResultGrid(selectedItems);
  };

  const removeSelectedFromResultGrid = (selectedItems) => {
    setChosenDisplaysIds((oldValue) => {
      const diff = differenceWith(oldValue, selectedItems, (itemA, itemB) => {
        return itemA === itemB.id;
      });
      return diff;
    });
  };

  const onClickClearChosenDisplaysIds = () => {
    setChosenDisplaysIds((oldValue) => {
      return [];
    });
  };

  return (
    <React.Fragment>
      <Popup
        className="customEditorPopup displayChooserMaster"
        visible={true}
        onHiding={closePopup}
        titleComponent={() => {
          return <span className="popup-title">{displaysMasterPopupOptions.title}</span>;
        }}
        {...displaysMasterPopupOptions}
      >
        <Position my="center" at="center" of={window} />
        <ScrollView showScrollbar="always">
          <div className="popupContent">
            <form>
              <Form>
                <SimpleItem>
                  <div className="editor-content">
                    <div className="dx-form-group-with-caption dx-form-group editor-group">
                      <div className="dx-form-group-content editor-content displaygroup-content-editor">
                        <div className="editor-item datagrid-item tabpanel-item">
                          <TabPanel
                            tabIndex={selectedTabIndex}
                            onOptionChanged={(e) => {
                              if (e.name === "selectedIndex") {
                                setSelectedTabIndex(e.value);
                              }
                            }}
                            defaultSelectedIndex={0}
                            itemTitleComponent={(e) => {
                              return <span className="tabpanel-item-title">{e.data.title}</span>;
                            }}
                            swipeEnabled={false}
                          >
                            <Item title="Список всех табло">
                              <div className="dx-field">
                                <DataGrid
                                  dataSource={notChosenDisplaysIdsDataSource}
                                  onDataErrorOccurred={onDataErrorOccurred}
                                  onSelectionChanged={onSelectionChanged}
                                  ref={sourceGridRef}
                                  {...displaySourcesGridOptions}
                                >
                                  <Scrolling mode="standart" />
                                  <Column dataField="id" visible={false} />
                                  <Column dataField={"name"} name="name" caption="Название" dataType="string"></Column>
                                  <Column
                                    dataField={"address"}
                                    name="address"
                                    caption="Адрес"
                                    dataType="string"
                                  ></Column>
                                  <Column dataField={"displayModelId"} caption="Модель дисплея">
                                    <Lookup dataSource={displayModelsArray} valueExpr="id" displayExpr="name" />
                                  </Column>
                                </DataGrid>
                              </div>
                            </Item>
                            <Item title="Карта всех табло">
                              <div className="dx-field">
                                <SeMapAreaSelect
                                  startingCenter={{
                                    lat: defaultValues.lat,
                                    lng: defaultValues.lng,
                                  }}
                                  dataSource={notChosenDisplaysIdsDataSource}
                                  selectedMarkersIds={selectedMarkersIds}
                                  onMarkersSelected={onMarkersSelected}
                                  className={"display-master-map"}
                                  onError={(error) => {
                                    onDataErrorOccurred({ error });
                                  }}
                                />
                              </div>
                            </Item>
                          </TabPanel>
                        </div>
                        <div className="editor-item displaygroup-controls">
                          <Button
                            icon={isSmall || isXSmall ? "arrowup" : "arrowleft"}
                            onClick={onClickRemoveSelectedFromResultGrid}
                          />
                          <Button
                            icon={isSmall || isXSmall ? "arrowdown" : "arrowright"}
                            onClick={onClickPushToResultGrid}
                          />
                        </div>
                        <div className="editor-item ">
                          <div className={"chosendisplays-grid-button-row"}>
                            <Button text="Очистить" onClick={onClickClearChosenDisplaysIds} />
                          </div>
                          <div className="dx-field">
                            <div className="dx-field-value">
                              <DataGrid
                                dataSource={chosenDisplaysIdsDataSource}
                                onDataErrorOccurred={onDataErrorOccurred}
                                onSelectionChanged={onSelectionChanged}
                                ref={relationsGridRef}
                                {...chosenDisplaysGridOptions}
                              >
                                <Scrolling mode="standart" />
                                <Column dataField="id" visible={false} />
                                <Column dataField={"name"} name="name" caption="Название" dataType="string"></Column>
                                <Column dataField={"address"} name="address" caption="Адрес" dataType="string"></Column>
                                <Column dataField={"displayModelId"} caption="Модель дисплея">
                                  <Lookup dataSource={displayModelsArray} valueExpr="id" displayExpr="name" />
                                </Column>
                              </DataGrid>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </SimpleItem>
              </Form>
            </form>
          </div>
        </ScrollView>
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Выбрать", onClick: onSave }}
          toolbar="bottom"
        />
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Отмена", onClick: onCancel }}
          toolbar="bottom"
        />
      </Popup>
    </React.Fragment>
  );
};

export default connect(null, { localDeauthorize: deauthorize })(DisplayChooserMasterPopup);
