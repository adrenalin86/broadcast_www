import React, { useState, useRef, useEffect } from "react";
import { connect } from "react-redux";

import DataGrid, {
  Column,
  StateStoring,
  Button,
  Export,
  ColumnChooser,
} from "devextreme-react/data-grid";
import "devextreme-react/text-area";
import { exportDataGrid } from "devextreme/excel_exporter";
import notify from "devextreme/ui/notify";
import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import DataSource from "devextreme/data/data_source";

import { Workbook } from "exceljs";
import saveAs from "file-saver";
import { filter, isEmpty } from "lodash-es";

import { deauthorize, getLocale } from "../../store/commonSlice";

import {
  getDisplaysDataStore,
  getDisplayGroupsDataStore,
  getBroadcastProgramsHydratedDataStore,
  getGenericEntityDataStore,
  GENERIC_ENTITIES,
  WINDOWS,
  getSystemWindowsDataStore,
} from "../../dataStores";

import BroadcastProgramsEditorPopup from "./broadcastProgramsEditorPopup";

import config from "../../config";

function onExporting(e) {
  const workbook = new Workbook();
  const worksheet = workbook.addWorksheet("Main sheet");

  exportDataGrid({
    component: e.component,
    worksheet: worksheet,
    autoFilterEnabled: true,
  }).then(function () {
    workbook.xlsx.writeBuffer().then(function (buffer) {
      saveAs(new Blob([buffer], { type: "application/octet-stream" }), "DataGrid.xlsx");
    });
  });
  e.cancel = true;
}

const windowId = WINDOWS.BROADCAST_PROGRAMS.id;

const displaysDataStore = getDisplaysDataStore({
  windowId: windowId,
});

const displayGroupsDataStore = getDisplayGroupsDataStore({
  windowId: windowId,
});

const tagsDataStore = getGenericEntityDataStore({
  windowId: windowId,
  entityName: GENERIC_ENTITIES.TAGS,
});

const systemWindowsDataStore = getSystemWindowsDataStore({
  windowId: windowId,
});

const BroadcastPrograms = ({ localDeauthorize, locale }) => {
  const [editorPopupVisible, setEditorPopupVisible] = useState(false);
  const [editedRow, setEditedRow] = useState({});
  const [deleteRowPopupVisible, setDeleteRowPopupVisible] = useState(false);
  const [deletedRowKey, setDeletedRowKey] = useState(null);
  const [filterCurrentDate, setFilterCurrentDate] = useState(false);
  const [windowTitle, setWindowTitle] = useState();
  const fixedFilter = useRef([]);
  const datagridRef = useRef(null);

  const broadcastProgramsDataStore = useRef(
    getBroadcastProgramsHydratedDataStore({
      windowId: windowId,
    })
  ).current;

  const broadcastProgramsDataSource = useRef(
    new DataSource({
      store: broadcastProgramsDataStore,
      key: "id",
      reshapeOnPush: true,
    })
  ).current;

  useEffect(() => {
    if (datagridRef?.current?.instance) {
      if (filterCurrentDate) {
        const currentTime = new Date().toISOString().split(".")[0] + "Z";
        fixedFilter.current = [
          ["startTimeUtc", "<=", currentTime],
          "and",
          ["endTimeUtc", ">=", currentTime],
        ];
        broadcastProgramsDataStore.fixedFilter(fixedFilter.current);
        datagridRef.current.instance.refresh();
      } else {
        if (!isEmpty(fixedFilter.current)) {
          fixedFilter.current = [];
          broadcastProgramsDataStore.fixedFilter(fixedFilter.current);
          datagridRef.current.instance.refresh();
        }
      }
    }
  }, [filterCurrentDate]);

  const broadcastProgramsGridOptions =
    config.pages.broadcastPrograms.broadcastProgramsGrid.dxOptions;

  const onDataErrorOccurred = (e) => {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);
    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  };

  const onToolbarPreparing = (e) => {
    let toolbarItems = e.toolbarOptions.items;

    toolbarItems.unshift({
      widget: "dxCheckBox",
      options: {
        text: "Только действующие сейчас",
        onValueChanged: (e) => {
          setFilterCurrentDate(!!e.value);
        },
        onInitialized: function (e) {
          e.component.option("value", filterCurrentDate);
        },
      },
      location: "before",
    });
    toolbarItems.unshift({
      widget: "dxButton",
      options: {
        icon: "add",
        onClick: openCreateEntryPopup,
      },
      location: "before",
    });
  };

  const openEditorPopup = (e) => {
    const currentRow = broadcastProgramsDataSource.items().find((item) => item.id === e.row.key);

    if (!isEmpty(currentRow)) {
      setEditedRow(currentRow);
      setEditorPopupVisible(true);
    }
  };

  const openCreateEntryPopup = () => {
    setEditedRow({});
    setEditorPopupVisible(true);
  };

  const openDeleteRowPopup = (e) => {
    setEditedRow({});
    setDeletedRowKey(e.row.key);
    setDeleteRowPopupVisible(true);
  };

  const closeEditorPopup = () => {
    setEditorPopupVisible(false);
  };

  const closeDeleteRowPopup = () => {
    setDeleteRowPopupVisible(false);
  };

  const createEntry = (e) => {
    broadcastProgramsDataSource
      .store()
      .insert(e.formData)
      .then((e) => {
        closeEditorPopup();
        datagridRef?.current?.instance?.refresh();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
        closeEditorPopup();
        datagridRef?.current?.instance?.refresh();
      });
  };

  const updateEntry = (e) => {
    if (!isEmpty(e.formData)) {
      broadcastProgramsDataSource
        .store()
        .update(e.editedRowId, e.formData)
        .then(() => {
          closeEditorPopup();
          datagridRef?.current?.instance?.refresh();
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
          closeEditorPopup();
          datagridRef?.current?.instance?.refresh();
        });
    } else {
      closeEditorPopup();
    }
  };

  const deleteEntry = (e) => {
    broadcastProgramsDataSource
      .store()
      .remove(deletedRowKey)
      .then(() => {
        closeDeleteRowPopup();
        datagridRef?.current?.instance?.refresh();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
        closeDeleteRowPopup();
        datagridRef?.current?.instance?.refresh();
      });
  };

  useEffect(() => {
    systemWindowsDataStore.byKey(windowId).then((result) => {
      const windowName = result[0]?.name?.split("/").pop();

      setWindowTitle(windowName);
    });
  }, []);

  return (
    <React.Fragment>
      <h2 className={"content-block"}>{windowTitle}</h2>
      {editorPopupVisible && (
        <BroadcastProgramsEditorPopup
          editedRow={editedRow}
          windowId={windowId}
          closeEditorPopup={closeEditorPopup}
          updateEntry={updateEntry}
          createEntry={createEntry}
        />
      )}
      {deleteRowPopupVisible && (
        <Popup
          className="deleteRowPopup"
          width="auto"
          height="auto"
          visible={true}
          showCloseButton={false}
          showTitle={false}
          onHiding={closeDeleteRowPopup}
        >
          <Position my="center" at="center" of={window} />
          <div>
            <span>Вы уверены, что хотите удалить эту запись?</span>
          </div>
          <ToolbarItem
            widget="dxButton"
            location="center"
            options={{ text: "Да", onClick: deleteEntry }}
            toolbar="bottom"
          />
          <ToolbarItem
            widget="dxButton"
            location="center"
            options={{ text: "Нет", onClick: closeDeleteRowPopup }}
            toolbar="bottom"
          />
        </Popup>
      )}
      <div className={"content-block"}>
        <div className={"dx-card responsive-paddings"}>
          <DataGrid
            ref={datagridRef}
            dataSource={broadcastProgramsDataSource}
            onDataErrorOccurred={onDataErrorOccurred}
            onToolbarPreparing={onToolbarPreparing}
            onExporting={onExporting}
            errorRowEnabled={false}
            renderAsync={true}
            allowColumnReordering={false}
            onContentReady={(e) => {
              //Для каждой колонки форсированно задаём порядок отрисовки в таблице.
              e.component.state().columns.forEach((col, index) => {
                e.component.columnOption(col.name, "visibleIndex", index);
              });
            }}
            {...broadcastProgramsGridOptions}
          >
            <StateStoring
              enabled={true}
              type="localStorage"
              storageKey="pages.displayModelsEditor.displayModelsGrid"
            />
            <ColumnChooser enabled={true} />
            <Export enabled={true} allowExportSelectedData={true} />

            <Column dataField="id" caption="id" width="40px" />
            <Column
              dataField="icon"
              caption=" "
              dataType="string"
              width={"30px"}
              cellComponent={(e) => {
                return <i className={"dx-icon-" + e?.data?.data?.iconName}></i>;
              }}
              allowSearch={false}
              allowFiltering={false}
            />
            <Column dataField="name" dataType="string" caption="Название" width="240px" />
            <Column
              dataField="duration"
              dataType="string"
              caption="Время действия"
              width="145px"
              cellComponent={(e) => {
                return (
                  <div>
                    <div>{new Date(e.data.data.startTimeUtc).toLocaleString(locale)}</div>
                    <div>{new Date(e.data.data.endTimeUtc).toLocaleString(locale)}</div>
                  </div>
                );
              }}
            />
            <Column
              dataField="priority"
              dataType="number"
              caption="Приоритет"
              width="86px"
              defaultSortOrder="desc"
            />
            <Column
              name="cond"
              dataType="object"
              caption="Условие"
              minWidth="200px"
              calculateCellValue={(e) => {
                return {
                  displays: e.currentDisplays,
                  groups: e.currentGroups,
                  tags: e.currentTags,
                };
              }}
              cellComponent={(e) => {
                const value = e?.data?.value;
                if (!value) {
                  return <div></div>;
                }
                return (
                  <React.Fragment>
                    {(value?.displays ? value?.displays?.length !== 0 : false) && (
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "column",
                          alignItems: "flex-start",
                        }}
                      >
                        <span>{"Табло"}</span>
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            padding: "0px 4px 4px 0px",
                            border: "1px dashed #ddd",
                            borderRadius: "4px",
                            background: "white",
                            width: "100%",
                          }}
                        >
                          {value.displays.map((item) => {
                            return (
                              <div className="dx-tag" key={item.id}>
                                <div
                                  className="dx-tag-content"
                                  style={{ padding: "3px 6px 4px 6px" }}
                                >
                                  <span>{item.name}</span>
                                </div>
                              </div>
                            );
                          })}
                        </div>
                      </div>
                    )}
                    {(value?.groups ? value?.groups?.length !== 0 : false) && (
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "column",
                          alignItems: "flex-start",
                        }}
                      >
                        <span>{"Группы табло"}</span>
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            padding: "0px 4px 4px 0px",
                            border: "1px dashed #ddd",
                            borderRadius: "4px",
                            background: "white",
                            width: "100%",
                          }}
                        >
                          {value.groups.map((item) => {
                            return (
                              <div className="dx-tag" key={item.id}>
                                <div
                                  className="dx-tag-content"
                                  style={{ padding: "3px 6px 4px 6px" }}
                                >
                                  <span>{item.name}</span>
                                </div>
                              </div>
                            );
                          })}
                        </div>
                      </div>
                    )}
                    {(value?.tags ? value?.tags?.length !== 0 : false) && (
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "column",
                          alignItems: "flex-start",
                        }}
                      >
                        <span>{"Тэги табло"}</span>
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            padding: "0px 4px 4px 0px",
                            border: "1px dashed #ddd",
                            borderRadius: "4px",
                            background: "white",
                            width: "100%",
                          }}
                        >
                          {value.tags.map((item) => {
                            return (
                              <div className="dx-tag" key={item.id}>
                                <div
                                  className="dx-tag-content"
                                  style={{ padding: "3px 6px 4px 6px" }}
                                >
                                  <span>{item.name}</span>
                                </div>
                              </div>
                            );
                          })}
                        </div>
                      </div>
                    )}
                  </React.Fragment>
                );
              }}
            />
            <Column name="buttons" type="buttons" width="70px">
              <Button name="custom-edit" text="Изменить" icon="edit" onClick={openEditorPopup} />
              <Button
                name="custom-delete"
                text="Удалить"
                icon="trash"
                onClick={openDeleteRowPopup}
              />
            </Column>
          </DataGrid>
        </div>
      </div>
    </React.Fragment>
  );
};

export default connect(
  (state) => ({
    locale: getLocale(state),
  }),
  (dispatch) => {
    return { localDeauthorize: () => dispatch(deauthorize()) };
  }
)(BroadcastPrograms);
