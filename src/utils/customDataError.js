class CustomDataError extends Error {
  constructor(message, data = {}) {
    super(message);
    this.name = "CustomDataError";
    this.data = data;
  }
}

export default CustomDataError;
