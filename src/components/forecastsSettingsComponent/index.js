import { SeNumberBox } from "../editors";

import { isEmpty, noop } from "lodash-es";

const ForecastsSettingsComponent = ({
  data,
  onFormItemValueChanged,
  className,
  editModeEnabled = false,
  dataField,
  onError = noop
}) => {
  const onValueChanged = (e) => {
    const eventDataField = e.component.option("dataField");
    const fullDataField = isEmpty(dataField)
      ? eventDataField
      : dataField?.endsWith(".")
      ? dataField + eventDataField
      : dataField + "." + eventDataField;

    e.component.option("dataField", fullDataField);

    onFormItemValueChanged(e);
  };

  return (
    <div className={"dx-form-group-with-caption dx-form-group " + className}>
      <div className="dx-form-group-caption">Общие настройки</div>
      <div className="dx-form-group-content">
        <div className="editor-item">
          <SeNumberBox
            data={data}
            onFormItemValueChanged={onValueChanged}
            editModeEnabled={editModeEnabled}
            dataField={"timeoutInSec"}
            title={"Таймаут подключения табло, сек."}
          />
        </div>
      </div>
    </div>
  );
};

export default ForecastsSettingsComponent;
