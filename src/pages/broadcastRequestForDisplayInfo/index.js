import React, { useState, useRef, useEffect } from "react";
import { connect } from "react-redux";
import { Popup, Position, ToolbarItem } from "devextreme-react/popup";

import DataGrid, {
  Column,
  Editing,
  StateStoring,
  Button,
  Export,
  ColumnChooser,
} from "devextreme-react/data-grid";
import notify from "devextreme/ui/notify";
import { exportDataGrid } from "devextreme/excel_exporter";
import DataSource from "devextreme/data/data_source";
import { Workbook } from "exceljs";
import saveAs from "file-saver";
import { isEmpty } from "lodash-es";
import dayjs from "dayjs";
import dayjs_utc from "dayjs/plugin/utc";

import {
  getBroadcastRequestForDisplayInfoDataStore,
  getSystemWindowsDataStore,
  WINDOWS,
} from "../../dataStores";
import BroadcastRequestForDisplayInfoEditorPopup from "./broadcastRequestForDisplayInfoEditorPopup.js";

import { deauthorize } from "../../store/commonSlice";

import config from "../../config";

dayjs.extend(dayjs_utc);

const windowId = WINDOWS.BROADCAST_INFORMATION_DISPLAY_REQUESTS.id;

const broadcastRequestForDisplayInfoDataGridConfig =
  config.pages.broadcastRequestForDisplayInfo.broadcastRequestForDisplayInfoDataGrid.dxOptions;

function onExporting(e) {
  const workbook = new Workbook();
  const worksheet = workbook.addWorksheet("Main sheet");

  exportDataGrid({
    component: e.component,
    worksheet: worksheet,
    autoFilterEnabled: true,
  }).then(function () {
    workbook.xlsx.writeBuffer().then(function (buffer) {
      saveAs(new Blob([buffer], { type: "application/octet-stream" }), "DataGrid.xlsx");
    });
  });
  e.cancel = true;
}

const broadcastRequestForDisplayInfoDataStore = getBroadcastRequestForDisplayInfoDataStore({
  windowId: windowId,
});

const broadcastRequestForDisplayInfoDataSource = new DataSource({
  store: broadcastRequestForDisplayInfoDataStore,
  reshapeOnPush: true,
});

const systemWindowsDataStore = getSystemWindowsDataStore({
  windowId: windowId,
});

const Page = ({ localDeauthorize }) => {
  const [editorPopupVisible, setEditorPopupVisible] = useState(false);
  const [editedRow, setEditedRow] = useState({});
  const [filterOutFinished, setFilterOutFinished] = useState(false);
  const [deleteRowPopupVisible, setDeleteRowPopupVisible] = useState(false);
  const [deletedRowKey, setDeletedRowKey] = useState(null);
  const [windowTitle, setWindowTitle] = useState();
  const datagridRef = useRef(null);

  useEffect(() => {
    if (filterOutFinished) {
      const value = new Date().toISOString().split(".")[0] + "Z";

      broadcastRequestForDisplayInfoDataStore.fixedFilter(["endTimeUtc", ">", value]);
    } else {
      broadcastRequestForDisplayInfoDataStore.fixedFilter([]);
    }
    if (datagridRef.current) {
      datagridRef.current.instance.refresh();
    }
  }, [filterOutFinished]);

  function onDataErrorOccurred(e) {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);
    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  }

  const openEditorPopup = (e) => {
    const currentRow = broadcastRequestForDisplayInfoDataSource
      .items()
      .find((item) => item.id === e.row.key);

    if (!isEmpty(currentRow)) {
      setEditedRow(currentRow);
      setEditorPopupVisible(true);
    }
  };

  const closeEditorPopup = () => {
    setEditorPopupVisible(false);
  };

  const createEntry = (e) => {
    if (!isEmpty(e.formData)) {
      broadcastRequestForDisplayInfoDataSource
        .store()
        .insert(e.formData)
        .then(() => {
          closeEditorPopup();
          datagridRef?.current?.instance?.refresh();
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
          closeEditorPopup();
          datagridRef?.current?.instance?.refresh();
        });
    } else {
      closeEditorPopup();
    }
  };

  const updateEntry = (e) => {
    if (!isEmpty(e.formData)) {
      broadcastRequestForDisplayInfoDataSource
        .store()
        .update(e.editedRowId, e.formData)
        .then(() => {
          closeEditorPopup();
          datagridRef?.current?.instance?.refresh();
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
          closeEditorPopup();
          datagridRef?.current?.instance?.refresh();
        });
    } else {
      closeEditorPopup();
    }
  };

  const deleteEntry = (e) => {
    broadcastRequestForDisplayInfoDataSource
      .store()
      .remove(deletedRowKey)
      .then(() => {
        closeDeleteRowPopup();
        datagridRef?.current?.instance?.refresh();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
        closeDeleteRowPopup();
        datagridRef?.current?.instance?.refresh();
      });
  };

  const openCreateEntryPopup = () => {
    setEditedRow({});
    setEditorPopupVisible(true);
  };

  const closeDeleteRowPopup = () => {
    setDeleteRowPopupVisible(false);
  };

  const openDeleteRowPopup = (e) => {
    setEditedRow({});
    setDeletedRowKey(e.row.key);
    setDeleteRowPopupVisible(true);
  };

  const onToolbarPreparing = (e) => {
    let toolbarItems = e.toolbarOptions.items;
    toolbarItems.unshift({
      widget: "dxCheckBox",
      options: {
        value: filterOutFinished,
        onValueChanged: function () {
          setFilterOutFinished((oldData) => !oldData);
        },
        text: "Убрать прошедшие",
      },
      location: "before",
    });
    toolbarItems.unshift({
      widget: "dxButton",
      options: {
        icon: "add",
        onClick: openCreateEntryPopup,
      },
      location: "before",
    });
  };

  useEffect(() => {
    systemWindowsDataStore.byKey(windowId).then((result) => {
      const windowName = result[0]?.name?.split("/").pop();

      setWindowTitle(windowName);
    });
  }, []);

  return (
    <>
      <h2 className={"content-block"}>{windowTitle}</h2>
      {deleteRowPopupVisible && (
        <Popup
          className="deleteRowPopup"
          width="auto"
          height="auto"
          visible={true}
          showCloseButton={false}
          showTitle={false}
          onHiding={closeDeleteRowPopup}
        >
          <Position my="center" at="center" of={window} />
          <div>
            <span>Вы уверены, что хотите удалить эту запись?</span>
          </div>
          <ToolbarItem
            widget="dxButton"
            location="center"
            options={{ text: "Да", onClick: deleteEntry }}
            toolbar="bottom"
          />
          <ToolbarItem
            widget="dxButton"
            location="center"
            options={{ text: "Нет", onClick: closeDeleteRowPopup }}
            toolbar="bottom"
          />
        </Popup>
      )}
      {editorPopupVisible && (
        <BroadcastRequestForDisplayInfoEditorPopup
          editedRow={editedRow}
          closeEditorPopup={closeEditorPopup}
          updateEntry={updateEntry}
          createEntry={createEntry}
          windowId={windowId}
        />
      )}
      <div className={"content-block"}>
        <div className={"dx-card responsive-paddings"}>
          <DataGrid
            key={"grid"}
            ref={datagridRef}
            dataSource={broadcastRequestForDisplayInfoDataSource}
            onDataErrorOccurred={onDataErrorOccurred}
            onExporting={onExporting}
            
            errorRowEnabled={false}
            onContentReady={(e) => {
              //Для каждой колонки форсированно задаём порядок отрисовки в таблице.
              e.component.state().columns.forEach((col, index) => {
                e.component.columnOption(col.name, "visibleIndex", index);
              });
            }}
            onToolbarPreparing={onToolbarPreparing}
            {...broadcastRequestForDisplayInfoDataGridConfig}
          >
            <Editing
              allowAdding={false} />
            <StateStoring
              enabled={true}
              type="localStorage"
              storageKey="pages.displayModelsEditor.broadcastRequestForDisplayInfoDataGrid"
            />
            <ColumnChooser enabled={true} />
            <Export enabled={true} allowExportSelectedData={true} />
            <Column dataField="id" caption="id" />
            <Column dataField="name" dataType="string" caption="Название" />
            <Column
              dataField="startTimeUtc"
              dataType="string"
              caption="Начало показа"
              cellComponent={(e) => {
                let formatedDate = "";
                const value = e.data?.value;
                if (value) {
                  formatedDate = dayjs(value).local().format("DD MMM YYYY HH:mm:ss");
                }
                return <div>{formatedDate}</div>;
              }}
            />
            <Column
              dataField="endTimeUtc"
              dataType="string"
              caption="Окончание показа"
              cellComponent={(e) => {
                let formatedDate = "";
                const value = e.data?.value;
                if (value) {
                  formatedDate = dayjs(value).local().format("DD MMM YYYY HH:mm:ss");
                }
                return <div>{formatedDate}</div>;
              }}
            />
            <Column dataField="text" dataType="string" caption="Текст" />
            <Column dataField="note" dataType="string" caption="Примечание" />
            <Column type="buttons">
              <Button name="custom-edit" text="Изменить" icon="edit" onClick={openEditorPopup} />
              <Button
                name="custom-delete"
                text="Удалить"
                icon="trash"
                onClick={openDeleteRowPopup}
              />
            </Column>
          </DataGrid>
        </div>
      </div>
    </>
  );
};

export default connect(null, { localDeauthorize: deauthorize })(Page);
