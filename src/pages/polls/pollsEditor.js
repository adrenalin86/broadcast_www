import React, { useState, useEffect, useRef } from "react";
import { Button as DxButton } from "devextreme-react";
import notify from "devextreme/ui/notify";

import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import { ScrollView } from "devextreme-react/scroll-view";
import Form, { SimpleItem } from "devextreme-react/form";

import { TextBox, CheckBox, DateBox, TextArea, TagBox } from "devextreme-react";

import DataGrid, { Column, Editing } from "devextreme-react/data-grid";

import ValidationSummary from "devextreme-react/validation-summary";
import ValidationEngine from "devextreme/ui/validation_engine";
import Validator, {
  RequiredRule,
  CustomRule,
} from "devextreme-react/validator";

import { set, cloneDeep, isEmpty, isBoolean } from "lodash-es";

import config from "../../config";

import { WINDOWS, getTagsDataStore } from "../../dataStores";
import { nanoid } from "nanoid";
const windowId = WINDOWS.POLLS.id;

const tagsDataStore = getTagsDataStore({ windowId: windowId });

function showErrorToast(message) {
  notify(message, "error", config.common.errorMessageLifespan);
}

const EditorPopup = ({
  editedRow,
  closeEditorPopup,
  updateEntry,
  createEntry,
}) => {
  const [formData, setFormData] = useState({});
  const [changedFormData] = useState({});

  const dataByOperations = useRef({
    update: {
      poll: {},
      questions: {},
      options: {},
    },
    remove: {
      poll: {},
      questions: {},
      options: {},
    },
    create: {
      poll: {},
      questions: {},
      options: {},
    },
  });
  const isNew = useRef(true);

  const popupOptions = config.pages.polls.grid.editorPopup.dxOptions;
  const defaults = config.defaults.polls;
  /**
   * Инициализация редактора.
   */
  useEffect(() => {
    if (isEmpty(editedRow)) {
      isNew.current = true;

      setFormData(cloneDeep(defaults));
    } else {
      isNew.current = false;

      setFormData(cloneDeep(editedRow));
    }
  }, []);

  const closePopup = () => {
    closeEditorPopup();
  };

  const onSave = () => {
    const questionsExist = formData?.questions?.length;

    if (!questionsExist) {
      showErrorToast("В опросе должен быть минимум один вопрос!");
    }

    const isValid =
      ValidationEngine.validateGroup("formEditor").isValid && questionsExist;

    if (isValid) {
      if (isNew.current) {
        createEntry({ formData: saveNewEntryParse(formData) });
      } else {
        updateEntry({
          data: saveEditEntryParse(dataByOperations.current),
          editedRowId: editedRow.id,
        });
      }
    }
  };

  const onCancel = () => {
    closePopup();
  };

  const saveEditEntryParse = (data) => {
    const parsedData = {
      create: {
        questions: Object.values(data.create.questions).filter(
          (item) => !isEmpty(item)
        ),
        options: Object.values(data.create.options).filter(
          (item) => !isEmpty(item)
        ),
      },
      update: {
        poll: data.update.poll,
        questions: Object.values(data.update.questions).filter(
          (item) => !isEmpty(item)
        ),
        options: Object.values(data.update.options).filter(
          (item) => !isEmpty(item)
        ),
      },
      remove: {
        pollIds: [],
        questionIds: Object.values(data.remove.questions).filter(
          (item) => !!item
        ),
        optionIds: Object.values(data.remove.options).filter((item) => !!item),
      },
    };

    parsedData.create.questions = parsedData.create.questions.map(
      (question) => {
        const clearedQuestion = { ...question, tid: undefined };
        clearedQuestion.options = clearedQuestion?.options?.map((option) => {
          const clearedOption = {
            ...option,
            tid: undefined,
            pollQuestionId: undefined,
          };
          return clearedOption;
        });
        return clearedQuestion;
      }
    );

    parsedData.create.options = parsedData.create.options.map((item) => ({
      ...item,
      tid: undefined,
    }));
    parsedData.update.questions = parsedData.update.questions.map((item) => ({
      ...item,
      tid: undefined,
    }));
    parsedData.update.options = parsedData.update.options.map((item) => ({
      ...item,
      tid: undefined,
    }));

    return parsedData;
  };

  const saveNewEntryParse = (data) => {
    const dataCopy = { ...data };
    dataCopy.questions = dataCopy?.questions?.map((question) => {
      const clearedQuestion = { ...question, tid: undefined };
      clearedQuestion.options = clearedQuestion?.options?.map((option) => {
        const clearedOption = { ...option, tid: undefined };
        return clearedOption;
      });
      return clearedQuestion;
    });
    return dataCopy;
  };

  const onQuestionDataChanged = (qid, dataField, value) => {
    setFormData((oldData) => ({
      ...oldData,
      questions: oldData.questions?.map((item) => {
        if (item.tid === qid || item.id === qid) {
          item[dataField] = value;
        }
        return item;
      }),
    }));
    if (!isNew.current) {
      const newQuestion = !!dataByOperations.current.create.questions[qid];

      if (newQuestion) {
        dataByOperations.current.create.questions[qid][dataField] = value;
      } else {
        dataByOperations.current.update.questions[qid] = {
          ...dataByOperations.current.update.questions[qid],
          [dataField]: value,
          id: qid,
        }; //может быть смешивание tid и id, надо удостовериться что не произойдёт.
      }
    }
  };

  const onOptionDataChanged = (qid, oid, data) => {
    setFormData((oldData) => {
      const newData = { ...oldData };
      let oldOptions = newData.questions.find(
        (item) => item.id === qid || item.tid === qid
      )?.options;
      if (oldOptions.length) {
        oldOptions = oldOptions.map((item) => {
          if (item.id === oid || item.tid === oid) {
            return { ...item, ...data };
          }
          return item;
        });
        newData.questions.find(
          (item) => item.id === qid || item.tid === qid
        ).options = oldOptions;
      }

      return newData;
    });
    if (!isNew.current) {
      const newQuestion = !!dataByOperations.current.create.questions[qid];

      if (newQuestion) {
        dataByOperations.current.create.questions[qid] = {
          ...dataByOperations.current.create.questions[qid],
          options: [
            ...dataByOperations.current.create.questions[qid]?.options?.map(
              (item) => {
                if (item.id === oid || item.tid === oid) {
                  return {
                    ...item,
                    ...data,
                  };
                } else {
                  return item;
                }
              }
            ),
          ],
        };
      } else {
        const newOption = !!dataByOperations.current.create.options[oid];

        if (newOption) {
          dataByOperations.current.create.options[oid] = {
            ...dataByOperations.current.create.options[oid],
            ...data,
          };
        } else {
          dataByOperations.current.update.options[oid] = {
            ...dataByOperations.current.update.options[oid],
            ...data,
            id: oid,
          };
        }
      }
    }
  };

  const addQuestion = (e) => {
    const newTid = nanoid();
    const questions = formData?.questions || [];
    const orders = questions?.map((item) => item.order);
    const maxOrder = orders?.length ? Math.max(...orders) : 0;

    setFormData((oldData) => ({
      ...oldData,
      questions: [
        ...(formData?.questions || []),
        {
          tid: newTid,
          order: maxOrder + 1,
          text: "",
          isMultipleChoice: false,
          isOwnAnswerPossible: false,
          options: [],
        },
      ],
    }));
    if (!isNew.current) {
      dataByOperations.current.create.questions[newTid] = {
        tid: newTid,
        order: maxOrder + 1,
        text: "",
        isMultipleChoice: false,
        isOwnAnswerPossible: false,
        pollId: formData?.id,
        options: [],
      };
    }
  };

  const deleteQuestion = (id) => {
    setFormData((oldData) => ({
      ...oldData,
      questions: oldData?.questions?.filter((item) => {
        return id !== item.id && id !== item.tid;
      }),
    }));
    if (!isNew.current) {
      const newQuestion = !!dataByOperations.current.create.questions[id];
      if (newQuestion) {
        dataByOperations.current.create.questions[id] = undefined;
      } else {
        dataByOperations.current.remove.questions[id] = id;

        const createOptionKeys = Object.keys(
          dataByOperations.current.create.options
        );
        for (let i = 0; i > createOptionKeys.length; i++) {
          if (
            dataByOperations.current.create.options[createOptionKeys[i]]
              ?.pollQuestionId === id
          ) {
            dataByOperations.current.create.options[createOptionKeys[i]] =
              undefined;
          }
        }

        const updateOptionKeys = Object.keys(
          dataByOperations.current.update.options
        );
        for (let i = 0; i > createOptionKeys.length; i++) {
          if (
            dataByOperations.current.update.options[updateOptionKeys[i]]
              ?.pollQuestionId === id
          ) {
            dataByOperations.current.update.options[updateOptionKeys[i]] =
              undefined;
          }
        }
      }
    }
  };

  const addOption = (qid) => {
    const newTid = nanoid();

    const options =
      formData?.questions?.find((item) => item.id === qid || item.tid === qid)
        ?.options || [];
    const orders = options?.map((item) => item.order);
    const maxOrder = orders?.length ? Math.max(...orders) : 0;

    setFormData((oldData) => {
      const newData = { ...oldData };
      if (!newData.questions?.length) {
        newData.questions = [];
      }
      newData.questions
        ?.find((item) => item.id === qid || item.tid === qid)
        ?.options.push({
          tid: newTid,
          order: maxOrder + 1,
          text: "Новый ответ",
        });

      return newData;
    });
    if (!isNew.current) {
      const isNewQuestion = !!dataByOperations.current.create.questions[qid];
      if (isNewQuestion) {
        dataByOperations.current.create.questions[qid] = {
          ...dataByOperations.current.create.questions[qid],
          options: [
            ...dataByOperations.current.create.questions?.[qid]?.options,
            {
              tid: newTid,
              order: maxOrder + 1,
              text: "Новый ответ",
              pollQuestionId: qid,
            },
          ],
        };
      } else {
        dataByOperations.current.create.options[newTid] = {
          pollQuestionId: qid,
          tid: newTid,
          order: maxOrder + 1,
          text: "Новый ответ",
        };
      }
    }
  };

  const deleteOption = (qid, oid) => {
    setFormData((oldData) => {
      const newData = { ...oldData };
      let oldOptions = newData.questions.find(
        (item) => item.id === qid || item.tid === qid
      )?.options;
      oldOptions = oldOptions.filter(
        (item) => item.id !== oid && item.tid !== oid
      );
      newData.questions.find(
        (item) => item.id === qid || item.tid === qid
      ).options = oldOptions;
      return newData;
    });
    if (!isNew.current) {
      const newQuestion = !!dataByOperations.current.create.questions[qid];

      if (newQuestion) {
        dataByOperations.current.create.questions[qid].options = [
          ...dataByOperations.current.create.questions[qid]?.options.filter(
            (item) => item.id !== oid && item.tid !== oid
          ),
        ];
      } else {
        const newOption = !!dataByOperations.current.create.options[oid];
        if (newOption) {
          dataByOperations.current.create.options[oid] = undefined;
        } else {
          dataByOperations.current.remove.options[oid] = oid;
        }
      }
    }
  };

  const onFormItemValueChanged = (e) => {
    const value =
      e.value !== undefined ? e.value : e?.component?.option("text");
    const dataField = e.component.option("dataField");
    if (dataField) {
      setFormDataValue(dataField, value);
    }
  };

  const setFormDataValue = (dataField, value) => {
    set(formData, dataField, value);
    set(changedFormData, dataField, value);
    if (!isNew.current) {
      set(dataByOperations.current.update.poll, dataField, value);
      dataByOperations.current.update.poll.id = editedRow.id;
    }
  };

  return (
    <React.Fragment>
      <Popup
        className="customEditorPopup"
        visible={true}
        onHiding={closePopup}
        titleComponent={() => {
          return <span className="popup-title">{popupOptions.title}</span>;
        }}
        {...popupOptions}
      >
        <Position my="center" at="center" of={window} />
        <ScrollView showScrollbar="always">
          <Form validationGroup="formEditor">
            <SimpleItem>
              <div className="editor-content">
                <div className="dx-form-group-with-caption dx-form-group editor-group">
                  <div className="dx-form-group-caption">Общие сведения</div>
                  <div className="dx-form-group-content editor-content">
                    <div className="editor-item">
                      <div className="dx-field">
                        <div className="dx-field-label">Название</div>
                        <div className="dx-field-value">
                          <TextBox
                            defaultValue={formData.title}
                            onInput={onFormItemValueChanged}
                            dataField="title"
                          >
                            <Validator validationGroup="formEditor">
                              <RequiredRule />
                            </Validator>
                          </TextBox>
                        </div>
                      </div>

                      <div className="dx-field">
                        <div className="dx-field-label">Время начала</div>
                        <div className="dx-field-value">
                          <DateBox
                            defaultValue={formData.startTimeUtc}
                            onValueChanged={(e) => {
                              let value =
                                e.value !== undefined
                                  ? e.value
                                  : e?.component?.option("text");
                              value =
                                new Date(value).toISOString().split(".")[0] +
                                "Z";
                              const dataField = e.component.option("dataField");
                              setFormDataValue(dataField, value);
                            }}
                            dataField="startTimeUtc"
                          >
                            <Validator validationGroup="formEditor">
                              <RequiredRule />
                            </Validator>
                          </DateBox>
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">Время окончания</div>
                        <div className="dx-field-value">
                          <DateBox
                            defaultValue={formData.endTimeUtc}
                            onValueChanged={(e) => {
                              let value =
                                e.value !== undefined
                                  ? e.value
                                  : e?.component?.option("text");
                              value =
                                new Date(value).toISOString().split(".")[0] +
                                "Z";
                              const dataField = e.component.option("dataField");
                              setFormDataValue(dataField, value);
                            }}
                            dataField="endTimeUtc"
                          >
                            <Validator validationGroup="formEditor">
                              <RequiredRule />
                            </Validator>
                          </DateBox>
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">
                          Показывать результаты
                        </div>
                        <div className="dx-field-value">
                          <CheckBox
                            defaultValue={formData.showResultAfterAnswer}
                            onValueChanged={onFormItemValueChanged}
                            dataField="showResultAfterAnswer"
                          >
                            <Validator validationGroup="formEditor">
                              <CustomRule
                                type={"custom"}
                                validationCallback={(e) => {
                                  return isBoolean(e.value);
                                }}
                                message="Поле необходимо заполнить"
                              />
                            </Validator>
                          </CheckBox>
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">Теги</div>
                        <div className="dx-field-value">
                          <TagBox
                            defaultValue={formData.tagIds}
                            onValueChanged={onFormItemValueChanged}
                            dataSource={{
                              store: tagsDataStore,
                              postProcess: (tags) => {
                                return tags.filter((tag) =>
                                  tag?.entityTypeIds?.includes(64)
                                );
                              },
                            }}
                            valueExpr={"id"}
                            displayExpr={"name"}
                            showClearButton={true}
                            dataField="tagIds"
                            searchEnabled={true}
                          ></TagBox>
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">Описание</div>
                        <div className="dx-field-value">
                          <TextArea
                            defaultValue={formData.content}
                            onInput={onFormItemValueChanged}
                            dataField="content"
                          >
                            <Validator validationGroup="formEditor">
                              <RequiredRule />
                            </Validator>
                          </TextArea>
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">Примечание</div>
                        <div className="dx-field-value">
                          <TextBox
                            defaultValue={formData.note}
                            onInput={onFormItemValueChanged}
                            dataField="note"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="dx-form-group-with-caption dx-form-group editor-group">
                  <div className="dx-form-group-caption">Вопросы</div>
                  <div className="dx-form-group-content editor-content">
                    <div className="dx-field">
                      <DxButton text="Добавить вопрос" onClick={addQuestion} />
                    </div>
                    {formData?.questions?.map((question) => {
                      const options = question.options || [];
                      const qid = question?.tid || question?.id;
                      return (
                        <div className="editor-item" key={qid}>
                          <fieldset className="flex-nowrap">
                            <legend className="editor-item-title">
                              Вопрос
                            </legend>
                            <div className="dx-field">
                              <div className="dx-field-label">Содержание</div>
                              <div className="dx-field-value">
                                <TextBox
                                  defaultValue={question.text}
                                  onInput={(e) => {
                                    const value =
                                      e.value !== undefined
                                        ? e.value
                                        : e?.component?.option("text");
                                    const dataField =
                                      e.component.option("dataField");
                                    onQuestionDataChanged(
                                      qid,
                                      dataField,
                                      value
                                    );
                                  }}
                                  dataField="text"
                                />
                              </div>
                            </div>
                            <div className="dx-field">
                              <div className="dx-field-label">
                                Множественный выбор
                              </div>
                              <div className="dx-field-value">
                                <CheckBox
                                  defaultValue={question.isMultipleChoice}
                                  onValueChanged={(e) => {
                                    const value =
                                      e.value !== undefined
                                        ? e.value
                                        : e?.component?.option("text");
                                    const dataField =
                                      e.component.option("dataField");
                                    onQuestionDataChanged(
                                      qid,
                                      dataField,
                                      value
                                    );
                                  }}
                                  dataField="isMultipleChoice"
                                >
                                  <Validator validationGroup="formEditor">
                                    <CustomRule
                                      type={"custom"}
                                      validationCallback={(e) => {
                                        return isBoolean(e.value);
                                      }}
                                      message="Поле необходимо заполнить"
                                    />
                                  </Validator>
                                </CheckBox>
                              </div>
                            </div>
                            <div className="dx-field">
                              <div className="dx-field-label">Свой вариант</div>
                              <div className="dx-field-value">
                                <CheckBox
                                  defaultValue={question.isOwnAnswerPossible}
                                  onValueChanged={(e) => {
                                    const value =
                                      e.value !== undefined
                                        ? e.value
                                        : e?.component?.option("text");
                                    const dataField =
                                      e.component.option("dataField");
                                    const qid = question?.tid || question?.id;
                                    onQuestionDataChanged(
                                      qid,
                                      dataField,
                                      value
                                    );
                                  }}
                                  dataField="isOwnAnswerPossible"
                                >
                                  <Validator validationGroup="formEditor">
                                    <CustomRule
                                      type={"custom"}
                                      validationCallback={(e) => {
                                        return isBoolean(e.value);
                                      }}
                                      message="Поле необходимо заполнить"
                                    />
                                  </Validator>
                                </CheckBox>
                              </div>
                            </div>
                            <div className="dx-field">
                              <DxButton
                                text="Добавить ответ"
                                onClick={() => {
                                  addOption(qid);
                                }}
                              />
                            </div>
                            <div className="dx-field" key={options?.length}>
                              <DataGrid
                                dataSource={options}
                                errorRowEnabled={false}
                                onRowUpdating={(e) => {
                                  const oid = e.oldData.tid || e.oldData.id;
                                  onOptionDataChanged(qid, oid, e.newData);
                                  e.cancel = true;
                                  e.component.cancelEditData();
                                  e.component.clearSelection();
                                }}
                                onRowRemoving={(e) => {
                                  const oid = e.data.tid || e.data.id;
                                  deleteOption(qid, oid);
                                  e.cancel = true;
                                }}
                                cacheEnabled={false}
                              >
                                <Editing
                                  allowUpdating={true}
                                  allowDeleting={true}
                                  allowAdding={false}
                                  mode="row"
                                  useIcons={true}
                                  d
                                />
                                <Column
                                  type="string"
                                  dataField="text"
                                  caption="Название"
                                  allowEditing={true}
                                />
                              </DataGrid>
                            </div>
                            <div className="dx-field">
                              <DxButton
                                text="Удалить вопрос"
                                onClick={() => {
                                  deleteQuestion(qid);
                                }}
                              />
                            </div>
                          </fieldset>
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
            </SimpleItem>
          </Form>
        </ScrollView>
        <ValidationSummary
          id="summary"
          validationGroup="formEditor"
        ></ValidationSummary>
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Сохранить", onClick: onSave }}
          toolbar="bottom"
        />
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Отмена", onClick: onCancel }}
          toolbar="bottom"
        />
      </Popup>
    </React.Fragment>
  );
};

export default EditorPopup;
