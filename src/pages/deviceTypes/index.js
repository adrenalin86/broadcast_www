import React, {useEffect, useRef, useState} from 'react';
import {
    getDeviceTypesDataStore,
    getSystemWindowsDataStore,
    WINDOWS
} from "../../dataStores";
import DataGrid, {Button, Column, ColumnChooser, StateStoring} from "devextreme-react/data-grid";
import notify from "devextreme/ui/notify";
import config from "../../config";
import EditorPopupDeviceTypes from "./editorPopup";
import {isEmpty} from "lodash-es";
import {Popup, Position, ToolbarItem} from "devextreme-react/popup";
import DataSource from "devextreme/data/data_source";
import {connect} from "react-redux";
import {deauthorize} from "../../store/commonSlice";
import {activeIcon} from "../../utils/utils";
import ColumnForDevices from "../smartStations/ColumnForDevices";

const windowId = WINDOWS.DEVICE_TYPES.id;

const roadDeviceTypes = getDeviceTypesDataStore({ windowId: windowId });
const roadDeviceTypesDataStore = new DataSource({
    store: roadDeviceTypes,
    reshapeOnPush: true,
});

function DeviceTypesPage({localDeauthorize}) {
    const [windowTitle, setWindowTitle] = useState('');
    const [editedRow, setEditedRow] = useState({});
    const [editorPopupVisible, setEditorPopupVisible] = useState(false);
    const [deletedRowKey, setDeletedRowKey] = useState(null);
    const [deleteRowPopupVisible, setDeleteRowPopupVisible] = useState(false);
    const gridOptions = config.pages.deviceType.grid.dxOptions;

    const closeDeleteRowPopup = () => {
        setDeleteRowPopupVisible(false);
    };

    const closeEditorPopup = () => {
        setEditorPopupVisible(false);
    };

    const openDeleteRowPopup = (e) => {
        setEditedRow({});
        setDeletedRowKey(e.row.key);
        setDeleteRowPopupVisible(true);
    };

    const systemWindowsDataStore = getSystemWindowsDataStore({
        windowId: windowId,
    });

    const datagridRef = useRef(null);

    function onDataErrorOccurred(e) {
        notify(e.error?.message, "error", config.common.errorMessageLifespan);
        if (e.error?.data?.reason === "noAuth") {
            localDeauthorize();
        }
    }

    function onToolbarPreparing(e) {
        let toolbarItems = e.toolbarOptions.items;

        toolbarItems.unshift({
            widget: "dxButton",
            options: {
                icon: "add",
            },
            location: "before",
            onClick: openCreateEntryPopup,
        });
    }

    useEffect(() => {
        systemWindowsDataStore.byKey(windowId).then((result) => {
            const windowName = result[0]?.name?.split("/").pop();

            setWindowTitle(windowName);
        });
    }, []);


    const openCreateEntryPopup = () => {
        setEditedRow({});
        setEditorPopupVisible(true);
    };

    const createEntry = (e) => {
        roadDeviceTypesDataStore
            .store()
            .insert(e.formData)
            .then((e) => {
                closeEditorPopup();
                datagridRef?.current?.instance?.refresh();
            })
            .catch((error) => {
                onDataErrorOccurred({ error });
                closeEditorPopup();
            });
    };

    const updateEntry = (e) => {
        if (!isEmpty(e.formData)) {
            roadDeviceTypesDataStore
                .store()
                .update(e.editedRowId, e.formData)
                .then(() => {
                    closeEditorPopup();
                    datagridRef?.current?.instance?.refresh();
                })
                .catch((error) => {
                    onDataErrorOccurred({ error });
                    closeEditorPopup();
                });
        } else {
            closeEditorPopup();
        }
    };

    const openEditorPopup = (e) => {
        const currentRow = roadDeviceTypesDataStore.items().find((item) => item.id === e.row.key);

        if (!isEmpty(currentRow)) {
            setEditedRow(currentRow);
            setEditorPopupVisible(true);
        }
    };

    const deleteEntry = (e) => {
        roadDeviceTypesDataStore
            .store()
            .remove(deletedRowKey)
            .then(() => {
                closeDeleteRowPopup();
                datagridRef?.current?.instance?.refresh();
            })
            .catch((error) => {
                onDataErrorOccurred({ error });
                closeDeleteRowPopup();
            });
    };

    return (
        <React.Fragment>
            <h2 className={"content-block"}>{windowTitle}</h2>
            {editorPopupVisible && (
                <EditorPopupDeviceTypes
                    editedRow={editedRow}
                    windowId={windowId}
                    closeEditorPopup={closeEditorPopup}
                    updateEntry={updateEntry}
                    createEntry={createEntry}
                />
            )}
            {deleteRowPopupVisible && (
                <Popup
                    className="deleteRowPopup"
                    width="auto"
                    height="auto"
                    visible={true}
                    showCloseButton={false}
                    showTitle={false}
                    onHiding={closeDeleteRowPopup}
                >
                    <Position my="center" at="center" of={window} />
                    <div>
                        <span>Вы уверены, что хотите удалить эту запись?</span>
                    </div>
                    <ToolbarItem
                        widget="dxButton"
                        location="center"
                        options={{ text: "Да", onClick: deleteEntry }}
                        toolbar="bottom"
                    />
                    <ToolbarItem
                        widget="dxButton"
                        location="center"
                        options={{ text: "Нет", onClick: closeDeleteRowPopup }}
                        toolbar="bottom"
                    />
                </Popup>
            )}
            <div className={"content-block"}>
                <div className={"dx-card responsive-paddings"}>
                    <DataGrid
                        id="displayEditorDataGrid"
                        ref={datagridRef}
                        dataSource={roadDeviceTypesDataStore}
                        onDataErrorOccurred={onDataErrorOccurred}
                        onToolbarPreparing={onToolbarPreparing}
                        errorRowEnabled={false}
                        onContentReady={(e) => {
                            e.component.state().columns.forEach((col, index) => {
                                e.component.columnOption(col.name, "visibleIndex", index);
                            });
                        }}
                        {...gridOptions}
                    >
                        <StateStoring
                            enabled={true}
                            type="localStorage"
                            storageKey="pages.deviceTypes.displaysGrid"
                        />
                        <ColumnChooser enabled={true} />
                        <Column allowSorting dataField="id" visible={false} />
                        <Column allowSorting type="string" dataField="name" caption="Название" />
                        <Column
                            allowSorting
                            dataField="iconName"
                            dataType="string"
                            width={200}
                            caption="Иконка"
                            cellComponent={(e) => {
                                if (e.data?.value) {
                                    return (
                                        <img src={`ico/deviceTypes/${e.data?.value}.svg`} alt={e.data?.value}/>
                                    );
                                } else {
                                    return <div>Иконка отсутствует</div>
                                }
                            }}
                        />
                        <Column type="buttons">
                            <Button name="custom-edit" text="Изменить" icon="edit" onClick={openEditorPopup}/>
                            <Button
                                name="custom-delete"
                                text="Удалить"
                                icon="trash"
                                onClick={openDeleteRowPopup}
                            />
                        </Column>
                    </DataGrid>
                </div>
            </div>
        </React.Fragment>
    );
}

export default connect((state) => ({}), {
    localDeauthorize: deauthorize,
})(DeviceTypesPage);

