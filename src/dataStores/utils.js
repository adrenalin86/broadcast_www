import { CustomDataError } from "../utils";

const handleResponseErrors = (response) => {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
};

const handleDataErrors = (data) => {
  if (!data.r || data.r !== "ok") {
    let reason = "Ошибка приложения";
    let errorData = {};

    if (!data.data) {
      switch (data.r) {
        case "denied":
          reason = "Доступ запрещён";
          break;
        case "no auth":
          reason = "Ошибка авторизации, повторите вход";
          errorData.reason = "noAuth";
          break;
        default:
          reason = "Ошибка на сервере";
      }
    } else {
      if (data.data.reason) {
        if (data.data.reason === "server error") {
          reason = "Ошибка на сервере";
        }
        if (data.data.reason === "no access to window") {
          reason = "Запрещён доступ к окну";
        }
        if (data.data.reason === "no access to region") {
          reason = "Запрещён доступ к региону";
        }
        //TODO: получить список возможных причин и подготовить соответствующие строки.
      } else {
        reason = "Ошибка на сервере";
      }
    }

    throw new CustomDataError(reason, errorData);
  }

  return data;
};

const handleCaughtErrors = (error) => {
  let modifiedError = error;

  if (modifiedError.name !== "CustomFetchDataError") {
    if (modifiedError.name === "TypeError") {
      if (modifiedError.message.startsWith("NetworkError")) {
        modifiedError.message = "Ошибка связи с сервером";
      }
    }
  }

  return modifiedError;
};

export { handleResponseErrors, handleDataErrors, handleCaughtErrors };
