import React from "react";

import { TextBox, SelectBox } from "devextreme-react";
import Validator, { RequiredRule } from "devextreme-react/validator";

import { get } from "lodash-es";

const authTypes = ["basic", "digest", "none"];

const GenericHttp = ({
  formData,
  onFormItemValueChanged,
  snapshotUrlDataField,
  videoStreamUrlDataField,
  authTypeDataField,
  authUserDataField,
  authPassDataField,
}) => {
  const currentValue = {
    snapshotUrl: get(formData, snapshotUrlDataField),
    videoStreamUrl: get(formData, videoStreamUrlDataField),
    authType: get(formData, authTypeDataField),
    authUser: get(formData, authUserDataField),
    authPass: get(formData, authPassDataField),
  };

  return (
    <div className="dx-form-group-content editor-content">
      <div className="editor-item">
        <div className="dx-field">
          <div className="dx-field-label item-label">URL для получения текущего кадра</div>
          <div className="dx-field-value">
            <TextBox
              dataField={snapshotUrlDataField}
              defaultValue={currentValue.snapshotUrl}
              onInput={onFormItemValueChanged}
            />
          </div>
        </div>
      </div>
      <div className="editor-item">
        <div className="dx-field">
          <div className="dx-field-label item-label">URL для получения видеопотока</div>
          <div className="dx-field-value">
            <TextBox
              dataField={videoStreamUrlDataField}
              defaultValue={currentValue.videoStreamUrl}
              onInput={onFormItemValueChanged}
            />
          </div>
        </div>
      </div>
      <div className="editor-item cols-2">
        <div className="dx-field">
          <div className="dx-field-label item-label">Тип авторизации</div>
          <div className="dx-field-value">
            <SelectBox
              dataField={authTypeDataField}
              defaultValue={currentValue.authType}
              dataSource={authTypes}
              onValueChanged={onFormItemValueChanged}
            >
              <Validator validationGroup="formEditor">
                <RequiredRule />
              </Validator>
            </SelectBox>
          </div>
        </div>
        <div className="dx-field">
          <div className="dx-field-label item-label">Пользователь</div>
          <div className="dx-field-value">
            <TextBox
              dataField={authUserDataField}
              defaultValue={currentValue.authUser}
              onInput={onFormItemValueChanged}
            />
          </div>
        </div>
        <div className="dx-field">
          <div className="dx-field-label item-label">Пароль</div>
          <div className="dx-field-value">
            <TextBox
              dataField={authPassDataField}
              defaultValue={currentValue.authPass}
              onInput={onFormItemValueChanged}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default GenericHttp;
