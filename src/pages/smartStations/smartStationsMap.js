import React, { useState, useEffect, useRef } from "react";

import Leaflet from "leaflet";

import { isEmpty } from "lodash-es";

import config from "../../config";

import {getSmartStationsDataStore, WINDOWS} from "../../dataStores";

import { LoadIndicator } from "devextreme-react";

const windowId = WINDOWS.SMART_STATIONS.id;

const roadSmartStations = getSmartStationsDataStore({
  windowId: windowId,
});

const AUTOMATIC_UPDATE_TIMEOUT_MS = config.pages.smartStations.grid.updateInterval;
const startingCenter = config.pages.smartStations.map.startingCenter;
const tooltipThreshold = config.pages.smartStations.map.zoomThreshold;

const SmartStationsMap = ({ onDataErrorOccurred, counter, openViewPopupFromMap }) => {
  const [myMap, setMyMap] = useState({});
  const [markersLayerGroup, setMarkersLayerGroup] = useState({});
  const [displays, setDisplays] = useState([]);
  const [isLoadedMarkers, setIsLoadedMarkers] = useState(false);

  const mapRef = useRef(null);

  const blueIcon = new Leaflet.Icon({
    iconUrl: `ico/marker-icon-2x-blue.png`,
    shadowUrl: `ico/marker-shadow.png`,
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41],
  });

  const setMarkersFromDisplays = (displays) => {
    if (mapRef.current) {
      if (!isEmpty(myMap)) {
        if (!isEmpty(markersLayerGroup)) {
          let newMarkers = [];

          for (let i = 0; i < displays.length; i++) {
            if (displays[i].status?.connected === 1) {
              const marker = Leaflet.marker([displays[i].lat, displays[i].lng], {
                icon: blueIcon
              });

              newMarkers.push(marker);
            } else {
              const marker = Leaflet.marker([displays[i].lat, displays[i].lng], {
                icon: blueIcon
              });

              newMarkers.push(marker);
            }
          }

          markersLayerGroup.clearLayers();

          for (let i = 0; i < newMarkers.length; i++) {
            markersLayerGroup.addLayer(newMarkers[i]);
          }

          const onLayerClick = (e) => {
            let lat = e.latlng.lat;
            let lng = e.latlng.lng;

            const result = displays.filter(
                (el) => el.lat === lat && el.lng === lng
            );

            let [clickedMarker] = result;

            openViewPopupFromMap(clickedMarker.id);
          };


          markersLayerGroup.eachLayer(function (layer) {
            let lat = layer._latlng.lat;
            let lng = layer._latlng.lng;

            displays.forEach((display) => {
              if (display.lat === lat && display.lng === lng) {
                layer
                  .bindTooltip(display.name, {
                    permanent: false,
                    direction: "bottom",
                    className: "marker",
                    offset: [1, 1],
                  })
                  .openTooltip();
              }
            });
            layer.on("click", onLayerClick);
          });

          let lastZoom = myMap.getZoom();

          if (lastZoom < tooltipThreshold) {
            myMap.eachLayer(function (l) {
              if (l.getTooltip()) {
                let tooltip = l.getTooltip();

                l.unbindTooltip().bindTooltip(tooltip, {
                  permanent: false,
                });
              }
            });
          }

          myMap.on("zoomend", function () {
            let zoom = myMap.getZoom();

            if (zoom < tooltipThreshold && (!lastZoom || lastZoom >= tooltipThreshold)) {
              myMap.eachLayer(function (l) {
                if (l.getTooltip()) {
                  let tooltip = l.getTooltip();

                  l.unbindTooltip().bindTooltip(tooltip, {
                    permanent: false,
                  });
                }
              });
            } else if (zoom >= tooltipThreshold && (!lastZoom || lastZoom < tooltipThreshold)) {
              myMap.eachLayer(function (l) {
                if (l.getTooltip()) {
                  let tooltip = l.getTooltip();

                  l.unbindTooltip().bindTooltip(tooltip, {
                    permanent: true,
                  });
                }
              });
            }

            lastZoom = zoom;
          });

          let markers = markersLayerGroup.getLayers().length;

          const checkIsLoadedMarkers = () => {
            if (--markers === 0) {
              setIsLoadedMarkers(true);
            }
          };

          markersLayerGroup.eachLayer(function (layer) {
            layer._icon.addEventListener("load", () => {
              checkIsLoadedMarkers();
            });
          });
          setIsLoadedMarkers(true)
        } else {
          const layerGroup = Leaflet.layerGroup([]).addTo(myMap);

          setMarkersLayerGroup(layerGroup);
        }
      }
    }
  };

  const fetchDisplays = () => {
    roadSmartStations
      .load()
      .then((result) => {
        setDisplays(result?.data || []);
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
      });
  };

  // Обновление карты
  useEffect(() => {
    setMarkersFromDisplays(displays);
  }, [displays, markersLayerGroup]);

  useEffect(() => {
    const timer = setInterval(() => {
      if (displays) {
        fetchDisplays();
      }
    }, AUTOMATIC_UPDATE_TIMEOUT_MS);

    return () => {
      clearInterval(timer);
    };
  }, []);

  useEffect(() => {
    fetchDisplays();
  }, [counter]);

  useEffect(() => {
    if (isEmpty(myMap)) {
      let map = Leaflet.map(mapRef.current, {
        center: {
          lat: startingCenter.lat,
          lng: startingCenter.lng,
        },
        zoom: 13,
      });

      Leaflet.tileLayer("//maps.bus62.ru/getTile.php?z={z}&x={x}&y={y}&e=@2x.png", {
        attribution:
          '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>',
      }).addTo(map);

      setMyMap(map);
    }
  }, []);

  return (
    <>
      <div className="map-editor">
        <div
          ref={mapRef}
          className="map-container"
          style={
            isLoadedMarkers
              ? {
                  height: "calc(100vh - 347px)",
                  width: "100%",
                  minWidth: "260px",
                  marginTop: "20px",
                  position: "relative",
                }
              : {
                  width: "100%",
                  height: "100%",
                  position: "absolute",
                  top: "-9999px",
                }
          }
        />
      </div>
      {!isLoadedMarkers && (
        <div
          style={{
            display: "flex",
            flexWrap: "wrap",
            height: "calc(100vh - 347px)",
            width: "100%",
            minWidth: "260px",
            overflowY: "auto",
            marginTop: "20px",
          }}
        >
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              width: "100%",
              height: "100%",
            }}
          >
            <LoadIndicator
              id="large-indicator"
              className="button-indicator"
              height={40}
              width={40}
              visible={!isLoadedMarkers}
            />
          </div>
        </div>
      )}
    </>
  );
};

export default SmartStationsMap;
