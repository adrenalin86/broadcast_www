import { useEffect, useState } from "react";
import { connect } from "react-redux";
import { LoadIndicator, Popup, TextBox, ValidationSummary } from "devextreme-react";
import Form, { SimpleItem } from "devextreme-react/form";
import { Position, ToolbarItem } from "devextreme-react/popup";
import Validator, { RequiredRule } from "devextreme-react/validator";
import validationEngine from "devextreme/ui/validation_engine";
import { set } from "lodash-es";
import { deauthorize } from "../../store/commonSlice";
import config from "../../config";
import { getSpecCompaniesDataStore, WINDOWS } from "../../dataStores";

const windowId = WINDOWS.SPEC_COMPANIES.id;

const specCompaniesDataStore = getSpecCompaniesDataStore({
  windowId: windowId,
});

const specCompaniesPopupOptions =
  config.pages.specCompanies.specCompaniesGrid.popupSpecCompanyEditor.dxOptions;

const SpecCompaniesEditorPopup = ({
  specCompanyId,
  createEntry,
  updateEntry,
  closeEditorPopup,
  onDataErrorOccurred,
}) => {
  const [formData, setFormData] = useState({});
  const [changedFormData] = useState({});
  const [isLoadedData, setIsLoadedData] = useState(false);

  const setFormDataValue = (dataField, value) => {
    set(formData, dataField, value);
    set(changedFormData, dataField, value);
  };

  const onFormItemValueChanged = (e) => {
    const value = e.value !== undefined ? e.value : e?.component?.option("text");
    const dataField = e.component.option("dataField");

    setFormDataValue(dataField, value);
  };

  const onSave = () => {
    const isValid = validationEngine.validateGroup("formEditor").isValid;

    if (isValid) {
      if (!specCompanyId) {
        createEntry(formData);
      } else {
        updateEntry({
          formData: changedFormData,
          editedRowId: formData.id,
        });
      }
    }
  };

  useEffect(() => {
    if (specCompanyId) {
      specCompaniesDataStore
        .byKey(specCompanyId)
        .then((result) => {
          const [specTransport] = result;

          setFormData(specTransport);
          setIsLoadedData(true);
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
        });
    } else {
      setIsLoadedData(true);
    }
  }, []);

  return (
    <Popup
      visible={true}
      onHiding={closeEditorPopup}
      titleComponent={() => {
        return <span className="popup-title">{specCompaniesPopupOptions.title}</span>;
      }}
      className="customEditorPopup"
      {...specCompaniesPopupOptions}
    >
      <Position my="center" at="center" of={window} />
      {!isLoadedData && (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "100%",
          }}
        >
          <LoadIndicator
            id="large-indicator"
            className="button-indicator m-v-65"
            height={60}
            width={60}
            visible={!isLoadedData}
          />
        </div>
      )}
      <Form validationGroup="formEditor">
        {isLoadedData && (
          <SimpleItem>
            <div className="editor-content">
              <div className="dx-form-group-with-caption dx-form-group editor-group">
                <div className="editor-item">
                  <div className="dx-field">
                    <div className="dx-field-label">Название</div>
                    <div className="dx-field-value">
                      <TextBox
                        dataField="name"
                        defaultValue={formData.name}
                        onInput={onFormItemValueChanged}
                      >
                        <Validator validationGroup="formEditor">
                          <RequiredRule />
                        </Validator>
                      </TextBox>
                    </div>
                  </div>
                </div>
                <div className="editor-item">
                  <div className="dx-field">
                    <div className="dx-field-label">Адрес</div>
                    <div className="dx-field-value">
                      <TextBox
                        dataField="address"
                        defaultValue={formData.address}
                        onInput={onFormItemValueChanged}
                      >
                        <Validator validationGroup="formEditor">
                          <RequiredRule />
                        </Validator>
                      </TextBox>
                    </div>
                  </div>
                </div>
                <div className="editor-item">
                  <div className="dx-field">
                    <div className="dx-field-label">ИНН</div>
                    <div className="dx-field-value">
                      <TextBox
                        dataField="inn"
                        defaultValue={formData.inn}
                        onInput={onFormItemValueChanged}
                      />
                    </div>
                  </div>
                </div>
                <div className="editor-item">
                  <div className="dx-field">
                    <div className="dx-field-label">ОГРН</div>
                    <div className="dx-field-value">
                      <TextBox
                        dataField="ogrn"
                        defaultValue={formData.ogrn}
                        onInput={onFormItemValueChanged}
                      />
                    </div>
                  </div>
                </div>
                <div className="editor-item">
                  <div className="dx-field">
                    <div className="dx-field-label">Заметки</div>
                    <div className="dx-field-value">
                      <TextBox
                        dataField="note"
                        defaultValue={formData.note}
                        onInput={onFormItemValueChanged}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </SimpleItem>
        )}
      </Form>
      <ValidationSummary id="summary" validationGroup="formEditor" />
      <ToolbarItem
        widget="dxButton"
        location="after"
        toolbar="bottom"
        options={{ text: "Сохранить", onClick: onSave }}
        disabled={!isLoadedData}
      />
      <ToolbarItem
        widget="dxButton"
        location="after"
        toolbar="bottom"
        options={{ text: "Отменить", onClick: closeEditorPopup }}
      />
    </Popup>
  );
};

export default connect(null, {
  localDeauthorize: deauthorize,
})(SpecCompaniesEditorPopup);
