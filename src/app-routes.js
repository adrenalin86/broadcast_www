import { withNavigationWatcher } from "./contexts/navigation";
import {
  DisplayModelsPage,
  DisplaysPage,
  UserActionsPage,
  UsersPage,
  DisplayGroupsPage,
  DisplayRegistryHistoryPage,
  GroupsRegistryHistoryPage,
  DisplayModelsRegistryHistoryPage,
  UsersRegistryHistoryPage,
  DisplayProtocolsPage,
  BroadcastProgramsPage,
  DisplayProtocolsRegistryHistoryPage,
  DefaultPage,
  BroadcastProgramTemplatesPage,
  BroadcastProgramTemplatesRegistryHistoryPage,
  DisplayWorkingRegistryHistoryPage,
  BroadcastProgramShowRegistryHistoryPage,
  TagsPage,
  TagsRegistryHistoryPage,
  BroadcastRequestForDisplayInfoPage,
  AreasEditorPage,
  PinCodesPage,
  WebNewsPage,
  WebClaimsPage,
  PollsPage,
  RoadEventsPage,
  VehicleRoutesPage,
  VehiclesPage,
  ControlCenterPage,
  VideoCamerasPage,
  VideoCamerasRegistryHistoryPage,
  SpecTransportTypesPage,
  SpecCompaniesPage,
  SpecTransportsPage, DeviceModelsRegistryHistoryPage, SmartStationsGroupsRegistryHistoryPage,
} from "./pages";
import { WINDOWS, ACCESS_TYPES } from "./dataStores";
import DeviceTypesPage from "./pages/deviceTypes";
import DevicesPage from "./pages/devices";
import SmartStationsPage from "./pages/smartStations";
import {
  DeviceTypesRegistryHistoryPage,
  SmartStationsRegistryHistoryPage,
  DevicesRegistryHistoryPage
} from "./pages/registryHistory/registryHistory";

const routes = [
  {
    path: "/specTransports",
    component: SpecTransportsPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.SPEC_TRANSPORTS.id,
    },
  },
  {
    path: "/specCompanies",
    component: SpecCompaniesPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.SPEC_COMPANIES.id,
    },
  },
  {
    path: "/specTransportTypes",
    component: SpecTransportTypesPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.SPEC_TRANSPORT_TYPES.id,
    },
  },
  {
    path: "/videoCameras",
    component: VideoCamerasPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.VIDEOCAMERAS.id,
    },
  },
  {
    path: "/displays",
    component: DisplaysPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.DISPLAYS.id,
    },
  },
  {
    path: "/displayModels",
    component: DisplayModelsPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.DISPLAY_MODELS.id,
    },
  },
  {
    path: "/controlCenter",
    component: ControlCenterPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.CONTROL.id,
    },
  },
  {
    path: "/displayProtocols",
    component: DisplayProtocolsPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.DISPLAY_PROTOCOLS.id,
    },
  },
  {
    path: "/reports/userActions",
    component: UserActionsPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.REPORTS_USER_ACTIONS.id,
    },
  },
  {
    path: "/users",
    component: UsersPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.USERS.id,
    },
  },
  {
    path: "/groups",
    component: DisplayGroupsPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.DISPLAY_GROUPS.id,
    },
  },
  {
    path: "/broadcastPrograms",
    component: BroadcastProgramsPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.BROADCAST_PROGRAMS.id,
    },
  },
  {
    path: "/broadcastProgramTemplates",
    component: BroadcastProgramTemplatesPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.BROADCAST_PROGRAM_TEMPLATES.id,
    },
  },
  {
    path: "/broadcastRequestForDisplayInfo",
    component: BroadcastRequestForDisplayInfoPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.BROADCAST_INFORMATION_DISPLAY_REQUESTS.id,
    },
  },
  {
    path: "/registryHistory/displays",
    component: DisplayRegistryHistoryPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.REPORTS_REGISTRY_HISTORY_DISPLAYS.id,
    },
  },
  {
    path: "/registryHistory/displayModels",
    component: DisplayModelsRegistryHistoryPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.REPORTS_REGISTRY_HISTORY_DISPLAY_MODELS.id,
    },
  },
  {
    path: "/registryHistory/displayProtocols",
    component: DisplayProtocolsRegistryHistoryPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.REPORTS_REGISTRY_HISTORY_DISPLAY_PROTOCOLS.id,
    },
  },
  {
    path: "/registryHistory/groups",
    component: GroupsRegistryHistoryPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.REPORTS_REGISTRY_HISTORY_DISPLAY_GROUPS.id,
    },
  },
  {
    path: "/registryHistory/users",
    component: UsersRegistryHistoryPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.REPORTS_REGISTRY_HISTORY_USERS.id,
    },
  },
  {
    path: "/registryHistory/broadcastProgramTemplates",
    component: BroadcastProgramTemplatesRegistryHistoryPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.REPORTS_REGISTRY_HISTORY_BROADCAST_PROGRAM_TEMPLATES.id,
    },
  },
  {
    path: "/registryHistory/tags",
    component: TagsRegistryHistoryPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.REPORTS_REGISTRY_HISTORY_TAGS.id,
    },
  },
  {
    path: "/registryHistory/videoCameras",
    component: VideoCamerasRegistryHistoryPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.REPORTS_REGISTRY_HISTORY_VIDEO_CAMERAS.id,
    },
  },
  {
    path: "/reports/displayWorking",
    component: DisplayWorkingRegistryHistoryPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.REPORTS_DISPLAY_WORKING.id,
    },
  },
  {
    path: "/reports/broadcastProgramShow",
    component: BroadcastProgramShowRegistryHistoryPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.REPORTS_BROADCAST_PROGRAM_SHOW.id,
    },
  },
  {
    path: "/tags",
    component: TagsPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.TAGS.id,
    },
  },
  {
    path: "/webNews",
    component: WebNewsPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.WEB_NEWS.id,
    },
  },
  {
    path: "/webClaims",
    component: WebClaimsPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.WEB_CLAIMS.id,
    },
  },
  {
    path: "/roadEvents",
    component: RoadEventsPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.ROAD_EVENTS.id,
    },
  },
  {
    path: "/routes",
    component: VehicleRoutesPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.ROUTES.id,
    },
  },
  {
    path: "/polls",
    component: PollsPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.POLLS.id,
    },
  },
  {
    path: "/vehicles",
    component: VehiclesPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.VEHICLES.id,
    },
  },
  {
    path: "/transport/pinCodes",
    component: PinCodesPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.PIN_CODES.id,
    },
  },
  {
    path: "/transport/areas",
    component: AreasEditorPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.AREAS.id,
    },
  },
  {
    path: "/deviceTypes",
    component: DeviceTypesPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.DEVICE_TYPES.id,
    },
  },
  {
    path: "/devices",
    component: DevicesPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.DEVICES.id,
    },
  },
  {
    path: "/registryHistory/devices",
    component: DevicesRegistryHistoryPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.REPORTS_REGISTRY_HISTORY_DEVICES.id,
    },
  },
  {
    path: "/registryHistory/deviceTypes",
    component: DeviceTypesRegistryHistoryPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.REPORTS_REGISTRY_HISTORY_DEVICE_TYPES.id,
    },
  },
  {
    path: "/registryHistory/smartStations",
    component: SmartStationsRegistryHistoryPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.REPORTS_REGISTRY_SMART_STATIONS.id,
    },
  },
  {
    path: "/reports/deviceWorking",
    component: DeviceModelsRegistryHistoryPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.REPORT_DEVICES.id,
    },
  },
  {
    path: "/smartStations",
    component: SmartStationsPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.SMART_STATIONS.id,
    },
  },
  {
    path: "/smartStationsGroup",
    component: SmartStationsGroupsRegistryHistoryPage,
    access: {
      type: ACCESS_TYPES.WINDOWS,
      id: WINDOWS.SMART_STATIONS_GROUP.id,
    },
  },
  {
    path: "/",
    component: DefaultPage,
    access: { type: ACCESS_TYPES.ALL, id: null },
  },
];

export default routes.map((route) => {
  return {
    ...route,
    component: withNavigationWatcher(route.component),
  };
});
