import { SeLookup, SeCheckBox, SeNumberBox } from "../editors";

import {
  getGenericEntityDataStore as getForecastsSourceStationIdDataStore,
  GENERIC_ENTITIES,
  getSmartStationsDataStore
} from "../../dataStores";

import { isEmpty, noop } from "lodash-es";

const Component = ({
  windowId,
  data,
  onFormItemValueChanged,
  className,
  editModeEnabled = false,
  dataField,
  onError = noop,
}) => {
  const forecastsSourceStationIdDataStore = getForecastsSourceStationIdDataStore({
    windowId: windowId,
    entityName: GENERIC_ENTITIES.STATIONS,
  });

  const onValueChanged = (e) => {
    const eventDataField = e.component.option("dataField");
    const fullDataField = isEmpty(dataField)
      ? eventDataField
      : dataField?.endsWith(".")
      ? dataField + eventDataField
      : dataField + "." + eventDataField;

    e.component.option("dataField", fullDataField);
    onFormItemValueChanged(e);
  };

  return (
    <div className={"dx-form-group-with-caption dx-form-group " + className}>
      <div className="dx-form-group-caption">Настройка источника расписаний</div>
      <div className="dx-form-group-content">
        <div className="editor-item">
          <SeCheckBox
            data={data}
            onFormItemValueChanged={onValueChanged}
            editModeEnabled={editModeEnabled}
            dataField={"turnOffSchedules"}
            title={"Выключить время прибытия по расписанию"}
          />
          <SeCheckBox
            data={data}
            onFormItemValueChanged={onValueChanged}
            editModeEnabled={editModeEnabled}
            dataField={"sortByRoute"}
            title={"Сортировка по маршруту"}
          />
          <SeLookup
            data={data}
            onFormItemValueChanged={onValueChanged}
            editModeEnabled={editModeEnabled}
            dataField={"stationId"}
            title={"Привязка к остановке"}
            dataSource={{
              store: forecastsSourceStationIdDataStore,
              onLoadError: (error) => {
                onError(error);
              },
            }}
            valueExpr={"id"}
            displayExpr={(e) => {
              return `${e?.name || ""} ${e?.description ? ", " + e.description : ""}`;
            }}
            searchExpr={["name"]}
            widgetOptions={{ allowClearing: true }}
          />
        </div>
      </div>
    </div>
  );
};

export default Component;
