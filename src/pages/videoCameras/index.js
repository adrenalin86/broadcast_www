import React, { useEffect, useState } from "react";
import { connect } from "react-redux";

import notify from "devextreme/ui/notify";
import { Tabs } from "devextreme-react";
import { Popup, Position, ToolbarItem } from "devextreme-react/popup";

import { isEmpty } from "lodash-es";

import {
  WINDOWS,
  getVideoCamerasWithStatusesDataStore,
  getSystemWindowsDataStore,
} from "../../dataStores";
import { deauthorize } from "../../store/commonSlice";

import config from "../../config";

import VideoCamerasEditorPopup from "./videoCamerasEditorPopup";
import VideoCamerasCards from "./videoCamerasCards";
import VideoStreamPopup from "../../components/videoCameras/videoStreamPopup/videoStreamPopup";
import VideoCamerasMap from "./videoCamerasMap";
import VideoCamerasTable from "./videoCamerasTable";

import "./index.scss";

const windowId = WINDOWS.VIDEOCAMERAS.id;

const videoCamerasWithStatusesDataStore = getVideoCamerasWithStatusesDataStore({
  windowId: windowId,
});

const systemWindowsDataStore = getSystemWindowsDataStore({
  windowId: windowId,
});

const tabs = [
  {
    id: 0,
    text: "Таблица",
  },
  {
    id: 1,
    text: "Видеостена",
  },
  {
    id: 2,
    text: "Карта",
  },
];

const VideoCamerasPage = ({ localDeauthorize }) => {
  const [editorPopupVisible, setEditorPopupVisible] = useState(false);
  const [videoStreamPopupVisible, setVideoStreamPopupVisible] = useState(false);
  const [deleteRowPopupVisible, setDeleteRowPopupVisible] = useState(false);
  const [isVisible, setIsVisible] = useState({ table: true, cards: false, map: false });
  const [isCardsLoaded, setIsCardsLoaded] = useState(false);
  const [isMapLoaded, setIsMapLoaded] = useState(false);
  const [videoCameraId, setVideoCameraId] = useState();
  const [counter, setCounter] = useState(0);
  const [windowTitle, setWindowTitle] = useState();

  const onDataErrorOccurred = (e) => {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);

    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  };

  const openEditorPopup = (id) => {
    setVideoCameraId(id);
    setEditorPopupVisible(true);
  };

  const openVideoStreamPopup = (id) => {
    setVideoCameraId(id);
    setVideoStreamPopupVisible(true);
  };

  const openCreateEntryPopup = () => {
    setVideoCameraId(null);
    setEditorPopupVisible(true);
  };

  const openDeleteRowPopup = (id) => {
    setVideoCameraId(id);
    setDeleteRowPopupVisible(true);
  };

  const closeEditorPopup = () => {
    setEditorPopupVisible(false);
  };

  const closeDeleteRowPopup = () => {
    setDeleteRowPopupVisible(false);
  };

  const closeVideoStreamPopup = () => {
    setVideoStreamPopupVisible(false);
  };

  const refreshVideoCameras = () => {
    setCounter(counter + 1);
  };

  const createEntry = (formData) => {
    videoCamerasWithStatusesDataStore
      .insert(formData)
      .then(() => {
        closeEditorPopup();
        refreshVideoCameras();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
      });
  };

  const updateEntry = ({ formData, editedRowId }) => {
    if (!isEmpty(formData)) {
      videoCamerasWithStatusesDataStore
        .update(editedRowId, formData)
        .then(() => {
          closeEditorPopup();
          refreshVideoCameras();
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
        });
    } else {
      closeEditorPopup();
    }
  };

  const deleteEntry = () => {
    videoCamerasWithStatusesDataStore
      .remove(videoCameraId)
      .then(() => {
        closeDeleteRowPopup();
        refreshVideoCameras();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
      });
  };

  const clickOnTable = () => {
    setIsVisible({
      table: true,
      cards: false,
      map: false,
    });
  };

  const clickOnCards = () => {
    setIsVisible({
      table: false,
      cards: true,
      map: false,
    });

    setIsCardsLoaded(true);
  };

  const clickOnMap = () => {
    setIsVisible({
      table: false,
      cards: false,
      map: true,
    });

    setIsMapLoaded(true);
  };

  useEffect(() => {
    systemWindowsDataStore.byKey(windowId).then((result) => {
      const windowName = result[0]?.name?.split("/").pop();

      setWindowTitle(windowName);
    });
  }, []);

  return (
    <>
      <h2 className="content-block">{windowTitle}</h2>
      <div className="content-block">
        {editorPopupVisible && (
          <VideoCamerasEditorPopup
            videoCameraId={videoCameraId}
            closeEditorPopup={closeEditorPopup}
            updateEntry={updateEntry}
            createEntry={createEntry}
            onDataErrorOccurred={onDataErrorOccurred}
          />
        )}
        {deleteRowPopupVisible && (
          <Popup
            className="deleteRowPopup"
            width="auto"
            height="auto"
            visible={true}
            showCloseButton={false}
            showTitle={false}
            onHiding={closeDeleteRowPopup}
          >
            <Position my="center" at="center" of={window} />
            <div>
              <span>Вы уверены, что хотите удалить эту запись?</span>
            </div>
            <ToolbarItem
              widget="dxButton"
              location="center"
              toolbar="bottom"
              options={{ text: "Да", onClick: deleteEntry }}
            />
            <ToolbarItem
              widget="dxButton"
              location="center"
              toolbar="bottom"
              options={{ text: "Нет", onClick: closeDeleteRowPopup }}
            />
          </Popup>
        )}
        {videoStreamPopupVisible && (
          <VideoStreamPopup
            closeVideoStreamPopup={closeVideoStreamPopup}
            onDataErrorOccurred={onDataErrorOccurred}
            videoCameraId={videoCameraId}
          />
        )}
        <div className="dx-card responsive-paddings">
          <Tabs
            dataSource={tabs}
            defaultSelectedIndex={0}
            width={374}
            onItemClick={(e) => {
              if (e.itemIndex === 0) {
                clickOnTable();
              } else if (e.itemIndex === 1) {
                clickOnCards();
              } else {
                clickOnMap();
              }
            }}
          />
          <div className={!isVisible.table ? "hidden" : "visible"}>
            <VideoCamerasTable
              openEditorPopup={openEditorPopup}
              openDeleteRowPopup={openDeleteRowPopup}
              openCreateEntryPopup={openCreateEntryPopup}
              openVideoStreamPopup={openVideoStreamPopup}
              onDataErrorOccurred={onDataErrorOccurred}
              counter={counter}
            />
          </div>
          {isCardsLoaded && (
            <div className={!isVisible.cards ? "hidden" : "visible"}>
              <VideoCamerasCards
                openEditorPopup={openEditorPopup}
                openDeleteRowPopup={openDeleteRowPopup}
                openVideoStreamPopup={openVideoStreamPopup}
                onDataErrorOccurred={onDataErrorOccurred}
                counter={counter}
              />
            </div>
          )}
          {isMapLoaded && (
            <div className={!isVisible.map ? "hidden" : "visible"}>
              <VideoCamerasMap
                openVideoStreamPopup={openVideoStreamPopup}
                onDataErrorOccurred={onDataErrorOccurred}
                counter={counter}
              />
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default connect(
  (state) => ({}),
  (dispatch) => {
    return { localDeauthorize: () => dispatch(deauthorize()) };
  }
)(VideoCamerasPage);
