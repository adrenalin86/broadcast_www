import React, { useEffect, useRef, useState } from "react";
import { connect } from "react-redux";

import DXCheckBox from "devextreme-react/check-box";
import DXLookup from "devextreme-react/lookup";
import DataGrid, {
  Column,
  Editing,
  StateStoring,
  Button,
  Lookup as DGLookup,
  Pager,
  Scrolling,
  RequiredRule,
} from "devextreme-react/data-grid";
import notify from "devextreme/ui/notify";
import DataSource from "devextreme/data/data_source";

import { isEmpty } from "lodash-es";

import { deauthorize } from "../../store/commonSlice";
import {
  getPinCodesDataStore,
  getAreasForPinCodeDataStore,
  getAreasDataStore,
  WINDOWS,
} from "./../../dataStores";

import config from "../../config";

const windowId = WINDOWS.PIN_CODES.id;

const pinCodesDataGridOptions =
  config.pages.pinCodes.pinCodesEditorDataGrid.dxOptions;
const areasInPinCodeDataGridOptions =
  config.pages.pinCodes.areasInPinCodeDataGrid.dxOptions;

const pinCodesDataStore = getPinCodesDataStore({ windowId });

const areasForPinCodeDataStore = getAreasForPinCodeDataStore({ windowId });

const areasDataStore = getAreasDataStore({ windowId });

const newAreasDataStore = getAreasDataStore({ windowId });

const pinCodesDataSource = new DataSource(pinCodesDataStore);

const areasForPinCodeDataSource = new DataSource(areasForPinCodeDataStore);

const Page = ({ localDeauthorize }) => {
  const [selectedPinCode, setSelectedPinCode] = useState({});
  const [areasInPinCode, setAreasInPinCode] = useState([]);
  const [insertedItem] = useState(null);

  const pinCodesDataGridRef = useRef(null);
  const areasInPinCodeDataGridRef = useRef(null);

  useEffect(() => {
    loadAreasInPinCode();
  }, [selectedPinCode]);

  function loadAreasInPinCode() {
    if (!isEmpty(selectedPinCode) && areasForPinCodeDataSource) {
      areasForPinCodeDataSource
        .store()
        .load({ filter: ["pinCodeId", "=", selectedPinCode.id] })
        .then((result) => {
          if (result?.data) {
            setAreasInPinCode((oldData) => result.data);
          } else {
            setAreasInPinCode((oldData) => []);
          }
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
        });
    } else {
      setAreasInPinCode((oldData) => []);
    }
  }

  function onDataErrorOccurred(e) {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);
    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  }

  function onPinCodesDataGridToolbarPreparing(e) {
    let toolbarItems = e.toolbarOptions.items;

    toolbarItems.forEach(function (item) {
      if (item.name === "addRowButton") {
        item.location = "before";
      }
    });
  }

  function onAddArea(e) {
    const areaId = e?.selectedItem?.id;
    const pinCodeId = selectedPinCode?.id;

    if (
      areaId === undefined ||
      areaId === null ||
      pinCodeId === undefined ||
      pinCodeId === null
    ) {
      return;
    }

    areasForPinCodeDataSource
      .store()
      .insert({
        areaId: areaId,
        pinCodeId: pinCodeId,
      })
      .then((result) => {
        e.value = null;
        loadAreasInPinCode();
      })
      .catch((error) => {
        e.value = null;
        onDataErrorOccurred({ error });
        loadAreasInPinCode();
      });
  }

  function onPinCodeSelectionChanged(e) {
    if (!isEmpty(e.selectedRowsData)) {
      setSelectedPinCode((oldData) => e.selectedRowsData[0]);
    } else {
      setSelectedPinCode((oldData) => {});
    }
  }

  function onClickRemoveArea(e) {
    const pinCodeArea = e.row?.key?.id;

    if (!pinCodeArea) {
      return;
    }

    areasForPinCodeDataSource
      .store()
      .remove(pinCodeArea)
      .then((result) => {
        loadAreasInPinCode();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
        loadAreasInPinCode();
      });
  }

  return (
    <>
      <div className="content-block">
        <div className={"dx-card responsive-paddings"}>
          <div className={"pinCodeEditor"}>
            <div className="title-section">
              <div className="title-row">Список пинкодов</div>
              <div className="title-row">
                Список субъектов у пинкода{" "}
                {`"${selectedPinCode?.name || "---"}"`}
              </div>
            </div>
            <div className="datagrid-section">
              <div className="pinCodeEditor-col">
                <div className="title-col">Список пинкодов</div>
                <DataGrid
                  id="pinCodeEditorDataGrid"
                  ref={pinCodesDataGridRef}
                  dataSource={pinCodesDataSource}
                  onDataErrorOccurred={onDataErrorOccurred}
                  errorRowEnabled={false}
                  onToolbarPreparing={onPinCodesDataGridToolbarPreparing}
                  onSelectionChanged={onPinCodeSelectionChanged}
                  selection={{ mode: "single" }}
                  onRowValidating={(e) => {
                    if (!e.isValid) {
                      const defaultMessage = "Проверьте заполнение полей";
                      const message = e.brokenRules.reduce((acc, item) => {
                        return acc + item.message + "\n";
                      }, "");
                      notify(
                        message || defaultMessage,
                        "error",
                        config.common.errorMessageLifespan
                      );
                    }
                  }}
                  onInitNewRow={(e) => {
                    e.data.accessToAll = false;
                  }}
                  {...pinCodesDataGridOptions}
                >
                  <Editing
                    allowAdding={true}
                    allowUpdating={true}
                    allowDeleting={true}
                  />
                  <Column dataField={"name"} caption={"Название"}>
                    <RequiredRule message="Необходимо указать название" />
                  </Column>
                  <Column dataField={"number"} caption={"Код"} width={"100px"}>
                    <RequiredRule message="Необходимо указать код" />
                  </Column>
                  <Column
                    dataField={"accessToAll"}
                    caption={"Доступ ко всему"}
                    width={"130px"}
                    dataType={"boolean"}
                  ></Column>
                  <Column
                    dataField={"id"}
                    caption={"id"}
                    width={"50px"}
                    visible={false}
                  />
                  <Column type="buttons" width={"100px"}>
                    <Button name="edit" icon="edit" text="Редактировать" />
                    <Button name="delete" icon="trash" text="Удалить" />
                    <Button name="save" icon="save" text="Сохранить" />
                    <Button name="cancel" icon="close" text="Отмена" />
                  </Column>
                </DataGrid>
              </div>
              <div className="pinCodeEditor-col">
                <div className="title-col">
                  Список субъектов у пинкода{" "}
                  {`"${selectedPinCode?.name || "---"}"`}
                </div>
                <DXLookup
                  dataSource={{
                    store: newAreasDataStore,
                    onLoadError: onDataErrorOccurred,
                    filter: [
                      "id",
                      "notin",
                      areasInPinCode.map((item) => item.areaId),
                    ],
                  }}
                  valuteExpr="id"
                  displayExpr="name"
                  value={insertedItem}
                  onSelectionChanged={onAddArea}
                  placeholder="Выберите субъект"
                  width="100%"
                  style={{ marginBottom: "10px" }}
                  disabled={isEmpty(selectedPinCode)}
                />
                <DataGrid
                  id="areasInPinCodeDataGrid"
                  ref={areasInPinCodeDataGridRef}
                  onDataErrorOccurred={onDataErrorOccurred}
                  errorRowEnabled={false}
                  dataSource={areasInPinCode}
                  {...areasInPinCodeDataGridOptions}
                >
                  <Editing
                    allowAdding={false}
                    allowUpdating={false}
                    allowDeleting={true}
                  />
                  <Column dataField={"areaId"} caption="Название">
                    <DGLookup
                      dataSource={areasDataStore}
                      valueExpr="id"
                      displayExpr="name"
                    />
                  </Column>
                  <Column type="buttons" width={"50px"}>
                    <Button
                      name="remove"
                      text="Удалить"
                      icon="trash"
                      onClick={onClickRemoveArea}
                    />
                  </Column>
                </DataGrid>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default connect((state) => ({}), {
  localDeauthorize: deauthorize,
})(Page);
