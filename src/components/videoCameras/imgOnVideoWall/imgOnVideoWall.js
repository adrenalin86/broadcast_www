import React, { useEffect, useRef, useState } from "react";

import { LoadIndicator } from "devextreme-react";

import config from "../../../config";

import { getVideoCameraStatusesDataStore, WINDOWS } from "../../../dataStores";

const AUTOMATIC_UPDATE_TIMEOUT_MS = config.pages.videoCameras.videoCamerasGrid.updateInterval;

const windowId = WINDOWS.VIDEOCAMERAS.id;

const videoCameraStatusesDataStore = getVideoCameraStatusesDataStore({
  windowId: windowId,
});

const ImgOnVideoWall = ({ openVideoStreamPopup, onDataErrorOccurred, videoCameraId }) => {
  const [isUpdatingImg, setIsUpdatingImg] = useState(false);
  const [imgLink, setImgLink] = useState();
  const [videoCameraStatus, setVideoCameraStatus] = useState();
  const [showMessageError, setShowMessageError] = useState(false);
  const [showLoader, setShowLoader] = useState(true);

  const imgRef = useRef(null);

  const fetchVideoCameraStatus = () => {
    videoCameraStatusesDataStore
      .load({ filter: ["videoCameraId", "=", videoCameraId], resizeToHeight: 170 })
      .then(({ data }) => {
        const [videoCameraStatus] = data;

        setVideoCameraStatus(videoCameraStatus);

        if (videoCameraStatus?.lastSnapshot?.base64) {
          setImgLink(`data:image/jpeg;base64, ${videoCameraStatus?.lastSnapshot?.base64}`);
          setShowMessageError(false);
        } else {
          setShowMessageError(true);
          setShowLoader(false);
        }
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
      });
  };

  /**
   * Ленивая загрузка изображений.
   * Отслеживание, находится ли изображение в поле видимости, уничтожается после того
   * как карточка прошла прокрутку.
   */
  useEffect(() => {
    const loadImg = (element) => {
      const observer = new IntersectionObserver(
        (entries) => {
          entries.forEach((entry) => {
            const { isIntersecting } = entry;

            if (isIntersecting) {
              fetchVideoCameraStatus();
              observer.unobserve(element);
            }
          });
        },
        {
          threshold: 0.5,
        }
      );

      observer.observe(element);
    };

    loadImg(imgRef.current);
  }, [videoCameraId]);

  /**
   * Обновляем только те изображения, которые в поле видимости.
   * Отслеживание, находится ли изображение в поле видимости - вечно.
   */
  useEffect(() => {
    const updateImg = (element) => {
      const observer = new IntersectionObserver(
        (entries) => {
          entries.forEach((entry) => {
            const { isIntersecting } = entry;

            if (isIntersecting) {
              setIsUpdatingImg(true);
            } else {
              setIsUpdatingImg(false);
            }
          });
        },
        {
          threshold: 0.5,
        }
      );

      observer.observe(element);
    };

    updateImg(imgRef.current);
  }, []);

  useEffect(() => {
    const timer = setInterval(() => {
      if (isUpdatingImg) {
        fetchVideoCameraStatus();
      }
    }, AUTOMATIC_UPDATE_TIMEOUT_MS);

    return () => {
      clearInterval(timer);
    };
  }, [isUpdatingImg, videoCameraId]);

  return (
    <div ref={imgRef}>
      <div style={{display: "flex", justifyContent: "center"}}>
        <div style={{ display: "flex", justifyContent: "center", maxWidth: "220px" }}>
          <div onClick={() => openVideoStreamPopup(videoCameraId)}>
            <LoadIndicator
              id="medium-indicator"
              className="button-indicator"
              style={{ cursor: "pointer", margin: "65px 0px" }}
              height={40}
              width={40}
              visible={showLoader}
            />
          </div>
        </div>
      </div>
      {showMessageError && (
        <div style={{display: "flex", justifyContent: "center"}}>
          <div
            style={{
              maxWidth: "220px",
              textAlign: "center",
              cursor: "pointer",
            }}
            onClick={() => openVideoStreamPopup(videoCameraId)}
          >
            Не удалось получить изображение с сервера
          </div>
        </div>
      )}
      <img
        style={
          showMessageError || showLoader
            ? { display: "none" }
            : videoCameraStatus.connected === 1
            ? { width: "207px", height: "170px", cursor: "pointer" }
            : {
                width: "207px",
                height: "170px",
                cursor: "pointer",
                filter: "grayscale(100%) blur(1px)",
              }
        }
        alt="Изображение для видеокамеры"
        src={imgLink}
        onLoad={() => setShowLoader(false)}
        onClick={() => openVideoStreamPopup(videoCameraId)}
      />
    </div>
  );
};

export default ImgOnVideoWall;
