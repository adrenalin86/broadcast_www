import React, { useState, useRef, useEffect } from "react";
import { connect } from "react-redux";

import DataGrid, {
  Column,
  Lookup,
  StateStoring,
  Button,
  Export,
  ColumnChooser,
} from "devextreme-react/data-grid";
import notify from "devextreme/ui/notify";
import DataSource from "devextreme/data/data_source";
import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import { exportDataGrid } from "devextreme/excel_exporter";

import { Workbook } from "exceljs";
import saveAs from "file-saver";
import { isEmpty, set } from "lodash-es";
import dayjs from "dayjs";
import dayjs_utc from "dayjs/plugin/utc";

import {
  getWebClaimsDataStore,
  getWebClaimTypesDataStore,
  getWebClaimStatusDataStore,
  WINDOWS,
  getSystemWindowsDataStore,
} from "../../dataStores";

import { deauthorize } from "../../store/commonSlice";

import WebClaimsEditor from "./webClaimsEditorPopup";

import config from "../../config";

dayjs.extend(dayjs_utc);

const windowId = WINDOWS.WEB_CLAIMS.id;

function onExporting(e) {
  const workbook = new Workbook();
  const worksheet = workbook.addWorksheet("Main sheet");

  exportDataGrid({
    component: e.component,
    worksheet: worksheet,
    autoFilterEnabled: true,
  }).then(function () {
    workbook.xlsx.writeBuffer().then(function (buffer) {
      saveAs(new Blob([buffer], { type: "application/octet-stream" }), "DataGrid.xlsx");
    });
  });
  e.cancel = true;
}

const webClaimsDataStore = getWebClaimsDataStore({ windowId: windowId });
const webClaimTypesDataStore = getWebClaimTypesDataStore({
  windowId: windowId,
});
const webClaimStatusDataStore = getWebClaimStatusDataStore({
  windowId: windowId,
});
const webClaimsDataSource = new DataSource({
  store: webClaimsDataStore,
  reshapeOnPush: true,
});

const systemWindowsDataStore = getSystemWindowsDataStore({
  windowId: windowId,
});

const DisplaysPage = ({ localDeauthorize }) => {
  const [editorPopupVisible, setEditorPopupVisible] = useState(false);
  const [editedRow, setEditedRow] = useState({});
  const [deleteRowPopupVisible, setDeleteRowPopupVisible] = useState(false);
  const [deletedRowKey, setDeletedRowKey] = useState(null);
  const [windowTitle, setWindowTitle] = useState();

  const gridOptions = config.pages.webClaims.grid.dxOptions;

  const datagridRef = useRef(null);

  function onDataErrorOccurred(e) {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);
    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  } //TODO: рассмотреть целесообразность и способы выноса обработки ошибок из компонентов в ещё большей степени.

  function onToolbarPreparing(e) {
    let toolbarItems = e.toolbarOptions.items;

    toolbarItems.unshift({
      widget: "dxButton",
      options: {
        icon: "add",
        onClick: openCreateEntryPopup,
      },
      location: "before",
    });
  }

  const openEditorPopup = (e) => {
    const currentRow = webClaimsDataSource.items().find((item) => item.id === e.row.key);

    if (!isEmpty(currentRow)) {
      setEditedRow(currentRow);
      setEditorPopupVisible(true);
    }
  };

  const openCreateEntryPopup = () => {
    setEditedRow({});
    setEditorPopupVisible(true);
  };

  const openDeleteRowPopup = (e) => {
    setEditedRow({});
    setDeletedRowKey(e.row.key);
    setDeleteRowPopupVisible(true);
  };

  const closeEditorPopup = () => {
    setEditorPopupVisible(false);
  };

  const closeDeleteRowPopup = () => {
    setDeleteRowPopupVisible(false);
  };

  const createEntry = (e) => {
    webClaimsDataSource
      .store()
      .insert(e.formData)
      .then((e) => {
        closeEditorPopup();
        datagridRef?.current?.instance?.refresh();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
        closeEditorPopup();
      });
  };

  const updateEntry = (e) => {
    if (!isEmpty(e.formData)) {
      webClaimsDataSource
        .store()
        .update(e.editedRowId, e.formData)
        .then(() => {
          closeEditorPopup();
          datagridRef?.current?.instance?.refresh();
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
          closeEditorPopup();
        });
    } else {
      closeEditorPopup();
    }
  };

  const deleteEntry = (e) => {
    webClaimsDataSource
      .store()
      .remove(deletedRowKey)
      .then(() => {
        closeDeleteRowPopup();
        datagridRef?.current?.instance?.refresh();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
        closeDeleteRowPopup();
      });
  };

  useEffect(() => {
    systemWindowsDataStore.byKey(windowId).then((result) => {
      const windowName = result[0]?.name?.split("/").pop();

      setWindowTitle(windowName);
    });
  }, []);

  return (
    <React.Fragment>
      <h2 className={"content-block"}>{windowTitle}</h2>
      {editorPopupVisible && (
        <WebClaimsEditor
          editedRow={editedRow}
          windowId={windowId}
          closeEditorPopup={closeEditorPopup}
          updateEntry={updateEntry}
          createEntry={createEntry}
        />
      )}
      {deleteRowPopupVisible && (
        <Popup
          className="deleteRowPopup"
          width="auto"
          height="auto"
          visible={true}
          showCloseButton={false}
          showTitle={false}
          onHiding={closeDeleteRowPopup}
        >
          <Position my="center" at="center" of={window} />
          <div>
            <span>Вы уверены, что хотите удалить эту запись?</span>
          </div>
          <ToolbarItem
            widget="dxButton"
            location="center"
            options={{ text: "Да", onClick: deleteEntry }}
            toolbar="bottom"
          />
          <ToolbarItem
            widget="dxButton"
            location="center"
            options={{ text: "Нет", onClick: closeDeleteRowPopup }}
            toolbar="bottom"
          />
        </Popup>
      )}
      <div className={"content-block"}>
        <div className={"dx-card responsive-paddings"}>
          <DataGrid
            id="displayEditorDataGrid"
            ref={datagridRef}
            dataSource={webClaimsDataSource}
            onDataErrorOccurred={onDataErrorOccurred}
            onExporting={onExporting}
            errorRowEnabled={false}
            onContentReady={(e) => {
              //Для каждой колонки форсированно задаём порядок отрисовки в таблице.
              e.component.state().columns.forEach((col, index) => {
                e.component.columnOption(col.name, "visibleIndex", index);
              });
            }}
            {...gridOptions}
          >
            <StateStoring
              enabled={true}
              type="localStorage"
              storageKey="pages.webClaimsEditor.displaysGrid"
            />
            <ColumnChooser enabled={true} />
            <Export enabled={true} allowExportSelectedData={true} />
            <Column dataField="id" visible={false} />
            <Column type="string" dataField="title" caption="Заголовок" />
            <Column type="number" dataField="typeId" caption="Тип">
              <Lookup dataSource={webClaimTypesDataStore} valueExpr="id" displayExpr="name" />
            </Column>
            <Column type="number" dataField="statusId" caption="Статус">
              <Lookup dataSource={webClaimStatusDataStore} valueExpr="id" displayExpr="name" />
            </Column>
            <Column type="string" dataField="webUserId" caption="Id пользователя" />
            <Column type="string" dataField="contactEmail" caption="email" />
            <Column type="buttons">
              <Button name="custom-edit" text="Изменить" icon="edit" onClick={openEditorPopup} />
              <Button
                name="custom-delete"
                text="Удалить"
                icon="trash"
                onClick={openDeleteRowPopup}
              />
            </Column>
          </DataGrid>
        </div>
      </div>
    </React.Fragment>
  );
};

export default connect(
  (state) => ({}),
  (dispatch) => {
    return { localDeauthorize: () => dispatch(deauthorize()) };
  }
)(DisplaysPage);
