import dayjs from "dayjs";

const formatDateTime = (value) => {
    const dateTime = dayjs.utc(value).local();
    const isBeforeDayAgo = dateTime.isBefore(dayjs.utc().local().subtract(1, "day"));
    let formatedDateTime = "";

    if (isBeforeDayAgo) {
        formatedDateTime = dateTime.format("HH:mm DD.MM.YYYY");
    } else {
        const isInPast = dateTime.isBefore(dayjs.utc().local());

        if (isInPast) {
            formatedDateTime = dateTime.fromNow();
        } else {
            formatedDateTime = "только что";
        }
    }

    return formatedDateTime;
};

export {
    formatDateTime
}