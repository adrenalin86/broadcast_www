import DataGrid, {Button, Column, Export} from "devextreme-react/data-grid";
import {getSpecTransportsDataStore, WINDOWS} from "../../dataStores";
import config from "../../config";
import {formatDateTime} from "../../utils/date";
import React, {useEffect} from "react";

const windowId = WINDOWS.SPEC_TRANSPORTS.id;

const specTransportsDataStore = getSpecTransportsDataStore({
    windowId: windowId,
});

const AUTOMATIC_UPDATE_TIMEOUT_MS = config.pages.specTransports.specTransportsGrid.updateInterval;

const SpecTransportsTable = ({
                                 datagridRef,
                                 openEditorPopup,
                                 openDeleteRowPopup,
                                 openCreateEntryPopup,
                                 onDataErrorOccurred,
                             }) => {
    const specTransportsGridOptions = config.pages.specTransports.specTransportsGrid.dxOptions;

    useEffect(() => {
        const updateData = () => {
            specTransportsDataStore
                .load()
                .then((result) => {
                    let changes = [];
                    result.data.forEach((item) => {
                        changes.push({
                            type: "update",
                            key: item.id,
                            data: item,
                        });
                    });

                    specTransportsDataStore?.push(changes);
                })
                .catch((error) => {
                    onDataErrorOccurred({error});
                });
        };

        const timer = setInterval(() => {
            updateData();
        }, AUTOMATIC_UPDATE_TIMEOUT_MS);

        return () => {
            clearInterval(timer);
        };
    }, [])

    const onToolbarPreparing = (e) => {
        let toolbarItems = e.toolbarOptions.items;

        toolbarItems.unshift({
            widget: "dxButton",
            options: {
                icon: "add",
                onClick: openCreateEntryPopup,
            },
            location: "before",
        });
    };

    return (
        <div className="dx-card responsive-paddings">
            <DataGrid
                ref={datagridRef}
                dataSource={specTransportsDataStore}
                onDataErrorOccurred={onDataErrorOccurred}
                onToolbarPreparing={onToolbarPreparing}
                errorRowEnabled={false}
                onContentReady={(e) => {
                    e.component.state().columns.forEach((col, index) => {
                        e.component.columnOption(col.name, "visibleIndex", index);
                    });
                }}
                {...specTransportsGridOptions}
            >
                <Export enabled={true} allowExportSelectedData={true}/>
                <Column dataField="id" caption="id" alignment="left"/>
                <Column dataField="gosNumber" caption="Гос. номер"/>
                <Column dataField="garageNumber" caption="Гаражный номер"/>
                <Column dataField="deviceCode" caption="Идентификатор навигационного прибора"/>
                <Column dataField="gpsMesCurrent.timeUtc" caption="Время последней координаты" cellComponent={(e) => {
                    return (
                        <div
                            className={"datagrid-display-status"}
                            style={{
                                display: "flex",
                                flexWrap: "nowrap",
                                wordBreak: "break-word",
                                whiteSpace: "normal",
                            }}
                            >
                                <span>{`${
                                    e.data?.value
                                        ? formatDateTime(e.data?.value)
                                        : "нет"
                                }`}</span>
                        </div>
                    );
                }}/>
                <Column dataField="phoneNumber" caption="Номер сим-карты"/>
                <Column dataField="note" caption="Заметки"/>
                <Column dataField="specTransportType.name" caption="Тип техники"/>
                <Column dataField="specCompany.name" caption="Организация"/>
                <Column dataField="showOnPortal" caption="Показывать на портале"/>
                <Column type="buttons" alignment="center">
                    <Button
                        name="custom-edit"
                        text="редактировать"
                        icon="edit"
                        onClick={(e) => openEditorPopup(e.row.key)}
                    />
                    <Button
                        name="custom-delete"
                        text="удалить"
                        icon="trash"
                        onClick={(e) => openDeleteRowPopup(e.row.key)}
                    />
                </Column>
            </DataGrid>
        </div>
    );
};

export default SpecTransportsTable;
