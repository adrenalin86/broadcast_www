import React, {useEffect, useRef, useState} from 'react';
import {getDeviceDataStore, getDeviceTypesDataStore, getSystemWindowsDataStore, WINDOWS} from "../../dataStores";
import notify from "devextreme/ui/notify";
import config from "../../config";
import DataGrid, {Button, Column, Lookup, StateStoring} from "devextreme-react/data-grid";
import DataSource from "devextreme/data/data_source";
import {isEmpty} from "lodash-es";
import {Popup, Position, ToolbarItem} from "devextreme-react/popup";
import EditorPopupDevice from "./editorPopup";
import {connect} from "react-redux";
import {deauthorize} from "../../store/commonSlice";
import dayjs from "dayjs";
import {formatDateTime} from "../../utils/date";

const windowId = WINDOWS.DEVICES.id;

const roadDeviceType = getDeviceDataStore({windowId: windowId});
const roadDeviceTypeDataStore = getDeviceTypesDataStore({windowId: windowId});

const roadDevicesDataStore = new DataSource({
    store: roadDeviceType,
    reshapeOnPush: true,
});

const AUTOMATIC_UPDATE_TIMEOUT_MS = config.pages.devices.grid.updateInterval;

function DevicesPage({localDeauthorize}) {

    const [windowTitle, setWindowTitle] = useState('');
    const [editedRow, setEditedRow] = useState({});
    const [editorPopupVisible, setEditorPopupVisible] = useState(false);
    const [deletedRowKey, setDeletedRowKey] = useState(null);
    const [deleteRowPopupVisible, setDeleteRowPopupVisible] = useState(false);
    const gridOptions = config.pages.roadEvents.grid.dxOptions;

    useEffect(() => {
        const updateData = () => {
            roadDeviceType
                .load()
                .then((result) => {
                    let changes = [];
                    result.data.forEach((item) => {
                        changes.push({
                            type: "update",
                            key: item.id,
                            data: {
                                status: item.status,
                            },
                        });
                    });

                    roadDeviceType?.push(changes);
                })
                .catch((error) => {
                    onDataErrorOccurred({error});
                });
        };

        const timer = setInterval(() => {
            updateData();
        }, AUTOMATIC_UPDATE_TIMEOUT_MS);

        return () => {
            clearInterval(timer);
        };
    }, [])

    const systemWindowsDataStore = getSystemWindowsDataStore({
        windowId: windowId,
    });

    const datagridRef = useRef(null);

    function onDataErrorOccurred(e) {
        notify(e.error?.message, "error", config.common.errorMessageLifespan);
        if (e.error?.data?.reason === "noAuth") {
            localDeauthorize();
        }
    }

    const closeEditorPopup = () => {
        setEditorPopupVisible(false);
    };

    const openDeleteRowPopup = (e) => {
        setEditedRow({});
        setDeletedRowKey(e.row.key);
        setDeleteRowPopupVisible(true);
    };

    function onToolbarPreparing(e) {
        let toolbarItems = e.toolbarOptions.items;

        toolbarItems.unshift({
            widget: "dxButton",
            options: {
                icon: "add",
            },
            location: "before",
            onClick: openCreateEntryPopup,
        });
    }

    const openCreateEntryPopup = () => {
        setEditedRow({});
        setEditorPopupVisible(true);
    };

    useEffect(() => {
        systemWindowsDataStore.byKey(windowId).then((result) => {
            const windowName = result[0]?.name?.split("/").pop();

            setWindowTitle(windowName);
        });
    }, []);

    const closeDeleteRowPopup = () => {
        setDeleteRowPopupVisible(false);
    };

    const createEntry = (e) => {
        roadDevicesDataStore
            .store()
            .insert(e.formData)
            .then((e) => {
                closeEditorPopup();
                datagridRef?.current?.instance?.refresh();
            })
            .catch((error) => {
                onDataErrorOccurred({error});
                closeEditorPopup();
            });
    };

    const openEditorPopup = (e) => {
        const currentRow = roadDevicesDataStore.items().find((item) => item.id === e.row.key);

        if (!isEmpty(currentRow)) {
            setEditedRow(currentRow);
            setEditorPopupVisible(true);
        }
    };

    const updateEntry = (e) => {
        if (!isEmpty(e.formData)) {
            roadDevicesDataStore
                .store()
                .update(e.editedRowId, e.formData)
                .then(() => {
                    closeEditorPopup();
                    datagridRef?.current?.instance?.refresh();
                })
                .catch((error) => {
                    onDataErrorOccurred({error});
                    closeEditorPopup();
                });
        } else {
            closeEditorPopup();
        }
    };

    const deleteEntry = (e) => {
        roadDevicesDataStore
            .store()
            .remove(deletedRowKey)
            .then(() => {
                closeDeleteRowPopup();
                datagridRef?.current?.instance?.refresh();
            })
            .catch((error) => {
                onDataErrorOccurred({error});
                closeDeleteRowPopup();
            });
    };

    return (
        <React.Fragment>
            <h2 className={"content-block"}>{windowTitle}</h2>
            {editorPopupVisible && (
                <EditorPopupDevice
                    editedRow={editedRow}
                    windowId={windowId}
                    closeEditorPopup={closeEditorPopup}
                    updateEntry={updateEntry}
                    createEntry={createEntry}
                />
            )}
            {deleteRowPopupVisible && (
                <Popup
                    className="deleteRowPopup"
                    width="auto"
                    height="auto"
                    visible={true}
                    showCloseButton={false}
                    showTitle={false}
                    onHiding={closeDeleteRowPopup}
                >
                    <Position my="center" at="center" of={window}/>
                    <div>
                        <span>Вы уверены, что хотите удалить эту запись?</span>
                    </div>
                    <ToolbarItem
                        widget="dxButton"
                        location="center"
                        options={{text: "Да", onClick: deleteEntry}}
                        toolbar="bottom"
                    />
                    <ToolbarItem
                        widget="dxButton"
                        location="center"
                        options={{text: "Нет", onClick: closeDeleteRowPopup}}
                        toolbar="bottom"
                    />
                </Popup>
            )}
            <div className={"content-block"}>
                <div className={"dx-card responsive-paddings"}>
                    <DataGrid
                        id="displayEditorDataGrid"
                        ref={datagridRef}
                        dataSource={roadDevicesDataStore}
                        onDataErrorOccurred={onDataErrorOccurred}
                        onToolbarPreparing={onToolbarPreparing}
                        errorRowEnabled={false}
                        onContentReady={(e) => {
                            e.component.state().columns.forEach((col, index) => {
                                e.component.columnOption(col.name, "visibleIndex", index);
                            });
                        }}
                        {...gridOptions}
                    >
                        <StateStoring
                            enabled={true}
                            type="localStorage"
                            storageKey="pages.devices.displaysGrid"
                        />
                        <Column allowSorting dataField="id"/>
                        <Column
                            allowSorting
                            width={70}
                            dataField="status.connected"
                            caption="Статус"
                            alignment="center"
                            cellComponent={(e) => {
                                return (
                                    <div
                                        className="datagrid-display-status"
                                        style={{
                                            display: "flex",
                                            flexWrap: "nowrap",
                                            wordBreak: "break-word",
                                            whiteSpace: "normal",
                                            alignItems: "center",
                                            justifyContent: "center",
                                        }}
                                    >
                                        {e.data.data.status?.connected === 1 && (
                                            <i
                                                className="dx-icon-check"
                                                style={{
                                                    color: "green",
                                                    fontSize: "1.2em",
                                                    marginRight: "4px",
                                                }}
                                            />
                                        )}
                                        {(e.data.data.status?.connected === 0 || !e.data.data.status?.connected) && (
                                            <i
                                                className="dx-icon-close"
                                                style={{
                                                    color: "gray",
                                                    fontSize: "1.2em",
                                                    marginRight: "4px",
                                                }}
                                            />
                                        )}
                                    </div>
                                );
                            }}
                        />
                        <Column allowSorting type="string" dataField="name" caption="Название"/>
                        <Column allowSorting type="string" dataField="ipAddress" caption="IP адрес"/>
                        <Column allowSorting type="number" dataField="deviceTypeId" caption="Тип">
                            <Lookup dataSource={roadDeviceTypeDataStore} valueExpr="id" displayExpr="name"/>
                        </Column>
                        <Column
                            allowSorting
                            dataField="status"
                            caption="Последнее подключение"
                            cellComponent={(e) => {
                                return (
                                    <div
                                        className={"datagrid-display-status"}
                                        style={{
                                            display: "flex",
                                            flexWrap: "nowrap",
                                            wordBreak: "break-word",
                                            whiteSpace: "normal",
                                        }}
                                    >
                <span>{`${
                    e.data.data.status?.lastConnectionTimeUtc
                        ? formatDateTime(e.data.data.status?.lastConnectionTimeUtc)
                        : "нет"
                }`}</span>
                                    </div>
                                );
                            }}
                        />
                        <Column
                            allowSorting
                            dataField="deviceType.iconName"
                            dataType="string"
                            width={200}
                            caption="Иконка"
                            cellComponent={(e) => {
                                if (e.data?.value) {
                                    return (
                                        <img src={`ico/deviceTypes/${e.data?.value}.svg`} alt={e.data?.value}/>
                                    );
                                } else {
                                    return <div>Иконка отсутствует</div>
                                }
                            }}
                        />
                        <Column type="buttons">
                            <Button name="custom-edit" text="Изменить" icon="edit" onClick={openEditorPopup}/>
                            <Button
                                name="custom-delete"
                                text="Удалить"
                                icon="trash"
                                onClick={openDeleteRowPopup}
                            />
                        </Column>
                    </DataGrid>
                </div>
            </div>
        </React.Fragment>
    );
}

export default connect((state) => ({}), {
    localDeauthorize: deauthorize,
})(DevicesPage);
