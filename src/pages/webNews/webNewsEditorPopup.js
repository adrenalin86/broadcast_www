import React, {useState, useEffect, memo} from "react";

import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import { ScrollView } from "devextreme-react/scroll-view";
import Form, { SimpleItem } from "devextreme-react/form";
import {TabPanel , DateBox, TextBox, TextArea, TagBox } from "devextreme-react";
import HtmlEditor, { Toolbar, MediaResizing, Item } from "devextreme-react/html-editor";

import ValidationSummary from "devextreme-react/validation-summary";
import ValidationEngine from "devextreme/ui/validation_engine";
import Validator, { RequiredRule, RangeRule } from "devextreme-react/validator";

import { set, cloneDeep, isEmpty } from "lodash-es";

import config from "../../config";
import { connect } from "react-redux";
import { deauthorize } from "../../store/commonSlice";

import { getTagsDataStore, WINDOWS } from "../../dataStores";
import DataSource from "devextreme/data/data_source";

const windowId = WINDOWS.WEB_NEWS.id;

const tagsDataStore = getTagsDataStore({ windowId: windowId });


const defaultValues = {};

const sizeValues = [
  "8pt",
  "10pt",
  "12pt",
  "14pt",
  "16pt",
  "18pt",
  "20pt",
  "22pt",
  "24pt",
  "26pt",
  "28pt",
  "30pt",
  "32pt",
  "34pt",
  "36pt",
];
const fontValues = [
  "Helvetica Neue",
  "Segoe UI",
  "Arial",
  "Courier New",
  "Georgia",
  "Impact",
  "Lucida Console",
  "Tahoma",
  "Times New Roman",
  "Verdana",
];
const headerValues = [false, 1, 2, 3, 4, 5];


const EditorPopup = ({ editedRow, closeEditorPopup, updateEntry, createEntry, localDeauthorize }) => {
  let [formData, setFormData] = useState({});
  const [index, setIndex] = useState(0);
  const [changedFormData] = useState({});

  const webNewsPopupOptions = config.pages.webNews.webNewsDataGrid.webNewsEditorPopup.dxOptions;

  /**
   * Инициализация редактора.
   */
  useEffect(() => {
    if (isEmpty(editedRow)) {
      setFormData(cloneDeep(defaultValues));
    } else {
      setFormData(cloneDeep(editedRow));
    }
  }, []);

  const closePopup = () => {
    closeEditorPopup();
  };

  const onSave = () => {
    const isValid = ValidationEngine.validateGroup("formEditor").isValid;

    if (isValid) {
      if (isEmpty(editedRow)) {
        createEntry({
          formData: formData,
        });
      } else {
        updateEntry({
          formData: changedFormData,
          editedRowId: editedRow.id,
        });
      }
    }
  };

  const onCancel = () => {
    closePopup();
  };

  const onFormItemValueChanged = (e) => {
    const value = e.value !== undefined ? e.value : e?.component?.option("text");
    const dataField = e.component.option("dataField");
    setFormDataValue(dataField, value);
  };

  const onTextAreaItemValueChanged = (value) => {
    setFormData(prev => ({...prev, contentHtml: value}))
  };


  const setFormDataValue = (dataField, value) => {
    set(formData, dataField, value);
    set(changedFormData, dataField, value);
  };


  const itemTitleRender = (object) => {
    return <span>{object.title}</span>;
  }

  return (
    <React.Fragment>
      <Popup
        className="customEditorPopup"
        visible={true}
        onHiding={closePopup}
        titleComponent={() => {
          return <span className="popup-title">{webNewsPopupOptions.title}</span>;
        }}
        {...webNewsPopupOptions}
      >
        <Position my="center" at="center" of={window} />
        <ScrollView showScrollbar="always">
          <Form validationGroup="formEditor">
            <SimpleItem>
              <div className="editor-content">
                <div className="dx-form-group-with-caption dx-form-group editor-group">
                  <div className="dx-form-group-caption">Общие сведения</div>
                  <div className="dx-form-group-content editor-content">
                    <div className="editor-item">
                      <div className="dx-field">
                        <div className="dx-field-label">Заголовок</div>
                        <div className="dx-field-value">
                          <TextBox defaultValue={formData.title} onInput={onFormItemValueChanged} dataField="title">
                            <Validator validationGroup="formEditor">
                              <RequiredRule />
                            </Validator>
                          </TextBox>
                        </div>
                      </div>
                    </div>
                    <div className="editor-item">
                      <div className="dx-field">
                        <div className="dx-field-label">Дата создания</div>
                        <div className="dx-field-value">
                          <DateBox
                            defaultValue={formData.creationTimeUtc}
                            dataField="creationTimeUtc"
                            type="datetime"
                            readOnly={true}
                          ></DateBox>
                        </div>
                      </div>
                    </div>
                    <div className="editor-item">
                      <div className="dx-field">
                        <div className="dx-field-label">Начало показа</div>
                        <div className="dx-field-value">
                          <DateBox
                            defaultValue={formData.startShowTimeUtc}
                            onValueChanged={(e) => {
                              e.value = new Date(e.value).toISOString().split(".")[0] + "Z";
                              onFormItemValueChanged(e);
                            }}
                            dataField="startShowTimeUtc"
                            max={formData.endShowTimeUtc}
                            type="datetime"
                          >
                            <Validator validationGroup="formEditor">
                              <RequiredRule />
                            </Validator>
                          </DateBox>
                        </div>
                      </div>
                    </div>
                    <div className="editor-item">
                      <div className="dx-field">
                        <div className="dx-field-label">Окончание показа</div>
                        <div className="dx-field-value">
                          <DateBox
                            defaultValue={formData.stopShowTimeUtc}
                            onValueChanged={(e) => {
                              e.value = new Date(e.value).toISOString().split(".")[0] + "Z";
                              onFormItemValueChanged(e);
                            }}
                            min={formData.startShowTimeUtc}
                            dataField="stopShowTimeUtc"
                            type="datetime"
                          ></DateBox>
                        </div>
                      </div>
                    </div>

                    <div className="editor-item">
                      <div className="dx-field">
                        <div className="dx-field-label">Теги</div>
                        <div className="dx-field-value">
                          <TagBox
                            defaultValue={formData.tagIds}
                            onValueChanged={onFormItemValueChanged}
                            dataSource={{
                              store: tagsDataStore,
                              postProcess: (tags)=>{
                                return tags.filter(tag=>tag?.entityTypeIds?.includes(63))
                              }
                            }}
                            valueExpr={"id"}
                            displayExpr={"name"}
                            showClearButton={true}
                            dataField="tagIds"
                            searchEnabled={true}
                          ></TagBox>
                        </div>
                      </div>
                    </div>
                    <div className="editor-item">
                      <div className="dx-field">
                        <div className="dx-field-label">Текст</div>
                        <div className="dx-field-value">
                          <TabPanel
                              dataSource={[{
                                id: 0,
                                title: 'Редактор'
                              }, {
                                id: 1,
                                title: 'HTML-код'
                              }]}
                              selectedIndex={index}
                              onOptionChanged={args => {
                                if (args.name === 'selectedIndex') {
                                    setIndex(args.value);
                                }
                              }}
                              itemTitleRender={itemTitleRender}
                              itemComponent={({index}) => {
                                switch (index) {
                                  case 0: {
                                    return <HtmlEditor
                                        defaultValue={formData.contentHtml}
                                        onValueChanged={onFormItemValueChanged}
                                        dataField="contentHtml"
                                        className="htmlEditor"
                                    >
                                      <MediaResizing enabled={true} />
                                      <Toolbar multiline={true}>
                                        <Item name="undo" />
                                        <Item name="redo" />
                                        <Item name="separator" />
                                        <Item name="size" acceptedValues={sizeValues} />
                                        <Item name="font" acceptedValues={fontValues} />
                                        <Item name="separator" />
                                        <Item name="bold" />
                                        <Item name="italic" />
                                        <Item name="strike" />
                                        <Item name="underline" />
                                        <Item name="separator" />
                                        <Item name="alignLeft" />
                                        <Item name="alignCenter" />
                                        <Item name="alignRight" />
                                        <Item name="alignJustify" />
                                        <Item name="separator" />
                                        <Item name="orderedList" />
                                        <Item name="bulletList" />
                                        <Item name="separator" />
                                        <Item name="header" acceptedValues={headerValues} />
                                        <Item name="separator" />
                                        <Item name="color" />
                                        <Item name="background" />
                                        <Item name="separator" />
                                        <Item name="link" />
                                        <Item name="image" />
                                        <Item name="separator" />
                                        <Item name="clear" />
                                        <Item name="codeBlock" />
                                        <Item name="blockquote" />
                                        <Item name="separator" />
                                        <Item name="insertTable" />
                                        <Item name="deleteTable" />
                                        <Item name="insertRowAbove" />
                                        <Item name="insertRowBelow" />
                                        <Item name="deleteRow" />
                                        <Item name="insertColumnLeft" />
                                        <Item name="insertColumnRight" />
                                        <Item name="deleteColumn" />
                                      </Toolbar>
                                    </HtmlEditor>
                                  }
                                  case 1: {
                                    return <TextArea height={200} onValueChange={onTextAreaItemValueChanged}
                                                     className="textarea" value={formData.contentHtml} />
                                  }
                                }
                              }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </SimpleItem>
          </Form>
        </ScrollView>
        <ValidationSummary id="summary" validationGroup="formEditor"></ValidationSummary>
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Сохранить", onClick: onSave }}
          toolbar="bottom"
        />
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Отмена", onClick: onCancel }}
          toolbar="bottom"
        />
      </Popup>
    </React.Fragment>
  );
};

export default connect(null, { localDeauthorize: deauthorize })(EditorPopup);
