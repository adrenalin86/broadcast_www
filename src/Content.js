import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import { cloneDeep } from "lodash-es";

import config from "./config";
import routes from "./app-routes";
import { SideNavOuterToolbar as SideNavBarLayout } from "./layouts";
import { Footer } from "./components";
import { ACCESS_TYPES } from "./dataStores";

export default function ({ windowsRights }) {
  let filteredRoutes = cloneDeep(routes);
  filteredRoutes = filteredRoutes.filter((route) => {
    let hasRight = windowsRights.some((right) => {
      return (
        (route.access.id === right.entityId &&
          route.access.type === ACCESS_TYPES.WINDOWS &&
          right.accessType !== 0) ||
        route.access.type === ACCESS_TYPES.ALL
      );
    });

    return hasRight;
  });

  return (
    <SideNavBarLayout title={config.common.appTitle} windowsRights={windowsRights}>
      <Switch>
        {filteredRoutes.map(({ path, component }) => (
          <Route exact key={path} path={path} component={component} />
        ))}
        {filteredRoutes.length ? <Redirect to={filteredRoutes[0].path} /> : <Redirect to={"/"} />}
        <Route path="/init">
          {filteredRoutes.length ? <Redirect to={filteredRoutes[0].path} /> : <Redirect to={"/"} />}
        </Route>
        <Route path="*">
          <Redirect to={"/"} />
        </Route>
      </Switch>
      <Footer>© ИП Кондрахин А. В. 2020-2022</Footer>
    </SideNavBarLayout>
  );
}
