export { default as DisplaysRegistryHistory } from "./displays";
export { default as DisplayModelsRegistryHistory } from "./displayModels";
export { default as DeviceModelsRegistryHistory } from "./deviceWorking";
export { default as GroupsRegistryHistory } from "./groups";
export { default as SmartStationsGroupsRegistryHistory } from "../smartStationsGroup";
export { default as UsersRegistryHistory } from "./users";
export { default as DisplayProtocolsRegistryHistory } from "./displayProtocols";
export { default as BroadcastProgramTemplatesRegistryHistory } from "./broadcastProgramTemplates";
export { default as TagsRegistryHistory } from "./tags";
export { default as VideoCamerasRegistryHistory } from "./videoCameras";
/**
 * Страницы типовые и создают компонент из одного шаблона. Они разделены на отдельные страницы для
 * наглядности и упрощения создания путей в маршрутиризаторе.
 */
/**
 * А эти не типовые
 */
export { default as DisplayWorkingRegistryHistory } from "./displayWorking";
export { default as BroadcastProgramShowRegistryHistory } from "./broadcastProgramShow";
