import { createSlice } from "@reduxjs/toolkit";

import config from "../config";
import { omitDeepByKeys } from "../utils";

export const USER_ACTIONS_PAGE_REDUCER_KEY = "userActions";

const userActionsPageSlice = createSlice({
  name: USER_ACTIONS_PAGE_REDUCER_KEY,
  initialState: {
    ...omitDeepByKeys(config.pages[USER_ACTIONS_PAGE_REDUCER_KEY], ["dxOptions"]),
  },
  reducers: {},
});

export const getUserActionsPage = (state) =>
  state.pages[USER_ACTIONS_PAGE_REDUCER_KEY];

export default userActionsPageSlice.reducer;
