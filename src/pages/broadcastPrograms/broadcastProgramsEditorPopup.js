import React, { useState, useEffect, Fragment } from 'react'

import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import { ScrollView } from "devextreme-react/scroll-view";
import Form, { SimpleItem } from "devextreme-react/form";
import {
  DateBox,
  NumberBox,
  TagBox,
  TextBox,
  Switch,
  SelectBox,
} from "devextreme-react";
import { Button as TagBoxButton } from "devextreme-react/tag-box";
import { Button as SelectBoxButton } from "devextreme-react/select-box";
import { ButtonsArray } from "../../components/editors";
import notify from "devextreme/ui/notify";
import { SeTextBox } from "../../components/editors";

import ValidationSummary from "devextreme-react/validation-summary";
import ValidationEngine from "devextreme/ui/validation_engine";
import Validator, {
  RequiredRule,
  CustomRule,
  RangeRule,
} from "devextreme-react/validator";
import { OnUnmountHandle } from './OnUnmountHandle'

import { set, get, cloneDeep, isEmpty, defaultsDeep, merge } from "lodash-es";

import {
  getDisplaysDataStore,
  getDisplayGroupsDataStore,
  getGenericEntityDataStore,
  getBroadcastProgramTemplatesDataStore,
  getDisplayProtocolsDataStore,
  GENERIC_ENTITIES,
  WINDOWS,
  DX_ICONS,
} from "../../dataStores";

import {
  ForecastsSettingsComponent,
  U646SettingsComponent,
  ForecastSourceComponent,
  SmartJson2SettingsComponent,
  WeatherForecastsSettingsComponent,
} from "../../components";

import DisplayChooserMasterPopup from "./displayChooserMasterPopup";

import config from "../../config";
import { connect } from "react-redux";
import { deauthorize } from "../../store/commonSlice";
import dayjs from "dayjs";
import customParseFormat from "dayjs/plugin/customParseFormat";
dayjs.extend(customParseFormat);

const windowId = WINDOWS.BROADCAST_PROGRAMS.id;

const defaultValues = {
  ...config.defaults.broadcastPrograms,
  displaySettings: config.defaults.displays.settings,
};

const displaysDataStore = getDisplaysDataStore({
  windowId: windowId,
});

const displayGroupsDataStore = getDisplayGroupsDataStore({
  windowId: windowId,
});
const tagsDataStore = getGenericEntityDataStore({
  windowId: windowId,
  entityName: GENERIC_ENTITIES.TAGS,
});

const broadcastsProgramsTemplatesDataStore =
  getBroadcastProgramTemplatesDataStore({
    windowId: windowId,
  });

const displayProtocolsDataStore = getDisplayProtocolsDataStore({
  windowId: windowId,
});

const broadcastProgramPopupOptions =
  config.pages.broadcastPrograms.broadcastProgramsGrid
    .popupBroadcastProgramsEditor.dxOptions;

const POSSIBLE_PROTOCOLS = [
  "itLineU653",
  "itLineU646",
  "smartNxp146",
  "smartStm140",
  "smartJson2",
  "smartJson1",
  "cpower",
];

const USED_PROTOCOLS = ["smartJson2", "smartJson1"];

const BroadcastProgramsEditorPopup = ({
  editedRow,
  closeEditorPopup,
  updateEntry,
  createEntry,
  localDeauthorize,
}) => {
  const [formData, setFormData] = useState({});
  const [changedFormData, setChangedFormData] = useState({});
  const [editModeEnabled, setEditModeEnabled] = useState(false);
  const [masterPopupVisible, setMasterPopupVisible] = useState(false);
  const [templateId, setTemplateId] = useState(null);
  const [protocolsSettings, setProtocolsSettings] = useState({});
  const [showSettingsOnReset, setShowSettingsOnReset] = useState(true);

  const mainEditorsSettings = {
    forecasts: { show: true },
    forecastSource: { show: true },
  };

  /**
   * Инициализация редактора.
   */
  useEffect(() => {
    if (isEmpty(editedRow)) {
      setFormData(clearUnusedProtocols(cloneDeep(defaultValues)));
    } else {
      setFormData(cloneDeep(editedRow));
    }
    displayProtocolsDataStore
      .load()
      .then((result) => {
        if (result?.data?.length >= 0) {
          setProtocolsSettings((oldData) => {
            const newData = {};
            result.data.forEach((item) => {
              newData[item.name] = {
                show: true,
              };
            });
            return newData;
          });
        }
      })
      .catch((error) => {
        console.log("Ошибка получения списка протоколов");
        onDataErrorOccurred({ error });
      });
  }, []);

  const onApplyTemplate = (e) => {
    if (templateId !== null && templateId !== undefined) {
      broadcastsProgramsTemplatesDataStore
        .byKey(templateId)
        .then((data) => {
          if (data.length >= 1) {
            setFormData((oldData) => {
              const newDisplaySettings = cloneDeep(data[0].displaySettings);

              return { ...oldData, displaySettings: newDisplaySettings };
            });
            setChangedFormData((oldData) => {
              const newDisplaySettings = cloneDeep(data[0].displaySettings);

              return { ...oldData, displaySettings: newDisplaySettings };
            });
            setTemplateId(null);

            setShowSettingsOnReset(false)
          }
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
        });
    }
  };

  function onDataErrorOccurred(e) {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);
    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  }

  const closePopup = () => {
    closeEditorPopup();
  };

  const onSave = () => {
    const isValid = ValidationEngine.validateGroup("formEditor").isValid;

    if (isValid) {
      if (isEmpty(editedRow)) {
        createEntry({
          formData: formData,
        });
      } else {
        updateEntry({
          formData: changedFormData,
          editedRowId: editedRow.id,
        });
      }
    }
  };

  const onCancel = () => {
    closePopup();
  };

  const onFormItemValueChanged = (e) => {
    const value =
      e.value !== undefined ? e.value : e?.component?.option("text");

    const dataField = e.component.option("dataField");
    const enabler = e.component.option("enabler");
    const repaint = e?.forceRepaint || e.component.option("forceRepaint");
    const forceMerge = e.forceMerge || e.component.option("forceMerge");
    if (enabler) {
      setFormDataState(dataField, value ? get(defaultValues, dataField) : null);
    } else {
      if (repaint) {
        setFormDataState(dataField, value, forceMerge);
      } else {
        setFormDataValue(dataField, value, forceMerge);
      }
    }
  };

  const onFormItemValueChangedData = ({
    value,
    dataField,
    enabler,
    forceRepaint,
    forceMerge = false,
  }) => {
    if (enabler) {
      if (forceRepaint) {
        setFormDataState(
          dataField,
          value ? get(defaultValues, dataField) : null
        );
      } else {
        setFormDataValue(
          dataField,
          value ? get(defaultValues, dataField) : null
        );
      }
    } else {
      if (forceRepaint) {
        setFormDataState(dataField, value, forceMerge);
      } else {
        setFormDataValue(dataField, value, forceMerge);
      }
    }
  };

  const setFormDataValue = (dataField, value, forceMerge = false) => {
    if (forceMerge) {
      merge(formData, set(cloneDeep(formData), dataField, value));
      merge(changedFormData, set(cloneDeep(changedFormData), dataField, value));
    } else {
      set(formData, dataField, value);
      set(changedFormData, dataField, value);
    }
  };

  const setFormDataState = (dataField, value, forceMerge = false) => {
    if (forceMerge) {
      setFormData((oldData) => {
        let newData = cloneDeep(oldData);
        merge(newData, set(cloneDeep(newData), dataField, value));
        return newData;
      });
      merge(changedFormData, set(cloneDeep(changedFormData), dataField, value));
    } else {
      setFormData((oldData) => {
        let newData = cloneDeep(oldData);
        set(newData, dataField, value);
        return newData;
      });
      set(changedFormData, dataField, value);
    }
  };

  const onClickDisplayChooserMaster = () => {
    setMasterPopupVisible(true);
  };

  const closeDisplayChooserMaster = () => {
    setMasterPopupVisible(false);
  };

  const onSaveMasterPopup = (e) => {
    closeDisplayChooserMaster();
    onFormItemValueChangedData({
      dataField: e.dataField,
      value: e.chosenDisplaysIds,
      forceRepaint: true,
    });
  };

  const clearUnusedProtocols = (data) => {
    const unusedProtocols = POSSIBLE_PROTOCOLS.filter(
      (item) => !USED_PROTOCOLS.includes(item)
    );
    unusedProtocols.forEach((protocolName) => {
      if (data?.displaySettings?.[protocolName]) {
        delete data.displaySettings[protocolName];
      }
    });
    return data;
  };

  return (
    <React.Fragment>
      {masterPopupVisible && (
        <DisplayChooserMasterPopup
          windowId={windowId}
          closeEditorPopup={closeDisplayChooserMaster}
          saveResult={onSaveMasterPopup}
          data={formData}
          dataField={"displayIds"}
          dataPath={"cond"}
        />
      )}

      <Popup
        className="customEditorPopup"
        visible={true}
        onHiding={closePopup}
        titleComponent={() => {
          return (
            <span className="popup-title">
              {broadcastProgramPopupOptions.title}
            </span>
          );
        }}
        {...broadcastProgramPopupOptions}
      >
        <Position my="center" at="center" of={window} />
        <ScrollView showScrollbar="always">
          <Form validationGroup="formEditor">
            <SimpleItem>
              <div className="editor-content">
                <div className="dx-form-group-with-caption dx-form-group editor-group">
                  <div className="dx-form-group-caption">Поля</div>
                  <div className="dx-form-group-content editor-content">
                    <div className="editor-item">
                      <div className="flex-row">
                        <div className="dx-field width-half">
                          <div className="dx-field-label">Название</div>
                          <div className="dx-field-value">
                            <TextBox
                              defaultValue={formData.name}
                              onInput={onFormItemValueChanged}
                              dataField="name"
                            >
                              <Validator validationGroup="formEditor">
                                <RequiredRule />
                              </Validator>
                            </TextBox>
                          </div>
                        </div>
                        <div className="dx-field width-half">
                          <div className="dx-field-label">Приоритет</div>
                          <div className="dx-field-value">
                            <NumberBox
                              defaultValue={formData.priority}
                              onValueChanged={onFormItemValueChanged}
                              dataField="priority"
                              showSpinButtons={true}
                            >
                              <Validator validationGroup="formEditor">
                                <RequiredRule />
                              </Validator>
                            </NumberBox>
                          </div>
                        </div>
                      </div>
                      <div className="dx-field width-half">
                        <div className="dx-field-label">Иконка</div>
                        <div className="dx-field-value">
                          <SelectBox
                            defaultValue={formData.iconName}
                            onInput={onFormItemValueChanged}
                            dataField="iconName"
                            items={DX_ICONS}
                            allowClearing={true}
                            onValueChanged={onFormItemValueChanged}
                            elementAttr={{ style: "width: 80px" }}
                            itemRender={(e) => {
                              return <i className={"dx-icon-" + e}></i>;
                            }}
                            fieldRender={(e) => {
                              return (
                                <div
                                  className="custom-item"
                                  style={{
                                    minHeight: "34px",
                                    display: "flex",
                                    alignItems: "center",
                                  }}
                                >
                                  <i
                                    className={"dx-icon-" + e}
                                    style={{ paddingLeft: "8px" }}
                                  ></i>
                                  <TextBox
                                    className="product-name"
                                    defaultValue={e}
                                    readOnly={true}
                                    visible={false}
                                  />
                                </div>
                              );
                            }}
                          >
                            <Validator validationGroup="formEditor">
                              <RequiredRule />
                            </Validator>
                          </SelectBox>
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">Начало действия</div>
                        <div className="dx-field-value">
                          <DateBox
                            defaultValue={formData.startTimeUtc}
                            onValueChanged={onFormItemValueChanged}
                            dataField="startTimeUtc"
                            type="datetime"
                          ></DateBox>
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">Окончание действия</div>
                        <div className="dx-field-value">
                          <DateBox
                            defaultValue={formData.endTimeUtc}
                            onValueChanged={onFormItemValueChanged}
                            dataField="endTimeUtc"
                            type="datetime"
                          ></DateBox>
                        </div>
                      </div>
                      <div className="flex-row">
                        <div className="dx-field width-half">
                          <SeTextBox
                            data={formData}
                            onFormItemValueChanged={(e) => {
                              let value = e.value
                                ? e.value
                                : e?.component?.option("text");

                              value =
                                /^(0[0-9]|1[0-9]|2[0-9]):[0-5][0-9]:[0-5][0-9]$/.test(
                                  value
                                )
                                  ? value
                                  : value + ":00";

                              const dataField = e.component.option("dataField");
                              onFormItemValueChangedData({
                                value: value,
                                dataField: dataField,
                                forceRepaint: true,
                                enabler: e.component.option("enabler"),
                              });
                            }}
                            editModeEnabled={editModeEnabled}
                            dataField={"timeOfDayFrom"}
                            title={"Начало времени действия"}
                            allowDisabling={true}
                            placeholder={"00:00:00"}
                          >
                            <Validator validationGroup="formEditor">
                            <CustomRule
                                message={"Формат должен быть ЧЧ:ММ:СС"}
                                validationCallback={(e) => {
                                  if (!e.value) {
                                    return true;
                                  }
                                  return /^(0[0-9]|1[0-9]|2[0-9]):[0-5][0-9]:[0-5][0-9]$/.test(
                                    e.value
                                  );
                                }}
                              />
                              <CustomRule
                                message={
                                  "Начало времени действия должно быть до окончания"
                                }
                                type={"custom"}
                                ignoreEmptyValue={true}
                                validationCallback={(e) => {
                                  if (!formData.timeOfDayTo || !e.value) {
                                    return true;
                                  }
                                  const value = e.value;

                                  const format =
                                    /^(0[0-9]|1[0-9]|2[0-9]):[0-5][0-9]:[0-5][0-9]$/.test(
                                      value
                                    )
                                      ? "HH:mm:ss"
                                      : "HH:mm";

                                  const formatDayTo =
                                    /^(0[0-9]|1[0-9]|2[0-9]):[0-5][0-9]:[0-5][0-9]$/.test(
                                      formData.timeOfDayTo
                                    )
                                      ? "HH:mm:ss"
                                      : "HH:mm";

                                  return dayjs(value, format).isBefore(
                                    dayjs(formData.timeOfDayTo, formatDayTo)
                                  );


                                }}
                              />
                              <CustomRule
                                message={
                                  "Время начала должно быть установленно вместе со временем окончания"
                                }
                                validationCallback={(e) => {
                                  return !(!!formData.timeOfDayTo && !e.value);
                                }}
                              />
                            </Validator>
                          </SeTextBox>
                        </div>
                        <div className="dx-field width-half">
                          <SeTextBox
                            data={formData}
                            onFormItemValueChanged={(e) => {
                              let value = e.value
                                ? e.value
                                : e?.component?.option("text");

                              value =
                                /^(0[0-9]|1[0-9]|2[0-9]):[0-5][0-9]:[0-5][0-9]$/.test(
                                  value
                                )
                                  ? value
                                  : value + ":00";

                              const dataField = e.component.option("dataField");
                              onFormItemValueChangedData({
                                value: value,
                                dataField: dataField,
                                forceRepaint: true,
                                enabler: e.component.option("enabler"),
                              });
                            }}
                            editModeEnabled={editModeEnabled}
                            dataField={"timeOfDayTo"}
                            title={"Окончание времени действия"}
                            allowDisabling={true}
                            placeholder={"00:00:00"}
                          >
                            <Validator validationGroup="formEditor">
                              <CustomRule
                                message={"Формат должен быть ЧЧ:ММ:СС"}
                                validationCallback={(e) => {
                                  if (!e.value) {
                                    return true;
                                  }
                                  return /^(0[0-9]|1[0-9]|2[0-9]):[0-5][0-9]:[0-5][0-9]$/.test(
                                    e.value
                                  );
                                }}
                              />
                              <CustomRule
                                message={
                                  "Окончание времени действия должно быть после начала"
                                }
                                type={"custom"}
                                ignoreEmptyValue={true}
                                validationCallback={(e) => {
                                  if (!formData.timeOfDayFrom || !e.value) {
                                    return true;
                                  }
                                  const value = e.value;

                                  const format =
                                    /^(0[0-9]|1[0-9]|2[0-9]):[0-5][0-9]:[0-5][0-9]$/.test(
                                      e.value
                                    )
                                      ? "HH:mm:ss"
                                      : "HH:mm";

                                  const formatDayFrom =
                                    /^(0[0-9]|1[0-9]|2[0-9]):[0-5][0-9]:[0-5][0-9]$/.test(
                                      e.value
                                    )
                                      ? "HH:mm:ss"
                                      : "HH:mm";

                                  return dayjs(value, format).isAfter(
                                    dayjs(formData.timeOfDayFrom, formatDayFrom)
                                  );
                                }}
                              />
                              <CustomRule
                                message={
                                  "Время начала должно быть установленно вместе со временем окончания"
                                }
                                validationCallback={(e) => {
                                  return !(
                                    !!formData.timeOfDayFrom && !e.value
                                  );
                                }}
                              />
                            </Validator>
                          </SeTextBox>
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">Дни недели</div>
                        <div className="dx-field-value">
                          <ButtonsArray
                            defaultValue={formData.daysOfWeek}
                            onFormItemValueChangedData={
                              onFormItemValueChangedData
                            }
                            dataField={"daysOfWeek"}
                            buttons={[
                              {
                                key: "monday",
                                text: "Пн",
                              },
                              {
                                key: "thuesday",
                                text: "Вт",
                              },
                              {
                                key: "wednesday",
                                text: "Ср",
                              },
                              {
                                key: "thursday",
                                text: "Чт",
                              },
                              {
                                key: "friday",
                                text: "Пт",
                              },
                              {
                                key: "saturday",
                                text: "Сб",
                              },
                              {
                                key: "sunday",
                                text: "Вс",
                              },
                            ]}
                          />
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">Фильтр по табло</div>
                        <div className="dx-field-value">
                          <TagBox
                            defaultValue={formData.cond?.displayIds || []}
                            value={formData.cond?.displayIds || []}
                            onValueChanged={onFormItemValueChanged}
                            dataSource={{
                              store: displaysDataStore,
                              onLoadError: (error) => {
                                onDataErrorOccurred({ error });
                              },
                            }}
                            valueExpr="id"
                            displayExpr="name"
                            dataField="cond.displayIds"
                            placeholder="Нет"
                            forceRepaint={true}
                            searchEnabled={true}
                            showClearButton={true}
                            multiline={false}
                          >
                            <TagBoxButton name="clear" />
                            <TagBoxButton
                              name="displayMaster"
                              location="after"
                              options={{
                                icon: "edit",
                                text: "Мастер выбора",
                                onClick: onClickDisplayChooserMaster,
                              }}
                              style={{ margin: 0 }}
                            />
                          </TagBox>
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">
                          Фильтр по группам табло
                        </div>
                        <div className="dx-field-value">
                          <TagBox
                            defaultValue={formData.cond?.displayGroupIds || []}
                            onValueChanged={onFormItemValueChanged}
                            dataSource={{
                              store: displayGroupsDataStore,
                              onLoadError: (error) => {
                                onDataErrorOccurred({ error });
                              },
                            }}
                            valueExpr="id"
                            displayExpr="name"
                            dataField="cond.displayGroupIds"
                            placeholder="Нет"
                            searchEnabled={true}
                            showClearButton={true}
                            multiline={false}
                          ></TagBox>
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">
                          Фильтр по тэгам табло
                        </div>
                        <div className="dx-field-value">
                          <TagBox
                            defaultValue={formData.cond?.tagIds || []}
                            dataSource={{
                              store: tagsDataStore,
                              onLoadError: (error) => {
                                onDataErrorOccurred({ error });
                              },
                            }}
                            dataField="cond.tagIds"
                            onValueChanged={onFormItemValueChanged}
                            placeholder="Нет"
                            valueExpr="id"
                            displayExpr="name"
                            searchEnabled={true}
                            searchTimeout={2000}
                            showClearButton={true}
                          ></TagBox>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="dx-form-group-with-caption dx-form-group editor-group">
                  <div className="dx-form-group-content editor-content">
                    <div className="editor-item">
                      <div className="dx-field">
                        <div className="dx-field-label">Шаблон</div>
                        <div className="dx-field-value">
                          <SelectBox
                            value={templateId}
                            dataSource={{
                              store: broadcastsProgramsTemplatesDataStore,
                              onLoadError: (error) => {
                                onDataErrorOccurred(error);
                              },
                            }}
                            allowClearing={true}
                            valueExpr="id"
                            displayExpr="name"
                            onValueChanged={(e) => {
                              setTemplateId(e.value);
                            }}
                          >
                            <SelectBoxButton
                              name="apply"
                              location="after"
                              options={{
                                icon: "check",
                                text: "Применить",
                                onClick: onApplyTemplate,
                              }}
                              style={{ margin: 0 }}
                            />
                          </SelectBox>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                {showSettingsOnReset && (
                    <Fragment>
                      <OnUnmountHandle
                          onUnmount={() => {
                            setShowSettingsOnReset(true)
                          }}
                      />

                      <WeatherForecastsSettingsComponent
                          className="editor-group cols-2"
                          data={formData.displaySettings ?? {}}
                          onFormItemValueChanged={onFormItemValueChanged}
                          windowId={windowId}
                          editModeEnabled={editModeEnabled}
                          dataField={"displaySettings"}
                          onError={(error) => {
                            onDataErrorOccurred({ error });
                          }}
                      />
                      {mainEditorsSettings?.forecasts?.show && (
                          <ForecastsSettingsComponent
                              className="editor-group cols-2"
                              data={formData.displaySettings ?? {}}
                              onFormItemValueChanged={onFormItemValueChanged}
                              windowId={windowId}
                              editModeEnabled={editModeEnabled}
                              dataField={"displaySettings"}
                              onError={(error) => {
                                onDataErrorOccurred({ error });
                              }}
                          />
                      )}
                      {mainEditorsSettings?.forecastSource?.show && (
                          <ForecastSourceComponent
                              className="editor-group cols-2"
                              data={formData.displaySettings?.forecastSource ?? {}}
                              onFormItemValueChanged={onFormItemValueChanged}
                              windowId={windowId}
                              editModeEnabled={editModeEnabled}
                              dataField={"displaySettings.forecastSource"}
                              onError={(error) => {
                                onDataErrorOccurred({ error });
                              }}
                          />
                      )}
                      {protocolsSettings?.itLineU646?.show && (
                          <U646SettingsComponent
                              className="editor-group"
                              data={formData.displaySettings?.itLineU646 ?? {}}
                              onFormItemValueChanged={onFormItemValueChanged}
                              onFormItemValueChangedData={onFormItemValueChangedData}
                              editModeEnabled={editModeEnabled}
                              dataField={"displaySettings.itLineU646"}
                              windowId={windowId}
                              onError={(error) => {
                                onDataErrorOccurred({ error });
                              }}
                          />
                      )}
                      {protocolsSettings?.smartJson2?.show && (
                          <SmartJson2SettingsComponent
                              className="editor-group"
                              data={formData.displaySettings?.smartJson2 ?? {}}
                              onFormItemValueChanged={onFormItemValueChanged}
                              onFormItemValueChangedData={onFormItemValueChangedData}
                              editModeEnabled={editModeEnabled}
                              dataField={"displaySettings.smartJson2"}
                              windowId={windowId}
                              onError={(error) => {
                                onDataErrorOccurred({ error });
                              }}
                          />
                      )}
                    </Fragment>
                )}



              </div>
            </SimpleItem>
          </Form>
        </ScrollView>
        <ValidationSummary
          id="summary"
          validationGroup="formEditor"
        ></ValidationSummary>
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Сохранить", onClick: onSave }}
          toolbar="bottom"
        />
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Отмена", onClick: onCancel }}
          toolbar="bottom"
        />
        <ToolbarItem toolbar="bottom" location="left">
          <Switch
            defaultValue={editModeEnabled}
            switchedOffText="Настройка редактора"
            switchedOnText="Закончить настройку"
            width="8rem"
            onValueChanged={(e) => {
              setEditModeEnabled(e.value);
            }}
          />
        </ToolbarItem>
      </Popup>
    </React.Fragment>
  );
};

export default connect(null, { localDeauthorize: deauthorize })(
  BroadcastProgramsEditorPopup
);
