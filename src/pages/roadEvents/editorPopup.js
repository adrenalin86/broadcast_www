import React, { useState, useEffect } from "react";
import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import { ScrollView } from "devextreme-react/scroll-view";
import Form, { SimpleItem } from "devextreme-react/form";
import { TextBox, Lookup, TextArea, DateBox } from "devextreme-react";
import ValidationSummary from "devextreme-react/validation-summary";
import ValidationEngine from "devextreme/ui/validation_engine";
import Validator, { RequiredRule } from "devextreme-react/validator";
import { set, cloneDeep, isEmpty } from "lodash-es";
import { SeMapSelectPoint } from "../../components/editors";
import { getRoadEventTypesDataStore, WINDOWS } from "../../dataStores";
import config from "../../config";

const windowId = WINDOWS.ROAD_EVENTS.id;

const roadEventTypesDataStore = getRoadEventTypesDataStore({ windowId: windowId });

const EditorPopup = ({ editedRow, closeEditorPopup, updateEntry, createEntry }) => {
  const [formData, setFormData] = useState({});
  const [changedFormData] = useState({});

  const popupOptions = config.pages.roadEvents.grid.editorPopup.dxOptions;
  const startingCenter = config.pages.roadEvents.map.startingCenter;

  const closePopup = () => {
    closeEditorPopup();
  };

  const onSave = () => {
    const isValid = ValidationEngine.validateGroup("formEditor").isValid;
    if (isValid) {
      if (isEmpty(editedRow)) {
        createEntry({
          formData: formData,
        });
      } else {
        updateEntry({
          formData: changedFormData,
          editedRowId: editedRow.id,
        });
      }
    }
  };

  const onCancel = () => {
    closePopup();
  };

  const onFormItemValueChanged = (e) => {
    const value = e.value !== undefined ? e.value : e?.component?.option("text");
    const dataField = e.component.option("dataField");
    if (dataField) {
      setFormDataValue(dataField, value);
    }
  };

  const setFormDataValue = (dataField, value) => {
    set(formData, dataField, value);
    set(changedFormData, dataField, value);
  };

  useEffect(() => {
    if (!isEmpty(editedRow)) {
      setFormData(cloneDeep(editedRow));
    }
  }, []);

  return (
    <React.Fragment>
      <Popup
        className="customEditorPopup"
        visible={true}
        onHiding={closePopup}
        titleComponent={() => {
          return <span className="popup-title">{popupOptions.title}</span>;
        }}
        {...popupOptions}
      >
        <Position my="center" at="center" of={window} />
        <ScrollView showScrollbar="always">
          <Form validationGroup="formEditor">
            <SimpleItem>
              <div className="editor-content">
                <div className="dx-form-group-with-caption dx-form-group editor-group">
                  <div className="dx-form-group-caption">Общие сведения</div>
                  <div className="dx-form-group-content editor-content">
                    <div className="editor-item">
                      <div className="dx-field">
                        <div className="dx-field-label">Заголовок</div>
                        <div className="dx-field-value">
                          <TextBox
                            defaultValue={formData.title}
                            onInput={onFormItemValueChanged}
                            dataField={"title"}
                          >
                            <Validator validationGroup="formEditor">
                              <RequiredRule />
                            </Validator>
                          </TextBox>
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">ID пользователя</div>
                        <div className="dx-field-value">
                          <TextBox
                            defaultValue={formData.webUserId}
                            dataField={null}
                            disabled={true}
                          />
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">Дата создания</div>
                        <div className="dx-field-value">
                          <DateBox
                            onValueChanged={(e) => {
                              e.value = new Date(e.value).toISOString().split(".")[0] + "Z";
                              onFormItemValueChanged(e);
                            }}
                            type="datetime"
                            defaultValue={formData.creationTimeUtc}
                            dataField={"creationTimeUtc"}
                          >
                            <Validator validationGroup="formEditor">
                              <RequiredRule />
                            </Validator>
                          </DateBox>
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">Тип</div>
                        <div className="dx-field-value">
                          <Lookup
                            defaultValue={formData.typeId}
                            dataSource={roadEventTypesDataStore}
                            valueExpr={"id"}
                            displayExpr={"name"}
                            searchExpr={["name"]}
                            searchEnabled={true}
                            dataField={"typeId"}
                            onValueChanged={onFormItemValueChanged}
                          >
                            <Validator validationGroup="formEditor">
                              <RequiredRule />
                            </Validator>
                          </Lookup>
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">Положение на карте</div>
                        <div className="dx-field-value">
                          <SeMapSelectPoint
                            formData={formData}
                            setFormDataValue={setFormDataValue}
                            defaultLat={startingCenter.lat}
                            defaultLng={startingCenter.lng}
                          />
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">Содержание</div>
                        <div className="dx-field-value">
                          <TextArea
                            defaultValue={formData.content}
                            onInput={onFormItemValueChanged}
                            dataField={"content"}
                          >
                            <Validator validationGroup="formEditor">
                              <RequiredRule />
                            </Validator>
                          </TextArea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </SimpleItem>
          </Form>
        </ScrollView>
        <ValidationSummary id="summary" validationGroup="formEditor"></ValidationSummary>
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Сохранить", onClick: onSave }}
          toolbar="bottom"
        />
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Отмена", onClick: onCancel }}
          toolbar="bottom"
        />
      </Popup>
    </React.Fragment>
  );
};

export default EditorPopup;
