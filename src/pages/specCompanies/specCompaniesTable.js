import DataGrid, { Button, Column, Export } from "devextreme-react/data-grid";
import config from "../../config";
import { getSpecCompaniesDataStore, WINDOWS } from "../../dataStores";

const windowId = WINDOWS.SPEC_COMPANIES.id;

const specCompaniesDataStore = getSpecCompaniesDataStore({
  windowId: windowId,
});

const SpecCompaniesTable = ({
  datagridRef,
  openEditorPopup,
  openDeleteRowPopup,
  openCreateEntryPopup,
  onDataErrorOccurred,
}) => {
  const specCompaniesGridOptions = config.pages.specCompanies.specCompaniesGrid.dxOptions;

  const onToolbarPreparing = (e) => {
    let toolbarItems = e.toolbarOptions.items;

    toolbarItems.unshift({
      widget: "dxButton",
      options: {
        icon: "add",
        onClick: openCreateEntryPopup,
      },
      location: "before",
    });
  };

  return (
    <div className="dx-card responsive-paddings">
      <DataGrid
        ref={datagridRef}
        dataSource={specCompaniesDataStore}
        onDataErrorOccurred={onDataErrorOccurred}
        onToolbarPreparing={onToolbarPreparing}
        {...specCompaniesGridOptions}
      >
        <Export enabled={true} allowExportSelectedData={true} />
        <Column dataField="id" caption="id" alignment="left" />
        <Column dataField="name" caption="Название" />
        <Column dataField="address" caption="Адрес" />
        <Column dataField="inn" caption="ИНН" />
        <Column dataField="ogrn" caption="ОГРН" />
        <Column dataField="note" caption="Заметки" />
        <Column type="buttons" alignment="center">
          <Button
            name="custom-edit"
            text="редактировать"
            icon="edit"
            onClick={(e) => openEditorPopup(e.row.key)}
          />
          <Button
            name="custom-delete"
            text="удалить"
            icon="trash"
            onClick={(e) => openDeleteRowPopup(e.row.key)}
          />
        </Column>
      </DataGrid>
    </div>
  );
};

export default SpecCompaniesTable;
