import React, {useEffect, useRef} from 'react';
import DataGrid, {Button, Column, ColumnChooser, StateStoring} from "devextreme-react/data-grid";
import config from "../../config";
import ColumnForDevices from "./ColumnForDevices";
import {activeIcon} from "../../utils/utils";
import {TagBox} from "devextreme-react";

const AUTOMATIC_UPDATE_TIMEOUT_MS = config.pages.smartStations.grid.updateInterval;


function SmartStationsTable({
                                counter, openCreateEntryPopup, openEditorPopup, openDeleteRowPopup, onDataErrorOccurred,
                                roadSmartStations, roadSmartStationsDataStore, openVideoStreamPopup, openViewPopupFromMap
                            }) {

    const gridOptions = config.pages.roadEvents.grid.dxOptions;


    useEffect(() => {
        const updateData = () => {
            roadSmartStations
                .load()
                .then((result) => {
                    let changes = [];
                    result.data.forEach((item) => {
                        changes.push({
                            type: "update",
                            key: item.id,
                            data: item,
                        });
                    });
                    setItems(result.data)
                    roadSmartStations?.push(changes);
                })
                .catch((error) => {
                    onDataErrorOccurred({error});
                });
        };

        const timer = setInterval(() => {
            updateData();
        }, AUTOMATIC_UPDATE_TIMEOUT_MS);

        return () => {
            clearInterval(timer);
        };
    }, [])

    const datagridRef = useRef(null);

    function onToolbarPreparing(e) {
        let toolbarItems = e.toolbarOptions.items;

        toolbarItems.unshift({
            widget: "dxButton",
            options: {
                icon: "add",
                onClick: openCreateEntryPopup,
            },
            location: "before",
        });
    }

    useEffect(() => {
        (async function () {
            datagridRef?.current?.instance?.refresh()
        }());
    }, [counter]);


    const getData = async () => {
        let data = await datagridRef.current?.instance.getDataSource()?.store()?.load();
        if (data?.data) {
            setItems(data?.data)
        }
    }

    React.useEffect(() => {
        (async function () {
            await getData()
        }())
    }, [datagridRef.current, counter])

    const [items, setItems] = React.useState([])

    const TableMemo = React.useMemo(() => {
        return <DataGrid
            style={{marginTop: "20px"}}
            id="displayEditorDataGrid"
            ref={datagridRef}
            dataSource={roadSmartStationsDataStore}
            onDataErrorOccurred={onDataErrorOccurred}
            onToolbarPreparing={onToolbarPreparing}
            errorRowEnabled={false}
            onContentReady={(e) => {
                e.component.state().columns.forEach((col, index) => {
                    e.component.columnOption(col.name, "visibleIndex", index);
                });
            }}
            {...gridOptions}
        >
            <StateStoring
                enabled={true}
                type="localStorage"
                storageKey="pages.smartStations.displaysGrid"
            />
            <ColumnChooser enabled={true}/>
            <Column allowSorting dataField="id"/>
            <Column allowSorting type="string" dataField="name" caption="Название"/>
            <Column allowSorting type="string" dataField="description" caption="Описание"/>
            <Column allowSorting type="string" dataField="address" caption="Адрес"/>
            <Column allowSorting type="string" dataField="model" caption="Модель"/>
            <Column allowSorting type="string" dataField="tags" caption="Теги" cellComponent={(e) => {
                return <React.Fragment>
                    {(e.data?.value ? e.data?.value.length !== 0 : false) && (

                            e.data?.value?.map(el => {
                                return <div>
                                    <div className="dx-tag">
                                        <div
                                            className="dx-tag-content"
                                            style={{ padding: "3px 6px 4px 6px" }}
                                        >
                                            <span>{el.name}</span>
                                        </div>
                                    </div>
                                </div>
                            })

                    )}
                </React.Fragment>
                }
            }/>
            <Column
                dataField="videoCameraIds"
                dataType="string"
                caption="Видеокамеры"
                cellComponent={(e) => {
                    const {data: {videoCameras, mainVideoCamera, mainVideoCameraId}, rowIndex} = e.data
                    const activeMainCamera = mainVideoCamera?.status?.connected === 1 ? 2 : 1
                    return (
                        <div className="smartStations-row">
                            {mainVideoCamera ?
                                <div className="mainCameraStyle">
                                    <ColumnForDevices openVideoStreamPopup={openVideoStreamPopup} counter={counter}
                                                      rowIndex={rowIndex} el={mainVideoCameraId}
                                                      active={activeMainCamera}
                                                      data={mainVideoCamera}/>
                                </div>
                                : null}
                            {
                                e.data?.value?.length ? e.data?.value.map(el => {
                                    const active = activeIcon(videoCameras, el)
                                    return <ColumnForDevices openVideoStreamPopup={openVideoStreamPopup}
                                                             counter={counter} rowIndex={rowIndex} el={el}
                                                             active={active} data={videoCameras}/>
                                }) : null
                            }
                        </div>
                    );
                }}
            />
            <Column
                dataField="deviceIds"
                dataType="string"
                caption="Оборудование"
                cellComponent={(e) => {
                    const {data: {devices}, rowIndex} = e.data
                    return (
                        <div className="smartStations-row">
                            {
                                e.data?.value?.length ? e.data?.value.map(el => {
                                    const active = activeIcon(devices, el, 'deviceId')
                                    return <ColumnForDevices counter={counter} rowIndex={rowIndex} el={el}
                                                             active={active} data={devices} type="device"/>
                                }) : null
                            }
                        </div>
                    );
                }}
            />
            <Column
                dataField="displayIds"
                dataType="string"

                caption="Табло"
                cellComponent={(e) => {
                    const {data: {displays}, rowIndex} = e.data
                    return (
                        <div className="smartStations-row">
                            {
                                e.data?.value?.length ? e.data?.value.map(el => {
                                    const active = activeIcon(displays, el, 'displayId')
                                    return <ColumnForDevices counter={counter} rowIndex={rowIndex} el={el}
                                                             active={active} data={displays} type="display"/>
                                }) : null
                            }
                        </div>
                    );
                }}
            />
            <Column visible={false} dataField="lat" caption="Широта" />
            <Column visible={false} dataField="lng" caption="Долгота"/>
            <Column type="buttons">
                <Button text="Просмотр" onClick={({row}) => {
                    const {data: {id}} = row
                    openViewPopupFromMap(id)
                }}/>
            </Column>
            <Column type="buttons">
                <Button name="custom-edit" text="Изменить" icon="edit" onClick={openEditorPopup}/>
                <Button
                    name="custom-delete"
                    text="Удалить"
                    icon="trash"
                    onClick={openDeleteRowPopup}
                />
            </Column>
        </DataGrid>
    }, [items, datagridRef, roadSmartStationsDataStore])

    return (
        TableMemo
    );
}

export default SmartStationsTable;
