import {
  combineReducers,
  configureStore,
  getDefaultMiddleware,
} from "@reduxjs/toolkit";
import { reduxBatch } from "@manaflair/redux-batch";
import { connectRouter, routerMiddleware } from "connected-react-router";

import { history } from "../services/history";

import { saveUserConfigMiddleware } from "./saveUserConfigMiddleware";
import commonReducer, { COMMON_REDUCER_KEY } from "./commonSlice";
import displayModelsPageReducer, {
  DISPLAY_MODELS_PAGE_REDUCER_KEY,
} from "./displayModelsSlice";
import displaysPageReducer, {
  DISPLAYS_PAGE_REDUCER_KEY,
} from "./displaysSlice";
import userActionsPageReducer, {
  USER_ACTIONS_PAGE_REDUCER_KEY,
} from "./userActionsSlice";
import usersPageReducer, { USERS_PAGE_REDUCER_KEY } from "./usersSlice";
import displayGroupsPageReducer, {
  DISPLAY_GROUPS_PAGE_REDUCER_KEY,
} from "./displayGroupsSlice";
import broadcastProgramsPageReducer, {
  BROADCAST_PROGRAMS_PAGE_REDUCER_KEY,
} from "./broadcastProgramsSlice";
import reportsPageReducer, { REPORTS_PAGE_REDUCER_KEY } from "./reportsSlice";

const rootReducer = combineReducers({
  router: connectRouter(history),
  [COMMON_REDUCER_KEY]: commonReducer,
  pages: combineReducers({
    [DISPLAYS_PAGE_REDUCER_KEY]: displaysPageReducer,
    [DISPLAY_MODELS_PAGE_REDUCER_KEY]: displayModelsPageReducer,
    [USER_ACTIONS_PAGE_REDUCER_KEY]: userActionsPageReducer,
    [USERS_PAGE_REDUCER_KEY]: usersPageReducer,
    [DISPLAY_GROUPS_PAGE_REDUCER_KEY]: displayGroupsPageReducer,
    [BROADCAST_PROGRAMS_PAGE_REDUCER_KEY]: broadcastProgramsPageReducer,
    [REPORTS_PAGE_REDUCER_KEY]: reportsPageReducer,
  }),
});

const store = configureStore({
  reducer: rootReducer,
  middleware: [
    ...getDefaultMiddleware(),
    saveUserConfigMiddleware,
    routerMiddleware(history),
  ],
  enhancers: [reduxBatch],
});

export default store;
