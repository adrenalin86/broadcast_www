import React, { useRef, useEffect } from 'react'

export const OnUnmountHandle = ({ onUnmount }) => {
    const onUnmountRef = useRef(onUnmount)

    onUnmountRef.current = onUnmount ? onUnmount : onUnmountRef.current

    useEffect(() => {
        return () => {
            setTimeout(() => {
                onUnmountRef.current()
            }, 0)
        }
    }, [])

    return null;
};
