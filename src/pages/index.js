export { default as DefaultPage } from "./default";
export { default as DisplayModelsPage } from "./displayModels";
export { default as DisplaysPage } from "./displays";
export { default as UserActionsPage } from "./userActions";
export { default as UsersPage } from "./users";
export { default as DisplayGroupsPage } from "./displayGroups";
export {
  DisplaysRegistryHistory as DisplayRegistryHistoryPage,
  DisplayModelsRegistryHistory as DisplayModelsRegistryHistoryPage,
  DeviceModelsRegistryHistory as DeviceModelsRegistryHistoryPage,
  GroupsRegistryHistory as GroupsRegistryHistoryPage,
  SmartStationsGroupsRegistryHistory as SmartStationsGroupsRegistryHistoryPage,
  UsersRegistryHistory as UsersRegistryHistoryPage,
  DisplayProtocolsRegistryHistory as DisplayProtocolsRegistryHistoryPage,
  BroadcastProgramTemplatesRegistryHistory as BroadcastProgramTemplatesRegistryHistoryPage,
  TagsRegistryHistory as TagsRegistryHistoryPage,
  VideoCamerasRegistryHistory as VideoCamerasRegistryHistoryPage,
  DisplayWorkingRegistryHistory as DisplayWorkingRegistryHistoryPage,
  BroadcastProgramShowRegistryHistory as BroadcastProgramShowRegistryHistoryPage,
} from "./registryHistory";
export { default as DisplayProtocolsPage } from "./displayProtocols";
export { default as BroadcastProgramsPage } from "./broadcastPrograms";
export { default as BroadcastProgramTemplatesPage } from "./broadcastProgramTemplates";
export { default as TagsPage } from "./tags";
export { default as PinCodesPage } from "./pinCodes";
export { default as BroadcastRequestForDisplayInfoPage } from "./broadcastRequestForDisplayInfo";
export { default as AreasEditorPage } from "./areas";
export { default as WebNewsPage } from "./webNews";
export { default as WebClaimsPage } from "./webClaims";
export { default as PollsPage } from "./polls";
export { default as RoadEventsPage } from "./roadEvents";
export { default as VehicleRoutesPage } from "./vehicleRoutes";
export { default as VehiclesPage } from "./vehicles";
export { default as ControlCenterPage } from "./controlCenter";
export { default as VideoCamerasPage } from "./videoCameras";
export { default as SpecTransportTypesPage } from "./specTransportTypes";
export { default as SpecCompaniesPage } from "./specCompanies";
export { default as SpecTransportsPage } from "./specTransports";
