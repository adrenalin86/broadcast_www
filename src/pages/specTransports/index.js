import { useEffect, useRef, useState } from "react";
import { connect } from "react-redux";
import notify from "devextreme/ui/notify";
import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import { isEmpty } from "lodash-es";
import { deauthorize } from "../../store/commonSlice";
import config from "../../config";
import { getSpecTransportsDataStore, getSystemWindowsDataStore, WINDOWS } from "../../dataStores";
import SpecTransportsTable from "./specTransportsTable";
import SpecTransportsEditorPopup from "./specTransportsEditorPopup";

const windowId = WINDOWS.SPEC_TRANSPORTS.id;

const specTransportsDataStore = getSpecTransportsDataStore({
  windowId: windowId,
});

const systemWindowsDataStore = getSystemWindowsDataStore({
  windowId: windowId,
});

const SpecTransportsPage = ({ localDeauthorize }) => {
  const [specTransportId, setSpecTransportId] = useState();
  const [deleteRowPopupVisible, setDeleteRowPopupVisible] = useState(false);
  const [editorPopupVisible, setEditorPopupVisible] = useState(false);
  const [windowTitle, setWindowTitle] = useState();

  const datagridRef = useRef(null);

  const onDataErrorOccurred = (e) => {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);

    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  };

  const openEditorPopup = (id) => {
    setSpecTransportId(id);
    setEditorPopupVisible(true);
  };

  const closeEditorPopup = () => {
    setEditorPopupVisible(false);
  };

  const openCreateEntryPopup = () => {
    setSpecTransportId(null);
    setEditorPopupVisible(true);
  };

  const openDeleteRowPopup = (id) => {
    setSpecTransportId(id);
    setDeleteRowPopupVisible(true);
  };

  const closeDeleteRowPopup = () => {
    setDeleteRowPopupVisible(false);
  };

  const createEntry = (formData) => {
    specTransportsDataStore
      .insert(formData)
      .then(() => {
        closeEditorPopup();
        datagridRef?.current?.instance?.refresh();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
      });
  };

  const updateEntry = ({ formData, editedRowId }) => {
    if (!isEmpty(formData)) {
      specTransportsDataStore
        .update(editedRowId, formData)
        .then(() => {
          closeEditorPopup();
          datagridRef?.current?.instance?.refresh();
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
        });
    } else {
      closeEditorPopup();
    }
  };

  const deleteEntry = () => {
    specTransportsDataStore
      .remove(specTransportId)
      .then(() => {
        closeDeleteRowPopup();
        datagridRef?.current?.instance?.refresh();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
      });
  };

  useEffect(() => {
    systemWindowsDataStore.byKey(windowId).then((result) => {
      const windowName = result[0]?.name?.split("/").pop();

      setWindowTitle(windowName);
    });
  }, []);

  return (
    <div className="content-block">
      <h2>{windowTitle}</h2>
      {editorPopupVisible && (
        <SpecTransportsEditorPopup
          specTransportId={specTransportId}
          createEntry={createEntry}
          updateEntry={updateEntry}
          closeEditorPopup={closeEditorPopup}
          onDataErrorOccurred={onDataErrorOccurred}
        />
      )}
      {deleteRowPopupVisible && (
        <Popup
          className="deleteRowPopup"
          width="auto"
          height="auto"
          visible={true}
          showCloseButton={false}
          showTitle={false}
          onHiding={closeDeleteRowPopup}
        >
          <Position my="center" at="center" of={window} />
          <div>
            <span>Вы уверены, что хотите удалить эту запись?</span>
          </div>
          <ToolbarItem
            widget="dxButton"
            location="center"
            toolbar="bottom"
            options={{ text: "Да", onClick: deleteEntry }}
          />
          <ToolbarItem
            widget="dxButton"
            location="center"
            toolbar="bottom"
            options={{ text: "Нет", onClick: closeDeleteRowPopup }}
          />
        </Popup>
      )}
      <SpecTransportsTable
        datagridRef={datagridRef}
        openEditorPopup={openEditorPopup}
        openDeleteRowPopup={openDeleteRowPopup}
        openCreateEntryPopup={openCreateEntryPopup}
        onDataErrorOccurred={onDataErrorOccurred}
      />
    </div>
  );
};

export default connect(
  (state) => ({}),
  (dispatch) => {
    return { localDeauthorize: () => dispatch(deauthorize()) };
  }
)(SpecTransportsPage);
