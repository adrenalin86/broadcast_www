import React, { useState, useEffect, useCallback, useRef } from "react";
import { connect } from "react-redux";
import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import Form, {EmailRule, SimpleItem} from "devextreme-react/form";
import { TextBox, CheckBox } from "devextreme-react";
import DataGrid, { Column, Editing } from "devextreme-react/data-grid";
import { Lookup as DXLookup, RadioGroup as DXRadioGroup } from "devextreme-react";
import { ScrollView } from "devextreme-react/scroll-view";
import notify from "devextreme/ui/notify";

import ValidationSummary from "devextreme-react/validation-summary";
import ValidationEngine from "devextreme/ui/validation_engine";
import Validator, { RequiredRule } from "devextreme-react/validator";

import { set, cloneDeep, isEmpty, differenceWith } from "lodash-es";

import {
  getAccessRightsDataStore,
  getDisplayGroupsDataStore,
  permissionsDataStore,
  ACCESS_TYPES,
  WINDOWS,
  getSystemWindowsDataStore,
} from "../../dataStores";

import { deauthorize } from "../../store/commonSlice";

import config from "../../config";

const windowId = WINDOWS.USERS.id;

const usersDefaultValues = {
  isActive: false,
};

const UsersEditorPopup = ({ editedRow, closeEditorPopup, updateEntry, createEntry, localDeauthorize }) => {
  const [formData, setFormData] = useState({});
  const [changedFormData] = useState({});
  const [displayGroupsAccessRightsDataArray, setDisplayGroupsAccessRightsDataArray] = useState([]);
  const [systemWindowsAccessRightsDataArray, setSystemWindowsAccessRightsDataArray] = useState([]);
  const [systemWindowsDataArray, setSystemWindowsDataArray] = useState([]);
  const [displayGroupsDataArray, setDisplayGroupsDataArray] = useState([]);
  const [accessRightsDataStoreActions] = useState({
    insertOrUpdate: [],
    delete: [],
  });

  const groupsGridRef = useRef(null);
  const windowsGridRef = useRef(null);

  const usersPopupOptions = config.pages.users.popupUsersEditor.dxOptions;
  const usersPopupGroupDatagridOptions = config.pages.users.popupUsersEditor.groupsDatagrid.dxOptions;

  /**
   * Инициализация хранилища прав доступа к группам. Производиться здесь, что-бы при каждом обновлении использующих
   * его компонентов не происходило повторных запросов.
   */
  const initDisplayGroupsAccessRightsDataArray = useCallback(async () => {
    let response = [];

    if (!isEmpty(editedRow)) {
      const store = getAccessRightsDataStore({
        windowId: windowId,
        userId: editedRow.id,
        type: ACCESS_TYPES.GROUPS,
      });
      await store
        .load()
        .then((data) => {
          response = data.data;
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
        });
    }

    setDisplayGroupsAccessRightsDataArray(response);

    return {
      id: "displayGroupsAccessRights",
      data: response,
    };
  });

  /**
   * Загрузка хранилища прав доступа к окнам. Производиться здесь, что-бы при каждом обновлении использующих
   * его компонентов не происходило повторных запросов.
   */
  const loadSystemWindowsAccessRightsDataArray = useCallback(async () => {
    let response = [];

    if (!isEmpty(editedRow)) {
      const store = getAccessRightsDataStore({
        windowId: windowId,
        userId: editedRow.id,
        type: ACCESS_TYPES.WINDOWS,
      });
      await store
        .load()
        .then((data) => {
          response = data.data;
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
        });
    }

    return {
      id: "systemWindowsAccessRights",
      data: response,
    };
  });

  const initSystemWindowsDataArray = useCallback(async () => {
    const store = getSystemWindowsDataStore();
    let response = [];

    await store
      .load()
      .then((data) => {
        response = data.data;
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
      });


    setSystemWindowsDataArray(response);

    return {
      id: "systemWindows",
      data: response,
    };
  });

  const initDisplayGroupsDataArray = useCallback(async () => {
    let response = [];
    if (!isEmpty(editedRow)) {
      const store = getDisplayGroupsDataStore({ windowId: windowId });
      await store
        .load()
        .then((data) => {
          response = data.data;
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
        });
    }

    setDisplayGroupsDataArray(response);
    return {
      id: "displayGroups",
      data: response,
    };
  });

  /**
   * Инициализация компонента.
   */
  useEffect(() => {
    if (isEmpty(editedRow)) {
      setFormData(cloneDeep(usersDefaultValues));
    } else {
      setFormData(cloneDeep(editedRow));
    }

    Promise.all([
      initDisplayGroupsAccessRightsDataArray(),
      loadSystemWindowsAccessRightsDataArray(),
      initSystemWindowsDataArray(),
      initDisplayGroupsDataArray(),
    ]).then((resolve, reject) => {
      let systemWindows = [];
      let systemWindowsAccessRights = [];

      if (!isEmpty(resolve)) {
        for (let index = 0; index < resolve.length; index++) {
          if (resolve[index]?.id === "systemWindowsAccessRights") {
            systemWindowsAccessRights = resolve[index].data;
          }
          if (resolve[index]?.id === "systemWindows") {
            systemWindows = resolve[index].data;
          }
        }
      }
      if (!isEmpty(reject)) {
        for (let index = 0; index < reject.length; index++) {
          if (reject[index]?.id === "systemWindowsAccessRights") {
            systemWindowsAccessRights = reject[index].data;
          }
          if (reject[index]?.id === "systemWindows") {
            systemWindows = reject[index].data;
          }
        }
      }

      if (!isEmpty(systemWindows)) {
        fillUndefinedAccessRights({ systemWindows, systemWindowsAccessRights });
      }
    });
  }, []);

  const fillUndefinedAccessRights = ({ systemWindows, systemWindowsAccessRights }) => {
    let windowsWithAccess = [];

    let newSystemWindowsAccessRights = [...systemWindowsAccessRights];

    newSystemWindowsAccessRights.forEach((item) => {
      windowsWithAccess.push(item.entityId);
    });

    //Добавляем в локальный список прав доступа недостающие права на окна как имеющие статус запрещённых.
    for (let item in systemWindows) {
      if (
        !windowsWithAccess.includes(systemWindows[item].id) &&
        systemWindows[item].id !== undefined
      ) {
        newSystemWindowsAccessRights.push({
          entityId: systemWindows[item].id,
          permission: 0,
          type: ACCESS_TYPES.WINDOWS,
          userId: editedRow.id,
          id: -1,
        });
      }
    }

    setSystemWindowsAccessRightsDataArray(newSystemWindowsAccessRights);
  };

  function onDataErrorOccurred(e) {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);
    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  }

  const closePopup = () => {
    closeEditorPopup();
  };

  const onSave = () => {
    groupsGridRef.current.instance.saveEditData();
    windowsGridRef.current.instance.saveEditData();

    const isValid = ValidationEngine.validateGroup("userFormEditor").isValid;

    if (isValid) {
      if (isEmpty(editedRow)) {
        createEntry({
          formData: formData,
          accessRightsDataStoreActions: accessRightsDataStoreActions,
        });
      } else {
        updateEntry({
          formData: changedFormData,
          editedRowId: editedRow.id,
          accessRightsDataStoreActions: accessRightsDataStoreActions,
        });
      }
    }
  };

  const onCancel = () => {
    closePopup();
  };

  const onGroupsToolbarPreparing = (e) => {
    let toolbarItems = e.toolbarOptions.items;

    toolbarItems.forEach(function (item) {
      if (item.name === "addRowButton") {
        item.location = "before";
      }
    });
  };

  const onFormItemValueChanged = (e) => {
    const value = e.value !== undefined ? e.value : e?.component?.option("text");
    const dataField = e.component.option("dataField");
    setFormDataValue(dataField, value);
  };

  const setFormDataValue = (dataField, value) => {
    set(formData, dataField, value);
    set(changedFormData, dataField, value);
  };

  //метод добавляет действие добавления записи в список accessRightsDataStore в список accessRightsDataStoreActions
  //он так-же следить что-бы не было дублирующихся и противоположных действий
  const insertAccessRightsEntry = (e) => {
    //проверить что такая-жа запись не стоит на удаление в списке действий удаления
    const alreadyOnDelete = accessRightsDataStoreActions.delete.some((item) => {
      if (item.entityId === e.formData.entityId && item.type === e.formData.type) {
        return true;
      } else {
        return false;
      }
    });
    if (alreadyOnDelete) {
      //исключить действие удаления для этой записи, что-бы не делать избыточных запросов
      accessRightsDataStoreActions.delete = accessRightsDataStoreActions.delete.filter((item) => {
        if (item.entityId === e.formData.entityId && item.type === e.formData.type) {
          return false;
        } else {
          return true;
        }
      });
    } else {
      //проверить что такая-же запись есть в списке действий на вставку или обновление
      const alreadyOnInsert = accessRightsDataStoreActions.insertOrUpdate.some((item) => {
        if (item.entityId === e.formData.entityId && item.type === e.formData.type) {
          return true;
        } else {
          return false;
        }
      });

      if (!alreadyOnInsert) {
        let alreadyInStore = true;

        //проверить есть-ли такая-же запись в groupsAccessRightsDataStore
        switch (e.formData.type) {
          case ACCESS_TYPES.GROUPS:
            alreadyInStore = displayGroupsAccessRightsDataArray.some((item) => {
              if (item.entityId === e.formData.entityId) {
                return true;
              } else {
                return false;
              }
            });
            break;
          case ACCESS_TYPES.WINDOWS:
            alreadyInStore = systemWindowsAccessRightsDataArray.some((item) => {
              if (item.entityId === e.formData.entityId) {
                return true;
              } else {
                return false;
              }
            });
            break;
          default:
            break;
        }

        if (!alreadyInStore) {
          accessRightsDataStoreActions.insertOrUpdate.push({
            userId: editedRow.id,
            permission: e.formData.permission,
            type: e.formData.type,
            entityId: e.formData.entityId,
          });
        }
      }
    }
  };

  //метод добавляет действие обновления записи из списка accessRightsDataStore в список accessRightsDataStoreActions
  //он так-же следить что-бы не было дублирующихся и противоположных действий
  const updateAccessRightsEntry = (e) => {
    let alreadyInStore = false;

    //проверить есть-ли такая-же запись в groupsAccessRightsDataStore
    switch (e.formData.type) {
      case ACCESS_TYPES.GROUPS:
        alreadyInStore = displayGroupsAccessRightsDataArray.find((item) => {
          if (item.entityId === e.formData.entityId) {
            return true;
          } else {
            return false;
          }
        });
        break;
      case ACCESS_TYPES.WINDOWS:
        alreadyInStore = systemWindowsAccessRightsDataArray.find((item) => {
          if (item.entityId === e.formData.entityId) {
            return true;
          } else {
            return false;
          }
        });
        break;
      default:
        break;
    }

    if (alreadyInStore) {
      accessRightsDataStoreActions.insertOrUpdate.push({
        userId: editedRow.id,
        permission: e.formData.permission,
        type: e.formData.type,
        entityId: e.formData.entityId,
      });
    }
  };

  //метод добавляет действие удаления записи из списка accessRightsDataStore в список accessRightsDataStoreActions
  //он так-же следить что-бы не было дублирующихся и противоположных действий
  const deleteAccessRightsEntry = (e) => {
    //проверить наличие действие на вставку такой-же записи в списке действий
    const alreadyOnInsert = accessRightsDataStoreActions.insertOrUpdate.find((item) => {
      if (item.entityId === e.formData.entityId && item.type === e.formData.type) {
        return true;
      } else {
        return false;
      }
    });

    if (alreadyOnInsert) {
      //исключить вставки для этой записи, что-бы не делать избыточных запросов
      accessRightsDataStoreActions.insertOrUpdate = accessRightsDataStoreActions.insertOrUpdate.filter((item) => {
        if (item.entityId === e.formData.entityId && item.type === e.formData.type) {
          return false;
        } else {
          return true;
        }
      });
    } else {
      //проверить есть-ли действие на удаление такой-же записи в списке действий
      const alreadyOnDelete = accessRightsDataStoreActions.delete.find((item) => {
        if (item.entityId === e.formData.entityId && item.type === e.formData.type) {
          return true;
        } else {
          return false;
        }
      });

      if (!alreadyOnDelete) {
        accessRightsDataStoreActions.delete.push({
          userId: editedRow.id,
          ...e.formData,
        });
      }
    }
  };

  return (
    <React.Fragment>
      <Popup
        id="userEditorPopup"
        className="customEditorPopup"
        visible={true}
        onHiding={closePopup}
        titleComponent={() => {
          return <span className="popup-title">{usersPopupOptions.title}</span>;
        }}
        {...usersPopupOptions}
      >
        <Position my="center" at="center" of={window} />
        <ScrollView showScrollbar="always">
          <Form validationGroup="userFormEditor">
            <SimpleItem>
              <div className="editor-content">
                <div className="dx-form-group-with-caption dx-form-group editor-group">
                  <div className="dx-form-group-caption">Общие сведения</div>
                  <div className="dx-form-group-content editor-content">
                    <div className="editor-item cols-2">
                      <div className="dx-field">
                        <div className="dx-field-label">Имя</div>
                        <div className="dx-field-value">
                          <TextBox
                            defaultValue={formData.description}
                            onInput={onFormItemValueChanged}
                            dataField="description"
                          >
                            <Validator validationGroup="userFormEditor">
                              <RequiredRule />
                            </Validator>
                          </TextBox>
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">Логин</div>
                        <div className="dx-field-value">
                          <TextBox defaultValue={formData.login} onInput={onFormItemValueChanged} dataField="login">
                            <Validator validationGroup="userFormEditor">
                              <RequiredRule />
                            </Validator>
                          </TextBox>
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">Адрес электронной почты</div>
                        <div className="dx-field-value">
                          <TextBox
                              defaultValue={formData.email}
                              onInput={onFormItemValueChanged}
                              dataField="email"
                          >
                            <Validator validationGroup="userFormEditor">
                              <EmailRule />
                            </Validator>
                          </TextBox>
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">Пароль</div>
                        <div className="dx-field-value">
                          <TextBox
                            defaultValue={formData.password}
                            onInput={onFormItemValueChanged}
                            dataField="password"
                          >
                            <Validator validationGroup="userFormEditor">
                              <RequiredRule />
                            </Validator>
                          </TextBox>
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">Постоянный токен</div>
                        <div className="dx-field-value">
                          <TextBox
                            defaultValue={formData.permanentToken}
                            onInput={onFormItemValueChanged}
                            dataField="permanentToken"
                          ></TextBox>
                        </div>
                      </div>
                      <div className="dx-field checkbox-field">
                        <div className="dx-field-label">Активен</div>
                        <div className="dx-field-value">
                          <CheckBox
                            defaultValue={formData.isActive}
                            onValueChanged={onFormItemValueChanged}
                            dataField="isActive"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="dx-form-group-with-caption dx-form-group editor-group">
                  <div className="dx-form-group-caption">Права доступа</div>
                  <div className="dx-form-group-content editor-content">
                    <div style={{display:'none'}} className="editor-item datagrid-item accessRights-datagrid-item cols-2">
                      <div className="dx-field">
                        <div className="dx-field-label item-label">Группы</div>
                        <div className="dx-field-value">
                          <DataGrid
                            ref={groupsGridRef}
                            dataSource={displayGroupsAccessRightsDataArray}
                            onToolbarPreparing={onGroupsToolbarPreparing}
                            onDataErrorOccurred={onDataErrorOccurred}
                            onRowInserting={(e) => {
                              insertAccessRightsEntry({
                                formData: {
                                  ...e.data,
                                  type: ACCESS_TYPES.GROUPS,
                                },
                              });
                            }}
                            onRowUpdating={(e) => {
                              updateAccessRightsEntry({
                                formData: { ...e.oldData, ...e.newData },
                              });
                            }}
                            onRowRemoving={(e) => {
                              deleteAccessRightsEntry({
                                formData: e.data,
                              });
                            }}
                            repaintChangesOnly={true}
                            {...usersPopupGroupDatagridOptions}
                          >
                            <Editing allowAdding={true} allowDeleting={false} allowUpdating={true} mode="cell" />
                            <Column
                              dataField="entityId"
                              caption="Группа"
                              showEditorAlways={true}
                              minWidth="140px"
                              editCellComponent={(e) => {
                                const data = e.data;
                                return (
                                  <DXLookup
                                    defaultValue={e.data.row.data.entityId}
                                    dataField="entityId"
                                    caption="Группа"
                                    noDataText="Нет доступных групп"
                                    dataSource={{
                                      store: displayGroupsDataArray,
                                      postProcess: (groups) => {
                                        //Отображаем только группы на которые нет прав доступа.
                                        const diff = differenceWith(
                                          groups,
                                          displayGroupsAccessRightsDataArray,
                                          (itemA, itemB) => {
                                            return itemA.id === itemB.entityId;
                                          }
                                        );
                                        return diff;
                                      },
                                      onLoadError: (error) => {
                                        onDataErrorOccurred({ error });
                                      },
                                    }}
                                    valueExpr="id"
                                    displayExpr="name"
                                    disabled={!e.data.row.isNewRow}
                                    onValueChanged={(e) => {
                                      data.setValue(e.value, e.component.option("dataField"));
                                    }}
                                    className={!e.data.row.isNewRow ? "hide-arrow" : ""}
                                  />
                                );
                              }}
                            >
                              <RequiredRule />
                            </Column>
                            <Column
                              dataField="permission"
                              caption="Доступ"
                              showEditorAlways={true}
                              minWidth="300px"
                              editCellComponent={(e) => {
                                const data = e.data;
                                return (
                                  <DXRadioGroup
                                    dataSource={permissionsDataStore}
                                    defaultValue={data.row.data.permission}
                                    valueExpr="value"
                                    displayExpr="name"
                                    layout="horizontal"
                                    onValueChanged={(e) => {
                                      data.setValue(e.value, e.component.option("dataField"));
                                    }}
                                  />
                                );
                              }}
                            >
                              <RequiredRule />
                            </Column>
                          </DataGrid>
                        </div>
                      </div>
                    </div>
                    <div className="editor-item datagrid-item accessRights-datagrid-item">
                      <div className="dx-field">
                        <div className="dx-field-label item-label">Окна</div>
                        <div className="dummy-datagrid-header-space" />
                        <div className="dx-field-value">
                          <DataGrid
                            ref={windowsGridRef}
                            dataSource={systemWindowsAccessRightsDataArray}
                            onToolbarPreparing={onGroupsToolbarPreparing}
                            onDataErrorOccurred={onDataErrorOccurred}
                            onRowInserting={(e) => {
                              insertAccessRightsEntry({
                                formData: {
                                  ...e.data,
                                  type: ACCESS_TYPES.WINDOWS,
                                },
                              });
                            }}
                            onRowUpdating={(e) => {
                              updateAccessRightsEntry({
                                formData: { ...e.oldData, ...e.newData },
                              });
                            }}
                            onRowRemoving={(e) => {
                              deleteAccessRightsEntry({
                                formData: e.data,
                              });
                            }}
                            repaintChangesOnly={true}
                            {...usersPopupGroupDatagridOptions}
                          >
                            <Editing allowAdding={false} allowDeleting={false} allowUpdating={true} mode="cell" />
                            <Column
                              dataField="entityId"
                              caption="Окно"
                              showEditorAlways={true}
                              minWidth="200px"
                              editCellComponent={(e) => {
                                const data = e.data;
                                return (
                                  <DXLookup
                                    defaultValue={e.data.row.data.entityId}
                                    dataField="entityId"
                                    caption="Окно"
                                    noDataText="Нет доступных окон"
                                    dataSource={{
                                      store: systemWindowsDataArray,
                                      postProcess: (windows) => {
                                        //Отображаем только окна на которые нет прав доступа.
                                        const diff = differenceWith(
                                          windows,
                                          systemWindowsAccessRightsDataArray,
                                          (itemA, itemB) => {
                                            return itemA.id === itemB.entityId;
                                          }
                                        );
                                        return diff;
                                      },
                                      onLoadError: (error) => {
                                        onDataErrorOccurred({ error });
                                      },
                                    }}
                                    valueExpr="id"
                                    displayExpr="name"
                                    disabled={!e.data.row.isNewRow}
                                    onValueChanged={(e) => {
                                      data.setValue(e.value, e.component.option("dataField"));
                                    }}
                                    className={!e.data.row.isNewRow ? "hide-arrow" : ""}
                                  />
                                );
                              }}
                            >
                              <RequiredRule />
                            </Column>
                            <Column
                              dataField="permission"
                              caption="Доступ"
                              showEditorAlways={true}
                              minWidth="300px"
                              editCellComponent={(e) => {
                                const data = e.data;
                                return (
                                  <DXRadioGroup
                                    dataSource={permissionsDataStore}
                                    defaultValue={data.row.data.permission}
                                    valueExpr="value"
                                    displayExpr="name"
                                    layout="horizontal"
                                    onValueChanged={(e) => {
                                      data.setValue(e.value, e.component.option("dataField"));
                                    }}
                                  />
                                );
                              }}
                            >
                              <RequiredRule />
                            </Column>
                          </DataGrid>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </SimpleItem>
          </Form>
        </ScrollView>
        <ValidationSummary id="summary" validationGroup="userFormEditor"></ValidationSummary>
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Сохранить", onClick: onSave }}
          toolbar="bottom"
        />
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Отмена", onClick: onCancel }}
          toolbar="bottom"
        />
      </Popup>
    </React.Fragment>
  );
};

export default connect(
  (state) => ({}),
  (dispatch) => {
    return { localDeauthorize: () => dispatch(deauthorize()) };
  }
)(UsersEditorPopup);
