import { useEffect, useRef, useState } from "react";
import { connect } from "react-redux";
import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import notify from "devextreme/ui/notify";
import { isEmpty } from "lodash-es";
import { deauthorize } from "../../store/commonSlice";
import { getSpecCompaniesDataStore, getSystemWindowsDataStore, WINDOWS } from "../../dataStores";
import config from "../../config";
import SpecCompaniesTable from "./specCompaniesTable";
import SpecCompaniesEditorPopup from "./specCompaniesEditorPopup";

const windowId = WINDOWS.SPEC_COMPANIES.id;

const specCompaniesDataStore = getSpecCompaniesDataStore({
  windowId: windowId,
});

const systemWindowsDataStore = getSystemWindowsDataStore({
  windowId: windowId,
});

const SpecCompaniesPage = ({ localDeauthorize }) => {
  const [specCompanyId, setSpecCompanyId] = useState();
  const [deleteRowPopupVisible, setDeleteRowPopupVisible] = useState(false);
  const [editorPopupVisible, setEditorPopupVisible] = useState(false);
  const [windowTitle, setWindowTitle] = useState();

  const datagridRef = useRef(null);

  const onDataErrorOccurred = (e) => {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);

    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  };

  const openEditorPopup = (id) => {
    setSpecCompanyId(id);
    setEditorPopupVisible(true);
  };

  const openCreateEntryPopup = () => {
    setSpecCompanyId(null);
    setEditorPopupVisible(true);
  };

  const closeEditorPopup = () => {
    setEditorPopupVisible(false);
  };

  const openDeleteRowPopup = (id) => {
    setSpecCompanyId(id);
    setDeleteRowPopupVisible(true);
  };

  const closeDeleteRowPopup = () => {
    setDeleteRowPopupVisible(false);
  };

  const createEntry = (formData) => {
    specCompaniesDataStore
      .insert(formData)
      .then(() => {
        closeEditorPopup();
        datagridRef?.current?.instance?.refresh();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
      });
  };

  const updateEntry = ({ formData, editedRowId }) => {
    if (!isEmpty(formData)) {
      specCompaniesDataStore
        .update(editedRowId, formData)
        .then(() => {
          closeEditorPopup();
          datagridRef?.current?.instance?.refresh();
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
        });
    } else {
      closeEditorPopup();
    }
  };

  const deleteEntry = () => {
    specCompaniesDataStore
      .remove(specCompanyId)
      .then(() => {
        closeDeleteRowPopup();
        datagridRef?.current?.instance?.refresh();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
      });
  };

  useEffect(() => {
    systemWindowsDataStore.byKey(windowId).then((result) => {
      const windowName = result[0]?.name?.split("/").pop();

      setWindowTitle(windowName);
    });
  }, []);

  return (
    <div className="content-block">
      <h2>{windowTitle}</h2>
      {editorPopupVisible && (
        <SpecCompaniesEditorPopup
          specCompanyId={specCompanyId}
          createEntry={createEntry}
          updateEntry={updateEntry}
          closeEditorPopup={closeEditorPopup}
          onDataErrorOccurred={onDataErrorOccurred}
        />
      )}
      {deleteRowPopupVisible && (
        <Popup
          className="deleteRowPopup"
          width="auto"
          height="auto"
          visible={true}
          showCloseButton={false}
          showTitle={false}
          onHiding={closeDeleteRowPopup}
        >
          <Position my="center" at="center" of={window} />
          <div>
            <span>Вы уверены, что хотите удалить эту запись?</span>
          </div>
          <ToolbarItem
            widget="dxButton"
            location="center"
            toolbar="bottom"
            options={{ text: "Да", onClick: deleteEntry }}
          />
          <ToolbarItem
            widget="dxButton"
            location="center"
            toolbar="bottom"
            options={{ text: "Нет", onClick: closeDeleteRowPopup }}
          />
        </Popup>
      )}
      <SpecCompaniesTable
        datagridRef={datagridRef}
        openEditorPopup={openEditorPopup}
        openDeleteRowPopup={openDeleteRowPopup}
        openCreateEntryPopup={openCreateEntryPopup}
        onDataErrorOccurred={onDataErrorOccurred}
      />
    </div>
  );
};

export default connect(
  (state) => ({}),
  (dispatch) => {
    return { localDeauthorize: () => dispatch(deauthorize()) };
  }
)(SpecCompaniesPage);
