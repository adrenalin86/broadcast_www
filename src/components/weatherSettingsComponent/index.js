import { SeLookup } from "../editors";

import { getGenericEntityDataStore, GENERIC_ENTITIES } from "../../dataStores";

import { isEmpty, noop } from "lodash-es";

const Component = ({
  windowId,
  data,
  onFormItemValueChanged,
  className,
  editModeEnabled = false,
  dataField,
  onError = noop,
}) => {
  const weatherForecastsDataStore = getGenericEntityDataStore({
    windowId: windowId,
    entityName: GENERIC_ENTITIES.WEATHER_FORECASTS_SOURCE,
  });

  const onValueChanged = (e) => {
    const eventDataField = e.component.option("dataField");
    const fullDataField = isEmpty(dataField)
      ? eventDataField
      : dataField?.endsWith(".")
      ? dataField + eventDataField
      : dataField + "." + eventDataField;

    e.component.option("dataField", fullDataField);
    onFormItemValueChanged(e);
  };

  return (
    <div className={"dx-form-group-with-caption dx-form-group " + className}>
      <div className="dx-form-group-caption">Настройки источника прогнозов погоды</div>
      <div className="dx-form-group-content">
        <div className="editor-item">
          <SeLookup
            data={data}
            onFormItemValueChanged={onValueChanged}
            editModeEnabled={editModeEnabled}
            dataField={"weatherForecastSource.measuringPointId"}
            title={"Метеопост"}
            dataSource={{
              store: weatherForecastsDataStore,
              onLoadError: (error) => {
                onError(error);
              },
            }}
            valueExpr={"id"}
            displayExpr={"name"}
            searchExpr={["name"]}
            widgetOptions={{ allowClearing: true }}
          />
        </div>
      </div>
    </div>
  );
};

export default Component;
