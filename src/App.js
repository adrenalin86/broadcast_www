import "devextreme/dist/css/dx.common.css";
import "./themes/generated/theme.base.css";
import "./themes/generated/theme.additional.css";

import React, { useEffect } from "react";
import { connect } from "react-redux";
import { HashRouter as Router } from "react-router-dom";

import LoadPanel from "devextreme-react/load-panel";
import { locale as setLocale, loadMessages } from "devextreme/localization";
import ruMessages from "devextreme/localization/messages/ru.json";

import { isEmpty } from "lodash-es";
import dayjs from "dayjs";
import "dayjs/locale/ru";
import relativeTime from "dayjs/plugin/relativeTime";

import { NavigationProvider } from "./contexts/navigation";
import { useScreenSizeClass } from "./utils";

import { getLocale, getToken, getLoading, getUserRights } from "./store/commonSlice";

import Content from "./Content";
import NotAuthenticatedContent from "./NotAuthenticatedContent";

import "./dx-styles.scss";
import "./common.scss";

function App({ token, loading, windowsRights }) {
  if (loading) {
    return <LoadPanel visible={true} />;
  }

  if (!isEmpty(token)) {
    return <Content windowsRights={windowsRights} />;
  }

  return <NotAuthenticatedContent />;
}

export default connect((state) => ({
  locale: getLocale(state),
  token: getToken(state),
  loading: getLoading(state),
  userRights: getUserRights(state),
}))(function ({ locale, token, loading, userRights }) {
  useEffect(() => {
    loadMessages(ruMessages);
    setLocale(locale);
    dayjs.locale("ru"); //по умолчанию используем русскую локаль
    dayjs.extend(relativeTime);
  }, []);

  const screenSizeClass = useScreenSizeClass();
  return (
    <Router>
      <NavigationProvider>
        <div className={`app ${screenSizeClass}`}>
          <App token={token} loading={loading} windowsRights={userRights.windowsRights} />
        </div>
      </NavigationProvider>
    </Router>
  );
});
