import React from 'react'
import TemplateRegisterHistory from "./templateRegisterHistory";
import {REGISTRY_HISTORY_ENTITIES, WINDOWS} from "../../dataStores";

const DevicesRegistryHistoryPage = () => {
  return <TemplateRegisterHistory entityConstName={REGISTRY_HISTORY_ENTITIES.DEVICE} constName={WINDOWS.REPORTS_REGISTRY_HISTORY_DEVICES}/>
}

const DeviceTypesRegistryHistoryPage = () => {
  return <TemplateRegisterHistory entityConstName={REGISTRY_HISTORY_ENTITIES.DEVICE_TYPE} constName={WINDOWS.REPORTS_REGISTRY_HISTORY_DEVICE_TYPES}/>
}

const SmartStationsRegistryHistoryPage = () => {
  return <TemplateRegisterHistory entityConstName={REGISTRY_HISTORY_ENTITIES.SMART_STATIONS} constName={WINDOWS.REPORTS_REGISTRY_SMART_STATIONS}/>
}

export {DeviceTypesRegistryHistoryPage, SmartStationsRegistryHistoryPage, DevicesRegistryHistoryPage}

