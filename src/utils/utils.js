import isPlainObject from "lodash-es/isPlainObject";
import fromPairs from "lodash-es/fromPairs";
import { toPairsIn } from "lodash-es";

/**
 * Трансформируем объект с вложеностями в плоский Map, где ключ - полный путь к значению.
 * Вложенный путь простраивается только для plain объектов,
 * т.е. массивы рассматриваются как значения и их индексы в путь не включаются
 * { a: { b: 2 }, c: 3 } => { 'a.b': 2, 'c': 3 }
 * @param object
 * @returns {{}}
 */
const getDeepPathValueMap = (object) => {
  const pairs = getPathValueMap(object);

  return fromPairs(pairs);

  function getPathValueMap(value, path = "") {
    if (isPlainObject(value)) {
      const outPairs = [];

      Object.keys(value).forEach((key) => {
        const newPairs = getPathValueMap(
          value[key],
          `${path}${path ? "." : ""}${key}`
        );
        outPairs.push(...newPairs);
      });

      return outPairs;
    }

    return [[path, value]];
  }
};

/**
 * Возвращает объект в котором удалены все узлы с ключом совпадающим
 * с содержимым keys. Работает и для массивов.
 * Не используйте для объектов которые имеют циклы.
 * Источник большей части кода https://stackoverflow.com/questions/55146456/
 * @param {(Object|Array)} obj фильтруемый объект
 * @param keys исключаемые ключи
 * @returns {(Object|Array)}
 */
function omitDeepByKeys(obj, keys) {
  if (Array.isArray(obj)) {
    return obj.map((value) => omitDeepByKeys(value, keys));
  } else if (typeof obj === "object") {
    return Object.entries(obj)
      .filter(([k, v]) => !keys.includes(k))
      .reduce(
        (r, [key, value]) => ({ ...r, [key]: omitDeepByKeys(value, keys) }),
        {}
      );
  } else {
    return obj;
  }
}

/**
 * Возвращает объект в котором удалены все узлы для которых condition возвращает false.
 * Работает и для массивов.
 * Не используйте для объектов которые имеют циклы.
 * @param {(Object|Array)} obj фильтруемый объект
 * @param {(Function)} condition функция условие исключения. Принимает ключ и значение узла объекта, возвращает false что-бы исключить узел.
 * @returns {(Object|Array)}
 */
function omitDeepWith(obj, condition) {
  if (Array.isArray(obj)) {
    return obj.map((value) => omitDeepWith(value, condition));
  } else if (typeof obj === "object") {
    return toPairsIn(obj)
      .filter(([k, v]) => condition(k, v))
      .reduce(
        (r, [key, value]) => ({ ...r, [key]: omitDeepWith(value, condition) }),
        {}
      );
  } else {
    return obj;
  }
}

/**
 * Метод обхода узлов объекта. Объект модифицируется.
 * Источник https://stackoverflow.com/a/45628445
 * @param {Object} obj обходимый объект.
 * @param {Function} func функция выполняемая над каждым узлом. Принимает: Key - ключ узла; value - значение узла; path - путь до узла; parent - родитель узла.
 */
function traverse(obj, func) {
  var TraverseFilter;
  (function (TraverseFilter) {
    // предотвращает дочерний элемент от итерации
    TraverseFilter["reject"] = "reject";
  })(TraverseFilter || (TraverseFilter = {}));
  function* traverse(o) {
    const memory = new Set();
    function* innerTraversal(root) {
      const queue = [];
      queue.push([root, []]);
      while (queue.length > 0) {
        const [o, path] = queue.shift();
        if (memory.has(o)) {
          // мы уже встречали это, не итерируем
          continue;
        }
        // добавить новый объект в память
        memory.add(o);
        for (var i of Object.keys(o)) {
          const item = o[i];
          const itemPath = path.concat([i]);
          const filter = yield [i, item, itemPath, o];
          if (filter === TraverseFilter.reject) continue;
          if (item !== null && typeof item === "object") {
            //спускаемся на одну ступень ниже в древе объекта!!
            queue.push([item, itemPath]);
          }
        }
      }
    }
    yield* innerTraversal(o);
  }

  for (var [key, value, path, parent] of traverse(obj)) {
    func(key, value, path, parent);
  }
}

/** Асинхронная. Пример использования:
 * //Ожидать 1000мс
 * await sleep(1000ms);  */
function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

/**
 * Асинхронная. Получение размеров изображения.
 * @param {base64||File} file
 */
function getImageDimensions(file) {
  return new Promise(function (resolved) {
    var image = new Image();
    image.onload = function () {
      resolved({ width: image.naturalWidth, height: image.naturalHeight });
    };
    image.src = file;
  });
}

const activeIcon = (array, value, key = 'videoCameraId') => {
  if (Array?.isArray(array)) {
    const findObject = array?.find(el => {
      if (el?.status) {
        return el?.status[key] === value
      }
    });

    if (!findObject) {
      return 0
    }

    if (findObject && findObject?.status?.connected === 1) {
      return 2
    }
    return 1
  }
  return 0
}


export {
  omitDeepByKeys,
  getDeepPathValueMap,
  omitDeepWith,
  traverse,
  sleep,
  getImageDimensions,
  activeIcon
};
