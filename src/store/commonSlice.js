import { createSlice } from "@reduxjs/toolkit";
import { urlParser, urlConstructor, provideCurrentProtocol } from "../utils";
import config from "../config";

import { clone, cloneDeep, isEmpty } from "lodash-es";

export const COMMON_REDUCER_KEY = "common";

const commonSlice = createSlice({
  name: COMMON_REDUCER_KEY,
  initialState: {
    locale: clone(config[COMMON_REDUCER_KEY].locale),
    themeName: clone(config[COMMON_REDUCER_KEY].themeName),
    token: clone(config[COMMON_REDUCER_KEY].token),
    userData: cloneDeep(config[COMMON_REDUCER_KEY].userData),
    backend: cloneDeep(config[COMMON_REDUCER_KEY].backend),
    loading: false,
    error: null,
    rememberUser: clone(config[COMMON_REDUCER_KEY].rememberUser),
    userRights: cloneDeep(config[COMMON_REDUCER_KEY].userRights),
  },
  reducers: {
    setLocale: (state, { payload }) => {
      state.locale = payload;
    },
    setTheme: (state, { payload }) => {
      state.themeName = payload;
    },
    setError: (state, { payload }) => {
      state.error = payload;
    },
    setBackendUrl: (state, { payload }) => {
      state.backend.url = payload;
    },
    setToken: (state, { payload }) => {
      state.token = payload;
    },
    setUserData: (state, { payload }) => {
      state.userData = payload;
    },
    setLoading: (state, { payload }) => {
      state.loading = payload;
    },
    setRememberUser: (state, { payload }) => {
      state.rememberUser = payload;
    },
    setUserRights: (state, { payload }) => {
      state.userRights = payload;
    },
    setRegion: (state, { payload }) => {
      state.backend.regionId = payload;
    },
    deauthorize: (state, { payload }) => {
      state.token = "";
      state.userData = {};
      state.userRights = {};
    },
  },
});

export const {
  setLocale,
  setTheme,
  setError,
  setToken,
  setUserData,
  setLoading,
  setRememberUser,
  setBackendUrl,
  setUserRights,
  setRegion,
  deauthorize,
} = commonSlice.actions;

export const getCommon = (state) => state[COMMON_REDUCER_KEY];
export const getThemeName = (state) => getCommon(state).themeName;
export const getLocale = (state) => getCommon(state).locale;
export const getRegion = (state) => getCommon(state).backend.regionId;
export const getToken = (state) => getCommon(state).token;
export const getUserData = (state) => getCommon(state).userData;
export const getLoading = (state) => getCommon(state).loading;
export const getError = (state) => getCommon(state).error;
export const getRememberUser = (state) => getCommon(state).rememberUser;
export const getBackendUrl = (state) => getCommon(state).backend.url;
export const getUserRights = (state) => getCommon(state).userRights;
export const getDataStoreConnectionData = (state) => {
  const data = {
    region: getRegion(state),
    token: getToken(state),
    backendUrl: getBackendUrl(state),
  };
  return data;
};

export default commonSlice.reducer;
