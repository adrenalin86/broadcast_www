import React, { useState, useEffect } from "react";

import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import { ScrollView } from "devextreme-react/scroll-view";
import Form, { SimpleItem } from "devextreme-react/form";
import { TextBox, Switch } from "devextreme-react";

import ValidationSummary from "devextreme-react/validation-summary";
import ValidationEngine from "devextreme/ui/validation_engine";
import Validator, { RequiredRule } from "devextreme-react/validator";
import notify from "devextreme/ui/notify";

import { set, get, cloneDeep, isEmpty, merge } from "lodash-es";

import {
  ForecastsSettingsComponent,
  U646SettingsComponent,
  ForecastSourceComponent,
  SmartJson2SettingsComponent,
  WeatherForecastsSettingsComponent,
} from "../../components";

import { WINDOWS, getDisplayProtocolsDataStore } from "../../dataStores";

import config from "../../config";
import { connect } from "react-redux";
import { deauthorize } from "../../store/commonSlice";

const defaultValues = {
  displaySettings: config.defaults.displays.settings,
};
const broadcastProgramTemplatePopupOptions =
  config.pages.broadcastProgramTemplates.broadcastProgramTemplatesGrid.popupBroadcastProgramTemplateEditor.dxOptions;

const windowId = WINDOWS.BROADCAST_PROGRAM_TEMPLATES.id;

const displayProtocolsDataStore = getDisplayProtocolsDataStore({ windowId: windowId });

const POSSIBLE_PROTOCOLS = [
  "itLineU653",
  "itLineU646",
  "smartNxp146",
  "smartStm140",
  "smartJson2",
  "smartJson1",
  "cpower",
];

const USED_PROTOCOLS = ["smartJson2", "smartJson1"];

const DisplayModelsEditorPopup = ({ editedRow, closeEditorPopup, updateEntry, createEntry, localDeauthorize }) => {
  const [formData, setFormData] = useState({});
  const [changedFormData] = useState({});
  const [editModeEnabled, setEditModeEnabled] = useState(false);
  const [protocolsSettings, setProtocolsSettings] = useState({});

  const mainEditorsSettings = {
    forecasts: { show: true },
    forecastSource: { show: true },
  };

  /**
   * Инициализация редактора.
   */
  useEffect(() => {
    if (isEmpty(editedRow)) {
      setFormData(clearUnusedProtocols(cloneDeep(defaultValues)));
    } else {
      setFormData(cloneDeep(editedRow));
    }
    displayProtocolsDataStore
      .load()
      .then((result) => {
        if (result?.data?.length >= 0) {
          setProtocolsSettings((oldData) => {
            const newData = {};
            result.data.forEach((item) => {
              newData[item.name] = {
                show: true,
              };
            });
            return newData;
          });
        }
      })
      .catch((error) => {
        console.log("Ошибка получения списка протоколов");
        onDataErrorOccurred(error);
      });
  }, []);

  function onDataErrorOccurred(e) {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);
    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  }

  const closePopup = () => {
    closeEditorPopup();
  };

  const onSave = () => {
    const isValid = ValidationEngine.validateGroup("formEditor").isValid;

    if (isValid) {
      if (isEmpty(editedRow)) {
        createEntry({
          formData: formData,
        });
      } else {
        updateEntry({
          formData: changedFormData,
          editedRowId: editedRow.id,
        });
      }
    }
  };

  const onCancel = () => {
    closePopup();
  };

  const onFormItemValueChanged = (e) => {
    const value = e.value !== undefined ? e.value : e?.component?.option("text");
    const dataField = e.component.option("dataField");
    const enabler = e.component.option("enabler");
    const repaint = e?.forceRepaint || e.component.option("forceRepaint");
    const forceMerge = e.forceMerge || e.component.option("forceMerge");
    if (enabler) {
      setFormDataState(dataField, value ? get(defaultValues, dataField) : null);
    } else {
      if (repaint) {
        setFormDataState(dataField, value, forceMerge);
      } else {
        setFormDataValue(dataField, value, forceMerge);
      }
    }
  };

  const onFormItemValueChangedData = ({ value, dataField, enabler, forceRepaint, forceMerge = false }) => {
    if (enabler) {
      setFormDataValue(dataField, value ? get(defaultValues, dataField) : null);
    } else {
      if (forceRepaint) {
        setFormDataState(dataField, value, forceMerge);
      } else {
        setFormDataValue(dataField, value, forceMerge);
      }
    }
  };

  const setFormDataValue = (dataField, value, forceMerge = false) => {
    if (forceMerge) {
      merge(formData, set(cloneDeep(formData), dataField, value));
      merge(changedFormData, set(cloneDeep(changedFormData), dataField, value));
    } else {
      set(formData, dataField, value);
      set(changedFormData, dataField, value);
    }
  };

  const setFormDataState = (dataField, value, forceMerge = false) => {
    if (forceMerge) {
      setFormData((oldData) => {
        let newData = cloneDeep(oldData);
        merge(newData, set(cloneDeep(newData), dataField, value));
        return newData;
      });
      merge(changedFormData, set(cloneDeep(changedFormData), dataField, value));
    } else {
      setFormData((oldData) => {
        let newData = cloneDeep(oldData);
        set(newData, dataField, value);
        return newData;
      });
      set(changedFormData, dataField, value);
    }
  };

  const clearUnusedProtocols = (data) => {
    const unusedProtocols = POSSIBLE_PROTOCOLS.filter((item) => !USED_PROTOCOLS.includes(item));
    unusedProtocols.forEach((protocolName) => {
      if (data?.displaySettings?.[protocolName]) {
        delete data.displaySettings[protocolName];
      }
    });
    return data;
  };

  return (
    <React.Fragment>
      <Popup
        className="customEditorPopup"
        visible={true}
        onHiding={closePopup}
        titleComponent={() => {
          return <span className="popup-title">{broadcastProgramTemplatePopupOptions.title}</span>;
        }}
        {...broadcastProgramTemplatePopupOptions}
      >
        <Position my="center" at="center" of={window} />
        <ScrollView showScrollbar="always">
          <Form validationGroup="formEditor">
            <SimpleItem>
              <div className="editor-content">
                <div className="dx-form-group-with-caption dx-form-group editor-group">
                  <div className="dx-form-group-caption">Общие сведения</div>
                  <div className="dx-form-group-content editor-content">
                    <div className="editor-item cols-2">
                      <div className="dx-field">
                        <div className="dx-field-label">Название</div>
                        <div className="dx-field-value">
                          <TextBox
                            defaultValue={formData.name}
                            onValueChanged={onFormItemValueChanged}
                            dataField="name"
                          >
                            <Validator validationGroup="formEditor">
                              <RequiredRule />
                            </Validator>
                          </TextBox>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <WeatherForecastsSettingsComponent
                  className="editor-group cols-2"
                  data={formData.displaySettings ?? {}}
                  onFormItemValueChanged={onFormItemValueChanged}
                  windowId={windowId}
                  editModeEnabled={editModeEnabled}
                  dataField={"displaySettings"}
                  onError={(error) => {
                    onDataErrorOccurred({ error });
                  }}
                />
                {mainEditorsSettings?.forecasts?.show && (
                  <ForecastsSettingsComponent
                    className="editor-group cols-2"
                    data={formData.displaySettings ?? {}}
                    onFormItemValueChanged={onFormItemValueChanged}
                    windowId={windowId}
                    editModeEnabled={editModeEnabled}
                    dataField={"displaySettings"}
                    onError={(error) => {
                      onDataErrorOccurred({ error });
                    }}
                  />
                )}
                {mainEditorsSettings?.forecastSource?.show && (
                  <ForecastSourceComponent
                    className="editor-group cols-2"
                    data={formData.displaySettings?.forecastSource ?? {}}
                    onFormItemValueChanged={onFormItemValueChanged}
                    windowId={windowId}
                    editModeEnabled={editModeEnabled}
                    dataField={"displaySettings.forecastSource"}
                    onError={(error) => {
                      onDataErrorOccurred({ error });
                    }}
                  />
                )}
                {protocolsSettings?.itLineU646?.show && (
                  <U646SettingsComponent
                    className="editor-group"
                    data={formData.displaySettings?.itLineU646 ?? {}}
                    onFormItemValueChanged={onFormItemValueChanged}
                    onFormItemValueChangedData={onFormItemValueChangedData}
                    editModeEnabled={editModeEnabled}
                    dataField={"displaySettings.itLineU646"}
                    windowId={windowId}
                    onError={(error) => {
                      onDataErrorOccurred({ error });
                    }}
                  />
                )}
                {protocolsSettings?.smartJson2?.show && (
                  <SmartJson2SettingsComponent
                    className="editor-group"
                    data={formData.displaySettings?.smartJson2 ?? {}}
                    onFormItemValueChanged={onFormItemValueChanged}
                    onFormItemValueChangedData={onFormItemValueChangedData}
                    editModeEnabled={editModeEnabled}
                    dataField={"displaySettings.smartJson2"}
                    windowId={windowId}
                    onError={(error) => {
                      onDataErrorOccurred({ error });
                    }}
                  />
                )}
              </div>
            </SimpleItem>
          </Form>
        </ScrollView>
        <ValidationSummary id="summary" validationGroup="formEditor"></ValidationSummary>
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Сохранить", onClick: onSave }}
          toolbar="bottom"
        />
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Отмена", onClick: onCancel }}
          toolbar="bottom"
        />
        <ToolbarItem toolbar="bottom" location="left">
          <Switch
            defaultValue={editModeEnabled}
            switchedOffText="Настройка редактора"
            switchedOnText="Закончить настройку"
            width="8rem"
            onValueChanged={(e) => {
              setEditModeEnabled(e.value);
            }}
          />
        </ToolbarItem>
      </Popup>
    </React.Fragment>
  );
};

export default connect(null, { localDeauthorize: deauthorize })(DisplayModelsEditorPopup);
