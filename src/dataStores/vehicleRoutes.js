import CustomStore from "devextreme/data/custom_store";

import store from "../store";

import { getBackendUrl, getToken, getRegion } from "../store/commonSlice";

import {
  handleResponseErrors,
  handleDataErrors,
  handleCaughtErrors,
} from "./utils";

const getDataStore = ({ windowId }) => {
  const restArgs = {
    w: windowId,
    ct: 1,
  };

  return new CustomStore({
    key: "id",
    load: (data) => {
      let body = {
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        ...restArgs,
        cd: "loadRoutes",
        data,
      };

      return fetch(getBackendUrl(store.getState())+"?"+body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .then((data) => {
          return {
            data: data.data.entities,
            summary: data.data.summary,
            totalCount: data.data.totalCount,
          };
        })
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },
    insert: function (values) {
      let body = {
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        ...restArgs,
        cd: "insertRoute",
        data: {
          ...values,
        },
      };

      return fetch(getBackendUrl(store.getState())+"?"+body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },

    update: function (key, values) {
      let body = {
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        ...restArgs,
        cd: "updateRoute",
        data: { id: key, values },
      };

      return fetch(getBackendUrl(store.getState())+"?"+body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },

    remove: function (key) {
      let body = {
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        ...restArgs,
        cd: "removeRoute",
        data: {
          id: key,
        },
      };

      return fetch(getBackendUrl(store.getState())+"?"+body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },

    byKey: (key) => {
      let body = {
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        ...restArgs,
        cd: "loadRoutes",
        data: { filter: ["id", "=", key] },
      };

      return fetch(getBackendUrl(store.getState())+"?"+body.cd+"byKey", {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .then((data) => {
          return data.data.entities;
        })
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },
    
  });
};

export default getDataStore;
