import React, { useState, useEffect } from "react";
import { Switch, Button } from "devextreme-react";

import { get, has } from "lodash-es";
import ImageUploading from "react-images-uploading";

import { getImageDimensions } from "../../utils";

const extToMimes = {
  "image/png": "png",
  "image/gif": "gif",
  "image/bmp": "bmp",
};

const SeImageUploaderField = ({
  data,
  onImageChanged,
  dataField,
  title = "",
  className = "",
  disabled,
  maxNumber = -1,
}) => {
  const [images, setImages] = useState([]);
  const [currentImageData, setCurrentImageData] = useState([]);

  useEffect(() => {
    const imageData = get(data, dataField);
    if (imageData) {
      if (imageData?.base64 && imageData.width && imageData.height && imageData.format) {
        setImages([{ data_url: imageData.base64 }]);
        setCurrentImageData({
          width: imageData.width,
          height: imageData.height,
          format: imageData.format,
        });
      } else {
        getImageDimensions(imageData).then((dimmensions) => {
          let format = imageData?.file?.type;
          if (format) {
            format = extToMimes[format];
          } else {
            format = "Не определён";
          }
          const extrapolatedImageData = {
            data_url: imageData,
            format: format,
            width: dimmensions.width,
            height: dimmensions.height,
          };
          setImages([{ data_url: extrapolatedImageData.data_url }]);
          setCurrentImageData({
            width: extrapolatedImageData.width,
            height: extrapolatedImageData.height,
            format: extrapolatedImageData.format,
          });
        });
      }
    } else {
      setImages([]);
      setCurrentImageData({
        width: null,
        height: null,
        format: "Не определён",
      });
    }
  }, []);

  useEffect(() => {
    const imageData = get(data, dataField);
    if (imageData) {
      if (imageData?.base64 && imageData.width && imageData.height && imageData.format) {
        setImages([{ data_url: imageData.base64 }]);
        setCurrentImageData({
          width: imageData.width,
          height: imageData.height,
          format: imageData.format,
        });
      } else {
        getImageDimensions(imageData).then((dimmensions) => {
          let format = imageData?.file?.type;
          if (format) {
            format = extToMimes[format];
          } else {
            format = "Не определён";
          }
          const extrapolatedImageData = {
            data_url: imageData,
            format: format,
            width: dimmensions.width,
            height: dimmensions.height,
          };
          setImages([{ data_url: extrapolatedImageData.data_url }]);
          setCurrentImageData({
            width: extrapolatedImageData.width,
            height: extrapolatedImageData.height,
            format: extrapolatedImageData.format,
          });
        });
      }
    } else {
      setImages([]);
      setCurrentImageData({
        width: null,
        height: null,
        format: "Не определён",
      });
    }
  }, [data]);

  async function onChange(imageList) {
    let imageData = null;
    if (imageList.length !== 0) {
      let format = imageList[0]?.file?.type;
      if (format) {
        format = extToMimes[format];
      }

      const dimmensions = await getImageDimensions(imageList[0]?.data_url);

      imageData = {
        base64: imageList[0]?.data_url,
        format: format,
        width: dimmensions.width,
        height: dimmensions.height,
      };

      onImageChanged(dataField, imageData);
      setImages(imageList);
      setCurrentImageData({
        width: imageData.width,
        height: imageData.height,
        format: imageData.format,
      });
    } else {
      onImageChanged(dataField, imageData);
      setImages(imageList);
    }
  }

  return (
    <div className={"dx-field-value image-uploader " + className} disabled={disabled}>
      <ImageUploading
        value={images}
        onChange={onChange}
        dataURLKey="data_url"
        acceptType={["png", "gif", "bmp"]}
        {...(maxNumber !== -1 ? { maxNumber: maxNumber } : {})}
      >
        {({ imageList, onImageUpload, onImageRemove, isDragging, dragProps }) => (
          <>
            <div className="dx-field-buttons">
              {maxNumber !== -1 && maxNumber !== 0 && images.length >= maxNumber ? null : (
                <Button
                  style={isDragging ? { color: "red" } : undefined}
                  className="upload-button"
                  onClick={onImageUpload}
                  disabled={disabled}
                  {...dragProps}
                >
                  Выберите изображение
                </Button>
              )}

              {imageList.length !== 0 && (
                <>
                  <div>{`Ширина: ${currentImageData.width}`}</div>
                  <div>{`Высота: ${currentImageData.height}`}</div>
                  <div>{`Формат: ${currentImageData.format}`}</div>
                </>
              )}
            </div>
            <div className="dx-field-value">
              <div className="upload__image-wrapper inline">
                <div className="images-container">
                  {imageList.length !== 0 &&
                    imageList.map((image, index) => (
                      <div key={index} className="image-item">
                        <img
                          src={image["data_url"]}
                          style={{
                            maxWidth: currentImageData?.width ? currentImageData.width + "px" : "auto",
                            maxHeight: currentImageData?.height ? currentImageData.height + "px" : "auto",
                          }}
                          alt=""
                        />
                        <Button onClick={() => onImageRemove(index)} disabled={disabled}>
                          Удалить
                        </Button>
                      </div>
                    ))}
                  {imageList.length === 0 && (
                    <div className="no-image-item">
                      <div>Нет изображения</div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </>
        )}
      </ImageUploading>
    </div>
  );
};

const SeImageUploader = ({
  data,
  onFormItemValueChanged,
  onFormItemValueChangedData,
  editModeEnabled = false,
  allowDisabling = true,
  dataField,
  title,
  className = "",
  widgetOptions = {},
  children,
  maxNumber,
}) => {
  const disabled = allowDisabling && (get(data, dataField) === null || !has(data, dataField));

  const onValueChanged = (e) => {
    e.component.option("dataField", dataField);
    e.value = e.value !== undefined ? e.value : parseFloat(e?.component?.option("text"));
    return onFormItemValueChanged(e);
  };

  const onValueChangedData = (dataField, imageData) => {
    return onFormItemValueChangedData({ dataField, value: imageData?.base64 || null });
  };

  return (
    <div className={"dx-field image-uploader-field " + className}>
      {allowDisabling && editModeEnabled && (
        <div className="field-enabler">
          <Switch
            defaultValue={get(data, dataField) === null || !has(data, dataField) ? false : true}
            onValueChanged={onValueChanged}
            dataField={dataField}
            enabler={true}
          />
        </div>
      )}
      <div className="dx-field-label" disabled={disabled}>
        {title}
      </div>
      <SeImageUploaderField
        data={data}
        defaultValue={get(data, dataField)}
        onInput={onValueChangedData}
        onImageChanged={onValueChangedData}
        showSpinButtons={true}
        disabled={disabled}
        maxNumber={maxNumber}
        dataField={dataField}
        {...widgetOptions}
      >
        {children}
      </SeImageUploaderField>
    </div>
  );
};

export { SeImageUploader, SeImageUploaderField };
