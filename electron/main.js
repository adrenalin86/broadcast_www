const electron = require("electron");
const path = require("path");
const isDev = require("electron-is-dev");
if (isDev) {
  require("electron-reload");
}

const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

let mainWindow;

function createWindow() {
  mainWindow = new BrowserWindow({
    width: 1200,
    height: 800,
    webPreferences: {
      nodeIntegration: true,
    },
  });
  mainWindow.removeMenu();
  mainWindow.loadURL(
    isDev ? "http://localhost:3000/#/init" : `file://${path.join(__dirname, "../build/index.html#/init")}`
  );

  mainWindow.on("closed", () => {
    mainWindow = null;
  });

  if (isDev) {
    mainWindow.webContents.openDevTools();
  }
}

app.on("ready", createWindow);

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (mainWindow === null) {
    createWindow();
  }
});
