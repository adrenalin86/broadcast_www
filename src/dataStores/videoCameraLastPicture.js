import CustomStore from "devextreme/data/custom_store";

import store from "../store";

import { getBackendUrl, getToken, getRegion } from "../store/commonSlice";

import { handleResponseErrors, handleDataErrors, handleCaughtErrors } from "./utils";

const getDataStore = ({ windowId }) => {
  const restArgs = {
    ct: 34,
    w: windowId,
  };

  return new CustomStore({
    key: "id",
    load: (data) => {
      let body = {
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        ...restArgs,
        cd: "getVideoCameraLastPicture",
        data: data,
      };

      return fetch(getBackendUrl(store.getState()) + "?" + body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((data) => {
          return data.blob();
        })
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },
  });
};

export default getDataStore;
