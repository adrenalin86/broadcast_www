import React, { useEffect, useState } from "react";
import { connect } from "react-redux";

import DataGrid, {
  Column,
  StateStoring,
  Export,
  Scrolling,
  LoadPanel,
} from "devextreme-react/data-grid";
import Form, { SimpleItem } from "devextreme-react/form";
import { DateBox } from "devextreme-react";
import { TagBox } from "devextreme-react/tag-box";
import { Button } from "devextreme-react/button";
import { LoadIndicator } from "devextreme-react/load-indicator";
import { ProgressBar } from "devextreme-react/progress-bar";
import { Popup } from "devextreme-react/popup";

import notify from "devextreme/ui/notify";
import { exportDataGrid } from "devextreme/excel_exporter";

import { Workbook } from "exceljs";
import saveAs from "file-saver";
import { sanitize } from "dompurify";
import { isNull, omitBy, isDate, mapValues, isEmpty, tail } from "lodash-es";
// import dayjs from "dayjs";

import { deauthorize } from "../../store/commonSlice";

import {
  getReportsDataStore,
  getUsersDataStore,
  WINDOWS,
  getHistoryReportsDataStore,
  getSystemWindowsDataStore,
} from "../../dataStores";

import config from "../../config";

const userActionsGridOptions = config.pages.reports.userActions.userActionsGrid.dxOptions;

const REPORT_STATUS_REQUEST_TIMEOUT = config.pages.reports.common.reportStatusRequestTimeout;

// var utc = require('dayjs/plugin/utc')
// dayjs.extend(utc)

function onExporting(e) {
  const workbook = new Workbook();
  const worksheet = workbook.addWorksheet("Main sheet");

  exportDataGrid({
    component: e.component,
    worksheet: worksheet,
    autoFilterEnabled: true,
  }).then(function () {
    workbook.xlsx.writeBuffer().then(function (buffer) {
      saveAs(new Blob([buffer], { type: "application/octet-stream" }), "DataGrid.xlsx");
    });
  });
  e.cancel = true;
}

const minDate = new Date(1900, 0, 1);

const endOfCurrentDay = new Date(
  new Date().getFullYear(),
  new Date().getMonth(),
  new Date().getDate(),
  23,
  59,
  59
);

const defaultFilters = {
  fromTimeUtc: new Date(new Date().getFullYear(), new Date().getMonth()),
  toTimeUtc: endOfCurrentDay,
  users: null,
};

const windowId = WINDOWS.REPORTS_USER_ACTIONS.id;

const reportsDataStore = getHistoryReportsDataStore({
  windowId: windowId,
  reportName: "userActions",
});
const usersDataStore = getUsersDataStore({
  windowId: windowId,
});

const systemWindowsDataStore = getSystemWindowsDataStore({
  windowId: windowId,
});

const UserActionsPage = ({ localDeauthorize }) => {
  const [userActionsStore, setUserActionsStore] = useState([]);
  const [reportBeingGenerated, setReportBeingGenerated] = useState(false);
  const [reportGenerationProgress, setReportGenerationProgress] = useState(0);
  const [reportFilters] = useState(defaultFilters);
  const [reportEnabled, setReportEnabled] = useState(true);
  const [windowTitle, setWindowTitle] = useState();

  let progressTimerId = null;

  function onDataErrorOccurred(e) {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);
    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  }

  const onFilterValueChanged = (e) => {
    const value = e.value;
    const dataField = e.component.option("dataField");
    reportFilters[dataField] = value;
  };

  const generateReport = () => {
    setReportBeingGenerated(false);
    setReportGenerationProgress(0);

    //Отфильтровываем пустые значения из тела запроса.
    let params = omitBy(reportFilters, (item) => {
      if (Array.isArray(item)) {
        return isEmpty(item);
      }
      return isNull(item);
    });

    params = mapValues(params, (item) => {
      if (isDate(item)) {
        return item.toISOString().split(".")[0] + "Z"; //Все даты переводим в формат нужный серверу, utc без миллисекунд.
      }
      return item;
    });

    startGeneration(params);
  };

  const startGeneration = async (params) => {
    setReportEnabled(false);

    await reportsDataStore.generateReport(params).catch((error) => {
      onGenerationError(error);
    });

    if (reportsDataStore.getStatus() === "generating") {
      setReportBeingGenerated(true);
      setReportEnabled(true);

      //Задаём периодическую проверку статуса отчёта.
      progressTimerId = setInterval(() => {
        updateReportProgress();
      }, REPORT_STATUS_REQUEST_TIMEOUT);
    } else if (reportsDataStore.getStatus() === "error") {
      onGenerationError();
      setReportEnabled(true);
    } else {
      setReportEnabled(true);
    }
  };

  const updateReportProgress = async () => {
    const reportProgress = await reportsDataStore.getReportProgress().catch((err) => {
      onGenerationError(err);
    });

    if (reportsDataStore.getStatus() === "done") {
      setReportGenerationProgress(reportProgress.reportGenerationProgress);

      clearInterval(progressTimerId);

      const report = await reportsDataStore.getReport().catch((err) => {
        onGenerationError(err);
      });

      //Если отчёт сформирован но данных нет выбрасываем ошибку.
      if (!report?.data) {
        onGenerationError();
        setReportBeingGenerated(false);
      } else {
        setUserActionsStore(report.data);
        setReportBeingGenerated(false);
      }
    } else if (reportsDataStore.getStatus() === "error") {
      clearInterval(progressTimerId);
      onGenerationError();
      setReportBeingGenerated(false);
    }
  };

  const cancelReportGeneration = () => {
    clearInterval(progressTimerId);
    setReportBeingGenerated(false);
    setReportGenerationProgress(0);
  };

  const onGenerationError = (error) => {
    notify("Ошибка формирования журнала", "Error", config.common.errorMessageLifespan);
    if (error) {
      onDataErrorOccurred({ error });
    }
  };

  useEffect(() => {
    systemWindowsDataStore.byKey(windowId).then((result) => {
      const windowName = result[0]?.name?.split("/").pop();

      setWindowTitle(windowName);
    });
  }, []);

  return (
    <React.Fragment>
      {reportBeingGenerated && (
        <Popup
          visible={true}
          closeOnOutsideClick={false}
          showTitle={false}
          showCloseButton={false}
          width={300}
          height={240}
          position={{
            my: "center",
            at: "center",
            of: "#userActionsPageContent",
          }}
        >
          <div className="progressPopup">
            <div className="progressPopup-title">Журнал формируется</div>
            <LoadIndicator />
            <ProgressBar
              className="progressBar"
              value={reportGenerationProgress}
              statusFormat={(ratio, value) => {
                return "Прогресс: " + Math.round(ratio * 100) + "%";
              }}
            />
            <Button text="Отменить" onClick={cancelReportGeneration} />
          </div>
        </Popup>
      )}
      <h2 className="content-block">{windowTitle}</h2>
      <div className="content-block">
        <div id="userActionsPageContent" className="dx-card responsive-paddings">
          <div className="filters-container">
            <Form formData={defaultFilters}>
              <SimpleItem>
                <div className="filters-row">
                  <div className="filter-item">
                    <div className="filter-label">Период отчёта</div>
                    <div className="filter-values">
                      <DateBox
                        type="datetime"
                        min={minDate}
                        dataField="fromTimeUtc"
                        defaultValue={defaultFilters.fromTimeUtc}
                        onValueChanged={onFilterValueChanged}
                      />
                      <DateBox
                        type="datetime"
                        min={reportFilters.fromTimeUtc}
                        dataField="toTimeUtc"
                        defaultValue={defaultFilters.toTimeUtc}
                        onValueChanged={onFilterValueChanged}
                      />
                    </div>
                  </div>
                  <div className="filter-item">
                    <div className="filter-label">Пользователь</div>
                    <div className="filter-values">
                      <TagBox
                        dataField="userIds"
                        dataSource={{
                          store: usersDataStore,
                          onLoadError: (error) => {
                            onDataErrorOccurred({ error });
                          },
                        }}
                        valueExpr="id"
                        displayExpr={(e) => {
                          if (e) {
                            return `${e?.description}(id=${e.id})`;
                          } else {
                            return "";
                          }
                        }}
                        placeholder="Все"
                        searchEnabled={true}
                        showClearButton={true}
                        multiline={false}
                        onValueChanged={onFilterValueChanged}
                      />
                    </div>
                  </div>
                  <div className="filter-item generateReport">
                    <Button
                      className="generateReport-button"
                      onClick={generateReport}
                      text="Получить журнал"
                      stylingMode="contained"
                      type="default"
                      disabled={!reportEnabled}
                    />
                  </div>
                </div>
              </SimpleItem>
            </Form>
          </div>
          <DataGrid
            id="userActionsDataGrid"
            dataSource={userActionsStore}
            onDataErrorOccurred={onDataErrorOccurred}
            onExporting={onExporting}
            errorRowEnabled={false}
            {...userActionsGridOptions}
          >
            <StateStoring
              enabled={true}
              type="localStorage"
              storageKey="pages.reports.userActions.userActionsGrid"
            />
            <LoadPanel enabled={true} />
            <Scrolling mode="standart" />
            <Export enabled={true} allowExportSelectedData={true} />
            {/* <Column dataField={"time"} caption={"Время"} dataType="datetime"
              cellReneder={(cell) => {
                const time = cell?.row?.data["time"];
                let formatedTime = "";
                if (time) {
                  formatedTime = dayjs.utc(time).local().format("DD-MM-YYYY HH:mm");
                }
                return <div>{time && formatedTime}</div>
              }} /> */}
            <Column dataField={"time"} caption={"Время"} dataType={"string"} />
            <Column dataField={"user"} caption={"Пользователь"} />
            <Column dataField={"action"} caption={"Действие"} />
            <Column
              dataField={"description"}
              caption={"Описание"}
              cellRender={(cell) => {
                //получить массив строк description
                const descriptionItems = cell.row.data["description"].split(/\r\n|\n/g);

                //добавить html форматирование в текст description
                const html = descriptionItems
                  .map(function (s) {
                    if (s) {
                      var arr = s.split(":");
                      return "<b>" + arr[0] + "</b>:" + tail(arr).join(":");
                    } else {
                      return "";
                    }
                  })
                  .join("<br/>")
                  .replace(/ /g, "&nbsp;");

                //"стерилизовать" description
                const shtml = sanitize(html, { USE_PROFILES: { html: true } });

                return (
                  <div
                    dangerouslySetInnerHTML={{
                      __html: shtml,
                    }}
                  ></div>
                );
              }}
            />
          </DataGrid>
        </div>
      </div>
    </React.Fragment>
  );
};

export default connect(
  (state) => ({}),
  (dispatch) => {
    return { localDeauthorize: () => dispatch(deauthorize()) };
  }
)(UserActionsPage);
