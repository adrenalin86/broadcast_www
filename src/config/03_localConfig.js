import { cleanUnavailableFields } from "./01_defaultsConfig";
import { getDeepPathValueMap } from "./../utils";

function getLocalConfig() {
  const res = window.app && window.app.env.LOCAL_CONFIG;
  return cleanUnavailableFields(getDeepPathValueMap(res));
}

export const localConfig = getLocalConfig();
