import { createSlice } from "@reduxjs/toolkit";

import { omitDeepByKeys } from "../utils";
import config from "../config";

export const USERS_PAGE_REDUCER_KEY = "users";

const usersPageSlice = createSlice({
  name: USERS_PAGE_REDUCER_KEY,
  initialState: {
    ...omitDeepByKeys(config.pages[USERS_PAGE_REDUCER_KEY], ["dxOptions"]),
  },
  reducers: {},
});

export const getUsersPage = (state) => state.pages[USERS_PAGE_REDUCER_KEY];

export default usersPageSlice.reducer;
