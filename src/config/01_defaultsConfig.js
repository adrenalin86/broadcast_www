import { getDeepPathValueMap } from "../utils";

// 1. Структура данных должна совпадать со структурой стейта стора
// 2. Массивы и их содержимое не мержится а заменяется

const defaultsConfig = {
  common: {
    appTitle: "Умный транспорт",
    locale: "ru",
    themeName: "",
    backend: { url: "", regionId: 21001, useProtocolFromUrl: false },
    errorMessageLifespan: 4000,
    token: "",
    userData: { userDescription: "", userId: null },
    rememberUser: true,
    userRights: { rights: [], regRights: [], windowsRights: [] },
  },
  pages: {
    login: {
      showUrlInput: true,
      showRegionInput: true,
    },
    controlCenter: {
      vehiclesGrid: {
        updateInterval: 10000,
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          columnAutoWidth: true,
          highlightChanges: true,
          repaintChangesOnly: false,
        },
        ordersPopup: {
          dxOptions: {
            title: "Наряды для тс",
            showTitle: true,
            width: "80%",
            height: "80%",
            showCloseButton: false,
          },
        },
      },
    },
    specTransports: {
      specTransportsGrid: {
        updateInterval: 10000,
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          repaintChangesOnly: true,
        },
        popupSpecTransportEditor: {
          dxOptions: {
            title: "Редактирование спецтранспорта",
            showTitle: true,
            width: "80%",
            height: "80%",
            showCloseButton: false,
          },
        },
      },
    },
    specCompanies: {
      specCompaniesGrid: {
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          repaintChangesOnly: true,
        },
        popupSpecCompanyEditor: {
          dxOptions: {
            title: "Редактирование организации для спецтранспорта",
            showTitle: true,
            width: "80%",
            height: "80%",
            showCloseButton: false,
          },
        },
      },
    },
    specTransportTypes: {
      specTransportTypesGrid: {
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          repaintChangesOnly: true,
        },
        popupTransportTypeEditor: {
          dxOptions: {
            title: "Редактирование типа спецтранспорта",
            showTitle: true,
            width: "80%",
            height: "80%",
            showCloseButton: false,
          },
        },
      },
    },
    videoCameras: {
      videoCamerasGrid: {
        updateInterval: 15000,
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          repaintChangesOnly: true,
        },
        popupVideoCameraEditor: {
          dxOptions: {
            title: "Редактирование видеокамеры",
            showTitle: true,
            width: "80%",
            height: "80%",
            showCloseButton: false,
          },
        },
      },
      videoCamerasMap: {
        zoomThreshold: 14,
        startingCenter: {
          lat: 54.6292,
          lng: 39.7364,
        },
      },
    },
    displays: {
      displaysGrid: {
        updateInterval: 15000,
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          repaintChangesOnly: true,
        },
        popupDisplayEditor: {
          dxOptions: {
            title: "Редактор табло",
            showTitle: true,
            width: "80%",
            height: "80%",
            showCloseButton: false,
          },
        },
      },
      displaysMap: {
        zoomThreshold: 14,
        startingCenter: {
          lat: 54.6292,
          lng: 39.7364,
        },
      },
    },
    displayModels: {
      displayModelsGrid: {
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          highlightChanges: true,
        },
        popupDisplayModelsEditor: {
          dxOptions: {
            title: "Редактор моделей табло",
            showTitle: true,
            width: "80%",
            height: "80%",
            showCloseButton: false,
          },
        },
      },
    },
    displayProtocols: {
      displayProtocolsGrid: {
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          columnAutoWidth: true,
          highlightChanges: true,
        },
        popupDisplayProtocolsEditor: {
          dxOptions: {
            title: "Редактор протоколов табло",
            showTitle: true,
            width: "80%",
            height: "80%",
            showCloseButton: false,
          },
        },
      },
    },
    displayGroups: {
      groupsGrid: {
        dxOptions: {
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
        },
        groupsEditorPopup: {
          dxOptions: {
            title: "Редактор групп табло",
            showTitle: true,
            width: "80%",
            height: "80%",
            showCloseButton: false,
          },
          groupItemsEditor: {
            groupDisplaysGrid: {
              dxOptions: {
                filterRow: { visible: true },
                showBorders: true,
                showColumnLines: true,
                rowAlternationEnabled: true,
                selection: { mode: "multiple", showCheckBoxesMode: "onClick" },
              },
            },
            displaySources: {
              displaysGrid: {
                dxOptions: {
                  filterRow: { visible: true },
                  showBorders: true,
                  showColumnLines: true,
                  rowAlternationEnabled: true,
                  selection: {
                    mode: "multiple",
                    showCheckBoxesMode: "onClick",
                  },
                },
              },
            },
          },
        },
      },
    },
    SmartStationsGroups: {
      groupsGrid: {
        dxOptions: {
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
        },
        groupsEditorPopup: {
          dxOptions: {
            title: "Редактор групп умных остановок",
            showTitle: true,
            width: "80%",
            height: "80%",
            showCloseButton: false,
          },
          groupItemsEditor: {
            groupDisplaysGrid: {
              dxOptions: {
                filterRow: { visible: true },
                showBorders: true,
                showColumnLines: true,
                rowAlternationEnabled: true,
                selection: { mode: "multiple", showCheckBoxesMode: "onClick" },
              },
            },
            displaySources: {
              displaysGrid: {
                dxOptions: {
                  filterRow: { visible: true },
                  showBorders: true,
                  showColumnLines: true,
                  rowAlternationEnabled: true,
                  selection: {
                    mode: "multiple",
                    showCheckBoxesMode: "onClick",
                  },
                },
              },
            },
          },
        },
      },
    },
    broadcastPrograms: {
      broadcastProgramsGrid: {
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          columnAutoWidth: true,
          highlightChanges: true,
        },
        popupBroadcastProgramsEditor: {
          dxOptions: {
            title: "Редактор эфира",
            showTitle: true,
            width: "80%",
            height: "80%",
            showCloseButton: false,
          },
          displaysMaster: {
            dxOptions: {
              title: "Мастер выбора табло",
              showTitle: true,
              width: "80%",
              height: "80%",
              showCloseButton: false,
            },
            displaySources: {
              dxOptions: {
                filterRow: { visible: true },
                showBorders: true,
                showColumnLines: true,
                rowAlternationEnabled: true,
                selection: {
                  mode: "multiple",
                  showCheckBoxesMode: "onClick",
                },
              },
            },
            chosenDisplays: {
              dxOptions: {
                filterRow: { visible: true },
                showBorders: true,
                showColumnLines: true,
                rowAlternationEnabled: true,
                selection: {
                  mode: "multiple",
                  showCheckBoxesMode: "onClick",
                },
              },
            },
          },
        },
      },
    },
    broadcastProgramTemplates: {
      broadcastProgramTemplatesGrid: {
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          columnAutoWidth: true,
          highlightChanges: true,
        },
        popupBroadcastProgramTemplateEditor: {
          dxOptions: {
            title: "Редактор шаблона программы эфира",
            showTitle: true,
            width: "80%",
            height: "80%",
            showCloseButton: false,
          },
        },
      },
    },
    reports: {
      common: {
        reportStatusRequestTimeout: 1000,
      },
      userActions: {
        userActionsGrid: {
          dxOptions: {
            filterRow: { visible: true },
            showBorders: true,
            showColumnLines: true,
            rowAlternationEnabled: true,
            columnAutoWidth: true,
          },
        },
      },
    },
    tags: {
      tagsGrid: {
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          columnAutoWidth: true,
          highlightChanges: true,
        },
        popupTagsEditor: {
          dxOptions: {
            title: "Редактор тэгов",
            showTitle: true,
            width: "80%",
            height: "80%",
            showCloseButton: false,
          },
        },
      },
    },
    users: {
      usersGrid: {
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          columnAutoWidth: true,
        },
      },
      popupUsersEditor: {
        dxOptions: {
          title: "Редактор пользователя",
          showTitle: true,
          width: "80%",
          height: "80%",
          showCloseButton: false,
        },
        groupsDatagrid: {
          dxOptions: {
            showBorders: true,
            showColumnLines: true,
            rowAlternationEnabled: true,
            columnAutoWidth: true,
          },
        },
      },
    },
    broadcastRequestForDisplayInfo: {
      broadcastRequestForDisplayInfoDataGrid: {
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          columnAutoWidth: true,
          highlightChanges: true,
        },
        broadcastRequestForDisplayInfoPopup: {
          dxOptions: {
            title: "Редактор запросов на отображение информации",
            showTitle: true,
            width: "80%",
            height: "80%",
            showCloseButton: false,
          },
        },
      },
    },
    areas: {
      areasTreeList: {
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          columnAutoWidth: true,
        },
      },
      routesDataGrid: {
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          columnAutoWidth: true,
        },
      },
    },
    pinCodes: {
      pinCodesEditorDataGrid: {
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          columnAutoWidth: true,
        },
      },
      areasInPinCodeDataGrid: {
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          columnAutoWidth: true,
        },
      },
    },
    webNews: {
      webNewsDataGrid: {
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          columnAutoWidth: true,
        },
        webNewsEditorPopup: {
          dxOptions: {
            title: "Редактор новости",
            showTitle: true,
            width: "80%",
            height: "80%",
            showCloseButton: false,
          },
        },
      },
    },
    webClaims: {
      grid: {
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          columnAutoWidth: true,
        },
        editorPopup: {
          dxOptions: {
            title: "Редактор обращений",
            showTitle: true,
            width: "80%",
            height: "80%",
            showCloseButton: false,
          },
        },
      },
    },
    roadEvents: {
      grid: {
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          columnAutoWidth: true,
        },
        editorPopup: {
          dxOptions: {
            title: "Редактор дорожного события",
            showTitle: true,
            width: "80%",
            height: "80%",
            showCloseButton: false,
          },
        },
      },
      map: {
        startingCenter: {
          lat: 54.6292,
          lng: 39.7364,
        },
      },
    },
    deviceType: {
      grid: {
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          columnAutoWidth: true,
        },
        editorPopup: {
          dxOptions: {
            title: "Редактор типа периферийоного оборудования",
            showTitle: true,
            width: "80%",
            height: "57%",
            showCloseButton: false,
          },
        },
      },
    },
    devices: {
      grid: {
        updateInterval: 15000,
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          columnAutoWidth: true,
        },
        editorPopup: {
          dxOptions: {
            title: "Редактор периферийоного оборудования",
            showTitle: true,
            width: "80%",
            height: "40%",
            showCloseButton: false,
          },
        },
      },
    },
    smartStations: {
      grid: {
        updateInterval: 15000,
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          columnAutoWidth: true,
        },
        editorPopup: {
          dxOptions: {
            title: "Редактор умных остановок",
            showTitle: true,
            width: "80%",
            height: "80%",
            showCloseButton: false,
          },
        },
        viewPopup: {
          dxOptions: {
            title: "Просмотр умной остановки",
            showTitle: true,
            width: "80%",
            height: "80%",
            showCloseButton: false,
          },
        },
      },
      map: {
        startingCenter: {
          lat: 54.6292,
          lng: 39.7364,
        },
      },
    },
    vehicleRoutes: {
      grid: {
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          columnAutoWidth: true,
        },
        editorPopup: {
          dxOptions: {
            title: "Редактор маршрутов транспорта",
            showTitle: true,
            width: "80%",
            height: "80%",
            showCloseButton: false,
          },
        },
        subroutesPopup: {
          dxOptions: {
            title: "Список подмаршуртов",
            showTitle: true,
            width: "80%",
            height: "80%",
            showCloseButton: false,
          },
          grid: {
            dxOptions: {
              filterRow: { visible: true },
              showBorders: true,
              showColumnLines: true,
              rowAlternationEnabled: true,
              columnAutoWidth: true,
            },
          },
        },
      },
    },
    polls: {
      grid: {
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          columnAutoWidth: true,
        },
        editorPopup: {
          dxOptions: {
            title: "Редактор опросов",
            showTitle: true,
            width: "80%",
            height: "80%",
            showCloseButton: false,
          },
        },
      },
    },
    vehicles: {
      grid: {
        dxOptions: {
          filterRow: { visible: true },
          showBorders: true,
          showColumnLines: true,
          rowAlternationEnabled: true,
          columnAutoWidth: true,
        },
        editorPopup: {
          dxOptions: {
            title: "Редактор транспортных средств",
            showTitle: true,
            width: "80%",
            height: "80%",
            showCloseButton: false,
          },
        },
      },
    },
  },
  /* В связи с механизмом работы выключателей полей надо помнить, что как пустое значение по умолчанию
    для полей не объектов здесь надо использовать undefined, а не null */
  defaults: {
    broadcastPrograms: {
      name: "",
      priority: 100,
      startTimeUtc: new Date(),
      endTimeUtc: new Date(),
      cond: {
        displayIds: [],
        displayGroupIds: [],
        tagIds: [],
      },
    },
    broadcastRequestForDisplayInfo:{
      lat: 54.6292,
      lng: 39.7364,
    },
    displayGroups:{
      lat: 54.6292,
      lng: 39.7364,
    },
    displays: {
      lat: 54.6292,
      lng: 39.7364,
      settings: {
        timeoutInSec: 60,
        forecastSource: {
          turnOffForecasts: false,
          forecastsSourceStationId: undefined,
          sortForesByRoute: false,
          showAtEndStation: true,
          arrtimeThreshold: 0,
        },
        itLineU653: {
          forecastsModeNumber: 1,
          pictureModeNumber: 2,
          rssStartIndex: 64,
          rnStartIndex: 64,
          busIcoType: 1,
          marshIcoType: 2,
          trollIcoType: 3,
          tramIcoType: 4,
          image: {
            trafficLight: null,
            simple: null,
          },
        },
        itLineU646: {
          brightness: undefined,
          turnOnWatchdog: false,
          outputMode: 1,
          customCommands: undefined,
          routeNameFormat: "",
          routeDescriptionTripFormat: "",
          routeDescriptionInEndStationFormat: "",
          routeForecastTripFormat: "",
          routeForecastInEndStationFormat: "",
          complexString: {
            pixelShift: 0,
            speed: 3,
            weatherString: null,
            simpleString: {
              text: "Табло работает в тестовом режиме",
            },
            newsString: null,
          },
        },
        smartJson2: {
          frame: {
            frameType: 2,
            topText: "",
            bottomText: "",
            imageUrl: "",
            imageData: undefined,
            videoUrl: "",
            items: [],
            duration: 0,
          },
          errorFrame: null,
        },
      },
    },
    videoCameras: {
      lat: 54.6292,
      lng: 39.7364,
    },
    webClaims: {
      lat: 54.6292,
      lng: 39.7364,
    },
    polls: {
      showResultAfterAnswer: false,
    },
  },
};

export const defaultsConfigMap = getDeepPathValueMap(defaultsConfig);
const availablePaths = Object.keys(defaultsConfigMap);

/**
 * Создает новый map, в который копируются только значения доступные в defaults
 * @param mapToCheck - map'а, значения которой чистим
 * @returns {{}} новый map
 */
export const cleanUnavailableFields = (mapToCheck) =>
  availablePaths.reduce((cleanedMap, path) => {
    if (mapToCheck[path] !== undefined) {
      cleanedMap[path] = mapToCheck[path];
    }

    return cleanedMap;
  }, {});
