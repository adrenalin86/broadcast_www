import throttle from "lodash-es/throttle";

import * as userConfig from "../config/04_userConfig";

const SEC = 1000;

const SAVE_BATCH_TIME = SEC;

export const saveUserConfigMiddleware = (store) => {
  const saveUserConfig = throttle(
    () => {
      const state = store.getState();

      userConfig.setToLocalStorage(state);
    },
    SAVE_BATCH_TIME,
    { leading: false }
  );


  return (next) => (action) => {
    next(action);

    saveUserConfig();
  };
};
