import CustomStore from "devextreme/data/custom_store";
import { isArray, isEmpty } from "lodash-es";

import store from "../store";

import { getBackendUrl, getToken, getRegion } from "../store/commonSlice";

import {
  handleResponseErrors,
  handleDataErrors,
  handleCaughtErrors,
} from "./utils";

const getDataStore = ({ windowId }) => {
  const restArgs = {
    w: windowId,
    ct: 24,
  };

  /* Я случайно узнал
   * из https://supportcenter.devexpress.com/ticket/details/t138454/extending-customstore-using-typescript-extends
   * что эта фича реализована
   */

  let _fixedFilter = [];

  return new (CustomStore.inherit({
    fixedFilter: function (filter) {
      /* Общи this тут не наблюдается */
      if (isArray(filter)) {
        _fixedFilter = filter;
      } else {
        return _fixedFilter;
      }
    },
  }))({
    key: "id",
    load: function (data) {
      let filter;
      const fixedFilter = _fixedFilter;
      if (fixedFilter) {
        filter = data?.filter ? [data.filter, "and", fixedFilter] : fixedFilter;
      } else {
        filter = data?.filter;
      }
      let body = {
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        ...restArgs,
        cd: "loadBroadcastPrograms",
        data: data ? { ...data, filter: filter } : { filter },
      };

      return fetch(getBackendUrl(store.getState()) + "?" + body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .then((data) => {
          return {
            data: data.data.entities,
            summary: data.data.summary,
            totalCount: data.data.totalCount,
          };
        })
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },
    insert: function (values) {
      let body = {
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        ...restArgs,
        cd: "insertBroadcastProgram",
        data: {
          ...values,
        },
      };

      return fetch(getBackendUrl(store.getState()) + "?" + body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },

    update: function (key, values) {
      let body = {
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        ...restArgs,
        cd: "updateBroadcastProgram",
        data: { id: key, values },
      };

      return fetch(getBackendUrl(store.getState()) + "?" + body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },

    remove: function (key) {
      let body = {
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        ...restArgs,
        cd: "removeBroadcastProgram",
        data: {
          id: key,
        },
      };

      return fetch(getBackendUrl(store.getState()) + "?" + body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },
  });
};

export default getDataStore;
