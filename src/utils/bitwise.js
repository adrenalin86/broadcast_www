/*
 * https://github.com/trekhleb/javascript-algorithms/tree/master/src/algorithms/math/bits */

/**
 * @param {number} number
 * @param {number} bitPosition - zero based.
 * @return {number}
 */
function getBit(number, bitPosition) {
  return (number >> bitPosition) & 1;
}

/**
 * @param {number} number
 * @param {number} bitPosition - zero based.
 * @return {number}
 */
function setBit(number, bitPosition) {
  return number | (1 << bitPosition);
}

/**
 * @param {number} number
 * @param {number} bitPosition - zero based.
 * @return {number}
 */
function clearBit(number, bitPosition) {
  const mask = ~(1 << bitPosition);
  return number & mask;
}

/**
 * @param {number} number
 * @param {number} bitPosition - zero based.
 * @param {number} bitValue - 0 or 1.
 * @return {number}
 */
function updateBit(number, bitPosition, bitValue) {
  // Normalized bit value.
  const bitValueNormalized = bitValue ? 1 : 0;
  // Init clear mask.
  const clearMask = ~(1 << bitPosition);
  // Clear bit value and then set it up to required value.
  return (number & clearMask) | (bitValueNormalized << bitPosition);
}

export { getBit, setBit, clearBit, updateBit };
