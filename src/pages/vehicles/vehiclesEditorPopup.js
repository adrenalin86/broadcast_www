import React, { useState, useEffect } from "react";

import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import { ScrollView } from "devextreme-react/scroll-view";
import Form, { SimpleItem } from "devextreme-react/form";
import { TextBox, NumberBox } from "devextreme-react";

import ValidationSummary from "devextreme-react/validation-summary";
import ValidationEngine from "devextreme/ui/validation_engine";
import Validator, { RequiredRule } from "devextreme-react/validator";

import { set, cloneDeep, isEmpty } from "lodash-es";

import config from "../../config";

const defaultValues = {};

const TagsEditorPopup = ({
  editedRow,
  closeEditorPopup,
  updateEntry,
  createEntry,
}) => {
  const [formData, setFormData] = useState({});
  const [changedFormData] = useState({});

  const popupOptions = config.pages.vehicles.grid.editorPopup.dxOptions;
  /**
   * Инициализация редактора.
   */
  useEffect(() => {
    if (isEmpty(editedRow)) {
      setFormData(cloneDeep(defaultValues));
    } else {
      setFormData(cloneDeep(editedRow));
    }
  }, []);

  const closePopup = () => {
    closeEditorPopup();
  };

  const onSave = () => {
    const isValid = ValidationEngine.validateGroup("formEditor").isValid;

    if (isValid) {
      if (isEmpty(editedRow)) {
        createEntry({
          formData: formData,
        });
      } else {
        updateEntry({
          formData: changedFormData,
          editedRowId: editedRow.id,
        });
      }
    }
  };

  const onCancel = () => {
    closePopup();
  };

  const onFormItemValueChanged = (e) => {
    const value =
      e.value !== undefined ? e.value : e?.component?.option("text");
    const dataField = e.component.option("dataField");

    setFormDataValue(dataField, value);
  };

  const onFormItemValueChangedData = ({ value, dataField }) => {
    setFormDataValue(dataField, value);
  };

  const setFormDataValue = (dataField, value) => {
    set(formData, dataField, value);
    set(changedFormData, dataField, value);
  };

  return (
    <React.Fragment>
      <Popup
        className="customEditorPopup"
        visible={true}
        onHiding={closePopup}
        titleComponent={() => {
          return <span className="popup-title">{popupOptions.title}</span>;
        }}
        {...popupOptions}
      >
        <Position my="center" at="center" of={window} />
        <ScrollView showScrollbar="always">
          <form>
            <Form validationGroup="formEditor">
              <SimpleItem>
                <div className="editor-content">
                  <div className="dx-form-group-with-caption dx-form-group editor-group">
                    <div className="dx-form-group-caption">Общие сведения</div>
                    <div className="dx-form-group-content editor-content">
                      <div className="editor-item cols-2">
                        <div className="dx-field">
                          <div className="dx-field-label">Госномер</div>
                          <div className="dx-field-value">
                            <TextBox
                              defaultValue={formData.gosNumber}
                              onInput={onFormItemValueChanged}
                              dataField="gosNumber"
                            >
                              <Validator validationGroup="formEditor">
                                <RequiredRule />
                              </Validator>
                            </TextBox>
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">Номер гаража</div>
                          <div className="dx-field-value">
                            <TextBox
                              defaultValue={formData.garageNumber}
                              onInput={onFormItemValueChanged}
                              dataField="garageNumber"
                            >
                              <Validator validationGroup="formEditor">
                                <RequiredRule />
                              </Validator>
                            </TextBox>
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">ID модели</div>
                          <div className="dx-field-value">
                            <NumberBox
                              defaultValue={formData.modelId}
                              onInput={(e) => {
                                const value =
                                  e.value !== undefined
                                    ? e.value
                                    : e?.component?.option("text");
                                const dataField =
                                  e?.component?.option("dataField");
                                onFormItemValueChangedData({
                                  value: parseFloat(value),
                                  dataField: dataField,
                                });
                              }}
                              dataField="modelId"
                            >
                              <Validator validationGroup="formEditor">
                                <RequiredRule />
                              </Validator>
                            </NumberBox>
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">ID компании</div>
                          <div className="dx-field-value">
                            <NumberBox
                              defaultValue={formData.companyId}
                              onInput={(e) => {
                                const value =
                                  e.value !== undefined
                                    ? e.value
                                    : e?.component?.option("text");
                                const dataField =
                                  e.component.option("dataField");
                                onFormItemValueChangedData({
                                  value: parseFloat(value),
                                  dataField: dataField,
                                });
                              }}
                              dataField="companyId"
                            >
                              <Validator validationGroup="formEditor">
                                <RequiredRule />
                              </Validator>
                            </NumberBox>
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">Код ТС</div>
                          <div className="dx-field-value">
                            <TextBox
                              defaultValue={formData.deviceCode}
                              onInput={onFormItemValueChanged}
                              dataField="deviceCode"
                            >
                              <Validator validationGroup="formEditor">
                                <RequiredRule />
                              </Validator>
                            </TextBox>
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">Имя оператора</div>
                          <div className="dx-field-value">
                            <TextBox
                              defaultValue={formData.operatorName}
                              onInput={onFormItemValueChanged}
                              dataField="operatorName"
                            >
                              <Validator validationGroup="formEditor">
                                <RequiredRule />
                              </Validator>
                            </TextBox>
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">Номер телефона</div>
                          <div className="dx-field-value">
                            <TextBox
                              defaultValue={formData.phoneNumber}
                              onInput={onFormItemValueChanged}
                              dataField="phoneNumber"
                              // mask="+X (X00) 000-0000"
                              // maskRules={{ X: /[1-9]/ }}
                            >
                              <Validator validationGroup="formEditor">
                                <RequiredRule />
                              </Validator>
                            </TextBox>
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">Примечание</div>
                          <div className="dx-field-value">
                            <TextBox
                              defaultValue={formData.note}
                              onInput={onFormItemValueChanged}
                              dataField="note"
                            ></TextBox>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </SimpleItem>
            </Form>
          </form>
        </ScrollView>
        <ValidationSummary
          id="summary"
          validationGroup="formEditor"
        ></ValidationSummary>
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Сохранить", onClick: onSave }}
          toolbar="bottom"
        />
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Отмена", onClick: onCancel }}
          toolbar="bottom"
        />
      </Popup>
    </React.Fragment>
  );
};

export default TagsEditorPopup;
