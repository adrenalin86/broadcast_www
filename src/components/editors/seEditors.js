import { get, has } from "lodash-es";

import {
  TextBox,
  Lookup,
  CheckBox,
  NumberBox,
  SelectBox,
  Switch,
  TagBox,
  DateBox,
  TextArea,
} from "devextreme-react";

const SeNumberBox = ({
  data,
  onFormItemValueChanged,
  editModeEnabled = false,
  allowDisabling = true,
  dataField,
  title,
  className = "",
  widgetOptions = {},
  children,
}) => {
  const disabled =
    allowDisabling && (get(data, dataField) === null || !has(data, dataField));

  const onValueChanged = (e) => {
    e.component.option("dataField", dataField);
    e.value =
      e.value !== undefined
        ? e.value
        : parseFloat(e?.component?.option("text"));
    return onFormItemValueChanged(e);
  };

  return (
    <div className={"dx-field number-field " + className}>
      {allowDisabling && editModeEnabled && (
        <div className="field-enabler">
          <Switch
            defaultValue={
              get(data, dataField) === null || !has(data, dataField)
                ? false
                : true
            }
            onValueChanged={onValueChanged}
            dataField={dataField}
            enabler={true}
          />
        </div>
      )}
      <div className="dx-field-label" disabled={disabled}>
        {title}
      </div>
      <div className="dx-field-value">
        <NumberBox
          defaultValue={get(data, dataField)}
          onInput={onValueChanged}
          onValueChanged={onValueChanged}
          showSpinButtons={true}
          disabled={disabled}
          {...widgetOptions}
        >
          {children}
        </NumberBox>
      </div>
    </div>
  );
};

const SeCheckBox = ({
  data,
  onFormItemValueChanged,
  editModeEnabled = false,
  allowDisabling = true,
  dataField,
  title,
  className = "",
  widgetOptions = {},
  children,
}) => {
  const disabled =
    allowDisabling && (get(data, dataField) === null || !has(data, dataField));

  const onValueChanged = (e) => {
    e.component.option("dataField", dataField);

    return onFormItemValueChanged(e);
  };

  return (
    <div className={"dx-field checkbox-field " + className}>
      {allowDisabling && editModeEnabled && (
        <div className="field-enabler">
          <Switch
            defaultValue={
              get(data, dataField) === null || !has(data, dataField)
                ? false
                : true
            }
            onValueChanged={onValueChanged}
            dataField={dataField}
            enabler={true}
          />
        </div>
      )}
      <div className="dx-field-label" disabled={disabled}>
        {title}
      </div>
      <div className="dx-field-value">
        <CheckBox
          defaultValue={get(data, dataField)}
          onValueChanged={onValueChanged}
          showSpinButtons={true}
          disabled={disabled}
          {...widgetOptions}
        >
          {children}
        </CheckBox>
      </div>
    </div>
  );
};

const SeTextBox = ({
  data,
  onFormItemValueChanged,
  editModeEnabled = false,
  allowDisabling = true,
  dataField,
  title,
  className = "",
  widgetOptions = {},
  children,
  placeholder,
}) => {
  const disabled =
    allowDisabling && (get(data, dataField) === null || !has(data, dataField));
  const onValueChanged = (e) => {
    e.component.option("dataField", dataField);

    return onFormItemValueChanged(e);
  };

  return (
    <div className={"dx-field textbox-field " + className}>
      {allowDisabling && editModeEnabled && (
        <div className="field-enabler">
          <Switch
            defaultValue={
              get(data, dataField) === null || !has(data, dataField)
                ? false
                : true
            }
            onValueChanged={onValueChanged}
            dataField={dataField}
            enabler={true}
          />
        </div>
      )}
      <div className="dx-field-label" disabled={disabled}>
        {title}
      </div>
      <div className="dx-field-value">
        <TextBox
          defaultValue={get(data, dataField)}
          onInput={onValueChanged}
          disabled={disabled}
          placeholder={placeholder}
          {...widgetOptions}
        >
          {children}
        </TextBox>
      </div>
    </div>
  );
};

const SeTextArea = ({
  data,
  onFormItemValueChanged,
  editModeEnabled = false,
  allowDisabling = true,
  dataField,
  title,
  className = "",
  widgetOptions = {},
  children,
}) => {
  const disabled =
    allowDisabling && (get(data, dataField) === null || !has(data, dataField));
  const onValueChanged = (e) => {
    e.component.option("dataField", dataField);

    return onFormItemValueChanged(e);
  };

  return (
    <div className={"dx-field textbox-field " + className}>
      {allowDisabling && editModeEnabled && (
        <div className="field-enabler">
          <Switch
            defaultValue={
              get(data, dataField) === null || !has(data, dataField)
                ? false
                : true
            }
            onValueChanged={onValueChanged}
            dataField={dataField}
            enabler={true}
          />
        </div>
      )}
      <div className="dx-field-label" disabled={disabled}>
        {title}
      </div>
      <div className="dx-field-value">
        <TextArea
          defaultValue={get(data, dataField)}
          onInput={onValueChanged}
          disabled={disabled}
          {...widgetOptions}
        >
          {children}
        </TextArea>
      </div>
    </div>
  );
};

const SeLookup = ({
  data,
  onFormItemValueChanged,
  editModeEnabled = false,
  allowDisabling = true,
  dataField,
  dataSource,
  valueExpr,
  displayExpr,
  searchExpr,
  title,
  className = "",
  widgetOptions = {},
  children,
}) => {
  const disabled =
    allowDisabling && (get(data, dataField) === null || !has(data, dataField));

  const onValueChanged = (e) => {
    e.component.option("dataField", dataField);

    return onFormItemValueChanged(e);
  };

  return (
    <div className={"dx-field lookup-field " + className}>
      {allowDisabling && editModeEnabled && (
        <div className="field-enabler">
          <Switch
            defaultValue={
              get(data, dataField) === null || !has(data, dataField)
                ? false
                : true
            }
            onValueChanged={onValueChanged}
            dataField={dataField}
            enabler={true}
          />
        </div>
      )}
      <div className="dx-field-label" disabled={disabled}>
        {title}
      </div>
      <div className="dx-field-value">
        <Lookup
          defaultValue={get(data, dataField)}
          dataSource={dataSource}
          valueExpr={valueExpr}
          displayExpr={displayExpr}
          searchExpr={searchExpr}
          onValueChanged={onValueChanged}
          disabled={disabled}
          {...widgetOptions}
        >
          {children}
        </Lookup>
      </div>
    </div>
  );
};

const SeSelectBox = ({
  data,
  onFormItemValueChanged,
  editModeEnabled = false,
  allowDisabling = true,
  dataField,
  dataSource,
  valueExpr,
  displayExpr,
  searchExpr,
  title,
  className = "",
  widgetOptions = {},
  children,
  controlled,
  showSwitch = true,
}) => {
  const disabled =
    allowDisabling && (get(data, dataField) === null || !has(data, dataField));

  const onValueChanged = (e) => {
    e.component.option("dataField", dataField);

    return onFormItemValueChanged(e);
  };

  return (
    <div className={"dx-field selectbox-field " + className}>
      {allowDisabling && editModeEnabled && showSwitch && (
        <div className="field-enabler">
          <Switch
            defaultValue={
              get(data, dataField) === null || !has(data, dataField)
                ? false
                : true
            }
            onValueChanged={onValueChanged}
            dataField={dataField}
            enabler={true}
          />
        </div>
      )}
      <div className="dx-field-label" disabled={disabled}>
        {title}
      </div>
      <div className="dx-field-value">
        <SelectBox
          defaultValue={get(data, dataField)}
          {...(controlled ? { value: get(data, dataField) } : {})}
          dataSource={dataSource}
          valueExpr={valueExpr}
          displayExpr={displayExpr}
          searchExpr={searchExpr}
          onValueChanged={onValueChanged}
          disabled={disabled}
          {...widgetOptions}
        >
          {children}
        </SelectBox>
      </div>
    </div>
  );
};

//TODO: перепутаны редакторы.
const SeTagBox = ({
  data,
  onFormItemValueChanged,
  editModeEnabled = false,
  allowDisabling = true,
  dataField,
  dataSource,
  valueExpr,
  displayExpr,
  searchExpr,
  title,
  className = "",
  widgetOptions = {},
  children,
}) => {
  const disabled =
    allowDisabling && (get(data, dataField) === null || !has(data, dataField));

  const onValueChanged = (e) => {
    e.component.option("dataField", dataField);

    return onFormItemValueChanged(e);
  };

  return (
    <div className={"dx-field selectbox-field " + className}>
      {allowDisabling && editModeEnabled && (
        <div className="field-enabler">
          <Switch
            defaultValue={
              get(data, dataField) === null || !has(data, dataField)
                ? false
                : true
            }
            onValueChanged={onValueChanged}
            dataField={dataField}
            enabler={true}
          />
        </div>
      )}
      <div className="dx-field-label" disabled={disabled}>
        {title}
      </div>
      <div className="dx-field-value">
        <DateBox
          defaultValue={get(data, dataField)}
          dataSource={dataSource}
          valueExpr={valueExpr}
          displayExpr={displayExpr}
          searchExpr={searchExpr}
          onValueChanged={onValueChanged}
          disabled={disabled}
          {...widgetOptions}
        >
          {children}
        </DateBox>
      </div>
    </div>
  );
};

const SeDataBox = ({
  data,
  onFormItemValueChanged,
  editModeEnabled = false,
  allowDisabling = true,
  dataField,
  dataSource,
  valueExpr,
  displayExpr,
  title,
  className = "",
  widgetOptions = {},
  children,
}) => {
  const disabled =
    allowDisabling && (get(data, dataField) === null || !has(data, dataField));

  const onValueChanged = (e) => {
    e.component.option("dataField", dataField);

    return onFormItemValueChanged(e);
  };

  return (
    <div className={"dx-field selectbox-field " + className}>
      {allowDisabling && editModeEnabled && (
        <div className="field-enabler">
          <Switch
            defaultValue={
              get(data, dataField) === null || !has(data, dataField)
                ? false
                : true
            }
            onValueChanged={onValueChanged}
            dataField={dataField}
            enabler={true}
          />
        </div>
      )}
      <div className="dx-field-label" disabled={disabled}>
        {title}
      </div>
      <div className="dx-field-value">
        <TagBox
          defaultValue={get(data, dataField)}
          dataSource={dataSource}
          valueExpr={valueExpr}
          displayExpr={displayExpr}
          onValueChanged={onValueChanged}
          disabled={disabled}
          {...widgetOptions}
        >
          {children}
        </TagBox>
      </div>
    </div>
  );
};

export {
  SeNumberBox,
  SeCheckBox,
  SeTextBox,
  SeLookup,
  SeSelectBox,
  SeTagBox,
  SeDataBox,
  SeTextArea,
};
