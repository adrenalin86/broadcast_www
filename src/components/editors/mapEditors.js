import React, { useState, useEffect, useRef, useCallback } from "react";

import Button from "devextreme-react/button";

import Leaflet from "leaflet";
import "leaflet-lasso";
import { get, isEmpty, isEqualWith, noop, uniqueId } from "lodash-es";

const SeMapSelectPoint = ({
  formData,
  setFormDataValue,
  latDatafield = "lat",
  lngDatafield = "lng",
  defaultLat = 0,
  defaultLng = 0,
  className, reloadValue
}) => {
  const [state, setState] = useState({
    mapCenterCoords: {},
    mapMarkerCoords: {},
  });

  const mapRef = useRef(null);

  let myMap = useRef();
  let mapMarker = useRef();

  const currentValue = {
    lat: get(formData, latDatafield),
    lng: get(formData, lngDatafield),
  };

  const isNotSetValue = !currentValue.lng || !currentValue.lat;
  const value = isNotSetValue
    ? {
        lat: defaultLat,
        lng: defaultLng,
      }
    : currentValue;

  if (isEmpty(state.mapMarkerCoords) || isEmpty(state.mapCenterCoords)) {
    setState({ mapCenterCoords: value, mapMarkerCoords: value });
  }

  const setCells = (latlng) => {
    setFormDataValue(latDatafield, latlng.lat);
    setFormDataValue(lngDatafield, latlng.lng);
  };

  const onMapClick = (e) => {
    setState({ ...state, mapMarkerCoords: e.latlng });
    mapMarker.current?.setLatLng(e.latlng);
    setCells(e.latlng);
  };

  useEffect(() => {
    if (formData?.lat && formData?.lng) {
      const latLng = {lat: +formData?.lat, lng: +formData?.lng}
      setState({ ...state, mapMarkerCoords: latLng });
      mapMarker.current?.setLatLng(latLng);
    }
  }, [reloadValue])

  useEffect(() => {
    if (mapRef.current) {
      if (isEmpty(myMap.current)) {
        myMap.current = Leaflet.map(mapRef.current, {
          center: state.mapCenterCoords,
          zoom: 13,
          maxZoom: 18,
          attributionControl: true,
          zoomControl: true,
          doubleClickZoom: true,
          scrollWheelZoom: true,
          fadeAnimation: true,
          dragging: true,
          animate: true,
          easeLinearity: 0.35,
        });

        if (isNotSetValue) {
          setCells(value);
        }

        Leaflet.tileLayer("//maps.bus62.ru/getTile.php?z={z}&x={x}&y={y}&e=@2x.png", {
          attribution:
            '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>',
        }).addTo(myMap.current);

        mapMarker.current = Leaflet.marker(state.mapMarkerCoords).addTo(myMap.current);

        myMap.current?.addEventListener("click", onMapClick);

        setTimeout(() => {
          myMap.current?.invalidateSize();
        });
      }
    }
    // return () => {
    //   myMap.current.removeEventListener("click", onMapClick);
    //   myMap.current.off();
    //   myMap.current.remove();
    //   mapMarker.current = undefined;
    //   myMap.current = undefined;
    // };
  }, []);

  return (
    <div className={"map-editor " + className}>
      <div
        ref={mapRef}
        style={{
          height: "100%",
          width: "100%",
          minHeight: "260px",
          minWidth: "260px",
        }}
      ></div>
    </div>
  );
};

const iconMarkerDefault = Leaflet.icon({
  iconUrl: `${process.env.PUBLIC_URL}/styles/images/marker-icon.png`,
  iconRetinaUrl: `${process.env.PUBLIC_URL}/styles/images/marker-icon-2x.png`,
  iconAnchor: [12, 41],
  iconSize: [25, 41],
  shadowUrl: `${process.env.PUBLIC_URL}/styles/images/marker-shadow.png`,
});

const iconMarkerRedoutline = Leaflet.icon({
  iconUrl: `${process.env.PUBLIC_URL}/styles/images/marker-redoutline.png`,
  iconAnchor: [12, 41],
  iconSize: [25, 41],
  shadowUrl: `${process.env.PUBLIC_URL}/styles/images/marker-shadow.png`,
});

const SeMapAreaSelect = ({
  startingCenter,
  latDatafield = "lat",
  lngDatafield = "lng",
  dataSource,
  onMarkersSelected,
  selectedMarkersIds,
  className,
  onError = noop,
}) => {
  const mapRef = useRef(null);
  const [myMap, setMyMap] = useState({});
  const [markersLayersGroup, setMarkersLayersGroup] = useState({});
  const [lasso, setLasso] = useState({});
  const [dsLoadMeta] = useState({ uid: null });

  /**
   * Устанавливает отображаемые на карте маркеры на основе
   * переданного массива табло
   * @param {Array} param0
   */
  const setMarkersFromDisplaysArray = ({ displaysArray }) => {
    if (mapRef.current) {
      if (!isEmpty(myMap)) {
        //Если ещё не создан слой маркеров на карте, то он создаётся
        if (!isEmpty(markersLayersGroup)) {
          //Сверка что массив табло и массив текущих маркеров не эквивалентны.
          let currentMarkers = { markers: markersLayersGroup.getLayers() }; //костыль что-бы lodash правильно работал как с массивом а не передавал их целиком в функцию сравнения.
          let markersChanged = displaysArray.length !== currentMarkers.length;

          if (!markersChanged) {
            markersChanged = !isEqualWith(
              displaysArray,
              currentMarkers.markers,
              displayEqualsMarker
            );
          }
          if (!markersChanged) {
            return;
          }
          currentMarkers = currentMarkers.markers;
          //Формирование массива маркеров и обновление слоя маркеров.
          let newMarkers = [];
          for (let i = 0; i < displaysArray.length; i++) {
            const markerOptions = {
              meta: { id: displaysArray[i].id },
            };

            const marker = Leaflet.marker(
              [get(displaysArray[i], latDatafield), get(displaysArray[i], lngDatafield)],
              markerOptions
            );

            newMarkers.push(marker);
          }

          markersLayersGroup.clearLayers();
          for (let i = 0; i < newMarkers.length; i++) {
            markersLayersGroup.addLayer(newMarkers[i]);
          }
        } else {
          //Создание слоя маркеров и передача его в состояние компонента.
          const layerGroup = Leaflet.layerGroup([]).addTo(myMap);
          setMarkersLayersGroup(layerGroup);

          //Проход по массиву дисплеев, создание и последующая передача маркеров
          //в слой маркеров.
          for (let i = 0; i < displaysArray.length; i++) {
            const markerOptions = {
              meta: { id: displaysArray[i].id },
            };

            const marker = Leaflet.marker(
              [get(displaysArray[i], latDatafield), get(displaysArray[i], lngDatafield)],
              markerOptions
            );

            layerGroup.addLayer(marker);
          }
        }
      }
    }
  };

  /**
   * Обход маркеров в слое маркеров и изменение иконки в соответствии с тем какие из них выделены.
   */
  const restyleMarkers = () => {
    if (!isEmpty(markersLayersGroup)) {
      markersLayersGroup.eachLayer((layer) => {
        if (selectedMarkersIds.includes(layer.options.meta.id)) {
          layer.setIcon(iconMarkerRedoutline);
        } else {
          layer.setIcon(iconMarkerDefault);
        }
      });
    }
  };

  /**
   * Сопоставление табло с маркером
   * @param {Object} display табло
   * @param {Object} marker маркер
   */
  const displayEqualsMarker = (display, marker) => {
    const markerLatLng = marker.getLatLng();
    if (
      get(display, lngDatafield) === markerLatLng.lng &&
      get(display, latDatafield) === markerLatLng.lat &&
      display.id === marker?.options?.meta?.id
    ) {
      return true;
    } else {
      return false;
    }
  };

  /**
   * Функция запроса массива табло из источника из параметров и передача результата
   * в функцию установки отображаемых маркеров.
   */
  const setMarkersFromDataSource = useCallback(async () => {
    //Создание и хранение уникального идентификатора каждый раз при начале загрузки данных
    //Идентификатор храниться в состоянии и всегда соответствует последней запущенной загрузке
    //Используем только данные если идентификатор внутри функции совпадает с сохранённым
    const uid = uniqueId();
    dsLoadMeta.uid = uid;
    let result = await dataSource.reload().catch((error) => {
      if (error !== "canceled") {
        onError(error);
      }
    }); //TODO: потом перепроверить на предмет возможных ошибок при использовании array
    if (dsLoadMeta.uid !== uid) {
      return;
    }

    if (result === "canceled" && dataSource.isLoaded()) {
      result = await dataSource.items();
    } else if (result === "canceled" && !dataSource.isLoaded()) {
      result = await dataSource.reload().catch((error) => {
        if (error !== "canceled") {
          onError(error);
        }
      });
    }

    if (result !== "canceled") {
      setMarkersFromDisplaysArray({ displaysArray: result });
    }
  });

  /**
   * Обновление карты.
   */
  const updateMap = useCallback(async () => {
    if (mapRef.current) {
      if (!isEmpty(myMap)) {
        //Обновить маркеры
        if (!isEmpty(dataSource)) {
          if (Array.isArray(dataSource)) {
            setMarkersFromDisplaysArray({ items: dataSource });
          } else {
            await setMarkersFromDataSource();
          }
        }
        //Обновить иконки маркеров
        restyleMarkers();
      }
    }
  });

  //Инициализация компонента
  useEffect(() => {
    if (mapRef.current) {
      if (isEmpty(myMap)) {
        let map = Leaflet.map(mapRef.current, {
          center: {
            lat: startingCenter[latDatafield],
            lng: startingCenter[lngDatafield],
          },
          zoom: 13,
          maxZoom: 18,
          attributionControl: true,
          zoomControl: true,
          doubleClickZoom: true,
          scrollWheelZoom: true,
          fadeAnimation: true,
          dragging: true,
          animate: true,
          easeLinearity: 0.35,
        });

        Leaflet.tileLayer("//maps.bus62.ru/getTile.php?z={z}&x={x}&y={y}&e=@2x.png", {
          attribution:
            '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>',
        }).addTo(map);

        setTimeout(() => {
          map.invalidateSize();
        });

        let lo = Leaflet.lasso(map);

        map.on("lasso.finished", (event) => {
          onLassoGrab(event);
        });

        setMyMap(map);
        setLasso(lo);
      }
    }
    return () => {
      if (dsLoadMeta.uid) {
        dataSource.cancelAll();
      }
    };
  }, []);

  useEffect(() => updateMap());

  /**
   * Устанавливает захваченные маркеры
   * @param {Object} e
   */
  const onLassoGrab = (e) => {
    //Нам достаточно только id табло из которых созданы маркеры. Их массив формируем.
    let selectedItems = e.layers.map((item) => {
      return item.options.meta.id;
    });
    setSelectedMarkers(selectedItems);
  };

  /**
   * Установка массива выделенных маркеров.
   * @param {Array} selectedItems
   */
  const setSelectedMarkers = (selectedItems) => {
    onMarkersSelected({ selectedMarkersIds: selectedItems });
  };

  return (
    <div className={"map-editor " + className}>
      <div className={"map-controls-row"}>
        <Button
          text="Лассо"
          onClick={() => {
            lasso.enable();
          }}
          className="map-button"
        />
        <Button
          text="X"
          onClick={() => {
            lasso.disable();
            setSelectedMarkers([]);
          }}
          className="map-button"
        />
      </div>
      <div
        ref={mapRef}
        className="map-container"
        style={{
          height: "100%",
          width: "100%",
          minHeight: "260px",
          minWidth: "260px",
        }}
      ></div>
    </div>
  );
};

export { SeMapSelectPoint, SeMapAreaSelect };
