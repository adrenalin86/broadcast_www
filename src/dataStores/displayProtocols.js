import CustomStore from "devextreme/data/custom_store";

import store from "../store";

import { getBackendUrl, getToken, getRegion } from "../store/commonSlice";

import {
  handleResponseErrors,
  handleDataErrors,
  handleCaughtErrors,
} from "./utils";

const getDataStore = ({ windowId }) => {
  const restArgs = {
    w: windowId,
    ct: 24,
  };

  return new CustomStore({
    key: "id",
    load: (data) => {
      let body = {
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        ...restArgs,
        cd: "loadDisplayProtocols",
        data: data,
      };

      return fetch(getBackendUrl(store.getState())+"?"+body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .then((data) => {
          return {
            data: data.data.entities,
            summary: data.data.summary,
            totalCount: data.data.totalCount,
          };
        })
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },
    update: (key, values) => {
      let body = {
        ...restArgs,
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        cd: "updateDisplayProtocol",
        data: {
          id: key,
          values: values,
        },
      };

      return fetch(getBackendUrl(store.getState())+"?"+body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },
    byKey: (key) => {
      let body = {
        ...restArgs,
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        cd: "loadDisplayProtocols",
        data: { filter: ["name", "=", key] },
      };

      return fetch(getBackendUrl(store.getState())+"?"+body.cd+"byKey", {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .then((data) => {
          return data.data.entities;
        })
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },
  });
};

export default getDataStore;
