<?php
include 'conf.php';

$curl = curl_init();

$isPost = $_SERVER['REQUEST_METHOD'] == 'POST';
if($isPost){
$json = file_get_contents("php://input");
curl_setopt($curl, CURLOPT_URL, $apiUrl);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
} else {
curl_setopt($curl, CURLOPT_URL, $apiUrl.'/?'.$_SERVER['QUERY_STRING']);
}

curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $connectionTimeout);
curl_setopt($curl, CURLOPT_TIMEOUT, $connectionTimeout);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

$result = curl_exec($curl);

if ($result === false) {
    $result = "Ошибка curl: ".curl_error($curl);

} 

$contentType = curl_getinfo($curl, CURLINFO_CONTENT_TYPE);
header('Content-Type: '.$contentType);

curl_close($curl);
echo $result;


?>