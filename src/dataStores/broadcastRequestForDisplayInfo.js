import CustomStore from "devextreme/data/custom_store";

import store from "../store";
import { isArray } from "lodash-es";

import { getBackendUrl, getToken, getRegion } from "../store/commonSlice";

import {
  handleResponseErrors,
  handleDataErrors,
  handleCaughtErrors,
} from "./utils";

const getDataStore = ({ windowId }) => {
  const restArgs = {
    ct: 24,
    w: windowId,
  };

  let _fixedFilter = [];

  return new (CustomStore.inherit({
    fixedFilter: function (filter) {
      /* Общи this тут не наблюдается */
      if (isArray(filter)) {
        _fixedFilter = filter;
      } else {
        return _fixedFilter;
      }
    },
  }))({
    key: "id",
    load: (data) => {
      let filter;
      const fixedFilter = _fixedFilter;
      if (fixedFilter) {
        filter = data?.filter ? [data.filter, "and", fixedFilter] : fixedFilter;
      } else {
        filter = data?.filter;
      }
      let body = {
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        ...restArgs,
        cd: "loadBroadcastRequestForDisplayInfo",
        data: data ? { ...data, filter: filter } : { filter },
      };

      return fetch(getBackendUrl(store.getState()) + "?" + body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .then((data) => {
          return {
            data: data.data.entities,
            summary: data.data.summary,
            totalCount: data.data.totalCount,
          };
        })
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },
    insert: function (values) {
      let body = {
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        ...restArgs,
        cd: "insertBroadcastRequestForDisplayInfo",
        data: {
          ...values,
        },
      };

      return fetch(getBackendUrl(store.getState()) + "?" + body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },
    update: function (key, values) {
      let body = {
        ...restArgs,
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        cd: "updateBroadcastRequestForDisplayInfo",
        data: { id: key, values },
      };

      return fetch(getBackendUrl(store.getState()) + "?" + body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },
    remove: function (key) {
      let body = {
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        ...restArgs,
        cd: "removeBroadcastRequestForDisplayInfo",
        data: {
          id: key,
        },
      };

      return fetch(getBackendUrl(store.getState()) + "?" + body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },  });
};

export default getDataStore;
