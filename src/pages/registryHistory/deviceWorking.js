import React, { useState, useEffect } from "react";
import { connect } from "react-redux";

import DataGrid, {
  Column,
  StateStoring,
  Export,
  Scrolling,
  LoadPanel,
} from "devextreme-react/data-grid";
import Form, { SimpleItem } from "devextreme-react/form";
import { DateBox, SelectBox } from "devextreme-react";
import { Button } from "devextreme-react/button";
import { LoadIndicator } from "devextreme-react/load-indicator";
import { ProgressBar } from "devextreme-react/progress-bar";
import { Popup } from "devextreme-react/popup";

import ValidationSummary from "devextreme-react/validation-summary";
import ValidationEngine from "devextreme/ui/validation_engine";
import Validator, { RequiredRule } from "devextreme-react/validator";

import notify from "devextreme/ui/notify";
import { exportDataGrid } from "devextreme/excel_exporter";

import { Workbook } from "exceljs";
import saveAs from "file-saver";
import { isDate, mapValues, capitalize, isEmpty } from "lodash-es";
// import dayjs from "dayjs";

import { deauthorize } from "../../store/commonSlice";

import {
  getHistoryReportsDataStore,
  getArchiveGenericEntityDataStore,
  REGISTRY_HISTORY_ENTITIES,
  getDisplaysDataStore,
  WINDOWS,
  getSystemWindowsDataStore, getDeviceDataStore,
} from "../../dataStores";

import config from "../../config";
import dayjs from "dayjs";

// var utc = require('dayjs/plugin/utc')
// dayjs.extend(utc)

function onExporting(e) {
  const workbook = new Workbook();
  const worksheet = workbook.addWorksheet("Main sheet");

  exportDataGrid({
    component: e.component,
    worksheet: worksheet,
    autoFilterEnabled: true,
  }).then(function () {
    workbook.xlsx.writeBuffer().then(function (buffer) {
      saveAs(new Blob([buffer], { type: "application/octet-stream" }), "DataGrid.xlsx");
    });
  });
  e.cancel = true;
}

const endOfCurrentDay = new Date(
  new Date().getFullYear(),
  new Date().getMonth(),
  new Date().getDate(),
  23,
  59,
  59
);

const minDate = new Date(1900, 0, 1);

const defaultFilters = {
  fromTimeUtc: new Date(new Date().getFullYear(), new Date().getMonth()),
  toTimeUtc: endOfCurrentDay,
  deviceId: null,
};

const windowId = WINDOWS.REPORT_DEVICES.id;

const reportsDataStore = getHistoryReportsDataStore({
  windowId: windowId,
  reportName: "deviceWorkingHistory",
});

const archivedEntityDataStore = getDeviceDataStore({
  windowId: windowId,
});

const systemWindowsDataStore = getSystemWindowsDataStore({
  windowId: windowId,
});

const HistoryReport = ({ localDeauthorize }) => {
  const [entityRegistryHistoryStore, setEntityRegistryHistoryStore] = useState([]);
  const [reportBeingGenerated, setReportBeingGenerated] = useState(false);
  const [reportGenerationProgress, setReportGenerationProgress] = useState(0);
  const [reportFilters] = useState({});
  const [reportEnabled, setReportEnabled] = useState(true);
  const [windowTitle, setWindowTitle] = useState();

  let progressTimerId = null;

  const userActionsGridOptions = config.pages.reports.userActions.userActionsGrid.dxOptions;

  const REPORT_STATUS_REQUEST_TIMEOUT = config.pages.reports.common.reportStatusRequestTimeout;
  /**
   * Инициализация компонента.
   */
  useEffect(() => {
    Object.keys(defaultFilters).forEach((key) => {
      reportFilters[key] = defaultFilters[key];
    });
  }, []);

  function onDataErrorOccurred(e) {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);
    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  }

  const onFilterValueChanged = (e) => {
    const value = e.value;
    const dataField = e.component.option("dataField");
    reportFilters[dataField] = value;
  };

  const generateReport = () => {
    setReportBeingGenerated(false);
    setReportGenerationProgress(0);

    const isValid = ValidationEngine.validateGroup("displayRegistryHistoryFilter").isValid;

    if (isValid) {
      let params = reportFilters;

      params = mapValues(params, (item) => {
        if (isDate(item)) {
          return item.toISOString().split(".")[0] + "Z"; //Все даты переводим в формат нужный серверу, utc без миллисекунд.
        }
        return item;
      });

      startGeneration(params);
    }
  };

  const startGeneration = async (params) => {
    setReportEnabled(false);
    cancelReportGeneration();

    await reportsDataStore.generateReport(params).catch((error) => {
      onGenerationError();
    });

    if (reportsDataStore.getStatus() === "generating") {
      setReportBeingGenerated(true);
      setReportEnabled(true);

      //Задаём периодическую проверку статуса отчёта.
      progressTimerId = setInterval(() => {
        updateReportProgress();
      }, REPORT_STATUS_REQUEST_TIMEOUT);
    } else if (reportsDataStore.getStatus() === "error") {
      onGenerationError();
      setReportEnabled(true);
    } else {
      setReportEnabled(true);
    }
  };

  const updateReportProgress = async () => {
    const reportProgress = await reportsDataStore.getReportProgress().catch((err) => {
      onGenerationError(err);
    });

    if (reportsDataStore.getStatus() === "done") {
      setReportGenerationProgress(reportProgress.reportGenerationProgress);

      clearInterval(progressTimerId);

      const report = await reportsDataStore.getReport().catch((err) => {
        onGenerationError(err);
      });
      //Если отчёт сформирован но данных нет выбрасываем ошибку.
      if (!report?.data) {
        onGenerationError();
        setReportBeingGenerated(false);
      } else {
        setEntityRegistryHistoryStore(report.data);
        setReportBeingGenerated(false);
      }
    } else if (reportsDataStore.getStatus() === "error") {
      clearInterval(progressTimerId);
      onGenerationError();
      setReportBeingGenerated(false);
    }
  };

  const cancelReportGeneration = () => {
    clearInterval(progressTimerId);
    setReportBeingGenerated(false);
    setReportGenerationProgress(0);
  };

  const onGenerationError = (error) => {
    notify("Ошибка формирования истории", "Error", config.common.errorMessageLifespan);
    if (error) {
      onDataErrorOccurred({ error });
    }
  };

  useEffect(() => {
    systemWindowsDataStore.byKey(windowId).then((result) => {
      const windowName = result[0]?.name?.split("/").pop();

      setWindowTitle(windowName);
    });
  }, []);

  return (
    <React.Fragment>
      {reportBeingGenerated && (
        <Popup
          visible={true}
          closeOnOutsideClick={false}
          showTitle={false}
          showCloseButton={false}
          width={300}
          height={240}
          position={{
            my: "center",
            at: "center",
            of: "#userActionsPageContent",
          }}
        >
          <div className="progressPopup">
            <div className="progressPopup-title">История формируется</div>
            <LoadIndicator />
            <ProgressBar
              className="progressBar"
              value={reportGenerationProgress}
              statusFormat={(ratio, value) => {
                return "Прогресс: " + Math.round(ratio * 100) + "%";
              }}
            />
            <Button text="Отменить" onClick={cancelReportGeneration} />
          </div>
        </Popup>
      )}
      <h2 className="content-block">{windowTitle && `История ${windowTitle.toLowerCase()}`}</h2>
      <div className="content-block">
        <div id="userActionsPageContent" className="dx-card responsive-paddings">
          <div className="filters-container">
            <Form formData={defaultFilters} validationGroup="displayRegistryHistoryFilter">
              <SimpleItem>
                <div className="filters-row">
                  <div className="filter-item">
                    <div className="filter-label">Период истории</div>
                    <div className="filter-values">
                      <DateBox
                        type="datetime"
                        min={minDate}
                        dataField="fromTimeUtc"
                        defaultValue={defaultFilters.fromTimeUtc}
                        showClearButton={true}
                        onValueChanged={onFilterValueChanged}
                      />
                      <DateBox
                        type="datetime"
                        min={reportFilters.fromTimeUtc}
                        dataField="toTimeUtc"
                        defaultValue={defaultFilters.toTimeUtc}
                        showClearButton={true}
                        onValueChanged={onFilterValueChanged}
                      />
                    </div>
                  </div>
                  <div className="filter-item">
                    <div className="filter-label">{capitalize("Оборудование")}</div>
                    <div className="filter-values">
                      <SelectBox
                        dataField="deviceId"
                        dataSource={{
                          store: archivedEntityDataStore,
                          onLoadError: (error) => {
                            onDataErrorOccurred({ error });
                          },
                        }}
                        valueExpr="id"
                        displayExpr={(e) => {
                          if (e) {
                            return `${e?.name}(id=${e.id})`;
                          } else {
                            return "";
                          }
                        }}
                        showClearButton={true}
                        searchEnabled={true}
                        multiline={false}
                        onValueChanged={onFilterValueChanged}
                      >
                        <Validator validationGroup="displayRegistryHistoryFilter">
                          <RequiredRule />
                        </Validator>
                      </SelectBox>
                    </div>
                  </div>
                  <div className="filter-item generateReport">
                    <Button
                      className="generateReport-button"
                      onClick={generateReport}
                      text="Получить историю"
                      stylingMode="contained"
                      type="default"
                      disabled={!reportEnabled}
                    />
                  </div>
                </div>
              </SimpleItem>
              <ValidationSummary
                id="summary"
                validationGroup="displayRegistryHistoryFilter"
              ></ValidationSummary>
            </Form>
          </div>
          <DataGrid
            id="userActionsDataGrid"
            dataSource={entityRegistryHistoryStore}
            onDataErrorOccurred={onDataErrorOccurred}
            onExporting={onExporting}
            errorRowEnabled={false}
            onContentReady={(e) => {
              //Для каждой колонки форсированно задаём порядок отрисовки в таблице.
              e.component.state().columns.forEach((col, index) => {
                e.component.columnOption(col.name, "visibleIndex", index);
              });
            }}
            {...userActionsGridOptions}
          >
            <StateStoring
              enabled={true}
              type="localStorage"
              storageKey="pages.reports.userActions.userActionsGrid"
            />
            <LoadPanel enabled={true} />
            <Scrolling mode="standart" />
            <Export enabled={true} allowExportSelectedData={true} />
            <Column dataField={"time"} caption={"Время"} dataType="datetime"
              cellReneder={(cell) => {
                const time = cell?.row?.data["time"];
                let formatedTime = "";
                if (time) {
                  formatedTime = dayjs.utc(time).local().format("DD-MM-YYYY HH:mm");
                }
                return <div>{time && formatedTime}</div>
              }} />
            <Column dataField={"actionName"} caption={"Действие"} dataType={"string"} />
          </DataGrid>
        </div>
      </div>
    </React.Fragment>
  );
};

export default connect(
  (state) => ({}),
  (dispatch) => {
    return { localDeauthorize: () => dispatch(deauthorize()) };
  }
)(HistoryReport);
