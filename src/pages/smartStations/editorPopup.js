import React, {useState, useEffect} from "react";
import {Popup, Position, ToolbarItem} from "devextreme-react/popup";
import {ScrollView} from "devextreme-react/scroll-view";
import Form, {SimpleItem} from "devextreme-react/form";
import {TextBox, TextArea, TagBox, SelectBox} from "devextreme-react";
import ValidationSummary from "devextreme-react/validation-summary";
import ValidationEngine from "devextreme/ui/validation_engine";
import Validator, {RequiredRule} from "devextreme-react/validator";
import {set, cloneDeep, isEmpty} from "lodash-es";
import {SeMapSelectPoint} from "../../components/editors";
import {
    getDeviceDataStore,
    getDisplaysDataStore, getSmartStationsAddressDataStore, getTagsToDisplayDataStore,
    getVideoCamerasWithStatusesDataStore,
    WINDOWS
} from "../../dataStores";
import config from "../../config";
import useReload from "../../hooks/useReload";
import {Button} from "devextreme-react/button";
import TextBoxMemo from "../../components/forms/TextBoxMemo";
import {TagBoxMemo, TextAreaMemo} from "../../components";
import SelectBoxMemo from "../../components/forms/SelectBoxMemo";
import notify from "devextreme/ui/notify";

const windowId = WINDOWS.SMART_STATIONS.id;
const videoCamerasWithStatusesDataStore = getVideoCamerasWithStatusesDataStore({windowId: windowId});
const displaysDataStore = getDisplaysDataStore({windowId: windowId});
const devicesDataStore = getDeviceDataStore({windowId: windowId});


const tagsDataStore = getTagsToDisplayDataStore({
    windowId: windowId,
});

const smartStationsAddressDataStore = getSmartStationsAddressDataStore({
    windowId: windowId,
});

const PopupMemo = React.memo(({closePopup, editedRow, closeEditorPopup, updateEntry, createEntry}) => {
        const popupOptions = config.pages.smartStations.grid.editorPopup.dxOptions;

        const [toolbarItems, setToolbarItems] = React.useState([])

        return <Popup
            className="customEditorPopup"
            toolbarItems={toolbarItems}
            visible={true}
            onHiding={closePopup}
            titleComponent={() => {
                return <span className="popup-title">{popupOptions.title}</span>;
            }}
            {...popupOptions}
        >
            <Position my="center" at="center" of={window}/>
            <EditorPopupSmartStations toolbarItems={toolbarItems} setToolbarItems={setToolbarItems}
                                      closeEditorPopup={closeEditorPopup} editedRow={editedRow} createEntry={createEntry}
                                      updateEntry={updateEntry}/>
        </Popup>
    }
)


const EditorPopupSmartStations = ({
                                      editedRow,
                                      closeEditorPopup,
                                      setToolbarItems,
                                      updateEntry,
                                      createEntry
                                  }) => {
    const [formData, setFormData] = useState({});

    const {
        reload,
        reloadValue
    } = useReload();

    const [changedFormData, setFormDataChanged] = useState({});

    const startingCenter = config.pages.smartStations.map.startingCenter;

    const closePopup = () => {
        closeEditorPopup();
    };

    React.useEffect(() => {
        if (isEmpty(editedRow)) {
            setToolbarItems([
                {
                    widget: "dxButton",
                    location: "after",
                    toolbar: "bottom",
                    options: {text: "Сохранить", onClick: onSave}
                },
                {
                    widget: "dxButton",
                    location: "after",
                    toolbar: "bottom",
                    options: {text: "Отмена", onClick: onCancel}
                }
            ])
        }
    }, [formData])

    React.useEffect(() => {
        if (!isEmpty(editedRow)) {
            setToolbarItems([
                {
                    widget: "dxButton",
                    location: "after",
                    toolbar: "bottom",
                    options: {text: "Сохранить", onClick: onSave}
                },
                {
                    widget: "dxButton",
                    location: "after",
                    toolbar: "bottom",
                    options: {text: "Отмена", onClick: onCancel}
                }
            ])
        }
    }, [changedFormData])


    const onSave = () => {
        const isValid = ValidationEngine.validateGroup("formEditor").isValid;
        if (isValid) {
            if (isEmpty(editedRow)) {
                createEntry({
                    formData: formData,
                });
            } else {
                updateEntry({
                    formData: changedFormData,
                    editedRowId: editedRow.id,
                });
            }
        }
    };

    const onCancel = () => {
        closePopup();
    };

    const onFormItemValueChanged = (e, needReload) => {
        const value = e.value !== undefined ? e.value : e?.component?.option("text");
        const dataField = e.component.option("dataField");
        if (dataField) {
            setFormDataValue(dataField, value);
        }
        if (needReload) {
            reload()
        }
    };

    const handleChangeFormData = React.useCallback(
        (objectKey, value) => {
            setFormData(prev => ({...prev, [objectKey]: value}));
        },
        [formData]
    );

    const handleChangeFormDataChanged = React.useCallback(
        (objectKey, value) => {
            setFormDataChanged(prev => ({...prev, [objectKey]: value}));
        },
        [changedFormData]
    );


    const setFormDataValue = (dataField, value) => {
        handleChangeFormData(dataField, value)
        handleChangeFormDataChanged(dataField, value)
    };

    useEffect(() => {
        if (!isEmpty(editedRow)) {
            setFormData(cloneDeep(editedRow));
        }
    }, []);

    const handleCheckCoords = () => {
        smartStationsAddressDataStore.load({lat: formData?.lat, lng: formData?.lng}).then(data => {
            if (!data?.city  && !data?.road && !data?.village) {
                return setFormDataValue('address', '')
            }
            const address = `${data?.city ? data?.city : data?.village ? data?.village : ''}, ${data?.road || ''}, ${data?.houseNumber || ''}`
            setFormDataValue('address', address)
        }).catch(() => {
            return notify('Ошибка при проверки координат', "error", config.common.errorMessageLifespan)
        })
        // setFormDataValue('address', "Тестовый адрес")
    }

    const CameraTagBox = React.useMemo(() => <TagBox
        dataField="videoCameraIds"
        defaultValue={formData.videoCameraIds}
        dataSource={videoCamerasWithStatusesDataStore}
        searchEnabled={true}
        valueExpr="id"
        displayExpr="name"
        showClearButton={true}
        onValueChanged={onFormItemValueChanged}
    />, [formData?.videoCameraIds])

    const DisplayTagBox = React.useMemo(() => <TagBox
        defaultValue={formData.displayIds}
        onValueChanged={onFormItemValueChanged}
        dataSource={{
            store: displaysDataStore,
        }}
        valueExpr={"id"}
        displayExpr={"name"}
        showClearButton={true}
        dataField="displayIds"
        searchEnabled={true}
    />, [formData?.displayIds])

    const DeviceTagBox = React.useMemo(() => <TagBox
        defaultValue={formData.deviceIds}
        onValueChanged={onFormItemValueChanged}
        dataSource={{
            store: devicesDataStore,
        }}
        valueExpr={"id"}
        displayExpr={"name"}
        showClearButton={true}
        dataField="deviceIds"
        searchEnabled={true}
    />, [formData?.deviceIds])

    const TagsTagBox = React.useMemo(() => <TagBox
        dataField="tagIds"
        defaultValue={formData.tagIds}
        dataSource={tagsDataStore}
        valueExpr="id"
        displayExpr="name"
        searchEnabled={true}
        searchTimeout={2000}
        showClearButton={true}
        onValueChanged={onFormItemValueChanged}
    />, [formData?.deviceIds])


    const AddressTextBox = React.useMemo(() => <TextBox
        defaultValue={formData.address}
        value={formData.address}
        onInput={onFormItemValueChanged}
        dataField={"address"}
    />, [formData?.address])

    const ModelTextBox = React.useMemo(() => <TextBox
        defaultValue={formData?.model}
        value={formData?.model}
        onInput={onFormItemValueChanged}
        dataField={"model"}
    />, [formData?.model])

    const LatTextBox = React.useMemo(() => <TextBox
        defaultValue={startingCenter.lat}
        value={formData.lat}
        onInput={e => onFormItemValueChanged(e, true)}
        placeholder="Шитора"
        dataField={"lat"}
        ty
    >
        <Validator validationGroup="formEditor">
            <RequiredRule/>
        </Validator>
    </TextBox>, [formData?.lat, startingCenter.lat])


    const LngTextBox = React.useMemo(() => <TextBox
        defaultValue={startingCenter.lng}
        value={formData.lng}
        onInput={e => onFormItemValueChanged(e, true)}
        placeholder="Долгота"
        dataField={"lng"}
    >
        <Validator validationGroup="formEditor">
            <RequiredRule/>
        </Validator>
    </TextBox>, [formData.lng, startingCenter.lng])


    const Map = React.useMemo(() => <SeMapSelectPoint
        reloadValue={reloadValue}
        formData={formData}
        setFormDataValue={setFormDataValue}
        defaultLat={startingCenter.lat}
        defaultLng={startingCenter.lng}
    />, [reloadValue, formData?.lat, formData?.lng, startingCenter.lat, startingCenter.lng])


    return (
        <React.Fragment>
            <ScrollView showScrollbar="always">
                <Form formData={formData} validationGroup="formEditor">
                    <SimpleItem>
                        <div className="editor-content">
                            <div className="dx-form-group-with-caption dx-form-group editor-group">
                                <div className="dx-form-group-caption">Общие сведения</div>
                                <div className="dx-form-group-content editor-content">
                                    <div className="editor-item">
                                        <div className="dx-field">
                                            <div className="dx-field-label">Название</div>
                                            <div className="dx-field-value">
                                                <TextBoxMemo value={formData.name} name="name"
                                                             onChange={onFormItemValueChanged} required/>
                                            </div>
                                        </div>
                                        <div className="dx-field">
                                            <div className="dx-field-label">Описание</div>
                                            <div className="dx-field-value">
                                                <TextAreaMemo value={formData.description} name="description"
                                                              onChange={onFormItemValueChanged} required/>
                                            </div>
                                        </div>
                                        <div className="dx-field">
                                            <div className="dx-field-label item-label ">Основная видеокамера</div>
                                            <div className="dx-field-value ">
                                                <SelectBoxMemo dataSource={videoCamerasWithStatusesDataStore}
                                                               value={formData.mainVideoCameraId}
                                                               name="mainVideoCameraId"
                                                               onChange={onFormItemValueChanged}/>
                                            </div>
                                        </div>
                                        <div className="dx-field">
                                            <div className="dx-field-label item-label">
                                                Дополнительные видеокамеры
                                            </div>
                                            <div className="dx-field-value">
                                                {CameraTagBox}
                                            </div>
                                        </div>
                                        <div className="dx-field">
                                            <div className="dx-field-label">Табло</div>
                                            <div className="dx-field-value">
                                                {DisplayTagBox}
                                            </div>
                                        </div>
                                        <div className="dx-field">
                                            <div className="dx-field-label">Периферийные устройства</div>
                                            <div className="dx-field-value">
                                                {DeviceTagBox}
                                            </div>
                                        </div>
                                        <div className="dx-field">
                                            <div className="dx-field-label">Тэги</div>
                                            <div className="dx-field-value">
                                                {TagsTagBox}
                                            </div>
                                        </div>
                                        <div className="dx-field">
                                            <div className="dx-field-label">Модель</div>
                                            <div className="dx-field-value">
                                                {ModelTextBox}
                                            </div>
                                        </div>
                                        <div className="dx-field">
                                            <div className="dx-field-label">Адрес</div>
                                            <div className="dx-field-value-two">
                                                {AddressTextBox}
                                                <Button onClick={handleCheckCoords} type="success"
                                                        text="Подставить адрес"/>
                                            </div>
                                        </div>
                                        <div className="dx-field">
                                            <div className="dx-field-label">Положение на карте</div>
                                            <div className="dx-field-value">
                                                {Map}
                                            </div>
                                        </div>
                                        <div className="dx-field">
                                            <div className="dx-field-label">Координаты</div>
                                            <div className="dx-field-value-two">
                                                {LatTextBox}
                                                {LngTextBox}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </SimpleItem>
                </Form>
            </ScrollView>
            <ValidationSummary id="summary" validationGroup="formEditor"></ValidationSummary>
        </React.Fragment>
    );
};

export default PopupMemo;
