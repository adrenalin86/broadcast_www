import React, { useEffect, useState } from "react";

import { getSystemWindowsDataStore, REGISTRY_HISTORY_ENTITIES, WINDOWS } from "../../dataStores";

import HistoryReport from "./historyReport";

const windowId = WINDOWS.REPORTS_REGISTRY_HISTORY_DISPLAYS.id;

const systemWindowsDataStore = getSystemWindowsDataStore({
  windowId: windowId,
});

const RegistryHistoryPage = () => {
  const [windowTitle, setWindowTitle] = useState();

  useEffect(() => {
    systemWindowsDataStore.byKey(windowId).then((result) => {
      const windowName = result[0]?.name?.split("/").pop();

      setWindowTitle(windowName);
    });
  }, []);

  return (
    <HistoryReport
      windowTitle={windowTitle}
      appliedEntity={REGISTRY_HISTORY_ENTITIES.DISPLAYS}
      windowId={windowId}
    />
  );
};

export default RegistryHistoryPage;
