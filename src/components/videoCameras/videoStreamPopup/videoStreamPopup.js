import React, { useEffect, useState } from "react";

import { LoadIndicator, Popup, Toolbar } from "devextreme-react";
import { Item } from "devextreme-react/accordion";

import {
  getVideoCamerasActualPictureDataStore,
  getVideoCamerasLastPictureDataStore,
  getVideoCamerasWithStatusesDataStore,
  WINDOWS,
} from "../../../dataStores";

const windowId = WINDOWS.VIDEOCAMERAS.id;

const videoCamerasActualPictureDataStore = getVideoCamerasActualPictureDataStore({
  windowId: windowId,
});

const videoCamerasLastPictureDataStore = getVideoCamerasLastPictureDataStore({
  windowId: windowId,
});

const videoCamerasWithStatusesDataStore = getVideoCamerasWithStatusesDataStore({
  windowId: windowId,
});

const VideoStreamPopup = ({ closeVideoStreamPopup, onDataErrorOccurred, videoCameraId }) => {
  const [img, setImg] = useState();
  const [isLoadedImg, setIsLoadedImg] = useState(false);
  const [isLoadedImgError, setIsLoadedImgError] = useState();
  const [videoCameraName, setVideoCameraName] = useState();
  const [counter, setCounter] = useState(0);

  const updateImg = () => {
    setCounter(counter + 1);
    setIsLoadedImg(false);
    setIsLoadedImgError(false);
  };

  useEffect(() => {
    if (counter === 0) {
      videoCamerasWithStatusesDataStore
        .byKey(videoCameraId)
        .then((result) => {
          const [videoCamera] = result;

          if (videoCamera) {
            setVideoCameraName(videoCamera.name);
          }
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
        });

      videoCamerasLastPictureDataStore
        .load({
          videoCameraId: videoCameraId,
          resizeToHeight: 450,
        })
        .then((result) => {
          if (result.type === "application/json") {
            setIsLoadedImgError(true);
          } else {
            let imgUrl = URL.createObjectURL(result);

            setImg(imgUrl);
          }
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
        });
    } else {
      videoCamerasActualPictureDataStore
        .load({
          videoCameraId: videoCameraId,
          resizeToHeight: 450,
        })
        .then((result) => {
          if (result.type === "application/json") {
            setIsLoadedImgError(true);
          } else {
            let imgUrl = URL.createObjectURL(result);

            setImg(imgUrl);
          }
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
        });
    }
  }, [counter]);

  return (
    <Popup
      className="customEditorPopup"
      showCloseButton={true}
      visible={true}
      onHiding={closeVideoStreamPopup}
      title={videoCameraName}
    >
      <Toolbar style={{ background: "transparent", marginBottom: "10px" }}>
        <Item
          disabled={!isLoadedImgError && !isLoadedImg}
          location="after"
          widget="dxButton"
          options={{ icon: "refresh", text: "Обновить", onClick: updateImg }}
        />
      </Toolbar>
      {isLoadedImgError && (
        <div
          style={{
            fontSize: "20px",
            fontWeight: "400",
            height: "70%",
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
          }}
        >
          Не удалось получить изображение
        </div>
      )}
      {!isLoadedImg && (
        <div
          style={
            isLoadedImgError
              ? { display: "none" }
              : { display: "flex", justifyContent: "center", alignItems: "center", height: "75%" }
          }
        >
          <LoadIndicator
            id="large-indicator"
            className="button-indicator"
            height={60}
            width={60}
            visible={!isLoadedImg}
          />
        </div>
      )}
      <img
        style={isLoadedImg ? { height: "92%", objectFit: "fill" } : { display: "none" }}
        src={img}
        alt="videoCamera"
        onLoad={() => setIsLoadedImg(true)}
      />
    </Popup>
  );
};

export default VideoStreamPopup;
