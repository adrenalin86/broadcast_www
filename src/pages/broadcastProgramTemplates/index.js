import React, { useState, useRef, useEffect } from "react";
import { connect } from "react-redux";

import DataGrid, {
  Column,
  StateStoring,
  Button,
  Export,
  ColumnChooser,
} from "devextreme-react/data-grid";
import "devextreme-react/text-area";
import { exportDataGrid } from "devextreme/excel_exporter";
import notify from "devextreme/ui/notify";
import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import DataSource from "devextreme/data/data_source";

import { Workbook } from "exceljs";
import saveAs from "file-saver";
import { isEmpty } from "lodash-es";

import { deauthorize, getLocale } from "../../store/commonSlice";

import {
  getBroadcastProgramTemplatesDataStore,
  getSystemWindowsDataStore,
  WINDOWS,
} from "../../dataStores";

import BroadcastProgramTemplatesEditorPopup from "./broadcastProgramTemplatesEditorPopup";

import config from "../../config";

function onExporting(e) {
  const workbook = new Workbook();
  const worksheet = workbook.addWorksheet("Main sheet");

  exportDataGrid({
    component: e.component,
    worksheet: worksheet,
    autoFilterEnabled: true,
  }).then(function () {
    workbook.xlsx.writeBuffer().then(function (buffer) {
      saveAs(new Blob([buffer], { type: "application/octet-stream" }), "DataGrid.xlsx");
    });
  });
  e.cancel = true;
}

const windowId = WINDOWS.BROADCAST_PROGRAM_TEMPLATES.id;

const broadcastProgramTemplatesDataStore = getBroadcastProgramTemplatesDataStore({
  windowId: windowId,
});

const broadcastProgramsTemplatesDataSource = new DataSource({
  store: broadcastProgramTemplatesDataStore,
  key: "id",
  reshapeOnPush: true,
});

const systemWindowsDataStore = getSystemWindowsDataStore({
  windowId: windowId,
});

const BroadcastPrograms = ({ localDeauthorize, locale }) => {
  const [editorPopupVisible, setEditorPopupVisible] = useState(false);
  const [editedRow, setEditedRow] = useState({});
  const [deleteRowPopupVisible, setDeleteRowPopupVisible] = useState(false);
  const [deletedRowKey, setDeletedRowKey] = useState(null);
  const [windowTitle, setWindowTitle] = useState();

  const datagridRef = useRef(null);

  const broadcastProgramTemplatesGridOptions =
    config.pages.broadcastProgramTemplates.broadcastProgramTemplatesGrid.dxOptions;

  const onDataErrorOccurred = (e) => {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);
    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  };

  const onToolbarPreparing = (e) => {
    let toolbarItems = e.toolbarOptions.items;

    toolbarItems.unshift({
      widget: "dxButton",
      options: {
        icon: "add",
        onClick: openCreateEntryPopup,
      },
      location: "before",
    });
  };

  const openEditorPopup = (e) => {
    const currentRow = broadcastProgramsTemplatesDataSource
      .items()
      .find((item) => item.id === e.row.key);

    if (!isEmpty(currentRow)) {
      setEditedRow(currentRow);
      setEditorPopupVisible(true);
    }
  };

  const openCreateEntryPopup = () => {
    setEditedRow({});
    setEditorPopupVisible(true);
  };

  const openDeleteRowPopup = (e) => {
    setEditedRow({});
    setDeletedRowKey(e.row.key);
    setDeleteRowPopupVisible(true);
  };

  const closeEditorPopup = () => {
    setEditorPopupVisible(false);
  };

  const closeDeleteRowPopup = () => {
    setDeleteRowPopupVisible(false);
  };

  const createEntry = (e) => {
    broadcastProgramsTemplatesDataSource
      .store()
      .insert(e.formData)
      .then((e) => {
        closeEditorPopup();
        datagridRef?.current?.instance?.refresh();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
        closeEditorPopup();
        datagridRef?.current?.instance?.refresh();
      });
  };

  const updateEntry = (e) => {
    if (!isEmpty(e.formData)) {
      broadcastProgramsTemplatesDataSource
        .store()
        .update(e.editedRowId, e.formData)
        .then(() => {
          closeEditorPopup();
          datagridRef?.current?.instance?.refresh();
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
          closeEditorPopup();
          datagridRef?.current?.instance?.refresh();
        });
    } else {
      closeEditorPopup();
    }
  };

  const deleteEntry = (e) => {
    broadcastProgramsTemplatesDataSource
      .store()
      .remove(deletedRowKey)
      .then(() => {
        closeDeleteRowPopup();
        datagridRef?.current?.instance?.refresh();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
        closeDeleteRowPopup();
        datagridRef?.current?.instance?.refresh();
      });
  };

  useEffect(() => {
    systemWindowsDataStore.byKey(windowId).then((result) => {
      const windowName = result[0]?.name?.split("/").pop();

      setWindowTitle(windowName);
    });
  }, []);
  return (
    <React.Fragment>
      <h2 className={"content-block"}>{windowTitle}</h2>
      {editorPopupVisible && (
        <BroadcastProgramTemplatesEditorPopup
          editedRow={editedRow}
          windowId={windowId}
          closeEditorPopup={closeEditorPopup}
          updateEntry={updateEntry}
          createEntry={createEntry}
        />
      )}
      {deleteRowPopupVisible && (
        <Popup
          className="deleteRowPopup"
          width="auto"
          height="auto"
          visible={true}
          showCloseButton={false}
          showTitle={false}
          onHiding={closeDeleteRowPopup}
        >
          <Position my="center" at="center" of={window} />
          <div>
            <span>Вы уверены, что хотите удалить эту запись?</span>
          </div>
          <ToolbarItem
            widget="dxButton"
            location="center"
            options={{ text: "Да", onClick: deleteEntry }}
            toolbar="bottom"
          />
          <ToolbarItem
            widget="dxButton"
            location="center"
            options={{ text: "Нет", onClick: closeDeleteRowPopup }}
            toolbar="bottom"
          />
        </Popup>
      )}
      <div className={"content-block"}>
        <div className={"dx-card responsive-paddings"}>
          <DataGrid
            ref={datagridRef}
            dataSource={broadcastProgramsTemplatesDataSource}
            onDataErrorOccurred={onDataErrorOccurred}
            onToolbarPreparing={onToolbarPreparing}
            onExporting={onExporting}
            errorRowEnabled={false}
            renderAsync={true}
            allowColumnReordering={false}
            onContentReady={(e) => {
              //Для каждой колонки форсированно задаём порядок отрисовки в таблице.
              e.component.state().columns.forEach((col, index) => {
                e.component.columnOption(col.name, "visibleIndex", index);
              });
            }}
            {...broadcastProgramTemplatesGridOptions}
          >
            <StateStoring
              enabled={true}
              type="localStorage"
              storageKey="pages.displayModelsEditor.displayModelsGrid"
            />
            <ColumnChooser enabled={true} />
            <Export enabled={true} allowExportSelectedData={true} />

            <Column dataField="id" caption="id" width="40px" />
            <Column dataField="name" dataType="string" caption="Название" />

            <Column name="buttons" type="buttons" width="70px">
              <Button name="custom-edit" text="Изменить" icon="edit" onClick={openEditorPopup} />
              <Button
                name="custom-delete"
                text="Удалить"
                icon="trash"
                onClick={openDeleteRowPopup}
              />
            </Column>
          </DataGrid>
        </div>
      </div>
    </React.Fragment>
  );
};

export default connect(
  (state) => ({
    locale: getLocale(state),
  }),
  (dispatch) => {
    return { localDeauthorize: () => dispatch(deauthorize()) };
  }
)(BroadcastPrograms);
