import React from 'react';
import Validator, {RequiredRule} from "devextreme-react/validator";
import {SelectBox, TextBox} from "devextreme-react";

const SelectBoxMemo = React.memo(({value, onChange, name, dataSource, valueExpr = 'id', displayExpr = 'name'}) => {
        return (
            <SelectBox
                dataField={name}
                defaultValue={value}
                dataSource={dataSource}
                valueExpr={valueExpr}
                displayExpr={displayExpr}
                allowClearing={true}
                searchEnabled={true}
                showClearButton={true}
                onValueChanged={onChange}
            />
        );
    }
)

export default SelectBoxMemo;