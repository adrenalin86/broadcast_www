import { useEffect, useRef, useState } from "react";
import { connect } from "react-redux";
import notify from "devextreme/ui/notify";
import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import { isEmpty } from "lodash-es";
import config from "../../config";
import { deauthorize } from "../../store/commonSlice";
import SpecTransportTypesTable from "./specTransportTypesTable";
import SpecTransportTypesEditorPopup from "./specTransportTypesEditorPopup";
import {
  getSpecTransportTypesDataStore,
  getSystemWindowsDataStore,
  WINDOWS,
} from "../../dataStores";

const windowId = WINDOWS.SPEC_TRANSPORT_TYPES.id;

const specTransportTypesDataStore = getSpecTransportTypesDataStore({
  windowId: windowId,
});

const systemWindowsDataStore = getSystemWindowsDataStore({
  windowId: windowId,
});

const SpecTransportTypesPage = ({ localDeauthorize }) => {
  const [specTransportTypeId, setSpecTransportTypeId] = useState();
  const [deleteRowPopupVisible, setDeleteRowPopupVisible] = useState(false);
  const [editorPopupVisible, setEditorPopupVisible] = useState(false);
  const [windowTitle, setWindowTitle] = useState();

  const datagridRef = useRef(null);

  const onDataErrorOccurred = (e) => {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);

    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  };

  const openEditorPopup = (id) => {
    setSpecTransportTypeId(id);
    setEditorPopupVisible(true);
  };

  const openCreateEntryPopup = () => {
    setSpecTransportTypeId(null);
    setEditorPopupVisible(true);
  };

  const closeEditorPopup = () => {
    setEditorPopupVisible(false);
  };

  const openDeleteRowPopup = (id) => {
    setSpecTransportTypeId(id);
    setDeleteRowPopupVisible(true);
  };

  const closeDeleteRowPopup = () => {
    setDeleteRowPopupVisible(false);
  };

  const createEntry = (formData) => {
    specTransportTypesDataStore
      .insert(formData)
      .then(() => {
        closeEditorPopup();
        datagridRef?.current?.instance?.refresh();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
      });
  };

  const updateEntry = ({ formData, editedRowId }) => {
    if (!isEmpty(formData)) {
      specTransportTypesDataStore
        .update(editedRowId, formData)
        .then(() => {
          closeEditorPopup();
          datagridRef?.current?.instance?.refresh();
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
        });
    } else {
      closeEditorPopup();
    }
  };

  const deleteEntry = () => {
    specTransportTypesDataStore
      .remove(specTransportTypeId)
      .then(() => {
        closeDeleteRowPopup();
        datagridRef?.current?.instance?.refresh();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
      });
  };

  useEffect(() => {
    systemWindowsDataStore.byKey(windowId).then((result) => {
      const windowName = result[0]?.name?.split("/").pop();

      setWindowTitle(windowName);
    });
  }, []);

  return (
    <div className="content-block">
      <h2>{windowTitle}</h2>
      {editorPopupVisible && (
        <SpecTransportTypesEditorPopup
          specTransportTypeId={specTransportTypeId}
          updateEntry={updateEntry}
          createEntry={createEntry}
          closeEditorPopup={closeEditorPopup}
          onDataErrorOccurred={onDataErrorOccurred}
        />
      )}
      {deleteRowPopupVisible && (
        <Popup
          className="deleteRowPopup"
          width="auto"
          height="auto"
          visible={true}
          showCloseButton={false}
          showTitle={false}
          onHiding={closeDeleteRowPopup}
        >
          <Position my="center" at="center" of={window} />
          <div>
            <span>Вы уверены, что хотите удалить эту запись?</span>
          </div>
          <ToolbarItem
            widget="dxButton"
            location="center"
            toolbar="bottom"
            options={{ text: "Да", onClick: deleteEntry }}
          />
          <ToolbarItem
            widget="dxButton"
            location="center"
            toolbar="bottom"
            options={{ text: "Нет", onClick: closeDeleteRowPopup }}
          />
        </Popup>
      )}
      <SpecTransportTypesTable
        datagridRef={datagridRef}
        openDeleteRowPopup={openDeleteRowPopup}
        openEditorPopup={openEditorPopup}
        openCreateEntryPopup={openCreateEntryPopup}
        onDataErrorOccurred={onDataErrorOccurred}
      />
    </div>
  );
};

export default connect(
  (state) => ({}),
  (dispatch) => {
    return { localDeauthorize: () => dispatch(deauthorize()) };
  }
)(SpecTransportTypesPage);
