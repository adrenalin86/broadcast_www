import React, { useEffect, useRef, useCallback, useState } from "react";
import TreeView from "devextreme-react/tree-view";
import notify from "devextreme/ui/notify";
import * as events from "devextreme/events";
import { cloneDeep } from "lodash-es";
import routes from "../../app-routes";
import { useNavigation } from "../../contexts/navigation";
import { traverse, useScreenSize } from "../../utils";
import { ACCESS_TYPES, getSystemWindowsDataStore } from "../../dataStores";
import config from "../../config";
import "./side-navigation-menu.scss";

const systemWindowsDataStore = getSystemWindowsDataStore();

const filterNavigation = (unfilteredNavigation, windowsRights) => {
  let filteredNavigation = cloneDeep(unfilteredNavigation);
  let routesAccess = {};

  routes.forEach((item) => {
    routesAccess[item.path] = { access: item.access };
  });

  let folders = [];

  traverse(filteredNavigation, (key, value, path, parent) => {
    if (key === "path") {
      if (routesAccess[value]) {
        let hasRight = windowsRights.some((item) => {
          return (
            item.entityId === routesAccess[value].access.id &&
            item.accessType !== 0 &&
            routesAccess[value].access.type === ACCESS_TYPES.WINDOWS
          );
        });

        if (!hasRight) {
          parent.visible = false;
        }
      } else {
        parent.visible = false;
      }
    }

    if (key === "items") {
      folders.push({ parent: parent, depth: path.length });
    }
  });

  folders.sort((itemA, itemB) => {
    return -itemA.depth + itemB.depth;
  });

  folders.forEach((item) => {
    if (item.parent.items) {
      const childrenHidden = !item.parent.items.some((child) => {
        if (child.visible === false) {
          return false;
        } else {
          return true;
        }
      });

      if (childrenHidden) {
        item.parent.visible = false;
      }
    }
  });

  return filteredNavigation;
};

export default function ({
  children,
  selectedItemChanged,
  openMenu,
  compactMode,
  onMenuReady,
  windowsRights,
}) {
  const [navigation, setNavigation] = useState([]);

  const { isLarge } = useScreenSize();


  const onDataErrorOccurred = (e) => {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);
  };

  function normalizePath(navigation) {
    let filteredNavigation = filterNavigation(navigation, windowsRights);

    return filteredNavigation.map((item) => {
      if (item.path && !/^\//.test(item.path)) {
        item.path = `/${item.path}`;
      }

      return { ...item, expanded: isLarge };
    });
  }

  const {
    navigationData: { currentPath },
  } = useNavigation();

  const items = normalizePath(navigation);


  const treeViewRef = useRef();
  const wrapperRef = useRef();

  const getWrapperRef = useCallback(
    (element) => {
      const prevElement = wrapperRef.current;

      if (prevElement) {
        events.off(prevElement, "dxclick");
      }

      wrapperRef.current = element;

      events.on(element, "dxclick", (e) => {
        openMenu(e);
      });
    },
    [openMenu]
  );

  useEffect(() => {
    const treeView = treeViewRef.current && treeViewRef.current.instance;

    if (!treeView) {
      return;
    }

    if (currentPath !== undefined) {
      treeView.selectItem(currentPath);
      treeView.expandItem(currentPath);
    }

    if (currentPath === undefined || currentPath === "/") {
      treeView.unselectAll();
    }

    if (compactMode) {
      treeView.collapseAll();
    }
  }, [currentPath, compactMode]);

  useEffect(() => {
    systemWindowsDataStore
      .load()
      .then(({ data }) => {
        const treeWindowsWithOneName = {};
        const treeWindowsWithSomeNames = {};
        let windowsWithOneName = data.filter((window) => window.name.split("/").length === 1);
        let windowsWithSomeNames = data.filter((window) => window.name.split("/").length > 1);

        let navigationMenu = [];

        function buildingTree(windowsArray, tree) {
          for (const element of windowsArray) {
            const { icon, url } = element;
            const names = element.name.split("/");
            let prev = tree;

            names.forEach((name, index) => {
              const isLast = index === names.length - 1;

              if (prev[name]) {
                prev = prev[name].items;

                return;
              } else {
                prev[name] = { text: name, items: {} };

                if (isLast) {
                  prev[name].icon = icon;
                  prev[name].path = url;
                  delete prev[name].items;
                }

                prev = prev[name].items;
              }
            });
          }
        }

        function treeWalker(tree, resultArray) {
          for (const [key, value] of Object.entries(tree)) {
            let { text, items, icon, path } = value;

            if (path !== undefined && icon !== undefined) {
              if (path && !/^\//.test(path)) {
                path = `/${path}`;
              }

              resultArray.push({ text, icon, path });
            }

            if (items !== undefined) {
              const resultObj = { text, items: [], icon: "folder" };

              resultArray.push(resultObj);
              treeWalker(items, resultObj.items);
            }
          }
        }

        buildingTree(windowsWithOneName, treeWindowsWithOneName);
        buildingTree(windowsWithSomeNames, treeWindowsWithSomeNames);

        treeWalker(treeWindowsWithSomeNames, navigationMenu);
        treeWalker(treeWindowsWithOneName, navigationMenu);

        setNavigation(navigationMenu);
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
      });
  }, []);

  return (
    <div className="dx-swatch-additional side-navigation-menu" ref={getWrapperRef}>
      {children}
      <div className="menu-container">
        <TreeView
          noDataText=""
          ref={treeViewRef}
          items={items}
          keyExpr="path"
          selectionMode="single"
          focusStateEnabled={false}
          expandEvent="click"
          onItemClick={selectedItemChanged}
          onContentReady={onMenuReady}
          width="100%"
        />
      </div>
    </div>
  );
}
