import React, { useState, useRef, useEffect } from "react";
import { connect } from "react-redux";

import DataGrid, {
  Column,
  StateStoring,
  Button,
  Export,
  ColumnChooser,
} from "devextreme-react/data-grid";
import "devextreme-react/text-area";
import { exportDataGrid } from "devextreme/excel_exporter";
import notify from "devextreme/ui/notify";
import DataSource from "devextreme/data/data_source";

import { Workbook } from "exceljs";
import saveAs from "file-saver";
import { isEmpty } from "lodash-es";

import { deauthorize } from "../../store/commonSlice";

import { getDisplayProtocolsDataStore, getSystemWindowsDataStore, WINDOWS } from "../../dataStores";

import DisplayProtocolsEditorPopup from "./displayProtocolsEditorPopup";

import config from "../../config";

function onExporting(e) {
  const workbook = new Workbook();
  const worksheet = workbook.addWorksheet("Main sheet");

  exportDataGrid({
    component: e.component,
    worksheet: worksheet,
    autoFilterEnabled: true,
  }).then(function () {
    workbook.xlsx.writeBuffer().then(function (buffer) {
      saveAs(new Blob([buffer], { type: "application/octet-stream" }), "DataGrid.xlsx");
    });
  });
  e.cancel = true;
}

const windowId = WINDOWS.DISPLAY_PROTOCOLS.id;

const displayProtocolsDataStore = getDisplayProtocolsDataStore({
  windowId: windowId,
});

const displayProtocolsDataSource = new DataSource({
  store: displayProtocolsDataStore,
  reshapeOnPush: true,
});

const systemWindowsDataStore = getSystemWindowsDataStore({
  windowId: windowId,
});

const DisplayProtocolsPage = ({ localDeauthorize }) => {
  const [editorPopupVisible, setEditorPopupVisible] = useState(false);
  const [editedRow, setEditedRow] = useState({});
  const [windowTitle, setWindowTitle] = useState();

  const datagridRef = useRef(null);

  const displayProtocolsGridOptions = config.pages.displayProtocols.displayProtocolsGrid.dxOptions;

  function onDataErrorOccurred(e) {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);
    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  }

  function onToolbarPreparing(e) {}

  const openEditorPopup = (e) => {
    const currentRow = displayProtocolsDataSource.items().find((item) => item.id === e.row.key);

    if (!isEmpty(currentRow)) {
      setEditedRow(currentRow);
      setEditorPopupVisible(true);
    }
  };

  const closeEditorPopup = () => {
    setEditorPopupVisible(false);
  };

  const updateEntry = (e) => {
    if (!isEmpty(e.formData)) {
      displayProtocolsDataSource
        .store()
        .update(e.editedRowId, e.formData)
        .then(() => {
          closeEditorPopup();
          datagridRef?.current?.instance?.refresh();
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
          closeEditorPopup();
          datagridRef?.current?.instance?.refresh();
        });
    } else {
      closeEditorPopup();
    }
  };

  useEffect(() => {
    systemWindowsDataStore.byKey(windowId).then((result) => {
      const windowName = result[0]?.name?.split("/").pop();

      setWindowTitle(windowName);
    });
  }, []);

  return (
    <React.Fragment>
      <h2 className={"content-block"}>{windowTitle}</h2>
      {editorPopupVisible && (
        <DisplayProtocolsEditorPopup
          editedRow={editedRow}
          closeEditorPopup={closeEditorPopup}
          updateEntry={updateEntry}
        />
      )}
      <div className={"content-block"}>
        <div className={"dx-card responsive-paddings"}>
          <DataGrid
            ref={datagridRef}
            dataSource={displayProtocolsDataSource}
            onDataErrorOccurred={onDataErrorOccurred}
            onToolbarPreparing={onToolbarPreparing}
            onExporting={onExporting}
            errorRowEnabled={false}
            onContentReady={(e) => {
              //Для каждой колонки форсированно задаём порядок отрисовки в таблице.
              e.component.state().columns.forEach((col, index) => {
                e.component.columnOption(col.name, "visibleIndex", index);
              });
            }}
            {...displayProtocolsGridOptions}
          >
            <StateStoring
              enabled={true}
              type="localStorage"
              storageKey="pages.displayProtocolsEditor.displayProtocolsGrid"
            />
            <ColumnChooser enabled={true} />
            <Export enabled={true} allowExportSelectedData={true} />

            <Column dataField="id" caption="id" width="70px"/>
            <Column dataField="name" dataType="string" caption="Название" />
            <Column dataField="port" dataType="number" caption="Порт" width="70px"/>
            <Column type="buttons">
              <Button name="custom-edit" text="Изменить" icon="edit" onClick={openEditorPopup} />
            </Column>
          </DataGrid>
        </div>
      </div>
    </React.Fragment>
  );
};

export default connect(
  (state) => ({}),
  (dispatch) => {
    return { localDeauthorize: () => dispatch(deauthorize()) };
  }
)(DisplayProtocolsPage);
