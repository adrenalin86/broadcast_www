import { useEffect, useState } from "react";
import { LoadIndicator, Popup, SelectBox, TextBox, ValidationSummary } from "devextreme-react";
import Form, { SimpleItem } from "devextreme-react/form";
import { Position, ToolbarItem } from "devextreme-react/popup";
import validationEngine from "devextreme/ui/validation_engine";
import Validator, { RequiredRule } from "devextreme-react/validator";
import { connect } from "react-redux";
import { set } from "lodash-es";
import { deauthorize } from "../../store/commonSlice";
import config from "../../config";
import { getSpecTransportTypesDataStore, WINDOWS } from "../../dataStores";
import { TRANSPORTTYPESICONS } from "../../dataStores/constant";
import { useRef } from "react";

const windowId = WINDOWS.SPEC_TRANSPORT_TYPES.id;

const specTransportTypesDataStore = getSpecTransportTypesDataStore({
  windowId: windowId,
});

const specTransportTypesPopupOptions =
  config.pages.specTransportTypes.specTransportTypesGrid.popupTransportTypeEditor.dxOptions;

const SpecTransportTypesEditorPopup = ({
  specTransportTypeId,
  updateEntry,
  createEntry,
  closeEditorPopup,
  onDataErrorOccurred,
}) => {
  const [formData, setFormData] = useState({});
  const [changedFormData] = useState({});
  const [isLoadedData, setIsLoadedData] = useState(false);
  const [transportTypesIcons, setTransportTypesIcons] = useState([]);

  const imgFieldRef = useRef();

  const setFormDataValue = (dataField, value) => {
    set(formData, dataField, value);
    set(changedFormData, dataField, value);
  };

  const onFormItemValueChanged = (e) => {
    const value = e.value !== undefined ? e.value : e?.component?.option("text");
    const dataField = e.component.option("dataField");

    setFormDataValue(dataField, value);
  };

  const onSave = () => {
    const isValid = validationEngine.validateGroup("formEditor").isValid;

    if (isValid) {
      if (!specTransportTypeId) {
        createEntry(formData);
      } else {
        updateEntry({
          formData: changedFormData,
          editedRowId: formData.id,
        });
      }
    }
  };

  const customItemCreating = (args) => {
    if (!args.text) {
      args.customItem = null;
      return;
    }

    const transportTypesIconsIds = transportTypesIcons.map((item) => item.id);
    const id = Math.max.apply(null, transportTypesIconsIds) + 1;

    args.customItem = {
      id,
      iconUrl: args.text,
    };
  };

  useEffect(() => {
    if (specTransportTypeId) {
      specTransportTypesDataStore
        .byKey(specTransportTypeId)
        .then((result) => {
          const [specTransport] = result;

          setFormData(specTransport);
          setIsLoadedData(true);
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
        });
    } else {
      setIsLoadedData(true);
    }
  }, []);

  useEffect(() => {
    const icons = TRANSPORTTYPESICONS.slice();

    if (icons.some((item) => item.iconUrl === formData.iconUrl)) {
      setTransportTypesIcons(icons);
    } else {
      const iconsIds = TRANSPORTTYPESICONS.map((item) => item.id);
      const id = Math.max.apply(null, iconsIds) + 1;

      icons.push({ id, iconUrl: formData.iconUrl });
      setTransportTypesIcons(icons);
    }
  }, [formData]);

  return (
    <Popup
      visible={true}
      onHiding={closeEditorPopup}
      titleComponent={() => {
        return <span className="popup-title">{specTransportTypesPopupOptions.title}</span>;
      }}
      className="customEditorPopup"
      {...specTransportTypesPopupOptions}
    >
      <Position my="center" at="center" of={window} />
      {!isLoadedData && (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <LoadIndicator
            id="large-indicator"
            className="button-indicator"
            style={{ marginTop: "40px" }}
            height={40}
            width={40}
            visible={!isLoadedData}
          />
        </div>
      )}
      <Form validationGroup="formEditor">
        {isLoadedData && (
          <SimpleItem>
            <div className="editor-content">
              <div className="dx-form-group-with-caption dx-form-group editor-group">
                <div className="editor-item">
                  <div className="dx-field">
                    <div className="dx-field-label">Название</div>
                    <div className="dx-field-value">
                      <TextBox
                        dataField="name"
                        defaultValue={formData.name}
                        onInput={onFormItemValueChanged}
                      >
                        <Validator validationGroup="formEditor">
                          <RequiredRule />
                        </Validator>
                      </TextBox>
                    </div>
                  </div>
                </div>
                <div className="editor-item">
                  <div className="dx-field">
                    <div className="dx-field-label">Иконка</div>
                    <div className="dx-field-value">
                      <SelectBox
                        height={45}
                        dataField="iconUrl"
                        onValueChanged={onFormItemValueChanged}
                        defaultValue={formData.iconUrl}
                        items={transportTypesIcons}
                        displayExpr="iconUrl"
                        valueExpr="iconUrl"
                        acceptCustomValue={true}
                        onCustomItemCreating={customItemCreating}
                        itemRender={(data) => {
                          return (
                            <div style={{ display: "flex", alignItems: "center" }}>
                              <img src={data.iconUrl} style={{ height: "auto", width: "35px" }} />
                              <div style={{ marginLeft: "5px" }}>{data.iconUrl}</div>
                            </div>
                          );
                        }}
                        fieldRender={(data) => {
                          return (
                            <div
                              style={{
                                display: "flex",
                                alignItems: "center",
                                margin: "5px 0 0 7px",
                              }}
                            >
                              <img
                                ref={imgFieldRef}
                                src={data && data.iconUrl}
                                style={{ height: "auto", width: "35px" }}
                              />
                              <TextBox
                                className="product-name"
                                defaultValue={data && data.iconUrl}
                                onInput={() => {
                                  imgFieldRef.current.hidden = true;
                                }}
                              />
                            </div>
                          );
                        }}
                      >
                        <Validator validationGroup="formEditor">
                          <RequiredRule />
                        </Validator>
                      </SelectBox>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </SimpleItem>
        )}
      </Form>
      <ValidationSummary id="summary" validationGroup="formEditor" />
      <ToolbarItem
        widget="dxButton"
        location="after"
        toolbar="bottom"
        options={{ text: "Сохранить", onClick: onSave }}
        disabled={!isLoadedData}
      />
      <ToolbarItem
        widget="dxButton"
        location="after"
        toolbar="bottom"
        options={{ text: "Отменить", onClick: closeEditorPopup }}
      />
    </Popup>
  );
};

export default connect(null, {
  localDeauthorize: deauthorize,
})(SpecTransportTypesEditorPopup);
