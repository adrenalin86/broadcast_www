import React, { useState, useEffect } from "react";

import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import { ScrollView } from "devextreme-react/scroll-view";
import Form, { SimpleItem } from "devextreme-react/form";
import { TextBox, TagBox, NumberBox, CheckBox } from "devextreme-react";

import ValidationSummary from "devextreme-react/validation-summary";
import ValidationEngine from "devextreme/ui/validation_engine";
import Validator, {
  RequiredRule,
  StringLengthRule,
} from "devextreme-react/validator";

import { set, cloneDeep, isEmpty } from "lodash-es";

import config from "../../config";

const defaultValues = {};

const EditorPopup = ({
  editedRow,
  closeEditorPopup,
  updateEntry,
  createEntry,
}) => {
  const [formData, setFormData] = useState({});
  const [changedFormData] = useState({});

  const tagsPopupOptions = config.pages.vehicleRoutes.grid.editorPopup.dxOptions;
  /**
   * Инициализация редактора.
   */
  useEffect(() => {
    if (isEmpty(editedRow)) {
      setFormData(cloneDeep(defaultValues));
    } else {
      setFormData(cloneDeep(editedRow));
    }
  }, []);

  const closePopup = () => {
    closeEditorPopup();
  };

  const onSave = () => {
    const isValid = ValidationEngine.validateGroup("formEditor").isValid;

    if (isValid) {
      if (isEmpty(editedRow)) {
        createEntry({
          formData: formData,
        });
      } else {
        updateEntry({
          formData: changedFormData,
          editedRowId: editedRow.id,
        });
      }
    }
  };

  const onCancel = () => {
    closePopup();
  };

  const onFormItemValueChanged = (e) => {
    const value =
      e.value !== undefined ? e.value : e?.component?.option("text");
    const dataField = e.component.option("dataField");

    setFormDataValue(dataField, value);
  };

  const setFormDataValue = (dataField, value) => {
    set(formData, dataField, value);
    set(changedFormData, dataField, value);
  };

  return (
    <React.Fragment>
      <Popup
        className="customEditorPopup"
        visible={true}
        onHiding={closePopup}
        titleComponent={() => {
          return <span className="popup-title">{tagsPopupOptions.title}</span>;
        }}
        {...tagsPopupOptions}
      >
        <Position my="center" at="center" of={window} />
        <ScrollView showScrollbar="always">
          <form>
            <Form validationGroup="formEditor">
              <SimpleItem>
                <div className="editor-content">
                  <div className="dx-form-group-with-caption dx-form-group editor-group">
                    <div className="dx-form-group-caption">Общие сведения</div>
                    <div className="dx-form-group-content editor-content">
                      <div className="editor-item cols-2">
                        <div className="dx-field">
                          <div className="dx-field-label">Название</div>
                          <div className="dx-field-value">
                            <TextBox
                              defaultValue={formData.name}
                              onInput={onFormItemValueChanged}
                              dataField="name"
                            >
                              <Validator validationGroup="formEditor">
                                <RequiredRule />
                              </Validator>
                            </TextBox>
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">Краткое название</div>
                          <div className="dx-field-value">
                            <TextBox
                              defaultValue={formData.shortName}
                              onInput={onFormItemValueChanged}
                              dataField="shortName"
                            >
                              <Validator validationGroup="formEditor">
                                <RequiredRule />
                              </Validator>
                            </TextBox>
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">Номер</div>
                          <div className="dx-field-value">
                            <TextBox
                              defaultValue={formData.number}
                              onInput={onFormItemValueChanged}
                              dataField="number"
                            >
                              <Validator validationGroup="formEditor">
                                <RequiredRule />
                              </Validator>
                            </TextBox>
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">Id типа маршрута</div>
                          <div className="dx-field-value">
                            <NumberBox defaultValue={formData.routeTypeId} onInput={onFormItemValueChanged} dataField="routeTypeId">
                              <Validator validationGroup="formEditor">
                                <RequiredRule />
                              </Validator>
                            </NumberBox>
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">Активен</div>
                          <div className="dx-field-value">
                            <CheckBox defaultValue={formData.isActive} onInput={onFormItemValueChanged} dataField="isActive">
                              <Validator validationGroup="formEditor">
                                <RequiredRule />
                              </Validator>
                            </CheckBox>
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">Протяжённость, км.</div>
                          <div className="dx-field-value">
                            <NumberBox defaultValue={formData.length} onInput={onFormItemValueChanged} dataField="length">
                              <Validator validationGroup="formEditor">
                                <RequiredRule />
                              </Validator>
                            </NumberBox>
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">Заметки</div>
                          <div className="dx-field-value">
                            <TextBox defaultValue={formData.note} onInput={onFormItemValueChanged} dataField="note">
                            </TextBox>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </SimpleItem>
            </Form>
          </form>
        </ScrollView>
        <ValidationSummary
          id="summary"
          validationGroup="formEditor"
        ></ValidationSummary>
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Сохранить", onClick: onSave }}
          toolbar="bottom"
        />
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Отмена", onClick: onCancel }}
          toolbar="bottom"
        />
      </Popup>
    </React.Fragment>
  );
};

export default EditorPopup;
