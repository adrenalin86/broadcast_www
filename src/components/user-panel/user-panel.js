import React, { useMemo } from "react";
import ContextMenu, { Position } from "devextreme-react/context-menu";
import List from "devextreme-react/list";
import "./user-panel.scss";
import { connect } from "react-redux";
import { getRegion, getToken, getUserData, getBackendUrl } from "../../store/commonSlice";
import { signOut } from "../../services/auth";

const UserPanel = ({ userData, menuMode, token, backendUrl }) => {
  const onSignOutClick = async (e) => {
    signOut(e.itemData.token, e.itemData.backendUrl);
  };

  const menuItems = useMemo(
    () => [
      {
        text: "Выход",
        icon: "runner",
        onClick: onSignOutClick,
        token: token,
        backendUrl: backendUrl,
      },
    ],
    []
  );

  return (
    <div className={"user-panel"}>
      <div className={"user-info"}>
        <i className="dx-icon-card"></i>
        <div className={"user-name"}>{userData.userDescription || "Пользователь"}</div>
      </div>

      {menuMode === "context" && (
        <ContextMenu items={menuItems} target={".user-button"} showEvent={"dxclick"} width={160} cssClass={"user-menu"}>
          <Position my={"top center"} at={"bottom center"} />
        </ContextMenu>
      )}
      {menuMode === "list" && <List className={"dx-toolbar-menu-action"} items={menuItems} />}
    </div>
  );
};

export default connect((state, ownProps) => ({
  userData: getUserData(state),
  region: getRegion(state),
  token: getToken(state),
  backendUrl: getBackendUrl(state),
  menuMode: ownProps.menuMode,
}))(UserPanel);
