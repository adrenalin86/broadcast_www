import React, { useEffect, useRef, useState } from "react";
import { connect } from "react-redux";

import TreeList, { Editing, RowDragging } from "devextreme-react/tree-list";
import DataGrid, {
  Column,
  StateStoring,
  Button,
  Pager,
  Scrolling,
  Paging,
  Lookup as DGLookup,
  RequiredRule,
} from "devextreme-react/data-grid";
import Lookup from "devextreme-react/lookup";
import notify from "devextreme/ui/notify";
import DataSource from "devextreme/data/data_source";
import { differenceWith, isEmpty } from "lodash-es";

import { deauthorize } from "../../store/commonSlice";

import { getAreasDataStore, getRoutesDataStore, getAreaRoutesDataStore, WINDOWS } from "./../../dataStores";

import config from "../../config";

const areasTreeListOptions = config.pages.areas.areasTreeList.dxOptions;
const routesDataGridOptions = config.pages.areas.routesDataGrid.dxOptions;

const windowId = WINDOWS.AREAS.id;

const areasDataStore = getAreasDataStore({
  windowId,
});

const routesDataStore = getRoutesDataStore({
  windowId,
});

const areaRoutesDataStore = getAreaRoutesDataStore({
  windowId,
});

const areasDataSource = new DataSource({
  store: areasDataStore,
  reshapeOnPush: true,
});

const areaRoutesDataSource = new DataSource({
  store: areaRoutesDataStore,
  reshapeOnPush: true,
});

const Page = ({ localDeauthorize }) => {
  const [selectedArea, setSelectedArea] = useState({});
  const [routesInArea, setRoutesInArea] = useState([]);
  const [insertedItem] = useState(null);

  const areasTreeListRef = useRef(null);
  const routesDataGridRef = useRef(null);

  useEffect(() => {
    loadAreaRoutes();
  }, [selectedArea]);

  function loadAreaRoutes() {
    if (!isEmpty(selectedArea) && areaRoutesDataSource) {
      areaRoutesDataSource
        .store()
        .load({ filter: ["areaId", "=", selectedArea.id] })
        .then((result) => {
          if (result?.data) {
            setRoutesInArea((oldData) => result.data);
          } else {
            setRoutesInArea((oldData) => []);
          }
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
        });
    } else {
      setRoutesInArea((oldData) => []);
    }
  }

  function onDataErrorOccurred(e) {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);
    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  }

  function onAreasTreeListToolbarPreparing(e) {
    let toolbarItems = e.toolbarOptions.items;

    toolbarItems.forEach(function (item) {
      if (item.name === "addRowButton") {
        item.location = "before";
      }
    });
  }

  function onAddRoute(e) {
    const routeId = e?.selectedItem?.id;
    const areaId = selectedArea.id;

    if (routeId === undefined || routeId === null || areaId === undefined || areaId === null) {
      return;
    }

    areaRoutesDataSource
      .store()
      .insert({
        areaId: areaId,
        routeId: routeId,
      })
      .then((result) => {
        e.value = null;
        loadAreaRoutes();
      })
      .catch((error) => {
        e.value = null;
        onDataErrorOccurred({ error });
        loadAreaRoutes();
      });
  }

  function onClickRemoveRoute(e) {
    const areaRouteId = e.row?.key?.id;

    if (!areaRouteId) {
      return;
    }

    areaRoutesDataSource
      .store()
      .remove(areaRouteId)
      .then((result) => {
        loadAreaRoutes();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
        loadAreaRoutes();
      });
  }

  function onAreaRowSelectionChanged(e) {
    if (!isEmpty(e.selectedRowsData)) {
      setSelectedArea((oldData) => e.selectedRowsData[0]);
    } else {
      setSelectedArea((oldData) => {});
    }
  }

  function onReorder(e) {
    const visibleRows = e.component.getVisibleRows();
    let sourceData = e.itemData;

    let areas = areasTreeListRef?.current?.instance?.getDataSource().items();

    if (isEmpty(visibleRows) || isEmpty(sourceData) || isEmpty(areas)) {
      return;
    }

    const updateData = {};
    updateData.id = sourceData.id;

    const toIndex = e.toIndex;

    if (e.dropInsideItem) {
      updateData.parentAreaId = visibleRows[toIndex].data.id;
    } else {
      updateData.parentAreaId = visibleRows[toIndex].data.parentAreaId;
    }

    if (updateData.id == updateData.parentAreaId) {
      return;
    }

    areasDataSource
      .store()
      .update(updateData.id, { parentAreaId: updateData.parentAreaId })
      .then((result) => {
        e.value = null;
        loadAreaRoutes();
        areasTreeListRef?.current?.instance?.refresh();
      })
      .catch((error) => {
        e.value = null;
        onDataErrorOccurred({ error });
        loadAreaRoutes();
        areasTreeListRef?.current?.instance?.refresh();
      });
  }

  return (
    <>
      <div className="content-block">
        <div className={"dx-card responsive-paddings"}>
          <div className={"areasEditor"}>
            <div className="title-section">
              <div className="title-row">Список субъектов</div>
              <div className="title-row">Список маршрутов для субъекта {`"${selectedArea?.name || "---"}"`}</div>
            </div>
            <div className="datagrid-section">
              <div className="areasEditor-col">
                <div className="title-col">Список субъектов</div>
                <TreeList
                  ref={areasTreeListRef}
                  dataSource={areasDataSource}
                  onToolbarPreparing={onAreasTreeListToolbarPreparing}
                  onSelectionChanged={onAreaRowSelectionChanged}
                  parentIdExpr={"parentAreaId"}
                  selection={{ mode: "single" }}
                  errorRowEnabled={false}
                  onDataErrorOccurred={onDataErrorOccurred}
                  rootValue={null}
                  onRowValidating={(e) => {
                    if (!e.isValid) {
                      const defaultMessage = "Проверьте заполнение полей";
                      const message = e.brokenRules.reduce((acc, item) => {
                        return acc + item.message + "\n";
                      }, "");
                      notify(message || defaultMessage, "error", config.common.errorMessageLifespan);
                    }
                  }}
                  {...areasTreeListOptions}
                >
                  <Editing allowUpdating={true} allowDeleting={true} allowAdding={true} mode="row"  />
                  <RowDragging
                    onReorder={onReorder}
                    allowDropInsideItem={true}
                    allowReordering={true}
                    showDragIcons={true}
                  />
                  <Scrolling mode="standard" />
                  <Paging enabled={true} defaultPageSize={10} />
                  <Pager showPageSizeSelector={true} allowedPageSizes={[10, 25, 50]} showInfo={true} />
                  <Column dataField={"name"} caption={"Название"}>
                    <RequiredRule message="Необходимо указать название" />
                  </Column>
                  <Column dataField={"id"} caption={"id"} width={"50px"} visible={false} />
                  <Column type="buttons" width={"100px"}>
                    <Button name="add" icon="add" text="Добавить" />
                    <Button name="edit" icon="edit" text="Редактировать" />
                    <Button name="delete" icon="trash" text="Удалить" />
                    <Button name="save" icon="save" text="Сохранить" />
                    <Button name="cancel" icon="close" text="Отмена" />
                  </Column>
                </TreeList>
              </div>
              <div className="areasEditor-col">
                <div className="title-col">Список маршрутов для субъекта {`"${selectedArea?.name || "---"}"`}</div>
                <Lookup
                  dataSource={{
                    store: routesDataStore,
                    reshapeOnPush: true,
                    filter: ["id", "notin", routesInArea.map((item) => item.routeId)],
                    onLoadError: onDataErrorOccurred,
                  }}
                  valuteExpr="id"
                  displayExpr="name"
                  onSelectionChanged={onAddRoute}
                  placeholder="Выберите маршрут для добавления"
                  width="100%"
                  value={insertedItem}
                  disabled={isEmpty(selectedArea)}
                  style={{ marginBottom: "10px" }}
                />
                <DataGrid
                  id="routesDataGrid"
                  ref={routesDataGridRef}
                  onDataErrorOccurred={onDataErrorOccurred}
                  errorRowEnabled={false}
                  dataSource={routesInArea}
                  {...routesDataGridOptions}
                >
                  <StateStoring enabled={true} type="localStorage" storageKey="pages.areasEditor.routeDataGrid" />
                  <Scrolling mode="standard" />
                  <Paging enabled={true} defaultPageSize={10} />
                  <Pager showPageSizeSelector={true} allowedPageSizes={[10, 25, 50]} showInfo={true} />
                  <Column dataField={"routeName"} name="routeName" caption={"Название"} />
                  <Column dataField={"routeId"} name="routeId" caption={"id"} width={"80px"} visible={false} />
                  <Column type={"buttons"} width={"50px"}>
                    <Button icon="trash" onClick={onClickRemoveRoute} />
                  </Column>
                </DataGrid>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default connect((state) => ({}), {
  localDeauthorize: deauthorize,
})(Page);
