import React, { useState, useEffect } from "react";

import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import { ScrollView } from "devextreme-react/scroll-view";
import Form, { SimpleItem } from "devextreme-react/form";
import { TextBox, DateBox } from "devextreme-react";

import ValidationSummary from "devextreme-react/validation-summary";
import ValidationEngine from "devextreme/ui/validation_engine";
import Validator, { RequiredRule } from "devextreme-react/validator";

import { set, cloneDeep, isEmpty } from "lodash-es";

import config from "../../config";
const broadcastRequestForDisplayInfoPopupConfig =
  config.pages.broadcastRequestForDisplayInfo.broadcastRequestForDisplayInfoDataGrid.broadcastRequestForDisplayInfoPopup
    .dxOptions;

const defaultValues = {};

const endOfCurrentDay = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), 23, 59, 59);

const minDate = new Date(1900, 0, 1);

const PopupEditor = ({ editedRow, closeEditorPopup, updateEntry, createEntry }) => {
  const [formData, setFormData] = useState({});
  const [changedFormData] = useState({});

  /**
   * Инициализация редактора.
   */
  useEffect(() => {
    if (isEmpty(editedRow)) {
      setFormData(cloneDeep(defaultValues));
    } else {
      setFormData(cloneDeep(editedRow));
    }
  }, []);

  const closePopup = () => {
    closeEditorPopup();
  };

  const onSave = () => {
    const isValid = ValidationEngine.validateGroup("formEditor").isValid;

    if (isValid) {
      if (isEmpty(editedRow)) {
        createEntry({
          formData: formData,
        });
      } else {
        updateEntry({
          formData: changedFormData,
          editedRowId: editedRow.id,
        });
      }
    }
  };

  const onCancel = () => {
    closePopup();
  };

  const onFormItemValueChanged = (e) => {
    let value = e.value !== undefined ? e.value : e?.component?.option("text");
    const dataField = e.component.option("dataField");

    if (dataField === "startTimeUtc" || dataField === "endTimeUtc") {
      if (value) {
        value = new Date(value).toISOString().split(".")[0] + "Z";
      }
    }

    setFormDataValue(dataField, value);
  };

  const setFormDataValue = (dataField, value) => {
    set(formData, dataField, value);
    set(changedFormData, dataField, value);
  };

  return (
    <React.Fragment>
      <Popup
        className="customEditorPopup"
        visible={true}
        onHiding={closePopup}
        titleComponent={() => {
          return <span className="popup-title">{broadcastRequestForDisplayInfoPopupConfig.title}</span>;
        }}
        {...broadcastRequestForDisplayInfoPopupConfig}
      >
        <Position my="center" at="center" of={window} />
        <ScrollView showScrollbar="always">
          <form>
            <Form validationGroup="formEditor">
              <SimpleItem>
                <div className="editor-content">
                  <div className="dx-form-group-with-caption dx-form-group editor-group">
                    <div className="dx-form-group-caption">Общие сведения</div>
                    <div className="dx-form-group-content editor-content">
                      <div className="editor-item cols-2">
                        <div className="dx-field">
                          <div className="dx-field-label">Название</div>
                          <div className="dx-field-value">
                            <TextBox defaultValue={formData.name} onInput={onFormItemValueChanged} dataField="name">
                              <Validator validationGroup="formEditor">
                                <RequiredRule />
                              </Validator>
                            </TextBox>
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">Начало показа информации</div>
                          <div className="dx-field-value">
                            <DateBox
                              defaultValue={formData.startTimeUtc}
                              onValueChanged={onFormItemValueChanged}
                              dataField="startTimeUtc"
                              type="datetime"
                            />
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">Окончание показа информации</div>
                          <div className="dx-field-value">
                            <DateBox
                              defaultValue={formData.endTimeUtc}
                              onValueChanged={onFormItemValueChanged}
                              dataField="endTimeUtc"
                              type="datetime"
                            />
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">Текст</div>
                          <div className="dx-field-value">
                            <TextBox defaultValue={formData.text} onInput={onFormItemValueChanged} dataField="text" />
                          </div>
                        </div>
                        <div className="dx-field">
                          <div className="dx-field-label">Примечание</div>
                          <div className="dx-field-value">
                            <TextBox defaultValue={formData.note} onInput={onFormItemValueChanged} dataField="note" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </SimpleItem>
            </Form>
          </form>
        </ScrollView>
        <ValidationSummary id="summary" validationGroup="formEditor"></ValidationSummary>
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Сохранить", onClick: onSave }}
          toolbar="bottom"
        />
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Отмена", onClick: onCancel }}
          toolbar="bottom"
        />
      </Popup>
    </React.Fragment>
  );
};

export default PopupEditor;
