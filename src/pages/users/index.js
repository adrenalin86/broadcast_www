import React, { useState, useRef, useEffect } from "react";
import { connect } from "react-redux";

import DataGrid, { StateStoring, Export, Column, Button } from "devextreme-react/data-grid";
import DataSource from "devextreme/data/data_source";
import { exportDataGrid } from "devextreme/excel_exporter";
import notify from "devextreme/ui/notify";
import { Popup, Position, ToolbarItem } from "devextreme-react/popup";

import { Workbook } from "exceljs";
import saveAs from "file-saver";
import { isEmpty } from "lodash-es";

import { deauthorize } from "../../store/commonSlice";

import {
  getAccessRightsDataStore,
  getUsersDataStore,
  ACCESS_TYPES,
  WINDOWS,
  getSystemWindowsDataStore,
} from "../../dataStores";

import UsersEditorPopup from "./usersEditorPopup";

import config from "../../config";

function onExporting(e) {
  const workbook = new Workbook();
  const worksheet = workbook.addWorksheet("Main sheet");

  exportDataGrid({
    component: e.component,
    worksheet: worksheet,
    autoFilterEnabled: true,
  }).then(function () {
    workbook.xlsx.writeBuffer().then(function (buffer) {
      saveAs(new Blob([buffer], { type: "application/octet-stream" }), "DataGrid.xlsx");
    });
  });
  e.cancel = true;
}

const windowId = WINDOWS.USERS.id;

const usersDataStore = getUsersDataStore({
  windowId: windowId,
});
const usersDataSource = new DataSource({
  store: usersDataStore,
  reshapeOnPush: true,
});

const systemWindowsDataStore = getSystemWindowsDataStore({
  windowId: windowId,
});

const UsersPage = ({ localDeauthorize }) => {
  const [editorPopupVisible, setEditorPopupVisible] = useState(false);
  const [editedRow, setEditedRow] = useState({});
  const [deleteRowPopupVisible, setDeleteRowPopupVisible] = useState(false);
  const [deletedRowKey, setDeletedRowKey] = useState(null);
  const [windowTitle, setWindowTitle] = useState();

  const datagridRef = useRef(null);

  const usersGridOptions = config.pages.users.usersGrid.dxOptions;

  function onDataErrorOccurred(e) {
    notify(e.error?.message, "error", config.common.errorMessageLifespan);
    if (e.error?.data?.reason === "noAuth") {
      localDeauthorize();
    }
  }

  const onToolbarPreparing = (e) => {
    let toolbarItems = e.toolbarOptions.items;

    toolbarItems.unshift({
      widget: "dxButton",
      options: {
        icon: "add",
        onClick: openCreateEntryPopup,
      },
      location: "before",
    });
  };

  const openEditorPopup = (e) => {
    const currentRow = usersDataSource.items().find((item) => item.id === e.row.key);

    if (!isEmpty(currentRow)) {
      setEditedRow(currentRow);
      setEditorPopupVisible(true);
    }
  };

  const openCreateEntryPopup = (e) => {
    setEditedRow({});
    setEditorPopupVisible(true);
  };

  const openDeleteRowPopup = (e) => {
    setDeletedRowKey(e.row.key);
    setEditedRow({});
    setDeleteRowPopupVisible(true);
  };

  const closeEditorPopup = () => {
    setEditorPopupVisible(false);
  };

  const closeDeleteRowPopup = () => {
    setDeleteRowPopupVisible(false);
  };

  const createEntry = (e) => {
    const accessRightsDataStoreActions = e.accessRightsDataStoreActions;

    usersDataSource
      .store()
      .insert(e.formData)
      .then((e) => {
        closeEditorPopup();
        setAccessRights(accessRightsDataStoreActions, e.data);
        //datagridRef?.current?.instance?.refresh(); рефреш вызывается в конце функции выше, этот всегда будет избыточен
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
        closeEditorPopup();
        datagridRef?.current?.instance?.refresh();
      });
  };

  const updateEntry = (e) => {
    const accessRightsDataStoreActions = e.accessRightsDataStoreActions;

    if (!isEmpty(e.formData)) {
      usersDataSource
        .store()
        .update(e.editedRowId, e.formData)
        .then((e) => {
          closeEditorPopup();
          datagridRef?.current?.instance?.refresh();
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
          closeEditorPopup();
          datagridRef?.current?.instance?.refresh();
        });
    } else {
      closeEditorPopup();
    }

    if (
      !isEmpty(accessRightsDataStoreActions.insertOrUpdate) ||
      !isEmpty(accessRightsDataStoreActions.delete)
    ) {
      setAccessRights(accessRightsDataStoreActions, e.editedRowId);
    }
  };

  const deleteEntry = (e) => {
    usersDataSource
      .store()
      .remove(deletedRowKey)
      .then(() => {
        closeDeleteRowPopup();
        datagridRef?.current?.instance?.refresh();
      })
      .catch((error) => {
        onDataErrorOccurred({ error });
        closeDeleteRowPopup();
        datagridRef?.current?.instance?.refresh();
      });
  };

  const setAccessRights = (accessRightsDataStoreActions, userId) => {
    const groupsAccessRightsDataStore = getAccessRightsDataStore({
      windowId: windowId,
      userId: userId,
      type: ACCESS_TYPES.GROUPS,
    });
    const windowsAccessRightsDataStore = getAccessRightsDataStore({
      windowId: windowId,
      userId: userId,
      type: ACCESS_TYPES.WINDOWS,
    });

    const promiseList = [];

    for (let i = 0; i < accessRightsDataStoreActions.insertOrUpdate.length; i++) {
      switch (accessRightsDataStoreActions.insertOrUpdate[i].type) {
        case ACCESS_TYPES.GROUPS:
          promiseList.push(async () =>
            groupsAccessRightsDataStore
              .insert(accessRightsDataStoreActions.insertOrUpdate[i])
              .catch((error) => {
                onDataErrorOccurred({ error });
              })
          );
          break;
        case ACCESS_TYPES.WINDOWS:
          promiseList.push(async () =>
            windowsAccessRightsDataStore
              .insert(accessRightsDataStoreActions.insertOrUpdate[i])
              .catch((error) => {
                onDataErrorOccurred({ error });
              })
          );
          break;
        default:
          break;
      }
    }

    for (let i = 0; i < accessRightsDataStoreActions.delete.length; i++) {
      switch (accessRightsDataStoreActions.delete[i].type) {
        case ACCESS_TYPES.GROUPS:
          promiseList.push(async () =>
            groupsAccessRightsDataStore
              .remove(accessRightsDataStoreActions.delete[i].id)
              .catch((error) => {
                onDataErrorOccurred({ error });
              })
          );
          break;
        case ACCESS_TYPES.WINDOWS:
          promiseList.push(async () =>
            windowsAccessRightsDataStore
              .remove(accessRightsDataStoreActions.delete[i].id)
              .catch((error) => {
                onDataErrorOccurred({ error });
              })
          );
          break;
        default:
          break;
      }
    }

    if (promiseList.length) {
      Promise.all(promiseList.map((f) => f()))
        .then(() => {
          datagridRef?.current?.instance?.refresh();
        })
        .catch(() => {
          datagridRef?.current?.instance?.refresh();
        });
    }
  };

  useEffect(() => {
    systemWindowsDataStore.byKey(windowId).then((result) => {
      const windowName = result[0]?.name?.split("/").pop();

      setWindowTitle(windowName);
    });
  }, []);

  return (
    <>
      <h2 className="content-block">{windowTitle}</h2>
      {editorPopupVisible && (
        <UsersEditorPopup
          windowId={windowId}
          editedRow={editedRow}
          closeEditorPopup={closeEditorPopup}
          updateEntry={updateEntry}
          createEntry={createEntry}
        />
      )}
      {deleteRowPopupVisible && (
        <Popup
          className="deleteRowPopup"
          width="auto"
          height="auto"
          visible={true}
          showCloseButton={false}
          showTitle={false}
          onHiding={closeDeleteRowPopup}
        >
          <Position my="center" at="center" of={window} />
          <div>
            <span>Вы уверены, что хотите удалить эту запись?</span>
          </div>
          <ToolbarItem
            widget="dxButton"
            location="center"
            options={{ text: "Да", onClick: deleteEntry }}
            toolbar="bottom"
          />
          <ToolbarItem
            widget="dxButton"
            location="center"
            options={{ text: "Нет", onClick: closeDeleteRowPopup }}
            toolbar="bottom"
          />
        </Popup>
      )}
      <div className={"content-block"}>
        <div className={"dx-card responsive-paddings"}>
          <DataGrid
            ref={datagridRef}
            dataSource={usersDataSource}
            onDataErrorOccurred={onDataErrorOccurred}
            onToolbarPreparing={onToolbarPreparing}
            onExporting={onExporting}
            errorRowEnabled={false}
            onContentReady={(e) => {
              //Для каждой колонки форсированно задаём порядок отрисовки в таблице.
              e.component.state().columns.forEach((col, index) => {
                e.component.columnOption(col.name, "visibleIndex", index);
              });
            }}
            {...usersGridOptions}
          >
            <StateStoring enabled={true} type="localStorage" storageKey="pages.users.usersGrid" />
            <Export enabled={true} allowExportSelectedData={true} />
            <Column dataField="id" width="70px"/>
            <Column dataField="description" caption="ФИО"/>
            <Column dataField="login" caption="Логин" width="170px"/>
            <Column dataField="email" caption="Адрес электронной почты" width="170px"/>
            <Column dataField="password" caption="Пароль" width="170px"/>
            <Column dataField="isActive" caption="Активен" width="70px" />
            {/* <Column
              dataField={"availableGroupsNames"}
              caption={"Доступные группы"}
              dataType="string"
              width="195px"
            /> */}
            <Column
              dataField={"availableWindowsNames"}
              caption={"Доступные окна"}
              dataType="string"
              width="295px"
            />
            <Column type="buttons">
              <Button name="custom-edit" text="Изменить" icon="edit" onClick={openEditorPopup} />
              <Button
                name="custom-delete"
                text="Удалить"
                icon="trash"
                onClick={openDeleteRowPopup}
              />
            </Column>
          </DataGrid>
        </div>
      </div>
    </>
  );
};

export default connect(
  (state) => ({}),
  (dispatch) => {
    return { localDeauthorize: () => dispatch(deauthorize()) };
  }
)(UsersPage);
