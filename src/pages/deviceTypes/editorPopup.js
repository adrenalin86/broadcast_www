import React, {useState, useEffect} from "react";
import {Popup, Position, ToolbarItem} from "devextreme-react/popup";
import {ScrollView} from "devextreme-react/scroll-view";
import Form, {SimpleItem} from "devextreme-react/form";
import {TextBox} from "devextreme-react";
import List from 'devextreme-react/list';
import ValidationSummary from "devextreme-react/validation-summary";
import ValidationEngine from "devextreme/ui/validation_engine";
import Validator, {RequiredRule} from "devextreme-react/validator";
import {set, cloneDeep, isEmpty} from "lodash-es";
import {
    WINDOWS
} from "../../dataStores";
import config from "../../config";

const windowId = WINDOWS.DEVICE_TYPES.id;

export const icons = [{
    type: 'button',
    Name: 'Кнопка 112',
}, {
    type: 'sound',
    Name: 'Модуль озвучивания',
}, {
    type: 'router',
    Name: 'Роутер',
}, {
    type: 'tv',
    Name: 'ТВ-экран',
}, {
    type: 'other',
    Name: 'Иное',
}
]


function ImageItem(item) {
    return (
        <div className="device-type">
            <img src={`ico/deviceTypes/${item.type}.svg`} alt={item.type}/>
            <div>{item.Name}</div>
        </div>
    );
}

const EditorPopupDeviceTypes = ({editedRow, closeEditorPopup, updateEntry, createEntry}) => {
    const [formData, setFormData] = useState({});
    const [changedFormData, setChangedFormData] = useState({});

    const popupOptions = config.pages.deviceType.grid.editorPopup.dxOptions;

    const closePopup = () => {
        closeEditorPopup();
    };

    const onSave = () => {
        const isValid = ValidationEngine.validateGroup("formEditor").isValid;
        if (isValid) {
            if (isEmpty(editedRow)) {
                createEntry({
                    formData: formData,
                });
            } else {
                updateEntry({
                    formData: changedFormData,
                    editedRowId: editedRow.id,
                });
            }
        }
    };

    const onCancel = () => {
        closePopup();
    };

    const onFormItemValueChanged = (e) => {
        const value = e.value !== undefined ? e.value : e?.component?.option("text");
        const dataField = e.component.option("dataField");
        if (dataField) {
            setFormDataValue(dataField, value);
        }
    };

    const setFormDataValue = (dataField, value) => {
        setFormData((prevState) => ({...prevState, [`${dataField}`]: value}))
        setChangedFormData((prevState) => ({...prevState, [`${dataField}`]: value}))
    };

    const handleSelectItem = (item) => {
        setFormDataValue('iconName', item[0]?.type)
    }

    useEffect(() => {
        if (!isEmpty(editedRow)) {
            setFormData(cloneDeep(editedRow));
        }
    }, []);

    return (
        <React.Fragment>
            <Popup
                className="customEditorPopup"
                visible={true}
                onHiding={closePopup}
                titleComponent={() => {
                    return <span className="popup-title">{popupOptions.title}</span>;
                }}
                {...popupOptions}
            >
                <Position my="center" at="center" of={window}/>
                <ScrollView showScrollbar="always">
                    <Form formData={formData} validationGroup="formEditor">
                        <SimpleItem>
                            <div className="editor-content">
                                <div className="dx-form-group-with-caption dx-form-group editor-group">
                                    <div className="dx-form-group-caption">Общие сведения</div>
                                    <div className="dx-form-group-content editor-content">
                                        <div className="editor-item">
                                            <div className="dx-field">
                                                <div className="dx-field-label">Название</div>
                                                <div className="dx-field-value">
                                                    <TextBox
                                                        defaultValue={formData.name}
                                                        onInput={onFormItemValueChanged}
                                                        dataField={"name"}
                                                    >
                                                        <Validator validationGroup="formEditor">
                                                            <RequiredRule/>
                                                        </Validator>
                                                    </TextBox>
                                                </div>
                                            </div>
                                            <div className="dx-field">
                                                <div className="dx-field-label">Иконка</div>
                                                <div className="dx-field-value">
                                                    <div className="list-container">
                                                        <List
                                                            keyExpr="type"
                                                            defaultSelectedItemKeys={formData?.iconName}
                                                            dataSource={icons}
                                                            onSelectedItemsChange={handleSelectItem}
                                                            height="100%"
                                                            selectionMode="single"
                                                            itemRender={ImageItem}/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </SimpleItem>
                    </Form>
                </ScrollView>
                <ValidationSummary id="summary" validationGroup="formEditor"></ValidationSummary>
                <ToolbarItem
                    widget="dxButton"
                    location="after"
                    options={{text: "Сохранить", onClick: onSave}}
                    toolbar="bottom"
                />
                <ToolbarItem
                    widget="dxButton"
                    location="after"
                    options={{text: "Отмена", onClick: onCancel}}
                    toolbar="bottom"
                />
            </Popup>
        </React.Fragment>
    );
};

export default EditorPopupDeviceTypes;
