import React, { useState, useEffect } from "react";

import { Switch, TextBox, SelectBox, Button } from "devextreme-react";
import { cloneDeep, isEmpty, set, get, has } from "lodash-es";

import { SeImageUploaderField } from "./imageUploader";

const emptyIconSelectData = {
  simple: null,
  trafficLight: null,
};

const EDITOR_GROUP = {
  NOTHING: 0,
  STATIC_IMAGE: 1,
  TRAFFIC_LIGHT: 2,
};

const SeComplexImageEditor = ({
  data,
  onFormItemValueChanged,
  editModeEnabled = false,
  allowDisabling = true,
  dataField,
  title,
  className = "",
}) => {
  const [currentEditorGroup, setCurrentEditorGroup] = useState(
    EDITOR_GROUP.NOTHING
  ); //селектор выбранной группы редакторов.
  const [objectData, setObjectData] = useState(emptyIconSelectData); //текущее состояние компонента
  const [initialData, setInitialData] = useState(emptyIconSelectData); //начальное состояние компонента

  const disabled =
    allowDisabling &&
    (get(data, dataField) === null || !has(data, dataField));

  //для передачи данных из компонента модифицируем объект события полученый от редактора или кнопки

  //Инициализация данных редактора
  useEffect(() => {
    const fieldData = get(data, dataField);
    if (!isEmpty(fieldData)) {
      setInitialData((oldData) => fieldData);
      setObjectData((oldData) => fieldData);
      //выбор первой открытой группы настроек источника
      if (!isEmpty(fieldData.simple)) {
        setCurrentEditorGroup(EDITOR_GROUP.STATIC_IMAGE);
      } else if (!isEmpty(fieldData.trafficLight)) {
        setCurrentEditorGroup(EDITOR_GROUP.TRAFFIC_LIGHT);
      }
    }
  }, []);

  //смена открытой подгруппы редакторов
  const onChangeEditorGroup = (e) => {
    setCurrentEditorGroup((oldData) => {
      return e.value;
    });

    //при смене подгруппы редакторов берём данные на основе начально полученных в редактор, данные от прочих групп выставляем в null
    if (e.value === EDITOR_GROUP.STATIC_IMAGE) {
      e.value = { ...emptyIconSelectData, simple: initialData.simple };
      setObjectData(e.value);
      onFormItemValueChanged({
        value: e.value,
        dataField: dataField,
        enabler: e.component.option("enabler"),
      });
    } else if (e.value === EDITOR_GROUP.TRAFFIC_LIGHT) {
      e.value = {
        ...emptyIconSelectData,
        trafficLight: initialData.trafficLight,
      };
      setObjectData(e.value);
      onFormItemValueChanged({
        value: e.value,
        dataField: dataField,
        enabler: e.component.option("enabler"),
      });
    }
  };

  const onValueChanged = (e) => {
    const objectDataField = e.component.option("dataField");

    if (e.component.option("enabler")) {
      if (e.value === false) {
        setObjectData((oldData) => {
          return emptyIconSelectData;
        });
      }
      onFormItemValueChanged({
        value: e.value,
        dataField: dataField,
        enabler: e.component.option("enabler"),
      });
      return;
    }

    set(objectData, objectDataField, e.value);
    e.value = cloneDeep(objectData);
    onFormItemValueChanged({
      value: e.value,
      dataField: dataField,
      enabler: e.component.option("enabler"),
    });
  };

  const cancelEditing = (e) => {
    setCurrentEditorGroup(EDITOR_GROUP.NOTHING);
    e.value = {
      trafficLight: emptyIconSelectData.trafficLight,
      simple: emptyIconSelectData.simple,
    };
    setObjectData((oldData) => e.value);
    onFormItemValueChanged({
      value: e.value,
      dataField: dataField,
      enabler: e.component.option("enabler"),
    });
  };

  const onImageChanged = (df, value) => {

    set(objectData, df, value);
   
    onFormItemValueChanged({
      value: cloneDeep(objectData),
      dataField: dataField,
      enabler: undefined,
    });
  };

  return (
    <>
      <div
        className={
          "dx-field complex-custom-editor icon-image-editor " + className
        }
      >
        {allowDisabling && editModeEnabled && (
          <div className="field-enabler">
            <Switch
              defaultValue={
                get(data, dataField) === null || !has(data, dataField)
                  ? false
                  : true
              }
              value={
                get(data, dataField) === null || !has(data, dataField)
                  ? false
                  : true
              }
              onValueChanged={onValueChanged}
              dataField={dataField}
              enabler={true}
            />
          </div>
        )}
        <div
          className={
            "dx-field"
          }
        >
          <div className="dx-field-label" disabled={disabled}>
            {title}
          </div>
          <div className="dx-field-value">
            <div className="source-type">
              <SelectBox
                value={currentEditorGroup}
                className="source-type-select"
                placeholder={"Выберите вариант"}
                dataSource={[
                  {
                    id: EDITOR_GROUP.STATIC_IMAGE,
                    name: "Статическая картинка",
                  },
                  {
                    id: EDITOR_GROUP.TRAFFIC_LIGHT,
                    name: "Светофор",
                  },
                ]}
                valueExpr={"id"}
                displayExpr={"name"}
                onValueChanged={onChangeEditorGroup}
                dropDownButtonComponent={(e) => {
                  return (
                    <div className="source-type-button">
                      <i className="dx-icon-more icon"></i>
                    </div>
                  );
                }}
                disabled={disabled}
              />
            </div>
          </div>
          {currentEditorGroup !== EDITOR_GROUP.NOTHING && (
            <div className="source-editor-controls">
              <Button
                name="cancelEditing"
                location="after"
                icon="close"
                onClick={cancelEditing}
                disabled={disabled}
              />
            </div>
          )}
        </div>

        {currentEditorGroup === EDITOR_GROUP.STATIC_IMAGE && (
          <div className="dx-field highlight-block-pale-gray">
            <div className="dx-field-value">
              <SeImageUploaderField
                data={objectData}
                onImageChanged={onImageChanged}
                disabled={disabled}
                dataField={"simple"}
              />
            </div>
          </div>
        )}
        {currentEditorGroup === EDITOR_GROUP.TRAFFIC_LIGHT && (
          <div className="dx-field highlight-block-pale-gray">
            <div className="dx-field-value">
              <div className="dx-field">
                <div className="dx-field-label">Позиция текста</div>
                <div className="dx-field-value">
                  <SelectBox
                    defaultValue={objectData?.trafficLight?.textPosition}
                    onValueChanged={onValueChanged}
                    dataSource={[
                      {
                        position: "top",
                        name: "Сверху",
                      },
                      {
                        position: "bottom",
                        name: "Снизу",
                      },
                      {
                        position: "left",
                        name: "Слева",
                      },
                      {
                        position: "tight",
                        name: "Справа",
                      },
                    ]}
                    valueExpr={"position"}
                    displayExpr={"name"}
                    disabled={disabled}
                    dataField={"trafficLight.textPosition"}
                  />
                </div>
              </div>
              <div className="dx-field">
                <div className="dx-field-label">Формат текста</div>
                <div className="dx-field-value">
                  <TextBox
                    defaultValue={objectData?.trafficLight?.textFormat}
                    onValueChanged={onValueChanged}
                    disabled={disabled}
                    dataField={"trafficLight.textFormat"}
                  />
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export { SeComplexImageEditor };
