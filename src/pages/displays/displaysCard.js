import ImgOnVideoWall from "../../components/videoCameras/imgOnVideoWall/imgOnVideoWall";

const DisplaysCard = ({
  openVideoStreamPopup,
  openEditorPopup,
  openDeleteRowPopup,
  onDataErrorOccurred,
  mainVideoCameraId,
  displayId,
  displayName,
  displayStatus,
    modeSee = false
}) => {
  return (
    <div className="card-box">
      <div
        style={{
            height: "100%", display: "flex", justifyContent: "space-between", flexDirection: "column", padding: 20,
        }}
      >
        <div style={{ display: "flex", alignItems: "center" }}>
          {displayStatus?.connected === 1 && (
            <i
              className="dx-icon-check"
              style={{
                color: "green",
                fontSize: "1.2em",
                margin: "0px 4px 6px 0px",
              }}
            />
          )}
          {(displayStatus?.connected === 0 || !displayStatus?.connected) && (
            <i
              className="dx-icon-close"
              style={{
                color: "gray",
                fontSize: "1.2em",
                margin: "0px 4px 6px 0px",
              }}
            />
          )}
          <div
            style={{
              cursor: "pointer", display: "inline-block", marginBottom: "8px", maxWidth: 240, wordWrap: "break-word"
            }}
            onClick={() => openVideoStreamPopup(mainVideoCameraId)}
          >
            {displayName}
          </div>
        </div>

        {!mainVideoCameraId ? (
          <div style={{display: "flex", justifyContent: "center"}}>
            <div
              style={{
                maxWidth: "220px",
                textAlign: "center",
                cursor: "pointer",
              }}
              onClick={() => openVideoStreamPopup(mainVideoCameraId)}
            >
              Отсутствует основная видеокамера
            </div>
          </div>
        ) : (
          <ImgOnVideoWall
            openVideoStreamPopup={openVideoStreamPopup}
            onDataErrorOccurred={onDataErrorOccurred}
            videoCameraId={mainVideoCameraId}
          />
        )}
          {!modeSee ?  <div>
          <button
            style={{ all: "unset", color: "#337ab7", cursor: "pointer" }}
            onClick={() => openEditorPopup(displayId)}
          >
            редактировать
          </button>
          <button
            style={{
              all: "unset",
              color: "#337ab7",
              cursor: "pointer",
              display: "inline-block",
              marginLeft: "10px",
            }}
            onClick={() => openDeleteRowPopup(displayId)}
          >
            удалить
          </button>
        </div> : <div></div>}
      </div>
    </div>
  );
};

export default DisplaysCard;
