import { SeTextBox, SeCheckBox, SeNumberBox, SeComplexStringEditor, SeSelectBox } from "../editors";

import { outputModesDataStore } from "../../dataStores";
import { getGenericEntityDataStore, GENERIC_ENTITIES } from "../../dataStores";

import { isEmpty, noop } from "lodash-es";

const U646SettingsComponent = ({
  data,
  onFormItemValueChanged,
  onFormItemValueChangedData,
  className,
  editModeEnabled = false,
  dataField,
  windowId,
  onError = noop,
}) => {
  const onValueChangedEventData = (e) => {
    const eventDataField = e.component.option("dataField");
    const fullDataField = isEmpty(dataField)
      ? eventDataField
      : dataField?.endsWith(".")
      ? dataField + eventDataField
      : dataField + "." + eventDataField;

    e.component.option("dataField", fullDataField);

    onFormItemValueChanged(e);
  };

  const onValueChangedRawData = (e) => {
    const eventDataField = e.dataField;
    const fullDataField = isEmpty(dataField)
      ? eventDataField
      : dataField?.endsWith(".")
      ? dataField + eventDataField
      : dataField + "." + eventDataField;

    e.dataField = fullDataField;
    onFormItemValueChangedData(e);
  };

  const tagsDataStore = getGenericEntityDataStore({
    windowId: windowId,
    entityName: GENERIC_ENTITIES.TAGS,
  });

  return (
    <div className={"dx-form-group-with-caption dx-form-group " + className}>
      <div className="dx-form-group-caption">Настройки протокола U646</div>
      <div className="dx-form-group-content">
        <div className="editor-item">
          <SeNumberBox
            data={data}
            onFormItemValueChanged={onValueChangedEventData}
            editModeEnabled={editModeEnabled}
            dataField={"brightness"}
            title={"Яркость"}
          />
          <SeCheckBox
            data={data}
            onFormItemValueChanged={onValueChangedEventData}
            editModeEnabled={editModeEnabled}
            dataField={"turnOnWatchdog"}
            title={"Включить сторожевой таймер"}
          />
          <SeSelectBox
            data={data}
            onFormItemValueChanged={onValueChangedEventData}
            editModeEnabled={editModeEnabled}
            searchEnabled={false}
            dataField={"outputMode"}
            dataSource={outputModesDataStore}
            valueExpr="value"
            displayExpr="name"
            title={"Режим вывода"}
          />
          <SeTextBox
            data={data}
            onFormItemValueChanged={onValueChangedEventData}
            editModeEnabled={editModeEnabled}
            dataField={"customCommands"}
            title={"Команды для табло"}
          />
          <SeTextBox
            data={data}
            onFormItemValueChanged={onValueChangedEventData}
            editModeEnabled={editModeEnabled}
            dataField={"routeNameFormat"}
            title={"Формат 'Маршрут'"}
          />
          <SeTextBox
            data={data}
            onFormItemValueChanged={onValueChangedEventData}
            editModeEnabled={editModeEnabled}
            dataField={"routeDescriptionTripFormat"}
            title={"Формат 'Направление'(в пути)"}
          />
          <SeTextBox
            data={data}
            onFormItemValueChanged={onValueChangedEventData}
            editModeEnabled={editModeEnabled}
            dataField={"routeDescriptionInEndStationFormat"}
            title={"Формат 'Направление'(на конечной)"}
          />
          <SeTextBox
            data={data}
            onFormItemValueChanged={onValueChangedEventData}
            editModeEnabled={editModeEnabled}
            dataField={"routeForecastTripFormat"}
            title={"Формат 'Прогноз'(в пути)"}
          />
          <SeTextBox
            data={data}
            onFormItemValueChanged={onValueChangedEventData}
            editModeEnabled={editModeEnabled}
            dataField={"routeForecastInEndStationFormat"}
            title={"Формат 'Прогноз'(на конечной)"}
          />
          <SeComplexStringEditor
            data={data}
            onFormItemValueChanged={onValueChangedRawData}
            editModeEnabled={editModeEnabled}
            dataField={"complexString"}
            title={"Подстрока 1"}
            tagIdsSource={{
              store: tagsDataStore,
              onLoadError: (error) => {
                onError(error);
              },
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default U646SettingsComponent;
