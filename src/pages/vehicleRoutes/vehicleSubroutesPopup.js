import React, { useState, useEffect, useRef } from "react";

import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import { ScrollView } from "devextreme-react/scroll-view";
import { DataGrid, Column } from "devextreme-react/data-grid";
import LoadPanel from "devextreme-react/load-panel";

import { getVehicleSubroutesDataStore, WINDOWS } from "../../dataStores";

import config from "../../config";

const windowId = WINDOWS.ROUTES.id;

const subroutesDataStore = getVehicleSubroutesDataStore({ windowId });

const EditorPopup = ({ selectedRoute, closePopup }) => {
  const gridRef = useRef(null);
  const [subroutesArray, setSubroutesArray] = useState([]);
  const [loadPanelVisible, setLoadPanelVisible] = useState(false);

  const popupOptions = config.pages.vehicleRoutes.grid.subroutesPopup.dxOptions;
  const gridOptions =
    config.pages.vehicleRoutes.grid.subroutesPopup.grid.dxOptions;

  function loadSubroutes() {
    setLoadPanelVisible((oldData) => true);
    subroutesDataStore
      .load({ filter: ["routeId", "=", selectedRoute] })
      .then((response) => {
        setSubroutesArray((oldData) => response.data);
        gridRef?.current?.instance?.refresh();
        setLoadPanelVisible((oldData) => false);
      })
      .catch((error) => {
        console.log(error);
        setLoadPanelVisible((oldData) => false);
      });
  }

  return (
    <React.Fragment>
      <Popup
        className="customEditorPopup"
        visible={true}
        onHiding={closePopup}
        titleComponent={() => {
          return <span className="popup-title">{popupOptions.title}</span>;
        }}
        {...popupOptions}
      >
        <Position my="center" at="center" of={window} />
        <ScrollView showScrollbar="always">
          <LoadPanel visible={loadPanelVisible} />
          <DataGrid
            ref={gridRef}
            dataSource={subroutesArray}
            errorRowEnabled={false}
            onContentReady={(e) => {
              //Для каждой колонки форсированно задаём порядок отрисовки в таблице.
              e.component.state().columns.forEach((col, index) => {
                e.component.columnOption(col.name, "visibleIndex", index);
              });
            }}
            onInitialized={() => {
              loadSubroutes();
            }}
            {...gridOptions}
          >
            <Column dataField={"name"} type="string" caption={"Название"} />
            <Column
              dataField="isActive"
              dataType="boolean"
              caption="Активен"
              width={"120px"}
              calculateFilterExpression={function (
                filterValue,
                selectedFilterOperation
              ) {
                if (
                  selectedFilterOperation === "between" &&
                  Array.isArray(filterValue)
                ) {
                  const filterExpression = [
                    [this.dataField, ">=", filterValue[0]],
                    "and",
                    [this.dataField, "<=", filterValue[1]],
                  ];
                  return filterExpression;
                } else if (
                  selectedFilterOperation === "=" &&
                  !Array.isArray(filterValue)
                ) {
                  const filterExpression = [
                    this.dataField,
                    "=",
                    filterValue ? 1 : 0,
                  ];
                  return filterExpression;
                } else {
                  const filterExpression = [
                    this.dataField,
                    selectedFilterOperation,
                    filterValue,
                  ];
                  return filterExpression;
                }
              }}
            />
          </DataGrid>
        </ScrollView>
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Закрыть", onClick: closePopup }}
          toolbar="bottom"
        />
      </Popup>
    </React.Fragment>
  );
};

export default EditorPopup;
