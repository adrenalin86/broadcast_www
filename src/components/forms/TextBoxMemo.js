import React from 'react';
import Validator, {RequiredRule} from "devextreme-react/validator";
import {TextBox} from "devextreme-react";

const TextBoxMemo = React.memo(({value, onChange, name, required}) => {
        return (
            <TextBox
                defaultValue={value}
                onInput={onChange}
                dataField={name}
            >
                {required ? <Validator validationGroup="formEditor">
                    <RequiredRule/>
                </Validator> : null}
            </TextBox>
        );
    }
)

export default TextBoxMemo;