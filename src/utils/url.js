import { isEmpty } from "lodash";

/**
 * Парсит строку с адресом через URL(). Возвращает
 * объект со строковыми, что удобно для хранения в нашем
 * случае.
 * @param {string} url Полный url или путь, как в стандартном URL().
 * @param {string} [base]  Основание url, как в стандартном URL().
 * @returns {Object} Объект со строковыми полями protocol, hostname, port, pathname, search, hash. Как приведённые к строке в URL().
 */
function urlParser(url, base) {
  if (typeof url !== "string" || isEmpty(url)) {
    return {};
  }

  let thisUrl = url.trim().replace(/(\/\/)$|(:)$/, "/");

  if (!thisUrl.startsWith("http://") && !thisUrl.startsWith("https://")) {
    thisUrl = "http://" + thisUrl.replace(/^(\/\/)|^(\/)/g, "");
  }

  const urlObject = new URL(thisUrl, base);
  //На момент 20.09.2021 класс имеет статус эксперементального,
  //но уже долгое время и сейчас поддерживается во всех актуальных версиях распространённых браузеров.
  //Если будет работать не корректно, то заменить своим.

  const parsedUrl = {
    protocol: urlObject.protocol.toString(),
    hostname: urlObject.hostname.toString(),
    port: urlObject.port.toString(),
    pathname: urlObject.pathname.toString(),
    search: urlObject.search.toString(),
    hash: urlObject.hash.toString(),
  };

  return parsedUrl;
}

/**
 * Предоставляет протокол через который открыт сайт. Если протокол file, то предоставляет http, что
 * издержка для electron версии приложения.
 * @returns {string} строка с протоколом, включая двоеточие.
 */
function provideCurrentProtocol() {
  return window.location.protocol === "file:" ? "http:" : window.location.protocol;
}

/**
 * Собирает строку url. Все параметры не обязательны и совпадают с объектом
 * возвращаемым методом urlParser().
 * @param {string=} [hostname] Название хоста, домен без протокола и порта.
 * @param {string=} [pathname] Название пути, путь без доменной части.
 * @param {string=} [protocol] Протокол, вместе с двоеточием.
 * @param {string=} [port]  Порт.
 * @param {string=} [search]  Поисковая строка, вместе с открывающим знаком вопроса.
 * @param {string=} [hash]  Хэш (якорь), вместе с хэш символом (решёткой).
 * @returns {string} Собранная строка url.
 */
function urlConstructor(hostname, pathname, protocol, port, search, hash) {
  let result = "";
  //Шаблонная строка была компактной, но громоздкой.
  if (!isEmpty(protocol)) {
    result += protocol + "//";
  }

  if (!isEmpty(hostname)) {
    result += hostname;
  }

  if (!isEmpty(port)) {
    result += ":" + port;
  }

  if (!isEmpty(pathname)) {
    result += pathname;
  }

  if (!isEmpty(search)) {
    result += search;
  }

  if (!isEmpty(hash)) {
    result += hash;
  }

  return result;
}

export { urlParser, provideCurrentProtocol, urlConstructor };
