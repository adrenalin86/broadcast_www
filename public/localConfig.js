const localConfig = {
  pages: {
    login: {
      showUrlInput: true,
      showRegionInput: true,
    },
    roadEvents: {
      map: {
        startingCenter: {
          lat: 54.6292,
          lng: 39.7364,
        },
      },
    },
  },
};
