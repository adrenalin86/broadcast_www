import React from 'react';
import Validator, {RequiredRule} from "devextreme-react/validator";
import {TextArea} from "devextreme-react";

const TextAreaMemo = React.memo(({value, onChange, name, required}) => {
        return (
            <TextArea
                defaultValue={value}
                onInput={onChange}
                dataField={name}
            >
                {required ? <Validator validationGroup="formEditor">
                    <RequiredRule/>
                </Validator> : null}
            </TextArea>
        );
    }
)

export default TextAreaMemo;