import { createSlice } from "@reduxjs/toolkit";
import config from "../config";

import { omitDeepByKeys } from "../utils";

export const DISPLAY_MODELS_PAGE_REDUCER_KEY = "displayModels";

const displayModelPageSlice = createSlice({
  name: DISPLAY_MODELS_PAGE_REDUCER_KEY,
  initialState: {
    ...omitDeepByKeys(config.pages[DISPLAY_MODELS_PAGE_REDUCER_KEY], ["dxOptions"]),
  },
  reducers: {
    setFilterRowVisibility: (state, { payload }) => {
      state.displayModelsGrid.dxOptions.filterRow = { visible: payload };
    },
  },
});

export const { setFilterRowVisibility } = displayModelPageSlice.actions;

export const getDisplayModelsPage = (state) =>
  state.pages[DISPLAY_MODELS_PAGE_REDUCER_KEY];


export default displayModelPageSlice.reducer;
