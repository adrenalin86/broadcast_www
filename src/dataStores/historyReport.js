import store from "../store";

import { getBackendUrl, getToken, getRegion } from "../store/commonSlice";

import { handleResponseErrors, handleDataErrors, handleCaughtErrors } from "./utils";

const getDataStore = ({ windowId, reportName }) => {
  const restArgs = {
    w: windowId,
    ct: 17,
  };

  let reportGenerationProgress = 0;
  let guid = "";
  let report = [];
  let status = "done"; //generating, error, done
  let reportReceived = false;

  const generateReport = async (params) => {
    let body = {
      t: getToken(store.getState()),
      reg: getRegion(store.getState()),
      ...restArgs,
      cd: "startAsync",
      data: {
        cd: "loadReportData",
        data: {
          reportName: reportName,
          ...params,
        },
      },
    };

    const result = await fetch(getBackendUrl(store.getState()) + "?" + body.cd, {
      method: "POST",
      body: JSON.stringify(body),
    })
      .then(handleResponseErrors)
      .then((response) => response.json())
      .then(handleDataErrors)
      .then((data) => {
        reportReceived = false;
        reportGenerationProgress = data.data.percent;
        guid = data.data.guid;
        status = data.data.done ? "done" : "generating";

        return { reportGenerationProgress };
      })
      .catch((error) => {
        status = "error";
        throw handleCaughtErrors(error);
      });

    return await result;
  };

  const getReportProgress = async () => {
    let body = {
      t: getToken(store.getState()),
      reg: getRegion(store.getState()),
      ...restArgs,
      cd: "getAsyncInfo",
      data: { cd: "loadReportData", guid: guid },
    };

    return await fetch(getBackendUrl(store.getState()) + "?" + body.cd, {
      method: "POST",
      body: JSON.stringify(body),
    })
      .then(handleResponseErrors)
      .then((response) => response.json())
      .then(handleDataErrors)
      .then((data) => {
        if (data.r !== "ok") {
          throw new Error("Ошибка статуса формирования отчёта")
        }
        
        reportGenerationProgress = data.data.percent;
        status = data.data.done ? "done" : status;



        return { reportGenerationProgress };
      })
      .catch((error) => {
        status = "error";
        throw handleCaughtErrors(error);
      });
  };

  const getReport = async () => {
    if (reportReceived) {
      return report;
    } else {
      let body = {
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        ...restArgs,
        cd: "getAsyncResult",
        data: {
          cd: "loadReportData",
          guid: guid,
        },
      };

      return await fetch(getBackendUrl(store.getState()) + "?" + body.cd, {
        method: "POST",
        body: JSON.stringify(body),
      })
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .then((data) => {
          reportReceived = true;
          if(data.r !== "ok"){
            throw new Error("Ошибка получения сформированного отчёта")
          }
          report = data.data;
          return { data: data.data };
        })
        .catch((error) => {
          status = "error";
          throw handleCaughtErrors(error);
        });
    }
  };

  const getStatus = () => {
    return status;
  };

  return { generateReport, getReportProgress, getReport, getStatus };
};

export default getDataStore;
