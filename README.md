# Использование проекта

Проект использует react, scss и devextreme-react.

## Сборка, развёртывание и разработка

При обновлениях версий devextreme и компонентов этого ui-фреймворка могут просиходить изменения разметки из-за чего темы созданные devextreme theme-builder перестанут корректно работать. По этому после поломки стилей оформления после обновления devextreme, или-же в качестве предосторожности после мажорных обновлений, нужно:

* Выполнить скрипт build-themes из package.json. Если файлы настроек тем оформления отличаются названием, то указать входные файлы при выполнении комманды, переименовать файлы, либо установить нужные файлы в devextreme.json.
если-же темы имеют изменения, то предварительно может быть необходимым:
* Импортировать сохранённый в проекте файл настроек темы (в данный момент это 'metadata.base.json' и 'metadata.additional.json') в редактор theme builder на сайте devextreme,
* Экспортировать тему, внеся изменения если нужно.

### Установка необходимых данных проекта

Обязательно выполнить если не была выполнена после скачивания проекта из хранилища

```console
yarn install
```

### Запуск проекта в разработке

В разработке electron версия приложения подключается к запущеному через CRA серверу на localhost:3000, по этому необходимо запускать и electron приложение и обычный сервер.

### Запуск проекта через CRA

```console
yarn start
```

### Без автоматического открытия окна браузера

```concole
yarn run start:silent
```

### Запуск приложения electron в разработке

```concole
yarn run electron
```

### С автоматической перезагрузкой при изменениях

```concole
yarn run electron-nodemon
```

### Сборка проекта в electron

### Сборка под текущие целевые платформы

```concole
yarn run electron-pack
```

### Только для Windows

```concole
yarn run electron-pack-win
```

### Только для Linux

```concole
yarn run electron-pack-linux
```

### Только для MacOS. Работает только на MacOS

```concole
yarn run electron-pack-mac
```

## Использование файлов конфигурации

### Файлы конфигурации проекта

Проект содержит шесть файлов конфигурации, которые имеют отличия по назначению и функционалу внутри проекта. Некоторые из них используются совместно. Их содержимое совмещается и настройки замещаются в порядке повышения приоритета. Вот эти файлы в порядке повышения приоритета:

* 01_defaultConfig.js
* 02_buildConfig.js и 02_buildConfig.json
* 04_localConfig.js и localConfig.js
* 03_userConfig.js

Все файлы конфигурации, кроме "localConfig.js", включаются в проект на этапе сборки. Файл "localConfig.js" находиться в публичной папке проекта и попадает в корень собранного проекта, откуда и берётся в качестве статичного ресурса.

* Файл "01_defaultConfig.js" содержит конфигурацию по умолчанию и служит структурой формируемой в проекте конфигурации.
* Файлы "02_buildConfig.js" и "02_buildConfig.json" используются для внесения изменений конфигурации на этапе сборки. Здесь файл "02_buildConfig.json" содержит непосредственно конфигурацию.
* Файлы "03_localConfig.js" и "localConfig.js" используются для внесения изменений конфигурации в собранном проекте. Здесь файл "localConfig.js" содержит непосредственно конфигурацию.
* Файл "04_userConfig.js" используется для управления сохранением данных из состояния redux хранилища.

Вся конфигурация имеет одну и ту-же структуру для всех файлов, с поправкой на формат файла. Т.е. например "02_buildConfig.json" это структура JSON, а в "localConfig.js" эквивалентная структура присвоена переменной.

### Использование локального файла конфигурации "localConfig.js"

Локальный файл конфигурации находиться в публичной папке проекта и служит для переписывания конфигурации по умолчанию в уже собранном проекте. В процессе сборки проект он не участвует и попадает в корень папки собранного проекта, по этому он может быть заменён и\или изменён уже после сборки проекта. Его отсутствие или неправильная структура вызовут вывод ошибки в консоль, но прерывания работы проекта не произойдёт. Он имеет следующую структуру:

```js
const localConfig = {
  %содержимое конфигурации%
}
```

Как пример:

```js
const localConfig = {
  common: {
    backend: { 
      url: "",
      regionId: 0 
    },
  },
  pages: {
    login: {
      showUrlInput: true,
      showRegionInput: true,
    },
  },
};
```

### Использование файла конфигурации при сборке "02_buildConfig.json"

Это файл содержит конфигурацию которая добавляется в проект при сборке. Она попадает и development. Он имеет стандартную структуру JSON файла, например:

```json
{
  "common": {
    "backend": { 
      "url": "", 
      "regionId": 0 }
  },
  "pages": {
    "login": {
      "showUrlInput": true,
      "showRegionInput": true
    }
  }
}
```

## Структура файлов конфигурации

Конфигурация имеет структуру соответствующую JSON объекту, либо JS объекту. Что зависит от файла в котором она записана. Некоторые значения параметров здесь возможны только при использовании JS файлов конфигурации. Конфигурация имеет три основных раздела:

* common - содержит общие для всего проекта элементы конфигурации.
* pages - содержит специфичные для отдельных страниц элементы конфигурации.
* defaults - содержит данные по умолчанию для структур данных используемых в проекте. Обычно используется редакторами.

### common. Общие элементы конфигурации

Тут находятся данные конфигурации общие для проекта, либо не специфичные для какой-либо страницы.

* locale - локаль проекта.
* themeName - название темы оформления проекта, иначе по умолчанию.
* backend - данные для подключения к серверу.
* backend.url - url сервера, по умолчанию.
* backend.region - регион пользователя, по умолчанию.
* errorMessageLifespan - время жизни всплывающего сообщения об ошибке.
* token - фиксированный токен, по умолчанию.
* userData - некоторые данные пользователя.
* userData.rights - права доступа.
* userData.regRights - права доступа к реестрам.
* userData.windowsRights - права доступа к окнам.

```json
{
  "common": {
    "locale": "ru", 
    "themeName":"", 
    "backend": {  
      "url": "",
      "regionId": 0, 
      "useProtocolFromUrl": false 
    },
    "errorMessageLifespan": 4000,
    "token": "",
    "userData": {
      "rights": [],
      "regRights": [],
      "windowsRights": []
    }
  }
}
```

### pages. Конфигурации страниц

Список страниц чаще всего дополняется. Конфигурации соответствующие компонентам devextreme обычно используют атрибуты в соответствии с документацией devextreme. Такая конфигурация обычно указывается в поле dxOptions внутри соответствующего пункта конфигурации. Пункты подчиняющиеся шаблону вида "...grid" обычно соотвестуют элементу devextreme DataGrid. Пункты подчиняющиеся шаблону вида "popup..." обычно соотвестуют элементу devextreme Popup.

### pages.login. Конфигурация страницы входа\логина

Управление отображением полей ввода

* loginSettings.showUrlInput - показывать поля "Адрес" и "Порт". Если не показывается, то берётся по умолчанию из common.backend.url.
* loginSettings.showRegionInput - показывать поле "Регион". Если не показывается, то берётся по умолчанию из common.backend.regionId.

```json
{
  "pages": {
    "login": {
      "showUrlInput": true,
      "showRegionInput": true
    }
  }
}
```

### pages.controlCenter. Конфигурация страницы контроля

* vehiclesGrid - конфигурация dxDataGrid реестра транспорта
* vehiclesGrid.updateInreval - интервал обновления таблицы, мс.
* vehiclesGrid.ordersPopup - конфигурация окна списка нарядов

```json 
{
  "controlCenter": {
    "vehiclesGrid": {
      "updateInterval":10000,
      "dxOptions": {
      },
      "ordersPopup":{
        "dxOptions":{
        }
    },
  },
}
```

### pages.displays. Конфигурация страницы реестра табло

* displaysGrid - конфигурация dxDataGrid реестра табло.
* displaysGrid.popupDisplayEditor - конфигурация dxPopup редактирования табло.
* displaysGrid.updateInterval - интервал обновления таблицы табло, мс.

```json
{
  "displays": {
    "displaysGrid": {
      "updateInterval": 10000,
      "dxOptions": {},
      "popupDisplayEditor": {
        "dxOptions": {}
      },
    },
  }
}
```

### pages.displayModels. Конфигурация страницы реестра моделей табло

* displayModelsGrid - кофнигурация dxDataGrid реестра моделей табло.
* displayModelsGrid - конфигурация dxPopup редактирования модели табло.

```json
{
  "displayModels": {
    "displayModelsGrid": {
      "dxOptions":{},
      "popupDisplayModelsEditor": {
        "dxOptions":{}
      }
    }
  }
}
```

### pages.displayProtocols. Конфигурация страницы реестра протоколов табло

* displayProtocolsGrid - конфигурация dxDataGrid реестра протоколов табло.
* displayProtocolsGrid.popupDisplayProtocolsEditor - конфигурация dxPopup редактора протокола табло.

```json
{
  "displayProtocols": {
    "displayProtocolsGrid": {
      "dxOptions": { },
      "popupDisplayProtocolsEditor": {
        "dxOptions": {},
      },
    },
  },
}
```

### pages.displayGroups. Конфигурация страницы реестра групп

* groupsGrid - конфигурация dxDataGrid реестра групп .
* groupsGrid.groupsEditorPopup - конфигурация dxPopup редактора группы.
* groupsGrid.groupsEditorPopup.groupItemsEditor - конфигурация редактора компонентов группы.
* groupsGrid.groupsEditorPopup.groupItemsEditor.groupDisplaysGrid - конфигурация dxDataGrid списка дисплеев в группе.
* groupsGrid.groupsEditorPopup.groupItemsEditor.groupDisplaysGrid.displaySources - конфигурация dxDataGrid списка табло в источнике.

```json
{
  "displayGroups": {
    "groupsGrid": {
      "dxOptions": {},
      "groupsEditorPopup": {
        "dxOptions": {},
        "groupItemsEditor": {
          "groupDisplaysGrid": {
            "dxOptions": {},
          },
          "displaySources": {
            "displaysGrid": {
              "dxOptions": {},
            },
          },
        },
      },
    },
  },
}
```

### pages.broadcastPrograms. Конфигурация страницы реестра программ в эфире

* broadcastProgramsGrid - конфигурация dxDataGrid реестра программ в эфире.
* broadcastProgramsGrid.popupBroadcastProgramsEditor - конфигурация dxPopup редактора программы в эфире.
* broadcastProgramsGrid.popupBroadcastProgramsEditor.displaysMaster - конфигурация dxPopup мастера выбора табло для показа в эфире.
* broadcastProgramsGrid.popupBroadcastProgramsEditor.displaysMaster.displaySources - конфигурация dxDataGrid списка табло в источнике.
* broadcastProgramsGrid.popupBroadcastProgramsEditor.displaysMaster.chosenDisplays - конфигурация dxDataGrid списка выбранных для показа табло.

```json
{
  "broadcastPrograms": {
    "broadcastProgramsGrid": {
      "dxOptions": {},
      "popupBroadcastProgramsEditor": {
        "dxOptions": {},
        "displaysMaster": {
          "dxOptions": {},
          "displaySources": {
            "dxOptions": {},
          },
          "chosenDisplays": {
            "dxOptions": {},
          },
        },
      },
    },
  },
}
```

### pages.broadcastProgramTemplates. Конфигурация страницы реестра шаблонов программ эфира

* broadcastProgramTemplatesGrid - конфигурация dxDataGrid шаблонов программ эфира
* broadcastProgramTemplatesGrid.popupBroadcastProgramTemplateEditor - конфигурация dxPopup редактира шаблона программы эфира

```json
{
  "broadcastProgramTemplates": {
      "broadcastProgramTemplatesGrid": {
        "dxOptions": {},
        "popupBroadcastProgramTemplateEditor": {
          "dxOptions": {},
        },
      },
    },
}
```

### pages.reports. Конфигурация страниц отчётов

* common - общие для всех отчётов настройки
* common.reportStatusRequestTimeout - время ожидания между запросами статуса формирования отчёта.
* userActions - конфигурация страницы отчётов о действиях пользователей
* userActions.userActionsGrid - конфигурация dxDataGrid списка отчётов о действиях пользователей

```json
{
  "reports": {
    "common": {
      "reportStatusRequestTimeout": 1000,
    },
    "userActions": {
      "userActionsGrid": {
        "dxOptions": {},
      },
    },
  },
}
```

### pages.tags. Конфигурация страницы реестра тегов табло

* tagsGrid - конфигурация dxDataGrid реестра тегов.
* tagsGrid.popupTagsEditor - конфигурация dxPopup редактора тега.

```json
{
  "tags": {
    "tagsGrid": {
      "dxOptions": {},
      "popupTagsEditor": {
        "dxOptions": {},
      },
    },
  },
}
```

### pages.users. Конфигурация страницы реестра пользователей

* usersGrid - конфигурация dxDataGrid реестра пользователей.
* usersGrid.popupUsersEditor - конфигурация dxPopup редактора пользователя.
* usersGrid.popupUsersEditor.groupsDataGrid - конфигурация dxDataGrid выбора групп.

```json
{
  "users": {
    "usersGrid": {
      "dxOptions": {},
      "popupUsersEditor": {
        "dxOptions": {},
        "groupsDatagrid": {
          "dxOptions": {},
        },
      },
  }
}
```

### pages.broadcastRequestForDisplayInfo. Конфигурация страницы реестра запросов на показ информации в эфире

* broadcastRequestForDisplayInfoDataGrid - конфигурация dxDataGrid реестра запросов на показ информации в эфире
* broadcastRequestForDisplayInfoDataGrid.broadcastRequestForDisplayInfoPopup - конфигурация dxPopup редактора запроса на показ информации в эфире

```json
{
  "broadcastRequestForDisplayInfo": {
    "broadcastRequestForDisplayInfoDataGrid": {
      "dxOptions": {},
      "broadcastRequestForDisplayInfoPopup": {
        "dxOptions": {},
      },
    },
  },
}
```

### pages.areas. Страница настройки иерархии субъектов и списка привязанных к ним маршрутов

* areasTreeList - конфигурация dxTreeList иерархии субъектов.
* routesDataGrid - конфигурация dxDataGRid списка привязаных к субъекту маршутов.

```json
{
  "areas": {
    "areasTreeList": {
      "dxOptions": {},
    },
    "routesDataGrid": {
      "dxOptions": {},
    },
  },
}
```

### pages.pinCodes. Страница настройки списка пинкодов и их связи с привязанными к ним субъектам

* pinCodesEditorDataGrid - конфигурация dxDataGrid списка пинкодов.
* areasInPinCodeDataGrid - конфигурация dxDataGrid списка субъектов привязанных к пинкоду.

```json
{
  "pinCodes": {
    "pinCodesEditorDataGrid": {
      "dxOptions": {},
    },
    "areasInPinCodeDataGrid": {
      "dxOptions": {},
    },
  },
}
```

### pages.webNews. Страница редактирования новостей для портала

* webNewsDataGrid - конифигурация dxDataGrid списка новостей
* webNewsDataGrid.webNewsEditorPopup - конфигурация редактора новости

```json
{
  "webNews":{
    "webNewsDataGrid":{
      "dxOptions":{},
      "webNewsEditorPopup":{
        "dxOptions":{}
      }
    }
  }
}
```

### pages.webClaims. Страница редактирования обращений для портала

* grid - конифигурация dxDataGrid списка обращений
* grid.editorPopup - конфигурация редактора новости

```json
{
  "webClaims":{
    "grid":{
      "dxOptions":{},
      "editorPopup":{
        "dxOptions":{}
      }
    }
  }
}
```

### pages.polls. Страница создания и редактирования опросов

* grid - конфигурация dxDataGrid списка опросов
* grid.editorPopup - конфигурация редактора опроса

```json
{
  "polls":{
    "grid":{
      "dxOptions":{},
      "editorPopup":{
        "dxOptions":{}
      }
    }
  }
}
```

### defaults. Значения по умолчанию

В данный, 20.09.2021, момент используются только для значений по умолчанию для некоторых редакторов.
В связи с механизмом работы выключателей полей надо помнить, что как пустое значение по умолчанию для полей, не объектов, здесь надо использовать undefined, а не null.

### defaults.broadcastPrograms. Значения по умолчанию для програм показа табло

* name - название программы. String.
* priority - приоритет программы. Integer.
* startTimeUtc - начало показа. Date.
* endTimeUtc - окончание показа. Date.
* cond - условия показа.
* cond.displayIds - ID табло для показа. Array(Integer)
* cond.displayGroupIds - ID групп для показа. Array(Integer)
* tagIds - теги. Array(Integer)

```js
{
  "broadcastPrograms": {
      "name": "",
      "priority": 100,
      "startTimeUtc": new Date(),
      "endTimeUtc": new Date(),
      "cond": {
        "displayIds": [],
        "displayGroupIds": [],
      "tagIds": [],
    },
  },
}
```

### defaults.displays. Параметры по умолчанию для табло

Этот раздел объёмнее по это этому настройки отдельных протоколов табло вынесены в отдельные параграфы.

* lat - широта координаты табло. Float.
* lng - долгота координаты табло. Float.
* settings - блоки настроек.
* settings.timeoutInSec - Таймаут подключения табло, сек. Float.
* settings.forecastSource - настройки прогнозов.
* settings.itLineU653 - настройки протокола itLineU653.
* settings.itLineU646 - настройки протокола itLineU646.
* settings.smartJson2 - настройки протокола smartJson2.

```js
{
 "displays": {
    "lat": 54.6292,
    "lng": 39.7364,
    "settings": {
      "timeoutInSec": 60,
      "forecastSource": {},
      "itLineU653": {},
      "itLineU646": {},
      "smartJson2": {},
    },
  },
}
```

### defaults.displays.settings.forecastSource. Параметры по умолчанию источников прогнозов

* turnOffForecasts - отключить прогнозы полностью. Boolean.
* forecastsSourceStationId - метеостанция источник прогнозов. Integer.
* sortForesByRoute - сортировка прогнозов по машруту. Boolean.
* showAtEndStation - показ на конечной. Boolean.
* arrTimeThreshold - Порог отображения прогнозов, сек. Integer.

```js
{
  "forecastSource": {
    "turnOffForecasts": false,
    "forecastsSourceStationId": undefined,
    "sortForesByRoute": false,
    "showAtEndStation": true,
    "arrtimeThreshold": 0,
  },
}
```

### defaults.displays.settings.itLineU653. Параметры по умолчанию протокола itLineU653

* forecastsModeNumber - номер режима с прогнозами. Integer.
* pictureModeNumber - номер режима с картинкой. Integer.
* rssStartIndex - номер первой ячейки прогноза. Integer.
* rnStartIndex - номер первой ячейки с иконкой. Integer.
* busIcoType - номер иконки для автобуса. Integer.
* marshIcoType - номер иконки для маршрутки. Integer.
* trollIcoType - номер иконки для троллейбуса. Integer.
* tramIcoType - номер иконки для трамвая. Integer.
* image - изображение
* image.trafficLight - светофор
* image.trafficLight.textPosition - позиция текста. Возможные значения "top", "bottom", "left", "right".
* image.trafficLight.textFormat - формат текста. String.
* image.simple - статическая картинка. Base64.

```js
{
  "itLineU653": {
    "forecastsModeNumber": 1,
    "pictureModeNumber": 2,
    "rssStartIndex": 64,
    "rnStartIndex": 64,
    "busIcoType": 1,
    "marshIcoType": 2,
    "trollIcoType": 3,
    "tramIcoType": 4,
    "image": {
      "trafficLight": {
        "textPosition": null,
        "textFormat": null
      },
      "simple": null,
      },
    }
}
```

### defaults.displays.settings.itLineU646. Параметры по умолчанию протокола itLineU646

* brightness - яркость. Integer.
* turnOnWatchdog - включить сторожевой таймер. Boolean.
* outputMode - режим вывода. Integer.
* customCommands - команды для табло. String.
* routeNameFormat - формат названия. String.
* routeDescriptionTripFormat - формат описания пути. String.
* routeDescriptionInEndStationFormat - формат описания конечной. String.
* routeForecastTripFormat - формат прогноза пути. String.
* routeForecastInEndStationFormat - формат прогноза конечной. String.
* complexString - подстрока 1. Из полей weatherString, simpleString и newsString одновременно должно быть заполнено только одно, остальные null.
* complexString.pixelShift - сдвиг пикселей. Integer.
* complexString.speed - скорость строки. Integer.
* complexString.weatherString - строка прогноза погоды.
* complexString.weatherString.showTemp - показывать температуру. Boolean.
* complexString.weatherString.showWind - показывать ветер. Boolean.
* complexString.weatherString.showPressure - показывать давление. Boolean.
* complexString.weatherString.showIco - показывать иконку. Boolean.
* complexString.weatherString.format - формат строки. String.
* complexString.simpleString - простая строка.
* complexString.simpleString.text - текст. String.
* complexString.newsString - новостная строка.
* complexString.newsString.limit - лимит. Integer.
* complexString.newsString.lastDaysLimit - дней. Integer.
* complexString.newsString.tagIds - тэги. Array(Integer).
* complexString.newsString.format - формат. String.

```js
{
  "itLineU646": {
    "brightness": undefined,
    "turnOnWatchdog": false,
    "outputMode": 1,
    "customCommands": undefined,
    "routeNameFormat": "",
    "routeDescriptionTripFormat": "",
    "routeDescriptionInEndStationFormat": "",
    "routeForecastTripFormat": "",
    "routeForecastInEndStationFormat": "",
    "complexString": {
      "pixelShift": 0,
      "speed": 3,
      "weatherString": {
        "showWind": false,
        "showTemp": false,
        "showPressure": false,
        "showIco": false,
        "format": null
      },
      "simpleString": {
        "text": "Табло работает в тестовом режиме",
      },
      "newsString": {
        "limit": 1,
        "lastDaysLimit": 1,
        "tagIds": [],
        "format": null
      },
    },
  },
}
```

### defaults.displays.settings.smartJson2. Параметры по умолчанию протокола smartJson2

* frame - показываемый кадр.
* frame.frameType - тип кадра. Integer.
* frame.topText - верхний текст. String.
* frame.bottomText - нижний текст. String.
* frame.imageUrl - ссылка на картинку. String.
* frame.imageData - картинка. Base64.
* frame.videoUrl - ссылка на видео. String.
* frame.items - суб-кадры кадра. Содержит объекты идентичные структуре frame, но без items. Array.
* frame.duration - длительность кадра. Integer.
* errorFrame - кадр ошибки. Идентичен струкутуре frame, но без items.

```js
{
  "smartJson2": {
    "frame": {
      "frameType": 2,
      "topText": "",
      "bottomText": "",
      "imageUrl": "",
      "imageData": undefined,
      "videoUrl": "",
      "items": [],
      "duration": 0,
    },
    "errorFrame": {
      "frameType": 2,
      "topText": "",
      "bottomText": "",
      "imageUrl": "",
      "imageData": undefined,
      "videoUrl": "",
      "duration": 0,
    },
  },
}
```

### defaults.webClaims. Параметры по умолчанию для страницы редактирования обращений

* lat - широта координаты маркера при создании обращения
* lng - долгота координаты маркера при создании обращения

```json
{
  "lat": 0,
  "lng": 0
}
```

### Способы вывода rtsp потока в браузер:
* Клиентская часть не умеет воспринимать rtsp поток, поэтому его нужно конвертировать в hls с помощью библиотеки и уже после отображать в браузере с помощью hls.js.
* Использование ActiveX - низкая кроссплатформенность. 
* Использование сервиса rtspme, суть которого заключается в том, что можно генерировать новые ссылки из rtsp-ссылок и вставлять их в качестве src в тег iframe. (Показал хорошую работоспособность в передаче видеопотока)
* Использование WebRTC плеера,  у которого под капотом технология WebRTC. Встроить в сайт его несложно, в src передается rtsp ссылка. (Видеопоток  идет с торможениями, не плавно).