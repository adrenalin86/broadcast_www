import { isEmpty } from "lodash-es";

import ImgOnVideoWall from "../../components/videoCameras/imgOnVideoWall/imgOnVideoWall";

const VideoCamerasCard = ({
  openVideoStreamPopup,
  openEditorPopup,
  openDeleteRowPopup,
  onDataErrorOccurred,
  videoCameraId,
  videoCameraName,
  videoCameraStatus,
  videoCameraSettings,
}) => {
  const removeProperty = (propKey, { [propKey]: propValue, ...rest }) => rest;

  const checkVideoCameraProtocolSettingsForEmpty = () => {
    if (!isEmpty(videoCameraSettings)) {
      const protocolSettings = Object.values(videoCameraSettings)[0];
      const newProtocolSettings = removeProperty("videoStreamUrl", protocolSettings);
      const isEmptyKeys = Object.values(newProtocolSettings).some(
        (value) => value === null || value === ""
      );

      if (isEmptyKeys) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  };

  const isEmptyVideoCameraProtocolSettings = checkVideoCameraProtocolSettingsForEmpty();

  return (
    <div className="card-box">
      <div 
        style={{ 
          height: "100%", display: "flex", justifyContent: "space-between", flexDirection: "column",
          flexDirection:"column", padding: 20,
        }}
      >
        <div style={{ display: "flex", alignItems: "center" }}>
          {videoCameraStatus?.connected === 1 && (
            <i
              className="dx-icon-check"
              style={{
                color: "green",
                fontSize: "1.2em",
                margin: "0px 4px 6px 0px",
              }}
            />
          )}
          {(videoCameraStatus?.connected === 0 || !videoCameraStatus?.connected) && (
            <i
              className="dx-icon-close"
              style={{
                color: "gray",
                fontSize: "1.2em",
                margin: "0px 4px 6px 0px",
              }}
            />
          )}
          <div
            style={{ 
              cursor: "pointer", display: "inline-block", marginBottom: "8px", maxWidth: 240, wordWrap: "break-word"
            }}
            onClick={() => openVideoStreamPopup(videoCameraId)}
          >
            {videoCameraName}
          </div>
        </div>
        {!videoCameraStatus?.lastConnectionTimeUtc || isEmptyVideoCameraProtocolSettings ? (
          <div style={{display: "flex", justifyContent: "center"}}>
            <div
              style={{
                maxWidth: "220px",
                textAlign: "center",
                cursor: "pointer",
              }}
              onClick={() => openVideoStreamPopup(videoCameraId)}
            >
              {!videoCameraStatus?.lastConnectionTimeUtc
                ? "Не удалось подключиться к камере"
                : "Не настроено подключение к камере"}
            </div>
          </div>
        ) : (
          <ImgOnVideoWall
            openVideoStreamPopup={openVideoStreamPopup}
            onDataErrorOccurred={onDataErrorOccurred}
            videoCameraId={videoCameraId}
          />
        )}
        <div>
          <button
            style={{ all: "unset", color: "#337ab7", cursor: "pointer" }}
            onClick={() => openEditorPopup(videoCameraId)}
          >
            редактировать
          </button>
          <button
            style={{
              all: "unset",
              color: "#337ab7",
              cursor: "pointer",
              display: "inline-block",
              marginLeft: "10px",
            }}
            onClick={() => openDeleteRowPopup(videoCameraId)}
          >
            удалить
          </button>
        </div>
      </div>
    </div>
  );
};

export default VideoCamerasCard;
