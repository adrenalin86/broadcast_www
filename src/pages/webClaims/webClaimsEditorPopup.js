import React, { useState, useEffect } from "react";

import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import { ScrollView } from "devextreme-react/scroll-view";
import Form, { SimpleItem } from "devextreme-react/form";
import { SeMapSelectPoint } from "../../components/editors";

import { TextBox, Lookup, TextArea } from "devextreme-react";

import ValidationSummary from "devextreme-react/validation-summary";
import ValidationEngine from "devextreme/ui/validation_engine";
import Validator, { RequiredRule } from "devextreme-react/validator";

import { set, cloneDeep, isEmpty } from "lodash-es";

import config from "../../config";
import dayjs from "dayjs";

import { getWebClaimTypesDataStore, WINDOWS, getWebClaimStatusDataStore } from "../../dataStores";
const windowId = WINDOWS.WEB_CLAIMS.id;

const webClaimTypesDatStore = getWebClaimTypesDataStore({ windowId: windowId });
const webClaimStatusDataStore = getWebClaimStatusDataStore({ windowId: windowId });

const defaultValues = config.defaults.webClaims;

const EditorPopup = ({ editedRow, closeEditorPopup, updateEntry, createEntry }) => {
  const [formData, setFormData] = useState({});
  const [changedFormData] = useState({});

  const popupOptions = config.pages.webClaims.grid.editorPopup.dxOptions;
  const defaults = config.defaults.webClaims;
  /**
   * Инициализация редактора.
   */
  useEffect(() => {
    if (isEmpty(editedRow)) {
      setFormData(cloneDeep(defaultValues));
    } else {
      setFormData(cloneDeep(editedRow));
    }
  }, []);

  const closePopup = () => {
    closeEditorPopup();
  };

  const onSave = () => {
    const isValid = ValidationEngine.validateGroup("formEditor").isValid;
    if (isValid) {
      if (isEmpty(editedRow)) {
        createEntry({
          formData: formData,
        });
      } else {
        updateEntry({
          formData: changedFormData,
          editedRowId: editedRow.id,
        });
      }
    }
  };

  const onCancel = () => {
    closePopup();
  };

  const onFormItemValueChanged = (e) => {
    const value = e.value !== undefined ? e.value : e?.component?.option("text");
    const dataField = e.component.option("dataField");
    if (dataField) {
      setFormDataValue(dataField, value);
    }
  };

  const setFormDataValue = (dataField, value) => {
    set(formData, dataField, value);
    set(changedFormData, dataField, value);
  };

  return (
    <React.Fragment>
      <Popup
        className="customEditorPopup"
        visible={true}
        onHiding={closePopup}
        titleComponent={() => {
          return <span className="popup-title">{popupOptions.title}</span>;
        }}
        {...popupOptions}
      >
        <Position my="center" at="center" of={window} />
        <ScrollView showScrollbar="always">
          <Form validationGroup="formEditor">
            <SimpleItem>
              <div className="editor-content">
                <div className="dx-form-group-with-caption dx-form-group editor-group">
                  <div className="dx-form-group-caption">Общие сведения</div>
                  <div className="dx-form-group-content editor-content">
                    <div className="editor-item">
                      <div className="dx-field">
                        <div className="dx-field-label">Заголовок</div>
                        <div className="dx-field-value">
                          <TextBox
                            defaultValue={formData.title}
                            onInput={onFormItemValueChanged}
                            dataField={"title"}
                          ></TextBox>
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">ID пользователя</div>
                        <div className="dx-field-value">
                          <TextBox defaultValue={formData.webUserId} dataField={null} disabled={true} />
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">email</div>
                        <div className="dx-field-value">
                          <TextBox defaultValue={formData.contactEmail} dataField={null} disabled={true} />
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">Дата создания</div>
                        <div className="dx-field-value">
                          <TextBox
                            defaultValue={dayjs(formData.creationTimeUtc).format("YYYY-MM-DD hh:mm:ss")}
                            dataField={null}
                            disabled={true}
                          />
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">Статус</div>
                        <div className="dx-field-value">
                          <Lookup
                            defaultValue={formData.statusId}
                            dataSource={webClaimStatusDataStore}
                            valueExpr={"id"}
                            displayExpr={"name"}
                            searchExpr={["name"]}
                            searchEnabled={true}
                            dataField={"statusId"}
                            onValueChanged={onFormItemValueChanged}
                          ></Lookup>
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">Тип</div>
                        <div className="dx-field-value">
                          <Lookup
                            defaultValue={formData.typeId}
                            dataSource={webClaimTypesDatStore}
                            valueExpr={"id"}
                            displayExpr={"name"}
                            searchExpr={["name"]}
                            searchEnabled={true}
                            dataField={"typeId"}
                            onValueChanged={onFormItemValueChanged}
                          ></Lookup>
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">Положение на карте</div>
                        <div className="dx-field-value">
                          <SeMapSelectPoint
                            formData={formData}
                            setFormDataValue={setFormDataValue}
                            defaultLat={defaults.lat}
                            defaultLng={defaults.lng}
                          />
                        </div>
                      </div>
                      <div className="dx-field">
                        <div className="dx-field-label">Содержание</div>
                        <div className="dx-field-value">
                          <TextArea
                            defaultValue={formData.content}
                            onInput={onFormItemValueChanged}
                            dataField={"content"}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </SimpleItem>
          </Form>
        </ScrollView>
        <ValidationSummary id="summary" validationGroup="formEditor"></ValidationSummary>
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Сохранить", onClick: onSave }}
          toolbar="bottom"
        />
        <ToolbarItem
          widget="dxButton"
          location="after"
          options={{ text: "Отмена", onClick: onCancel }}
          toolbar="bottom"
        />
      </Popup>
    </React.Fragment>
  );
};

export default EditorPopup;
