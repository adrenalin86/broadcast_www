import React, { useState, useEffect } from "react";
import { connect } from "react-redux";

import { Popup, Position, ToolbarItem } from "devextreme-react/popup";
import { ScrollView } from "devextreme-react/scroll-view";
import Form, { SimpleItem } from "devextreme-react/form";
import { TextBox, SelectBox, LoadIndicator } from "devextreme-react";
import ValidationSummary from "devextreme-react/validation-summary";
import ValidationEngine from "devextreme/ui/validation_engine";
import Validator, { RequiredRule } from "devextreme-react/validator";
import DataSource from "devextreme/data/data_source";

import GenericHttp from "../../components/videoCameras/genericHttp/genericHttp";
import { SeMapSelectPoint } from "../../components";

import { set } from "lodash-es";

import {
  getVideoCamerasProtocolsDataStore,
  getVideoCamerasWithStatusesDataStore,
  WINDOWS,
} from "../../dataStores";

import { deauthorize } from "../../store/commonSlice";

import config from "../../config";

const windowId = WINDOWS.VIDEOCAMERAS.id;

const videoCamerasProtocolsDataStore = getVideoCamerasProtocolsDataStore({
  windowId: windowId,
});

const videoCamerasProtocolsDataSource = new DataSource({
  store: videoCamerasProtocolsDataStore,
  reshapeOnPush: true,
});

const videoCamerasWithStatusesDataStore = getVideoCamerasWithStatusesDataStore({
  windowId: windowId,
});

const videoCamerasPopupOptions =
  config.pages.videoCameras.videoCamerasGrid.popupVideoCameraEditor.dxOptions;
const defaultValues = config.defaults.videoCameras;

const VideoCamerasEditorPopup = ({
  videoCameraId,
  closeEditorPopup,
  updateEntry,
  createEntry,
  onDataErrorOccurred,
}) => {
  const [formData, setFormData] = useState({});
  const [changedFormData] = useState({});
  const [isLoadedData, setIsLoadedData] = useState(false);
  const [displayedProtocol, setDisplayedProtocol] = useState();

  const setFormDataValue = (dataField, value) => {
    set(formData, dataField, value);
    set(changedFormData, dataField, value);
  };

  const onFormItemValueChanged = (e) => {
    const value = e.value !== undefined ? e.value : e?.component?.option("text");
    const dataField = e.component.option("dataField");

    setFormDataValue(dataField, value);
  };

  const onProtocolChanged = (e) => {
    onFormItemValueChanged(e);
    setDisplayedProtocol(e.value);
  };

  const onSave = () => {
    const isValid = ValidationEngine.validateGroup("formEditor").isValid;

    if (isValid) {
      if (!videoCameraId) {
        createEntry(formData);
      } else {
        updateEntry({
          formData: changedFormData,
          editedRowId: formData.id,
        });
      }
    }
  };

  useEffect(() => {
    if (videoCameraId) {
      videoCamerasWithStatusesDataStore
        .byKey(videoCameraId)
        .then((result) => {
          const [videoCamera] = result;

          setFormData(videoCamera);
          setDisplayedProtocol(videoCamera.protocolName);
          setIsLoadedData(true);
        })
        .catch((error) => {
          onDataErrorOccurred({ error });
        });
    } else {
      setIsLoadedData(true);
    }
  }, []);

  return (
    <Popup
      className="customEditorPopup"
      visible={true}
      onHiding={closeEditorPopup}
      titleComponent={() => {
        return <span className="popup-title">{videoCamerasPopupOptions.title}</span>;
      }}
      {...videoCamerasPopupOptions}
    >
      <Position my="center" at="center" of={window} />
      {!isLoadedData && (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "100%",
          }}
        >
          <LoadIndicator
            id="large-indicator"
            className="button-indicator m-v-65"
            height={60}
            width={60}
            visible={!isLoadedData}
          />
        </div>
      )}
      <ScrollView showScrollbar="always">
        <Form validationGroup="formEditor">
          {isLoadedData && (
            <SimpleItem>
              <div className="editor-content">
                <div className="dx-form-group-with-caption dx-form-group editor-group">
                  <div style={{ display: "flex" }}>
                    <div className="dx-form-group-content editor-content" style={{ width: "70%" }}>
                      <div className="editor-item">
                        <div className="dx-field">
                          <div className="dx-field-label">Название</div>
                          <div className="dx-field-value">
                            <TextBox
                              dataField="name"
                              defaultValue={formData.name}
                              onInput={onFormItemValueChanged}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="editor-item">
                        <div className="dx-field">
                          <div className="dx-field-label">Адрес</div>
                          <div className="dx-field-value">
                            <TextBox
                              defaultValue={formData.address}
                              dataField="address"
                              onInput={onFormItemValueChanged}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="editor-item">
                        <div className="dx-field">
                          <div className="dx-field-label">Протокол работы</div>
                          <div className="dx-field-value">
                            <SelectBox
                              dataField="protocolName"
                              defaultValue={formData.protocolName}
                              dataSource={videoCamerasProtocolsDataSource}
                              allowClearing={true}
                              valueExpr="name"
                              displayExpr="name"
                              forceRepaint={true}
                              style={{ maxWidth: "41%" }}
                              onValueChanged={onProtocolChanged}
                            >
                              <Validator validationGroup="formEditor">
                                <RequiredRule />
                              </Validator>
                            </SelectBox>
                          </div>
                        </div>
                      </div>
                      <div className="editor-item">
                        <div className="dx-field">
                          <div className="dx-field-label">Примечание</div>
                          <div className="dx-field-value">
                            <TextBox
                              dataField="note"
                              defaultValue={formData.note}
                              onInput={onFormItemValueChanged}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="editor-item cols-2">
                      <div className="editor-item-label">Местоположение на карте</div>
                      <SeMapSelectPoint
                        formData={formData}
                        setFormDataValue={setFormDataValue}
                        latDatafield="lat"
                        lngDatafield="lng"
                        defaultLat={defaultValues.lat}
                        defaultLng={defaultValues.lng}
                      />
                    </div>
                  </div>
                </div>
                <div className="dx-form-group-with-caption dx-form-group editor-group">
                  {displayedProtocol && (
                    <div className="dx-form-group-caption">
                      Настройка протокола {displayedProtocol}
                    </div>
                  )}
                  {formData.protocolName === "genericHttp" && (
                    <GenericHttp
                      formData={formData}
                      onFormItemValueChanged={onFormItemValueChanged}
                      snapshotUrlDataField="settings.genericHttp.snapshotUrl"
                      videoStreamUrlDataField="settings.genericHttp.videoStreamUrl"
                      authTypeDataField="settings.genericHttp.authType"
                      authUserDataField="settings.genericHttp.authUser"
                      authPassDataField="settings.genericHttp.authPass"
                    />
                  )}
                  {formData.protocolName === "hikvisionTcp" && <div />}
                </div>
              </div>
            </SimpleItem>
          )}
        </Form>
      </ScrollView>
      <ValidationSummary id="summary" validationGroup="formEditor" />
      {isLoadedData && (
        <ToolbarItem
          widget="dxButton"
          location="after"
          toolbar="bottom"
          options={{ text: "Сохранить", onClick: onSave }}
        />
      )}
      <ToolbarItem
        widget="dxButton"
        location="after"
        toolbar="bottom"
        options={{ text: "Отменить", onClick: closeEditorPopup }}
      />
    </Popup>
  );
};

export default connect(null, {
  localDeauthorize: deauthorize,
})(VideoCamerasEditorPopup);
