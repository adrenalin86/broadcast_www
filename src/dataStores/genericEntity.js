import CustomStore from "devextreme/data/custom_store";
import store from "../store";

import { getBackendUrl, getToken, getRegion } from "../store/commonSlice";

import {
  handleResponseErrors,
  handleDataErrors,
  handleCaughtErrors,
} from "./utils";

const getGenericEntityDataStore = ({ windowId, entityName, ct = 24 }) => {
  const restArgs = {
    w: windowId,
    ct: ct,
  };
  //TODO: вынести key в параметры

  return new CustomStore({
    key: "id",
    load: (data) => {
      let body = {
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        ...restArgs,
        cd: "loadEntities",
        data: { entityName: entityName, ...data },
      };
      return fetch(
        getBackendUrl(store.getState()) + "?" + body.cd + entityName,
        {
          method: "POST",
          body: JSON.stringify(body),
        }
      )
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .then((data) => {
          return {
            data: data.data.entities,
            summary: data.data.summary,
            totalCount: data.data.totalCount,
          };
        })
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },
    byKey: (key) => {
      let body = {
        t: getToken(store.getState()),
        reg: getRegion(store.getState()),
        ...restArgs,
        cd: "loadEntities",
        data: { entityName: entityName, filter: ["id", "=", key] },
      };

      return fetch(
        getBackendUrl(store.getState()) + "?" + body.cd + "byKey" + entityName,
        {
          method: "POST",
          body: JSON.stringify(body),
        }
      )
        .then(handleResponseErrors)
        .then((response) => response.json())
        .then(handleDataErrors)
        .then((data) => {
          return data.data.entities;
        })
        .catch((error) => {
          throw handleCaughtErrors(error);
        });
    },
  });
};

export default getGenericEntityDataStore;
