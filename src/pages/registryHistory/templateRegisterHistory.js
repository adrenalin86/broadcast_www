import React, { useEffect, useState } from "react";

import { getSystemWindowsDataStore } from "../../dataStores";

import HistoryReport from "./historyReport";

const TemplateRegisterHistory = ({constName, entityConstName}) => {

  const [windowTitle, setWindowTitle] = useState();

  const windowId = constName.id;

  const systemWindowsDataStore = getSystemWindowsDataStore({
    windowId: windowId,
  });

  useEffect(() => {
    systemWindowsDataStore.byKey(windowId).then((result) => {
      const windowName = result[0]?.name?.split("/").pop();

      setWindowTitle(windowName);
    });
  }, []);

  return (
    <HistoryReport
      windowTitle={windowTitle}
      appliedEntity={entityConstName}
      windowId={windowId}
    />
  );
};

export default TemplateRegisterHistory;
