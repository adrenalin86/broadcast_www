import { createSlice } from "@reduxjs/toolkit";
import config from "../config";

import { omitDeepByKeys } from "../utils";

export const REPORTS_PAGE_REDUCER_KEY = "reports";

const reportsPageSlice = createSlice({
  name: REPORTS_PAGE_REDUCER_KEY,
  initialState: {
    ...omitDeepByKeys(config.pages[REPORTS_PAGE_REDUCER_KEY], [
      "dxOptions",
    ]),
  },
  reducers: {},
});


export const getReportsPage = (state) =>
  state.pages[REPORTS_PAGE_REDUCER_KEY];

export default reportsPageSlice.reducer;
