import DataGrid, { Button, Column, Export } from "devextreme-react/data-grid";
import { getSpecTransportTypesDataStore, WINDOWS } from "../../dataStores";
import config from "../../config";

const windowId = WINDOWS.SPEC_TRANSPORT_TYPES.id;

const specTransportTypesDataStore = getSpecTransportTypesDataStore({
  windowId: windowId,
});

const SpecTransportTypesTable = ({
  datagridRef,
  openDeleteRowPopup,
  openEditorPopup,
  openCreateEntryPopup,
  onDataErrorOccurred,
}) => {
  const specTransportTypesGridOptions =
    config.pages.specTransportTypes.specTransportTypesGrid.dxOptions;

  const onToolbarPreparing = (e) => {
    let toolbarItems = e.toolbarOptions.items;

    toolbarItems.unshift({
      widget: "dxButton",
      options: {
        icon: "add",
        onClick: openCreateEntryPopup,
      },
      location: "before",
    });
  };

  return (
    <div className="dx-card responsive-paddings">
      <DataGrid
        ref={datagridRef}
        dataSource={specTransportTypesDataStore}
        onDataErrorOccurred={onDataErrorOccurred}
        onToolbarPreparing={onToolbarPreparing}
        {...specTransportTypesGridOptions}
      >
        <Export enabled={true} allowExportSelectedData={true} />
        <Column dataField="id" caption="id" alignment="left" />
        <Column dataField="name" caption="Название" />
        <Column
          dataField="iconUrl"
          caption="Иконка"
          allowExporting={false}
          cellComponent={(e) => {
            return (
              <div
                style={{
                  display: "flex",
                  flexWrap: "nowrap",
                  wordBreak: "break-word",
                  whiteSpace: "normal",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                {e.data.data?.iconUrl && (
                  <img
                    src={e.data.data?.iconUrl}
                    style={{ width: "30px", height: "auto" }}
                    alt="Иконка для типа спецтранспорта"
                    onError={(e) => {
                      e.target.style = { width: "auto" };
                    }}
                  />
                )}
              </div>
            );
          }}
        />
        <Column type="buttons" alignment="center">
          <Button
            name="custom-edit"
            text="редактировать"
            icon="edit"
            onClick={(e) => openEditorPopup(e.row.key)}
          />
          <Button
            name="custom-delete"
            text="удалить"
            icon="trash"
            onClick={(e) => openDeleteRowPopup(e.row.key)}
          />
        </Column>
      </DataGrid>
    </div>
  );
};

export default SpecTransportTypesTable;
