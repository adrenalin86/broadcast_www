import { createSlice } from "@reduxjs/toolkit";
import config from "../config";

import { omitDeepByKeys } from "../utils";

export const BROADCAST_PROGRAMS_PAGE_REDUCER_KEY = "broadcastPrograms";

const broadcastProgramsPageSlice = createSlice({
  name: BROADCAST_PROGRAMS_PAGE_REDUCER_KEY,
  initialState: {
    ...omitDeepByKeys(config.pages[BROADCAST_PROGRAMS_PAGE_REDUCER_KEY], [
      "dxOptions",
    ]),
  },
  reducers: {},
});


export const getBroadcastProgramsPage = (state) =>
  state.pages[BROADCAST_PROGRAMS_PAGE_REDUCER_KEY];

export default broadcastProgramsPageSlice.reducer;
