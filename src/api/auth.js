export async function signIn(user, password, backendUrl) {
  try {
    let body = {
      w: 0,
      ct: 0,
      t: "",
      cd: "login",
      data: {
        user: user,
        pass: password,
      },
    };

    let data = await fetch(backendUrl+"?login", {
      method: "POST",
      body: JSON.stringify(body),
    })
      .then((response) => response.json())
      .then((response) => {
        return {
          response,
        };
      })
      .catch((error) => {
        throw new Error("Сетевая ошибка");
      });

    let isOk = data.response.r === "ok";
    
    return {
      isOk: isOk,
      data: data.response.data,
    };
  } catch (error) {
    return {
      isOk: false,
      message: "Авторизация на сервере не удалась",
    };
  }
}

export async function signOut(token, backendUrl) {
  try {
    let body = {
      w: 0,
      ct: 0,
      t: token,
      cd: "logout",
      data: {},
    };

    let data = await fetch(backendUrl+"?logout", {
      method: "POST",
      body: JSON.stringify(body),
    })
      .then((response) => response.json())
      .then((response) => {
        return {
          response,
        };
      })
      .catch(() => {
        throw new Error("Сетевая ошибка");
      });

    let isOk = data.response.r === "ok";

    return {
      isOk: isOk,
      data: data.response.data,
    };
  } catch {
    return {
      isOk: false,
      message: "Деавторизация на сервере не удалась",
    };
  }
}
