import { useEffect } from "react";
import { get, noop, set, has } from "lodash-es";
import { getDeepPathValueMap } from "../../utils";

import { SMART_JSON_2_FRAME_TYPES } from "../../dataStores";

import { SeTextBox, SeSelectBox, SeImageUploader, SeMultipleFrameEditor } from "../editors";
import { Switch } from "devextreme-react";

import { isEmpty } from "lodash-es";
import config from "../../config";
const SMART_JSON_2_FRAME_TYPES_NO5 = SMART_JSON_2_FRAME_TYPES.filter((item) => item.value !== 5);

const DEFAULTS = config.defaults.displays.settings.smartJson2;

const SettingsComponent = ({
  data,
  onFormItemValueChanged,
  onFormItemValueChangedData,
  className,
  editModeEnabled = false,
  dataField,
  onError = noop,
}) => {
  const onValueChangedEventData = (e) => {
    //TODO: пересмотреть механизм склейки путей dataField
    const eventDataField = e.component.option("dataField");
    const fullDataField = isEmpty(dataField)
      ? eventDataField
      : dataField?.endsWith(".")
      ? dataField + eventDataField
      : dataField + "." + eventDataField;

    e.component.option("dataField", fullDataField);

    e.forceRepaint = true;

    onFormItemValueChanged(e);
  };

  const onValueChangedRawData = (e) => {
    const eventDataField = e.dataField;
    const fullDataField = isEmpty(dataField)
      ? eventDataField
      : dataField?.endsWith(".")
      ? dataField + eventDataField
      : dataField + "." + eventDataField;

    e.dataField = fullDataField;

    e.forceRepaint = true;

    onFormItemValueChangedData(e);
  };

  const onFrameTypeChanged = (e) => {
    let valuesToReset = {};

    const enabler = e.component.option("enabler");
    //Этот код нужен из за того, что тип кадра контролирует показ полей
    //Нужно было выставлять поля в их изначальные значения каждый раз когда тип кадра сменялся.
    //Так-же, когда поле выбора кадра включается, то оно выставляется в начальное значение.
    if (enabler) {
      if (e.value === false) {
        valuesToReset = null;
      } else {
        valuesToReset.frameType = DEFAULTS?.frame?.frameType;
        valuesToReset.topText = DEFAULTS?.frame?.topText;
        valuesToReset.bottomText = DEFAULTS?.frame?.bottomText;
        valuesToReset.imageUrl = DEFAULTS?.frame?.imageUrl;
        valuesToReset.imageData = DEFAULTS?.frame?.imageData;
        valuesToReset.videoUrl = DEFAULTS?.frame?.videoUrl;
        valuesToReset.duration = DEFAULTS?.frame?.duration;
      }
    } else {
      switch (e.value) {
        case 1:
          valuesToReset.topText = DEFAULTS?.frame?.topText;
          valuesToReset.bottomText = DEFAULTS?.frame?.bottomText;
          break;
        case 2:
          valuesToReset.imageUrl = DEFAULTS?.frame?.imageUrl;
          valuesToReset.imageData = DEFAULTS?.frame?.imageData;
          break;
        case 3:
          valuesToReset.videoUrl = DEFAULTS?.frame?.videoUrl;
          break;
        case 4:
          valuesToReset.topText = DEFAULTS?.frame?.topText;
          valuesToReset.bottomText = DEFAULTS?.frame?.bottomText;
          valuesToReset.imageUrl = DEFAULTS?.frame?.imageUrl;
          valuesToReset.imageData = DEFAULTS?.frame?.imageData;
          break;
        default:
          break;
      }
      valuesToReset.frameType = e.value;
    }

    onValueChangedRawData({
      value: valuesToReset,
      dataField: "frame",
      forceRepaint: true,
    });
  };

  const onErrorFrameTypeChanged = (e) => {
    let valuesToReset = {};

    const enabler = e.component.option("enabler");
    if (enabler) {
      if (e.value === false) {
        valuesToReset = null;
      } else {
        valuesToReset.frameType = DEFAULTS?.errorFrame?.frameType;
        valuesToReset.topText = DEFAULTS?.errorFrame?.topText;
        valuesToReset.bottomText = DEFAULTS?.errorFrame?.bottomText;
        valuesToReset.imageUrl = DEFAULTS?.errorFrame?.imageUrl;
        valuesToReset.imageData = DEFAULTS?.errorFrame?.imageData;
        valuesToReset.videoUrl = DEFAULTS?.errorFrame?.videoUrl;
        valuesToReset.duration = DEFAULTS?.errorFrame?.duration;
      }
    } else {
      switch (e.value) {
        case 1:
          valuesToReset.topText = DEFAULTS?.errorFrame?.topText;
          valuesToReset.bottomText = DEFAULTS?.errorFrame?.bottomText;
          break;
        case 2:
          valuesToReset.imageUrl = DEFAULTS?.errorFrame?.imageUrl;
          valuesToReset.imageData = DEFAULTS?.errorFrame?.imageData;
          break;
        case 3:
          valuesToReset.videoUrl = DEFAULTS?.errorFrame?.videoUrl;
          break;
        case 4:
          valuesToReset.topText = DEFAULTS?.errorFrame?.topText;
          valuesToReset.bottomText = DEFAULTS?.errorFrame?.bottomText;
          valuesToReset.imageUrl = DEFAULTS?.errorFrame?.imageUrl;
          valuesToReset.imageData = DEFAULTS?.errorFrame?.imageData;
          break;
        default:
          break;
      }
      valuesToReset.frameType = e.value;
    }

    onValueChangedRawData({
      value: valuesToReset,
      dataField: "errorFrame",
      forceRepaint: true,
    });
  };

  return (
    <div className={"dx-form-group-with-caption dx-form-group smartjson2-editor-component " + className}>
      <div className="dx-form-group-caption">Настройки протокола SmartJson2</div>
      <div className="dx-form-group-content">
        <div className="editor-item">
          <fieldset className="flex-row flex-nowrap">
            <legend className="editor-item-title">Кадр</legend>
            {editModeEnabled && (
              <div className="dx-field m-v-8">
                <div className="field-enabler top m-v-8">
                  <Switch
                    defaultValue={get(data, "frame") === null || !has(data, "frame") ? false : true}
                    onValueChanged={onFrameTypeChanged}
                    dataField={dataField}
                    enabler={true}
                  />
                </div>
              </div>
            )}
            <div className="editor-item">
              {get(data, "frame") ? (
                <SeSelectBox
                  data={data}
                  onFormItemValueChanged={onFrameTypeChanged}
                  editModeEnabled={editModeEnabled}
                  dataField={"frame.frameType"}
                  dataSource={SMART_JSON_2_FRAME_TYPES}
                  valueExpr="value"
                  displayExpr="name"
                  searchEnabled={false}
                  title={"Тип кадра"}
                  controlled={true}
                  allowDisabling={false}
                />
              ) : (
                <SeSelectBox
                  data={null}
                  onFormItemValueChanged={noop()}
                  editModeEnabled={editModeEnabled}
                  dataField={""}
                  dataSource={SMART_JSON_2_FRAME_TYPES}
                  valueExpr="value"
                  displayExpr="name"
                  searchEnabled={false}
                  title={"Тип кадра"}
                  controlled={true}
                  allowDisabling={true}
                  showSwitch={false}
                />
              )}

              {get(data, "frame.frameType") === 1 || get(data, "frame.frameType") === 4 ? (
                <SeTextBox
                  data={data}
                  onFormItemValueChanged={onValueChangedEventData}
                  editModeEnabled={editModeEnabled}
                  dataField={"frame.topText"}
                  title={"Верхний текст"}
                />
              ) : null}
              {get(data, "frame.frameType") === 1 || get(data, "frame.frameType") === 4 ? (
                <SeTextBox
                  data={data}
                  onFormItemValueChanged={onValueChangedEventData}
                  editModeEnabled={editModeEnabled}
                  dataField={"frame.bottomText"}
                  title={"Нижний текст"}
                />
              ) : null}
              {get(data, "frame.frameType") === 2 || get(data, "frame.frameType") === 4 ? (
                <SeTextBox
                  data={data}
                  onFormItemValueChanged={onValueChangedEventData}
                  editModeEnabled={editModeEnabled}
                  dataField={"frame.imageUrl"}
                  title={"Url картинки"}
                />
              ) : null}
              {get(data, "frame.frameType") === 2 || get(data, "frame.frameType") === 4 ? (
                <SeImageUploader
                  data={data}
                  onFormItemValueChanged={onValueChangedEventData}
                  onFormItemValueChangedData={onValueChangedRawData}
                  editModeEnabled={editModeEnabled}
                  dataField={"frame.imageData"}
                  title={"Файл картинки"}
                  maxNumber={1}
                />
              ) : null}
              {get(data, "frame.frameType") === 3 ? (
                <SeTextBox
                  data={data}
                  onFormItemValueChanged={onValueChangedEventData}
                  editModeEnabled={editModeEnabled}
                  dataField={"frame.videoUrl"}
                  title={"Url видео"}
                />
              ) : null}
              {get(data, "frame.frameType") === 5 ? (
                <SeMultipleFrameEditor
                  data={data}
                  onFormItemValueChangedData={onValueChangedRawData}
                  editModeEnabled={editModeEnabled}
                  dataField={"frame.items"}
                  maxNumber={-1}
                  title={"Субкадры"}
                  subFrameTitle={"Субкадр"}
                />
              ) : null}
            </div>
          </fieldset>

          <fieldset>
            <legend className="editor-item-title">Кадр ошибки</legend>
            <SeSelectBox
              data={data}
              onFormItemValueChanged={onErrorFrameTypeChanged}
              editModeEnabled={editModeEnabled}
              dataField={"errorFrame.frameType"}
              dataSource={SMART_JSON_2_FRAME_TYPES_NO5}
              valueExpr="value"
              displayExpr="name"
              searchEnabled={false}
              title={"Тип кадра"}
              controlled={true}
            />
            {get(data, "errorFrame.frameType") === 1 || get(data, "errorFrame.frameType") === 4 ? (
              <SeTextBox
                data={data}
                onFormItemValueChanged={onValueChangedEventData}
                editModeEnabled={editModeEnabled}
                dataField={"errorFrame.topText"}
                title={"Верхний текст"}
              />
            ) : null}
            {get(data, "errorFrame.frameType") === 1 || get(data, "errorFrame.frameType") === 4 ? (
              <SeTextBox
                data={data}
                onFormItemValueChanged={onValueChangedEventData}
                editModeEnabled={editModeEnabled}
                dataField={"errorFrame.bottomText"}
                title={"Нижний текст"}
              />
            ) : null}
            {get(data, "errorFrame.frameType") === 2 || get(data, "errorFrame.frameType") === 4 ? (
              <SeTextBox
                data={data}
                onFormItemValueChanged={onValueChangedEventData}
                editModeEnabled={editModeEnabled}
                dataField={"errorFrame.imageUrl"}
                title={"Url картинки"}
              />
            ) : null}
            {get(data, "errorFrame.frameType") === 2 || get(data, "errorFrame.frameType") === 4 ? (
              <SeImageUploader
                data={data}
                onFormItemValueChanged={onValueChangedEventData}
                onFormItemValueChangedData={onValueChangedRawData}
                editModeEnabled={editModeEnabled}
                dataField={"errorFrame.imageData"}
                title={"Файл картинки"}
                maxNumber={1}
              />
            ) : null}
            {get(data, "errorFrame.frameType") === 3 ? (
              <SeTextBox
                data={data}
                onFormItemValueChanged={onValueChangedEventData}
                editModeEnabled={editModeEnabled}
                dataField={"errorFrame.videoUrl"}
                title={"Url видео"}
              />
            ) : null}
          </fieldset>
        </div>
      </div>
    </div>
  );
};

export default SettingsComponent;
